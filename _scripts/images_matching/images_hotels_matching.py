# %%
import time
import requests
import pandas as pd
import concurrent.futures

from PIL import Image
import imagehash
pd.set_option('display.max_colwidth', 500)
# %%
###################################### Datasets preparation
# Prepare hotelbeds dataset
# hb_room_urls = pd.read_csv('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/datasets/hb_room_urls.csv')
# bcom_room_urls = pd.read_csv('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/datasets/bcom_room_urls.csv')
# matching = pd.read_csv('/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/hotel_room_database_table/hb_bcom_matching.csv')

hb_room_urls = pd.read_csv(r'C:\Users\mkgos\Dev\wwpt_projects\ooredoo\hotelbeds_data\scripts\datasets\hb_room_urls.csv')
bcom_room_urls = pd.read_csv(r'C:\Users\mkgos\Dev\wwpt_projects\ooredoo\hotelbeds_data\scripts\datasets\bcom_room_urls.csv')
matching = pd.read_csv(r'G:\Shared drives\Dashboards (KPIs)\POWER BI\Leap_HotelBeds_&_Airbnb\KN_hb_MATCHING_bing_URL\hb_bcom_matching\hotel_room_database_table/hb_bcom_matching.csv')

# %%
hb_room_urls = hb_room_urls.dropna(subset = ['code']).reset_index()
hb_room_urls['code'] = hb_room_urls['code'].astype(int).astype(str)
# hb_room_urls['hb_hotel_room_id'] = hb_room_urls['code'] + hb_room_urls['room_code']

hb_room_urls
# %%
# Prepare Booking.com dataset
bcom_room_urls = bcom_room_urls[['hotel_id', 'associated_rooms', 'id','orientation',
                'created', 'highres_url', 'thumb_url', 'large_url']]
# bcom_room_urls['bcom_hotel_room_id'] = bcom_room_urls['hotel_id'].astype(str) + bcom_room_urls['associated_rooms'].astype(str)
bcom_room_urls['hotel_id'] = bcom_room_urls['hotel_id'].astype(str)

bcom_room_urls
# %%

# %%
# Prepare HB and Bcom matching dataset
# matching['hb_hotel_room_id'] = matching['hotel_code'].astype(str) + matching['rooms.code']
# matching['b_room_id'] = matching['b_room_id'].fillna(0).astype(int)
# matching['bcom_hotel_room_id'] = matching['b_hotel_id'].astype(str) + matching['b_room_id'].astype(str)

matching['hotel_code'] = matching['hotel_code'].astype(str)
matching['b_hotel_id'] = matching['b_hotel_id'].astype(str)
matching
# %%
# matching[matching['hotel_code'] == '285609']

# %%
matching_hb = matching.merge(hb_room_urls, left_on='hotel_code', right_on='code', how='inner') #.drop_duplicates(subset='hb_hotel_room_id')
# matching_hb = matching.merge(hb_room_urls, left_on='hotel_code', right_on='code', how='inner')
matching_hb

# %%
# Merge HB and Bcom into the matching dataset
matching_hb_bcom = matching_hb.merge(bcom_room_urls, left_on='b_hotel_id', right_on='hotel_id', how='left') #.drop_duplicates(subset='bcom_hotel_room_id')
matching_hb_bcom
# %%
t = matching_hb_bcom[ matching_hb_bcom['hb_hotel_room_id'] == '156257FAM.C6' ]
# t.to_csv('~/Downloads/t.csv')
# bcom_room_urls[ bcom_room_urls['bcom_hotel_room_id'] == '34284334284305']

# %%

# %%
matched_urls = matching_hb_bcom
# %%
matched_urls = matched_urls[:500]
matched_urls
# %%
###################################### Image Matching
start = time.perf_counter()

hb_static_url = 'http://photos.hotelbeds.com/giata/bigger/' # original
# bcom_room_urls['bcom_static_url'] = 'https://cf.bstatic.com/xdata/images/hotel/max800x600/' + 
hb_list_ids = list(matched_urls['hotel_code'])
count_ls = []
final_list = []

def img_scraper( hb_list_ids ):
    count_ls.append(hb_list_ids)
    index = len(count_ls)
    
    hb_url = matched_urls['giata_path'][index]
    bcom_url = matched_urls['large_url'][index]
    print(hb_url)
    print(bcom_url)

    # Hotelbeds image
    hb_url = hb_static_url + hb_url
    r1 = requests.get(hb_url, stream=True)
    r1.raw.decode_content = True # handle spurious Content-Encoding
    im1 = Image.open(r1.raw)
    hash1 = imagehash.phash(im1, hash_size=8)

    # Booking.com image
    r2 = requests.get(bcom_url, stream=True)
    r2.raw.decode_content = True # handle spurious Content-Encoding
    im2 = Image.open(r2.raw)
    hash2 = imagehash.phash(im2, hash_size=8)

    img_similarity_score = hash1 - hash2
    # print(hash1 - hash2)
    # print(im2.format, im2.mode, im2.size)

    # offset unique IDs for HB and Bcom
    hb_hotel_code = matched_urls['hotel_code'][index]
    hb_room_code = matched_urls['rooms.code'][index]
    bcom_hotel_code = matched_urls['b_hotel_id'][index]
    bcom_room_code = matched_urls['b_room_id'][index]

    final_list.append([index, img_similarity_score, hb_hotel_code, hb_room_code,
                        bcom_hotel_code, bcom_room_code, hb_url, bcom_url])

# for index, (i, j) in enumerate(zip(matched_urls['giata_path'][:limit], matched_urls['large_url'][:limit] )):
with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(img_scraper, hb_list_ids)

end = time.perf_counter()
print( round(end - start, 2))
# %%
pd.DataFrame(final_list)

# %%
# # Calculate similarity
# cutoff = 5

# if hash1 - hash2 < cutoff:
#   print('images are similar')
# else:
#   print('images are not similar')

# print(hash1 - hash2)
# %%
# %%

# %%
