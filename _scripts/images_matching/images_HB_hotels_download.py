# %%
import time
import requests
import csv
import pandas as pd

import concurrent.futures
from PIL import Image
import imagehash
pd.set_option('display.max_colwidth', 500)
# %%
###################################### Datasets preparation
# Prepare hotelbeds dataset
# hb_room_urls = pd.read_csv('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/datasets/hb_room_urls.csv')
hb_room_urls = pd.read_csv(r'C:\Users\mkgos\Dev\wwpt_projects\ooredoo\hotelbeds_data\scripts\datasets\hb_room_urls.csv')

# %%
hb_room_urls = hb_room_urls.dropna(subset = ['code'])
hb_room_urls = hb_room_urls.dropna(subset = ['giata_path']).reset_index()
hb_room_urls['code'] = hb_room_urls['code'].astype(int).astype(str)
# hb_room_urls['hb_hotel_room_id'] = hb_room_urls['code'] + hb_room_urls['room_code']

hb_room_urls
# %%
# matched_urls = hb_room_urls
# %%
# matched_urls = matched_urls[:50]
# matched_urls
hb_room_urls = hb_room_urls[6070104:]
# %%
###################################### Image Matching
start = time.perf_counter()

csv_output_path = r'C:\Users\mkgos\Dev\wwpt_projects\ooredoo\hotelbeds_data\scripts\datasets\images_HB_hotels.csv'
hb_static_url = 'http://photos.hotelbeds.com/giata/bigger/' # original

hb_list_ids = list(hb_room_urls['code'])
count_ls = []
# final_list = []

def img_scraper( hb_list_ids ):
    count_ls.append(hb_list_ids)
    index = len(count_ls)
    # print(index)
    hb_url_static = hb_room_urls['giata_path'][index]

    # Hotelbeds image
    hb_url = hb_static_url + hb_url_static
    r1 = requests.get(hb_url, stream=True)
    r1.raw.decode_content = True # handle spurious Content-Encoding
    im1 = Image.open(r1.raw)
    hash1 = imagehash.phash(im1, hash_size=8)

    # offset unique IDs for HB and Bcom
    hb_hotel_code = hb_room_urls['code'][index]
    hb_room_code = hb_room_urls['room_code'][index]

    # final_list.append([index, hb_hotel_code, hb_room_code, hb_url, hash1])
    data_list = [index, hb_hotel_code, hb_room_code, hb_url_static, hash1]

    with open (csv_output_path, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, lineterminator='\n')
        writer.writerow(data_list)

# for index, (i, j) in enumerate(zip(hb_room_urls['giata_path'][:limit], hb_room_urls['large_url'][:limit] )):
with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(img_scraper, hb_list_ids)

end = time.perf_counter()
print( round(end - start, 2))
# %%
# pd.DataFrame(final_list)
pd.read_csv(csv_output_path,
            names= ['index', 'hb_hotel_code', 'hb_room_code', 'hb_url', 'hash1'])

# %%

# %%
import pandas as pd
csv_output_path = r'C:\Users\mkgos\Dev\wwpt_projects\ooredoo\hotelbeds_data\scripts\datasets\images_HB_hotels.csv'
pd.read_csv(csv_output_path, 
            names= ['index', 'hb_hotel_code', 'hb_room_code', 'hb_url', 'hash1'],
            dtype={'index': str, 'hb_hotel_code': str, 
            'hb_room_code': str, 'hb_url': str, 'hash1': str})

# %%

# %%

# %%
# # Calculate similarity
# cutoff = 5

# if hash1 - hash2 < cutoff:
#   print('images are similar')
# else:
#   print('images are not similar')

# print(hash1 - hash2)
# %%
# %%

# %%
