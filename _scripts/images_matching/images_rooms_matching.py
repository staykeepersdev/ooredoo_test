# %%
import time
import requests
import pandas as pd
import numpy as np

import concurrent.futures

from PIL import Image
import imagehash
pd.set_option('display.max_colwidth', 500)
# %%
###################################### Datasets preparation
# Prepare hotelbeds dataset
hb_room_urls = pd.read_csv('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/datasets/hb_room_urls.csv')
bcom_room_urls = pd.read_csv('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/datasets/bcom_room_urls.csv')
matching = pd.read_csv('/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/hotel_room_database_table/hb_bcom_matching.csv')
# %%
hb_room_urls = hb_room_urls.dropna(subset = ['code']).reset_index()
hb_room_urls['code'] = hb_room_urls['code'].astype(int).astype(str)
hb_room_urls['hb_hotel_room_id'] = hb_room_urls['code'] + hb_room_urls['room_code']

hb_room_urls
# %%
# Prepare Booking.com dataset
bcom_room_urls = bcom_room_urls[['hotel_id', 'associated_rooms', 'id','orientation',
                'created', 'highres_url', 'thumb_url', 'large_url']]
bcom_room_urls['bcom_hotel_room_id'] = bcom_room_urls['hotel_id'].astype(str) + bcom_room_urls['associated_rooms'].astype(str)

bcom_room_urls
# %%

# %%
# Prepare HB and Bcom matching dataset
matching['hb_hotel_room_id'] = matching['hotel_code'].astype(str) + matching['rooms.code']
matching['b_room_id'] = matching['b_room_id'].fillna(0).astype(int)
matching['bcom_hotel_room_id'] = matching['b_hotel_id'].astype(str) + matching['b_room_id'].astype(str)
matching

# %%
# matching[matching['hotel_code'] == '285609']

# %%
matching_hb = matching.merge(hb_room_urls, on='hb_hotel_room_id', how='inner') #.drop_duplicates(subset='hb_hotel_room_id')
# matching_hb = matching.merge(hb_room_urls, left_on='hotel_code', right_on='code', how='inner')
matching_hb

# %%
# Merge HB and Bcom into the matching dataset
matching_hb_bcom = matching_hb.merge(bcom_room_urls, on='bcom_hotel_room_id', how='inner') #.drop_duplicates(subset='bcom_hotel_room_id')
matching_hb_bcom
# %%
t = matching_hb_bcom[ matching_hb_bcom['hb_hotel_room_id'] == '156257FAM.C6' ]
# t.to_csv('~/Downloads/t.csv')
# bcom_room_urls[ bcom_room_urls['bcom_hotel_room_id'] == '34284334284305']

# %%

# %%
matched_urls = matching_hb_bcom
# %%
matched_urls = matched_urls[:100]
matched_urls
# %%
###################################### Image Matching
start = time.perf_counter()

hb_static_url = 'http://photos.hotelbeds.com/giata/bigger/' # original
# bcom_room_urls['bcom_static_url'] = 'https://cf.bstatic.com/xdata/images/hotel/max800x600/' + 
hb_list_ids = list(matched_urls['hotel_code'])
count_ls = []
final_list = []

def img_scraper( hb_list_ids ):
    count_ls.append(hb_list_ids)
    index = len(count_ls)
    print(index)
    
    hb_url = matched_urls['giata_path'][index]
    bcom_url = matched_urls['large_url'][index]
    # print(hb_url)
    # print(bcom_url)

    # Hotelbeds image
    hb_url = hb_static_url + hb_url
    r1 = requests.get(hb_url, stream=True)
    r1.raw.decode_content = True # handle spurious Content-Encoding
    im1 = Image.open(r1.raw)
    hash1 = imagehash.phash(im1, hash_size=8)

    # Booking.com image
    r2 = requests.get(bcom_url, stream=True)
    r2.raw.decode_content = True # handle spurious Content-Encoding
    im2 = Image.open(r2.raw)
    hash2 = imagehash.phash(im2, hash_size=8)

    img_similarity_score = hash1 - hash2
    # print(hash1 - hash2)
    # print(im2.format, im2.mode, im2.size)

    # offset unique IDs for HB and Bcom
    hb_hotel_code = matched_urls['hotel_code'][index]
    hb_room_code = matched_urls['rooms.code'][index]
    bcom_hotel_code = matched_urls['b_hotel_id'][index]
    bcom_room_code = matched_urls['b_room_id'][index]

    final_list.append([index, img_similarity_score, hb_hotel_code, hb_room_code,
                        bcom_hotel_code, bcom_room_code, hb_url, bcom_url, hash1, hash2])

# for index, (i, j) in enumerate(zip(matched_urls['giata_path'][:limit], matched_urls['large_url'][:limit] )):
with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(img_scraper, range(1000)) #hb_list_ids

end = time.perf_counter()
print( round(end - start, 2))
# %%
df = pd.DataFrame(final_list, columns=['index', 'img_similarity_score', 'hb_hotel_code', 'hb_room_code',
                        'bcom_hotel_code', 'bcom_room_code', 'hb_url', 'bcom_url', 'hash1', 'hash2'])
df
# %%
df.to_csv('~/Downloads/images_rooms_matching.csv')
# %%
df[df['img_similarity_score'] < 5]
# %%
# # Calculate similarity
# cutoff = 5

# if hash1 - hash2 < cutoff:
#   print('images are similar')
# else:
#   print('images are not similar')

# print(hash1 - hash2)
# %%
df1 = df.drop_duplicates(subset=['hash1'])
df1 = df1.drop_duplicates(subset=['hash2'])
df1.reset_index(inplace=True)
# %%
df1
# %%
# df1 = df1[['index', 'img_similarity_score', 'hb_hotel_code', 'hb_room_code',
#         'bcom_hotel_code', 'bcom_room_code', 'hb_url', 'bcom_url', 'hash1', 'hash2']]
# df1
# %%

for index in df1.index[:10]:
    
    # print(index)
    arr = (np.repeat(df1['hash1'][index],len(df1)) - df1['hash2'])
    arr1 = arr[arr < 24].reset_index()
    arr1.rename(columns = {'index': 'bcom_index'}, inplace=True)
    # print(arr1)
    
    s_0 = pd.Series(index, name='hb_index').reset_index()
    s_1 = pd.Series(df1['hb_hotel_code'][index], name='hb_hotel_code').reset_index()
    s_2 = pd.Series(df1['bcom_hotel_code'][arr1['bcom_index']]).reset_index()
    s_3 = pd.Series(df1['hb_url'][index], name='hb_url').reset_index()
    s_4 = pd.Series(df1['bcom_url'][arr1['bcom_index']], name='bcom_url').reset_index()

    df = pd.concat([s_0, s_1, s_2, s_3, s_4, arr1], axis=1).ffill(axis=0).reset_index()
    print(df)

    df = df[['hb_index', 'bcom_index', 'hb_hotel_code',
       'bcom_hotel_code', 'hb_url', 'bcom_url','hash2']]

    df.to_csv('file.csv', mode='a', header=False)

# %%
# pd.read_csv('file.csv')
# %%
# %%
# %%
colnames = ['index_1', 'hb_hotel_code', 'hb_room_code', 'hb_url', 'hb_hash']
hb_all_images = pd.read_csv('/Users/miro/Downloads/images_HB_hotels.csv', names=colnames, header=None)
hb_all_images['hb_hash'] = hb_all_images['hb_hash'].fillna('0').astype(str)
hb_all_images

# %%
def hex_to_hash(hexstr):
	"""
	Convert a stored hash (hex, as retrieved from str(Imagehash))
	back to a Imagehash object.
	Notes:
	1. This algorithm assumes all hashes are either
	   bidimensional arrays with dimensions hash_size * hash_size,
	   or onedimensional arrays with dimensions binbits * 14.
	2. This algorithm does not work for hash_size < 2.
	"""
	hash_size = int(np.sqrt(len(hexstr)*4))
	#assert hash_size == numpy.sqrt(len(hexstr)*4)
	binary_array = '{:0>{width}b}'.format(int(hexstr, 16), width = hash_size * hash_size)
	bit_rows = [binary_array[i:i+hash_size] for i in range(0, len(binary_array), hash_size)]
	hash_array = np.array([[bool(int(d)) for d in row] for row in bit_rows])
	return hash_array

# %%
hb_all_images['hb_hash_decode'] = hb_all_images['hb_hash'].apply(imagehash.hex_to_hash)
hb_all_images
# %%
# Find similar images
"""
query = "8eb171d990974d9a"
matches = {}
for i, row in hb_all_images.iterrows():
    # Convert hash hex string to original hash (binary array) 
    diff = imagehash.hex_to_hash(query) - imagehash.hex_to_hash(row['hb_hash'])
    # phashes with Hamming distance less than 10 are likely to be similar images
    
    if diff <= 10:
        print("Found match: ", row["hb_hash"])
        print("Hamming distance from query:", diff)
        matches[row["hb_hash"]] = 'http://photos.hotelbeds.com/giata/bigger/' + row["hb_url"]
        print("")
"""
# %%
# %%
def hash_size(hexhash):
    hash_size_row = int(np.sqrt(len(hexhash ) * 4 ))
    return hash_size_row
# %%
hb_all_images['hb_hash_decode_count'] = hb_all_images['hb_hash_decode'].apply(hash_size)
hb_all_images
# %%
hb_all_images = hb_all_images[hb_all_images['hb_hash_decode_count'] == 16]
hb_all_images.reset_index(inplace=True)
hb_all_images
# %%
# %%
# %%
hb_all_images_chunks = hb_all_images['hb_hash_decode']
df1_hash_list = list(df1['hash1'][:5])

def test_subtract(df1_hash_list):
    return print(np.repeat(df1_hash_list, len(hb_all_images_chunks)) - hb_all_images_chunks)

# test_subtract()
# %%
with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
    executor.map(test_subtract, df1_hash_list)

# %%
import threading
def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

hb_all_images_chunks = hb_all_images['hb_hash_decode']
df1_hash_list = list(df1['hash1'][:3])

def test_subtract( counter, hotel_hash):
    print(counter)
    diff = np.repeat(hotel_hash), len(hb_all_images_chunks) - hb_all_images_chunks
    print(diff)
    # imagehash.hex_to_hash(query)

def exec_thread():
    for counter, hotel_hash in enumerate(df1_hash_list):
        print(hotel_hash)
        while threading.active_count() >= MAX_THREADS:
            time.sleep(2)
            run_threaded(test_subtract, (counter, hotel_hash))

MAX_THREADS = 6

exec_thread()
# def get_country_prices(country_code):
#     global total_hotel_ids
#     country_hotel_ids = get_country_hotel_ids(country_code)
#     total_hotel_ids = len(country_hotel_ids)
#     print(f'Total :{total_hotel_ids}')

#     slices_hotel_ids = split_list_on_n_elements(country_hotel_ids, 1000)
#     for counter, hotel_ids in enumerate(slices_hotel_ids):
#         # Check running threads before starting new one
#         while threading.active_count() >= MAX_THREADS:
#             time.sleep(2)
#         run_threaded(get_hotels_prices, (counter, hotel_ids) )


# %%
import multiprocess as mp
from multiprocess import Pool, Process
mp.cpu_count()
# %%
def function(lst):
  arr = np.zeros_like(lst)
  for i in range(lst.shape[0]):
    for j in range(lst.shape[1]):
      arr[i][j] = lst[i][j] ** 2
  return arr

array = np.random.randint(1, 9, (2**10, 10000))
data = np.array_split(array, 2)
# %%

# %%
# %%
# %%

# %%
import numba
x = np.repeat(df1['hash1'][0], len(hb_all_images))
y = hb_all_images['hb_hash_decode'].values
# @numba.vectorize
@numba.jit(nopython=True)
def numba_hash(u):
    u = np.repeat(df1['hash1'][0], len(hb_all_images)) - hb_all_images['hb_hash_decode'].values
    return u

# %%
@numba.njit(parallel = True)
def func_hsah(x,y):
    return x - y
func_hsah(x, y)
# %%

# %%

# %%
for index in df1.index[:2]:
    
    # print(index)
    arr = (np.repeat(df1['hash1'][index],len(hb_all_images)) - hb_all_images['hb_hash'])
    arr1 = arr[arr < 5].reset_index()
    arr1.rename(columns = {'index': 'bcom_index'}, inplace=True)
    # print(arr1)
    
    s_0 = pd.Series(index, name='hb_index').reset_index()
    s_1 = pd.Series(df1['hb_hotel_code'][index], name='hb_hotel_code').reset_index()
    s_2 = pd.Series(df1['bcom_hotel_code'][arr1['bcom_index']]).reset_index()
    s_3 = pd.Series(df1['hb_url'][index], name='hb_url').reset_index()
    s_4 = pd.Series(df1['bcom_url'][arr1['bcom_index']], name='bcom_url').reset_index()

    df = pd.concat([s_0, s_1, s_2, s_3, s_4, arr1], axis=1).ffill(axis=0).reset_index()
    print(df)

    df = df[['hb_index', 'bcom_index', 'hb_hotel_code',
       'bcom_hotel_code', 'hb_url', 'bcom_url','hash2']]
    print(df)
    df.to_csv('file.csv', mode='a', header=False)

# %%

# %%
# %%
image_hash_1 = hex_to_hash(str(df1['hash1'][0]))
image_hash_2 = hb_all_images['hb_hash_decode'][0]
# %%
xor = image_hash_1 ^ image_hash_2
# %%
np.count_nonzero(xor != 0)
# %%
# %%
hb_url = 'http://photos.hotelbeds.com/giata/bigger/' + '00/000001/000001a_hb_a_004.jpg'
r1 = requests.get(hb_url, stream=True)
r1.raw.decode_content = True # handle spurious Content-Encoding
im1 = Image.open(r1.raw)
hash1 = imagehash.phash(im1, hash_size=8)
hash1
# %%
hash_string = str(hash1)
hash_string
# %%

# %%
# %%
# %%
arr = np.repeat(df1['hash1'][0],13) - df1['hash2']
arr
# %%
arr1 = arr[arr < 30]
arr1
# %%
s_1 = pd.Series(df1['hb_hotel_code'][arr1.index])
s_2 = pd.Series(df1['bcom_hotel_code'][df1.index])

# %%
pd.concat([s_1, s_2, arr1], axis=1).reset_index()

# %%
pd.Series([df1['hb_hotel_code'][0],df1['bcom_hotel_code'][0]])
# %%
