# %%
#!/usr/bin/env python
import requests # $ pip install requests

from PIL import Image # $ pip install pillow
import imagehash

# Hotelbeds image
hb_url = 'http://photos.hotelbeds.com/giata/bigger/13/135926/135926a_hb_ro_013.jpg'
r1 = requests.get(hb_url, stream=True)
r1.raw.decode_content = True # handle spurious Content-Encoding
im1 = Image.open(r1.raw)
hash1 = imagehash.phash(im1, hash_size=8)

print(hash1)
# hash1
# print(im1.format, im1.mode, im1.size)

# Booking.com image
bcom_url = 'https://cf.bstatic.com/xdata/images/hotel/max1024x768/76113587.jpg?k=70bbbdb3965f9c4647e8ba185da88250193259d86c81ec679871f607f85d60b6&o='
r2 = requests.get(bcom_url, stream=True)
r2.raw.decode_content = True # handle spurious Content-Encoding
im2 = Image.open(r2.raw)
hash2 = imagehash.phash(im2, hash_size=8)

print(hash2)
hash2
# print(im2.format, im2.mode, im2.size)

# %%
# Calculate similarity
cutoff = 5

if hash1 - hash2 < cutoff:
  print('images are similar')
else:
  print('images are not similar')

print(hash1 - hash2)
# %%
