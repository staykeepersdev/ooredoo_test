import sys, os, django
import pandas as pd

import dask.dataframe as dd
from dask.distributed import Client

# sys.path.append('/srv/www/ooredoo/')
# sys.path.append('/home/miroslav/Dev/wwpt_projects/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price, database_url

if __name__ == '__main__':
    # client = Client()
    client = Client(processes=False)
                    # n_workers=8,
                    # threads_per_worker=1,
                    # memory_limit='8GB')

today_str = pd.to_datetime('today').strftime('%Y-%m-%d') # '2021-06-03'
tomorrow_str = (pd.to_datetime(today_str) + pd.DateOffset(1)).strftime('%Y-%m-%d')

connection = engine_local_db_hb_price.connect()
my_query = f"""
        CREATE TABLE today_table AS
        SELECT * FROM public.hb_price_6
        WHERE scraped_time >= '{today_str}' AND scraped_time < '{tomorrow_str}';
"""
connection.execute(my_query)

df_dd = dd.read_sql_table('today_table', database_url, index_col='check_in')
df_dd['scraped_date'] = df_dd.scraped_time.dt.date
print(df_dd)

path = '/media/miroslav/Storage/Dev/parquet_storage/hb_price_worldwide_daily_1/'
dd.to_parquet(df_dd, path,
                write_metadata_file=False,
                # append=True,
                ignore_divisions=True,
                engine='pyarrow',
                partition_on=['scraped_date', 'country_code'] )

my_query = f"""
        DROP TABLE today_table;
"""
connection.execute(my_query)

# with Client() as client:
client.close()
    # client.shutdown()

connection.close()