# %%
import pandas as pd
import numpy as np
# %%
df = pd.read_csv('/home/miroslav/Downloads/hb_df.csv')
df
# %%
df['email'].value_counts().head(60)
# %%
df['email_unique'] = df['email'].str.extract(r'(@.+?\.)')
# %%
df['email_unique'] = df['email_unique'].str.lower()
series_unique = df['email_unique']
series_unique
# %%
series_unique = series_unique.str.replace('@','').str.replace('.','').drop_duplicates()
series_unique.reset_index(inplace=True, drop=True)
# %%
series_unique.to_csv('/home/miroslav/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/hb_price_db/BingScrapper/bing/spiders/hb_email_unique.csv')
# %%
df = pd.read_csv('/home/miroslav/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/hb_price_db/BingScrapper/bing/spiders/hb_email_unique.csv')
df
# %%
