# %%
import datetime
import json
from numpy.core import records
import pandas as pd
import requests
import threading
import time
import os, sys

import django
from timeit import default_timer as timer

from requests.api import get
# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from collections import defaultdict as ddict
from django.core.paginator import Paginator

from calendar_app.models import (
    CalendarHotel,
    CalendarRoomType,
    CalendarDay
)

from hotelbeds_api.services import get_hotels_availability, get_check_rate
from hotelbeds_scraper.services import get_available_destinations_rooms

# %%
import hashlib
HB_B2C_API_KEY = "56wuc5b3me7sehbfr6mxe25e"
HB_B2C_SECRET = "uZmqmEBUKy"
HB_STATIC_HOTELS_ENDPOINT = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels"
HB_AVAILABILITY_ENDPOINT = 'https://api.hotelbeds.com/hotel-api/1.0/hotels'

B2B_HOTELBEDS_API_KEY="6zuee8yw6ff55qmp96k9egma" 
B2B_HOTELBEDS_SECRET="f6PqqhUtG5"

TODAY = datetime.date.today()
DAYS_TO_CHECK = 1 # 90
CHECK_IN_DATES = [TODAY + datetime.timedelta(days=day) for day in range(1, DAYS_TO_CHECK + 1 )]

LENGTHS_OF_STAY = [1] # ,3,7,28

start = time.perf_counter()

# %%
def request_headers():
    time_as_int = int(time.time())

    signature_string = f'{B2B_HOTELBEDS_API_KEY}{B2B_HOTELBEDS_SECRET}{time_as_int}'
    signature = hashlib.sha256(signature_string.encode('utf-8')).hexdigest()
    
    headers = {
        "X-Signature": signature,
        "Api-Key": B2B_HOTELBEDS_API_KEY,
        "Accept": "application/json",
        'Accept-Encoding': 'gzip',
        'Content-Type': 'application/json',
        '6zuee8yw6ff55qmp96k9egma': ''
    }

    return headers

# %%
import requests
import datetime

hotel_id = 1212
room_type = 'TPL.DX'

check_in = "2021-04-04"
check_out = "2021-04-05"

url = "https://api.hotelbeds.com/hotel-api/1.0/hotels"

payload={
    "stay": {
        "checkIn": check_in,
        "checkOut": check_out,
        # "shiftDays": 5
    },
    "occupancies": [
        {
            "rooms": 1,
            "adults": 1,
            "children": 0
        }
    ],
    "hotels": {
        "hotel": [hotel_id
        ]
    },
    # "rooms": {
    #     "room": [
    #         room_type,
    #     ],
    # },
    # "boards": {
    #         "included": "true",
    #         "board": [
    #             "RO"
    #         ]
    # },
    "filter": {
        "paymentType": "AT_WEB",
        "packaging": "False"
    },
    # "dailyRate": True,
    # "sourceMarket": "UK"
}

response = requests.request("POST", url, headers=request_headers(), data=json.dumps(payload))

response_json = response.json()
response_list = response_json['hotels']['hotels']
response_list


# %%
json_data2 = pd.json_normalize(
                    data=response_list, 
                    record_path=['rooms', 'rates' ], # 'cancellationPolicies', 'dailyRates'
                    meta=['code', 'name', 'destinationCode', 'destinationName', 'zoneName', 'currency', 'categoryName', 
                    ['rooms', 'name'], ['rooms', 'code'] ],
                    record_prefix='rates_')
json_data2
# %%
try:
    json_data3 = pd.json_normalize(
                        data=response_list, 
                        record_path=['rooms', 'rates', 'cancellationPolicies'],
                        errors='ignore')

    json_data2 = json_data2.join(json_data3)
except:
    print('skip')
json_data2
# %%
pd.json_normalize(
            data=response_list, 
            record_path=['rooms', 'rates', 'cancellationPolicies'],
            errors='ignore')
# %%
# %%
import numpy as np
df_cancel = pd.DataFrame(columns=['amount', 'from'])

for i in response_list:
    rooms = i['rooms']
    for j in rooms: 
        rates = j['rates']
        for k in rates:
            try:
                cancel = k['cancellationPolicies'] 
                # print(cancel)
            except:
                cancel = [{'amount': 'nan', 'from': 'nan'}]
                # print('nan')

            df_cancel = df_cancel.append(pd.DataFrame(cancel, columns=['amount', 'from']), ignore_index=True)
            
df_cancel
# %%
json_data2.join(df_cancel)
# %%
# %%
json_data2.rename(columns={'rooms.code':'rooms_code'}, inplace=True)

scraped_time = pd.to_datetime(pd.to_datetime('today').strftime('%Y-%m-%d %H:%M:%S') )
json_data2['scraped_time'] = scraped_time
json_data2['check_in'] = pd.to_datetime(check_in)
json_data2['check_out'] = pd.to_datetime(check_out)

json_data2['rates_cancellationPolicies'] = json_data2['rates_cancellationPolicies'].astype(str)

# json_data2 = json_data2[['scraped_time', 'check_in', 'check_out', 
#             'code', 'name','destinationCode', 'destinationName', 'categoryName', 'currency',
#             'rooms.name', 'rooms.code', 
#             'rates.net', 'rates.allotment', 'rates.boardName', 'rates.cancellationPolicies',
#             'rates.rateClass', 'rates.rateType', 'rates.rateKey'
#             ]]

json_data2['hotel_room_id'] = json_data2['code'].astype(str) + json_data2['rooms_code']
json_data2 = json_data2.sort_values('rates_net', ascending=True).drop_duplicates('hotel_room_id').reset_index()

# json_data2['country_code'] = country_code

json_data2 = json_data2[['scraped_time', 'check_in', 'check_out', 'country_code', 'destinationCode', 'code', 'rooms_code',
            'rates_net', 'rates_allotment', 'rates_boardCode', 'rates_cancellationPolicies'
            ]]
json_data2['rates_cancellationPolicies'] = json_data2['rates_cancellationPolicies'].fillna('0')
print(json_data2)
# %%
# %%
# %%
# %%
import pandas as pd
# df = pd.read_excel(r'G:\Shared drives\Dashboards (KPIs)\POWER BI\Leap_HotelBeds_&_Airbnb\KN_hb_MATCHING_bing_URL\hb_bcom_matching\_25_26_Jan_batch\hb_bcom_hotel_unique.xlsx')
df = pd.read_excel(r'/home/miroslav/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/data/hb_bcom_hotel_unique.xlsx')

df.drop('booking_link',axis=1, inplace=True)
df = df.iloc[:1]
df_list = df['hb_hotel_id'].to_list()

# %%
def divide_chunks(_list, amount): 

    for i in range(0, len(_list), amount):  
        yield _list[i:i + amount]

hotel_ids_list_chunk = list(divide_chunks(_list=df_list,amount=500))

# %%
######################################### Variables
LOS = 5
today = pd.to_datetime('today').normalize()

check_in = "2021-03-25" # today.strftime('%Y-%m-%d')
check_out = (pd.to_datetime(check_in) + pd.to_timedelta(f'{LOS} day') ).strftime('%Y-%m-%d')

# %%

json_data = []

for i in hotel_ids_list_chunk:
    payload ={
			"stay": {
                "checkIn": "2021-03-25",
                "checkOut": "2021-03-30"
            },
            "occupancies": [
                {
                    "rooms": 1,
                    "adults": 1,
                    "children": 0
                }
            ],
            "hotels": {
                "hotel": 4826
            },
            "filter": {
                "paymentType": "AT_WEB",
                "packaging": "True"
            },
            "dailyRate": True
        }   

    response = requests.post(url=HB_AVAILABILITY_ENDPOINT, headers=request_headers(), data=json.dumps(payload))
    print(response)
    response_json = response.json()
    print(response_json)
    # response_json = (response_json['hotels']['hotels'])
    json_data.extend(response_json)
    print(len(json_data))
    print(json_data)
    # time.sleep(10)

# %%
json_data2 = json.dumps(json_data)
json_data1 = json.loads(json_data2)
json_data1
# %%
f = open('hb_hotel_json', 'w')
f.write(str(json_data2))
f.close()
# %%
pd.set_option('display.max_colwidth', 500)
json_data2 = pd.json_normalize(
                    data=json_data1, 
                    record_path=['rooms', 'rates' ], # 'cancellationPolicies', 'dailyRates'
                    meta=['code', 'name', 'destinationCode', 'destinationName', 'currency', 'categoryName', 
                    ['rooms', 'name'], ['rooms', 'code'] ],
                    record_prefix='rates.')
json_data2 #[['rates.cancellationPolicies']]
# %%
json_data2['scraped_time'] = pd.to_datetime(pd.to_datetime('today').strftime('%Y-%m-%d %H:%M:%S') )
json_data2['check_in'] = pd.to_datetime(check_in)
json_data2['check_out'] = pd.to_datetime(check_out)

# %%
json_data2 = json_data2[['scraped_time', 'check_in', 'check_out', 
            'code', 'name','destinationCode', 'destinationName', 'categoryName', 'currency',
            'rooms.name', 'rooms.code', 
            'rates.net', 'rates.allotment', 'rates.boardName', 'rates.cancellationPolicies',
            'rates.rateClass', 'rates.rateType', 'rates.rateKey'
            ]]
# %%
json_data2['hotel_room_id'] = json_data2['code'].astype(str) + json_data2['rooms.code']
json_data2 = json_data2.sort_values('rates.net', ascending=True).drop_duplicates('hotel_room_id').reset_index()

# %%
df_price_UK_daily = pd.json_normalize(data=json_data1, record_path=['rooms','rates', 'dailyRates'], meta=['code', ['rooms', 'code'] ] )
df_price_UK_daily            
# t = pd.json_normalize(data=json_data1, record_path=['rooms','rates', 'cancellationPolicies'] )
# t['cancellationPolicies'] = t['cancellationPolicies'].fillna('0')
# t
# %%
json_data2.to_csv(r'~/Downloads/hb_price5.csv')

# %%
# import flat_table as ft
# t = pd.json_normalize(json_data1)
# ft.normalize(t.iloc[:1])

# %%

# %%
# while True:
#     hotels_data = response
#     if 'error' in hotels_data:
#         if type(hotels_data['error']) == str:
#             time.sleep(20)
#         else:
#             hotels_data = []
#             break
#     else:
#         break

# response_json = hotels_data.json()
# print(response_json)
# %%

# %%
######################################### Compare Hotelbeds live prices with manual run


# %%

# %%

# %%

# %%
