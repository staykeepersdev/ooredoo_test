# %%
import django, os, sys
import concurrent.futures

import numpy as np
import pandas as pd

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()
# %%
########################################
# Auth Octoparse login
from octoparse.client import OctoClient
OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)

# %%
# Scraper task to download all items in each page
task_id_list = ['570328c5-2c05-4b83-9fd8-0e0b5b8b5f02', 
                '65f642c1-5f99-4777-a9db-e714a6366fff',
                '5f5fbe46-de99-461c-887d-a90ec9929a42',
                'c62c8285-4d81-4b71-b433-615a601e46bf',
                'd8f5abe7-6121-46da-875f-6a5036a015c4',
                'e91cf62f-8b29-4e75-885f-667904c0f36a',
                'd77fa2b0-e27f-4564-ad17-ef54b864fac0',
                'd6468547-5aea-4419-a9a6-bd438f7bceb2',
                'Cc054939-f859-4ebf-b861-29be6c0cc3d9',
                '7cc92c9f-22f5-4ba0-817a-7c68acc225ef',
                '43c7d91a-49ff-454d-8dd0-d55628c725de',
                '44b4e9c6-2c61-4f62-88d7-b872824ac0d3',
                '9979f476-31ec-4ee2-8d4e-b9a43ac01c0f',
                '6cad3bc3-a00b-4d64-b36b-ebdd1ff4b5e3',
                '6316b00a-0a52-438a-a7be-a1e53c4b9848'
]

for task_id in task_id_list:

  r_d = OctoClient.get_data_by_offset(task_id, 0, 1)
  r_d

  n = 1000
  total = r_d['data']['total']
  # print(total)
  pages = int(np.ceil(total / n))
  # print(pages)
  pages_list = list(np.arange(0,pages,1))

  def task_download(pages_list):
    LIMIT = pages_list * 1000
    print(LIMIT)
    r = OctoClient.get_data_by_offset(task_id, LIMIT, n)
    dataList = r['data']['dataList']

    df = pd.DataFrame(dataList)
    df.to_csv('file.csv', mode='a', header=False)

  with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
      executor.map(task_download, pages_list)

# %%
# %%
columns = ['index_1', 'Current_Time', 'Page_URL', 'Page_Title', 'Title',
            'Title_URL', 'b_attribution', 'Field']
# %%
df_all_tasks = pd.read_csv('file.csv', header=None, names=columns)
df_all_tasks
# %%
# df_all_tasks[['Page_Title', 'Title', 'b_attribution']].sort_values(by='Title', ascending=False).to_csv('~/Downloads/df_all_tasks.csv')
# df_all_tasks[df_all_tasks['b_attribution'].str.contains(r'hotelier')]

# %%
df_all_tasks['unique_sources'] = [".".join(x.split(".")[1:2]) for x in df_all_tasks['b_attribution'].astype(str)]
df_all_tasks
# %%
df_exclude = df_all_tasks.drop_duplicates(subset=['unique_sources']).drop_duplicates(subset=['Page_URL']) #.to_csv('~/Downloads/df_all_tasks1.csv')

# %%
urls_exclude_list = ['tripadvisor', 'expedia', 'facebook', 'booking.com', 'hotels.com', 'kayak',
                    'agoda', 'tui.com', 'hotelcheapestdeals', 'marriott', 'ebookers', 'pinterest',
                    'hostelworld', 'tropki', 'hyatt', 'laterooms', 'lastminute',
                    'skyscanner', 'travelocity', 'wikipedia', 'linkedin', 'instagram',
                    'ihg.', 'lhw.', 'tui.', 'tsogosun', 'getaroom', 'walksofitaly', 'allseasonsresortlodging',
                    'hilton', 'travelrepublic', 'hotelsclick', 'hotelmix'
                     ]
filtered_urls_df = df_exclude[~df_exclude['b_attribution']\
                        .str.contains('|'.join(urls_exclude_list))]
filtered_urls_df

# %%
filtered_urls_df.to_csv('~/Downloads/filtered_urls_df_1.csv')
# %%

# %%

# %%
# df_all_tasks[['unique_sources', 'b_attribution']].groupby(by='unique_sources')\
#               .count().sort_values(by='b_attribution', ascending=False).to_csv('~/Downloads/all_hotel_urls.csv')

# %%
urls_exclude_list = ['tripadvisor', 'expedia', 'facebook', 'booking.com', 'hotels.com', 'kayak',
                    'agoda', 'tui.com', 'hotelcheapestdeals', 'marriott', 'ebookers', 'pinterest',
                    'hostelworld', 'tropki', 'hyatt', 'laterooms', 'lastminute',
                    'skyscanner', 'travelocity', 'wikipedia', 'linkedin', 'instagram',
                    'ihg.', 'lhw.', 'tui.', 'tsogosun', 'getaroom', 'walksofitaly', 'allseasonsresortlodging',
                    'hilton', 'travelrepublic', 'hotelsclick'
                     ]
filtered_urls_df = df_all_tasks[~df_all_tasks['b_attribution']\
                        .str.contains('|'.join(urls_exclude_list))]
filtered_urls_df
# %% 
filtered_urls_df[200:250]
# %%
df_all_tasks
# %%
# %%
# %%
# %%
# %%
# Scraper task to download all items in each page
# task_id = '570328c5-2c05-4b83-9fd8-0e0b5b8b5f02'
# r_d = OctoClient.export_non_exported_data(task_id, 1000)

# n = 1000
# total = r_d['data']['total']
# # print(total)
# pages = int(np.ceil(total / n))
# # print(pages)

# scraped_data = []

# for i in range(pages):
#         i = OctoClient.export_non_exported_data(task_id, 1000)
#         print(i)

#         task_update = OctoClient.update_data_status(task_id)
#         print(task_update)

#         item = i['data']['dataList']
#         # print(item)
#         scraped_data.append(item)
# %%

# %%
