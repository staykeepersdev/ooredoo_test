# %%
import sys, os, django
import pandas as pd

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.leap_ooredoo import engine_leap_ooredoo
# %%
query = '''SELECT * 
            FROM hotelbeds_data_apihotel
        '''

hb_hotel_content = pd.read_sql_query(query, engine_leap_ooredoo)
# %%
df = pd.json_normalize(hb_hotel_content['data'])
df
# %%
hb_df = df[['code', 'countryCode', 'destinationCode', 'accommodationTypeCode', 'chainCode', 'city.content', 'name.content',
    'address.street', 'address.content', 'coordinates.latitude', 'coordinates.longitude', 'S2C',
    'web', 'email'
]]
# %%
hb_df.to_csv('~/Downloads/hb_df.csv')

# %%

# %%
import numpy as np
def hotel_chunks(hb_df, n):
    total = np.arange(0,n * (len(hb_df) / n))
    c = -1
    total_ls = []
    for i in total:
        c += 1
        if c == n:
            c = 0 
        else:
            c
        total_ls.append(c)
    return total_ls
total_ls = hotel_chunks(hb_df=hb_df, n=20000)
total_ls
# %%
hb_hotel_join = hb_df.join(pd.DataFrame(total_ls))
# %%
hb_df.to_csv('~/Downloads/hb_hotel_content.csv')
hb_hotel_join.to_excel('~/Downloads/hb_hotel_join1.xlsx')
# %%

# %%
