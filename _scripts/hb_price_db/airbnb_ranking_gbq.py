# %%
import pandas as pd

df = pd.read_csv('/home/miroslav/Dev/wwpt_projects/ooredoo/_scripts/scrapers/airbnb/crawled/airbnb_ranking1.csv',
        delimiter='|', header=None, 
        names=['scrape_time', 'df_index', 'room_id', 'ranking_position', 'ranking_position_page', 
        'current_page_number', 'rating', 'reviews', 'total_listings'])
df
# %%
today = pd.to_datetime('today').strftime('%Y-%m-%d')
df = df[df['scrape_time'] > today]
# %%
df.to_gbq(destination_table="hotels_slg_db.hb_listing_ranking",
            project_id = "xero-245302",
            if_exists="append")

# %%
