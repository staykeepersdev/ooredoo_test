import django, os, sys
import hashlib
import json
import requests
import time
import threading

import pandas as pd
import datetime 
import pytz 
import concurrent.futures

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price
from _scripts.sql_alchemy_settings.leap_ooredoo import engine_leap_ooredoo


HB_B2C_API_KEY = "56wuc5b3me7sehbfr6mxe25e"
HB_B2C_SECRET = "uZmqmEBUKy"
HB_STATIC_HOTELS_ENDPOINT = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels"
HB_AVAILABILITY_ENDPOINT = 'https://api.hotelbeds.com/hotel-api/1.0/hotels'

B2B_HOTELBEDS_API_KEY="6zuee8yw6ff55qmp96k9egma" 
B2B_HOTELBEDS_SECRET="f6PqqhUtG5"

TODAY = datetime.date.today()
print(TODAY)
DAYS_TO_CHECK = 1
CHECK_IN_DATES = [TODAY + datetime.timedelta(days=day) for day in range(1, DAYS_TO_CHECK + 180 )]

LENGTHS_OF_STAY = [1] # ,3,7,28

start = time.perf_counter()

def split_list_on_n_elements(_list, number_of_elements):
    for i in range(0, len(_list), number_of_elements):  
        yield _list[i:i + number_of_elements]

def request_headers():
    time_as_int = int(time.time())

    signature_string = f'{B2B_HOTELBEDS_API_KEY}{B2B_HOTELBEDS_SECRET}{time_as_int}'
    signature = hashlib.sha256(signature_string.encode('utf-8')).hexdigest()
    
    headers = {
        "X-Signature": signature,
        "Api-Key": B2B_HOTELBEDS_API_KEY,
        "Accept": "application/json"
    }

    return headers

def get_static_hotel_params(_from, _to, country_code, fields='code'):
    params ={
        "fields" : fields,
        "from": _from,
        "to": _to,
        "countryCode": country_code
    }
    return params

def check_response_errors(response):
    if 'error' in response:
        if type(response['error']) == str and 'limit exceeded' in response['error']:
            return True
        else:
            return False
    else:
        return False

def get_hotelbeds(endpoint, params):
    while True:
        response = requests.get(
            url=endpoint,
            headers=request_headers(),
            params=params
        )
        if not check_response_errors(response):
            break
        else:
            time.sleep(0.5)

    return json.loads(response.content)

def post_hotelbeds(endpoint, params):
    while True:
        response = requests.post(
            url=endpoint,
            headers=request_headers(), 
            json=params
        )
        if not check_response_errors(response):
            break
        else:
            time.sleep(0.5)

    return json.loads(response.content)

def get_country_hotel_ids(country_code):
    _from = 1
    _to = 1000
    hotel_ids = []
    
    while True:
        params = get_static_hotel_params(_from,_to, country_code)
        response_data = get_hotelbeds(HB_STATIC_HOTELS_ENDPOINT, params)

        
        for hotel in response_data['hotels']:
            hotel_ids.append(hotel['code'])

        total_hotels = response_data['total']
        
        print(f'Fetching Hotel IDS from: {_from} to: {_to}. Total: {total_hotels}')
        if total_hotels > _to:
            _from += 1000
            _to += 1000
        else:
            break        

    return hotel_ids

def get_availability_params(check_in, check_out, hotel_ids):
    params = {
        "stay": {
            "checkIn": check_in.strftime("%Y-%m-%d"),
            "checkOut": check_out.strftime("%Y-%m-%d")
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": 2,
                "children": 0
            }
        ],
        "hotels": {
            "hotel": hotel_ids
        },
        "filter": {
            "paymentType": 'AT_WEB',

            # CHANGE IF YOU WANT PACKAGES OR NO
            "packaging": False,
            
            # TO REDUCE THE RESPONSE SIZE and SPEED
            "maxRatesPerRoom": 1
        },
            # "dailyRate": True
    }
    return params

def get_hotels_prices(counter, hotel_ids):
   
    end = time.perf_counter()

    for length_of_stay in LENGTHS_OF_STAY:
        end = time.perf_counter()
        print(f'Starting LOS {length_of_stay}| from {counter*1000} to {(counter+1)*1000} | {round( (end - start)/60, 2)} min')


        for check_in_date in CHECK_IN_DATES:
            check_out_date = check_in_date + datetime.timedelta(days=length_of_stay)

            params = get_availability_params(check_in_date, check_out_date, hotel_ids)

            response_data = post_hotelbeds(HB_AVAILABILITY_ENDPOINT, params)

            ############################### process the response data
            try:
                json_data = response_data['hotels']['hotels']
            except:
                print(response_data)
                continue 
            # print(len(json_data))

            df_price_UK_sum = pd.json_normalize(
                            data=json_data, 
                            record_path=[ 'rooms', 'rates' ], # 'cancellationPolicies'
                            meta=['code', 'name', 'destinationCode', 'destinationName', 'zoneName', 'currency', 'categoryName',
                            ['rooms', 'name'], ['rooms', 'code' ] ],
                            record_prefix='rates_')

            
            df_cancel = pd.DataFrame(columns=['amount', 'from'])

            for i in json_data:
                rooms = i['rooms']
                for j in rooms: 
                    rates = j['rates']
                    for k in rates:
                        try:
                            cancel = k['cancellationPolicies']
                            # print(cancel)
                        except:
                            cancel = [{'amount': 'nan', 'from': 'nan'}]
                            # print('nan')

                        df_cancel = df_cancel.append(pd.DataFrame(cancel, columns=['amount', 'from']), ignore_index=True)
            
            df_price_UK_sum = df_price_UK_sum.join(df_cancel)

            df_price_UK_sum.rename(columns={'rooms.code':'rooms_code'}, inplace=True)

            utc_now = pytz.utc.localize(datetime.datetime.utcnow())
            pst_now = utc_now.astimezone(pytz.timezone("Europe/London")).isoformat()

            scraped_time = pd.to_datetime(pd.to_datetime(pst_now).strftime('%Y-%m-%d %H:%M:%S'))
            df_price_UK_sum['scraped_time'] = scraped_time
            df_price_UK_sum['check_in'] = pd.to_datetime(check_in_date)
            df_price_UK_sum['check_out'] = pd.to_datetime(check_out_date)

            df_price_UK_sum['hotel_room_id'] = df_price_UK_sum['code'].astype(str) + df_price_UK_sum['rooms_code']
            df_price_UK_sum = df_price_UK_sum.sort_values('rates_net', ascending=True).drop_duplicates('hotel_room_id').reset_index()
            
            df_price_UK_sum['LOS'] = length_of_stay
            df_price_UK_sum['country_code'] = country_code

            df_price_UK_sum = df_price_UK_sum[['scraped_time', 'check_in', 'LOS', 'country_code', 'destinationCode', 'zoneName', 'code', 'rooms_code',
                        'rates_net', 'rates_allotment', 'rates_boardCode', 'amount', 'from', 'rooms.name'
                        ]]

            # push to PostgreSQL
            df_price_UK_sum.to_sql('hb_price_6', con=engine_local_db_hb_price, if_exists='append',
                        method='multi')
            

            ################################# hb_price_UK_daily
            """
            df_price_UK_daily = pd.json_normalize(data=json_data1, record_path=['rooms','rates', 'dailyRates'], meta=['code', ['rooms', 'code'] ] )
            
            df_price_UK_daily['scraped_time'] = scraped_time
            df_price_UK_daily['check_in'] = pd.to_datetime(check_in_date)
            df_price_UK_daily['check_out'] = pd.to_datetime(check_out_date)

            df_price_UK_daily['hotel_room_id'] = df_price_UK_daily['code'].astype(str) + df_price_UK_daily['rooms.code'] + df_price_UK_daily['offset'].astype(str)
            df_price_UK_daily = df_price_UK_daily.sort_values('dailyNet', ascending=True).drop_duplicates('hotel_room_id').reset_index()

            df_price_UK_daily = df_price_UK_daily[['scraped_time', 'check_in', 'check_out', 
                        'code', 'rooms.code', 'offset', 'dailyNet'
                        ]]

            # push to PostgreSQL
            df_price_UK_daily.to_sql('hb_price_UK_daily', con=engine_local_db_hb_price, if_exists='append',
                        method='multi')
            """
            end = time.perf_counter()
            # print( f"Push to database {round( (end - start)/60, 2)} min")

def hb_countries_codes():
    query = '''
            SELECT code FROM public.hotelbeds_data_apicountry
            '''
    countries_df = pd.read_sql_query(query, engine_leap_ooredoo)
    countries_list = countries_df['code'].to_list()
    return countries_list



def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

def get_country_prices(country_code):
    global total_hotel_ids
    country_hotel_ids = get_country_hotel_ids(country_code)
    total_hotel_ids = len(country_hotel_ids)
    print(f'Total :{total_hotel_ids}')
    
    slices_hotel_ids = split_list_on_n_elements(country_hotel_ids, 1000)
    for counter, hotel_ids in enumerate(slices_hotel_ids):
        # Check running threads before starting new one
        while threading.active_count() >= MAX_THREADS:
            time.sleep(2)
        run_threaded(get_hotels_prices, (counter, hotel_ids) )

MAX_THREADS = 5

total_global_ids = 0
start = time.perf_counter()

global country_code
for country_code in hb_countries_codes():
    try:
        country_code_global = get_country_prices(country_code)
    except:
        time.sleep(360)
        print('Country_Error')
    print(country_code)

end = time.perf_counter()
print( f"End of script {round( (end - start)/60, 2)} min")


time.sleep(720)
import _scripts.hb_price_db.daily_parquet_update