import scrapy
import django, os, sys

from datetime import datetime
import pytz 
import csv, re
import pandas as pd

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price
from _scripts.scrapers.bcom_price.analysis.hb_bcom_hotel import bcom_active_hotel_price_urls

#bcom_active_hotel_price_urls = bcom_active_hotel_price_urls.loc[:10]

# DATE_STR = datetime.now().strftime('%Y_%m_%d-%H_%M')

class HbBcomPriceStaticSpider(scrapy.Spider):
    name = 'hb_bcom_price'
    allowed_domains = ['booking.com/']
    request_counter = 0
    bcom_active_us_uk_ae_price_total = len(bcom_active_hotel_price_urls)
    print(bcom_active_us_uk_ae_price_total)

    def start_requests(self):

        for index, row in bcom_active_hotel_price_urls.iterrows():
            df_index = index
            hotel_id = row['hotel_id']
            check_in = row['check_in']
            start_urls = row['bcom_hotel_url_price']
            yield scrapy.Request(url=start_urls,
                                 meta={'hotel_id': hotel_id,
                                       'check_in': check_in,
                                       'index':df_index})

    def parse(self, response):
        utc_now = pytz.utc.localize(datetime.utcnow())
        pst_now = utc_now.astimezone(pytz.timezone("Europe/London")).strftime('%Y-%m-%d %H:%M:%S') #.isoformat()
        scrape_time = datetime.fromisoformat(pst_now) 

        self.request_counter +=1
        print (f'Checked #{self.request_counter} out of {self.bcom_active_us_uk_ae_price_total}.')

        PRICES = response.xpath('//tbody/tr/td[@class="hprt-table-cell -first hprt-table-cell-roomtype droom_seperator"]')
        for price in PRICES:
            room_code = price.xpath('./div/div/a/@name').extract_first()
            room_code = re.search('\d+',room_code).group(0)
            room_name = price.xpath('./div/div/a/span/text()').extract_first().strip()

            occupancy = price.xpath('./parent::*/td[contains(@class,"hprt-table-cell hprt-table-cell-occupancy")]/div/div/span[@class="bui-u-sr-only"]/text()').extract_first().strip()
            occupancy = re.search('\d+',occupancy).group(0)
            
            total_price = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/span[contains(@class,"prco-valign-middle-helper")]/text()').extract_first().strip()
            price_info_night = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"night")]/text()').extract_first()
            price_info_night_value = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"night")]/following-sibling::td/span/text()').extract_first()
            price_info_VAT = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"VAT")]/text()').extract_first()
            price_info_VAT_value = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"VAT")]/following-sibling::td/text()').extract_first()
            price_info_Tax = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"Tax")]/text()').extract_first()
            price_info_Tax_value = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"Tax")]/following-sibling::td/text()').extract_first()
            price_info_City_tax_value = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"City tax")]/following-sibling::td/text()').extract_first()
            price_info_Resort_fee_value = price.xpath('./parent::*/td/div[contains(@class,"hprt-price-block")]/div/div/div/div/div/div/table/tbody/tr/td[contains(text(),"Resort fee")]/following-sibling::td/text()').extract_first()

            df = pd.DataFrame({
                    'srape_time':scrape_time, ### To fix the column name in the DB
                    'index_':response.meta['index'],
                    'hotel_id':response.meta['hotel_id'],
                    'check_in':response.meta['check_in'],
                    'room_code':room_code,
                    'room_name':room_name,
                    'occupancy':occupancy,
                    'total_price':total_price,
                    'price_info_night':price_info_night,
                    'price_info_night_value':price_info_night_value,
                    'price_info_VAT':price_info_VAT,
                    'price_info_VAT_value':price_info_VAT_value,
                    'price_info_Tax':price_info_Tax,
                    'price_info_Tax_value':price_info_Tax_value,
                    'price_info_City_tax_value':price_info_City_tax_value,
                    'price_info_Resort_fee_value':price_info_Resort_fee_value
            }, index=[0])

            df.to_sql('bcom_price', con=engine_local_db_hb_price, 
                    if_exists='append', method='multi')
            
            # with open(f'./_scripts/scrapers/bcom_price/crawled_data/hotel_prices_all_11.csv', 'a+', encoding="utf-8") as file: # {DATE_STR}
            #     files = csv.writer(file, delimiter='|')
            #     files.writerow([
            #         scrape_time,
            #         response.meta['index'],
            #         response.meta['hotel_id'],
            #         response.meta['check_in'],
            #         room_code,
            #         room_name,
            #         occupancy,
            #         total_price,
            #         price_info_night,
            #         price_info_night_value,
            #         price_info_VAT,
            #         price_info_VAT_value,
            #         price_info_Tax,
            #         price_info_Tax_value,
            #         price_info_City_tax_value,
            #         price_info_Resort_fee_value
            #     ])