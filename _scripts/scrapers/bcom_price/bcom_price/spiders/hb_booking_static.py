import scrapy
import django, os, sys

import pandas as pd
from datetime import datetime
import pytz
import csv, re

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price
from _scripts.scrapers.bcom_price.analysis.hb_bcom_hotel import bcom_active_hotel_price_urls

# bcom_active_hotel_price_urls = bcom_active_hotel_price_urls.loc[:10]

DATE_STR = datetime.now().strftime('%Y_%m_%d-%H_%M')

class HbBookingStaticSpider(scrapy.Spider):
    name = 'hb_booking_static'
    allowed_domains = ['booking.com/']
    request_counter = 0
    bcom_active_hotel_price_urls_total = len(bcom_active_hotel_price_urls)
    print(bcom_active_hotel_price_urls_total)

    def start_requests(self):

        for index, row in bcom_active_hotel_price_urls.iterrows():
            df_index = index
            hotel_id = row['hotel_id']
            start_urls = row['bcom_hotel_url']
            yield scrapy.Request(url=start_urls, 
                                 meta={'hotel_id': hotel_id})

    def parse(self, response):
        utc_now = pytz.utc.localize(datetime.utcnow())
        pst_now = utc_now.astimezone(pytz.timezone("Europe/London")).strftime('%Y-%m-%d %H:%M:%S') #.isoformat()
        scrape_time = datetime.fromisoformat(pst_now) ### To fix the column name in the DB

        self.request_counter +=1
        print (f'Checked #{self.request_counter} out of {self.bcom_active_hotel_price_urls_total}.')

        ROOMS = response.xpath('//td[@class="occ_no_dates"]/parent::*')
        for room in ROOMS:

            hotel_type = response.xpath("//*[@class='hp__hotel-type-badge']/text()").extract_first()
            try:
                hotel_name = response.xpath("//*[@class='hp__hotel-type-badge']/following-sibling::text()").extract_first().strip()
            except:
                hotel_name = ''
            country = response.xpath('//div/a[contains(@href,"country")]/text()').extract_first().strip()
            try:
                region = response.xpath('//div/a[contains(@href,"region")]/text()').extract_first().strip()
            except:
                region = ''
            city = response.xpath('//div/a[contains(@href,"city")]/text()').extract_first().strip()
            try:
                district = response.xpath('//div/a[contains(@href,"district")]/text()').extract_first().strip()
            except:
                district = ''
            try:
                lat = response.xpath('//a[@id="hotel_address"]/@data-atlas-latlng').extract_first().split(',')[0]
                lng = response.xpath('//a[@id="hotel_address"]/@data-atlas-latlng').extract_first().split(',')[1]
            except:
                lat = ''
                lng = ''
            try:
                address = response.xpath('//p[@class="address address_clean"]/a/following-sibling::span[1]/text()').extract_first().strip()
            except:
                address = ''
            try:
                rating = response.xpath('//div[@class="bui-review-score__badge"]/text()').extract_first().strip()
            except:
                rating = ''
            try:
                reviews = response.xpath('//div[@class="bui-review-score__text"]/text()').extract_first().strip()
                reviews = re.search(r'\d+(?:,\d+)?',reviews).group(0)
            except:
                reviews = ''
            
            #######################################
            
            try:
                occupancy_adults = room.xpath('./td/span[@class="bui-u-sr-only"][contains(text(),"Max")]/text()').extract_first().split('.')[0]
                occupancy_adults = re.search('\d+',occupancy_adults)[0]
            except:
                occupancy_adults = ''
            try:
                occupancy_children = room.xpath('./td/span[@class="bui-u-sr-only"][contains(text(),"Max")]/text()').extract_first().split('.')[1]
                occupancy_children = re.search('\d+',occupancy_children)[0]
            except:
                occupancy_children = ''
            room_name = room.xpath('./td/div/div/a[@class="jqrt togglelink"]/@data-room-name-en').extract_first()
            try:
                room_code = room.xpath('./td/div/div/a[@class="jqrt togglelink"]/@href').extract_first()
                room_code = re.search('\d+',room_code).group(0)
            except:
                room_code = ''
            try:
                bed_type = room.xpath('./td/div/div/div/ul/li/span').extract_first()
                bed_type = re.search(r'\n(.*?)\n', bed_type).group(0).strip()
            except:
                bed_type = ''

            df = pd.DataFrame({
                    'scrape_time':[scrape_time],
                    'hotel_id': [response.meta['hotel_id']],
                    'hotel_type':[hotel_type],
                    'hotel_name':[hotel_name],
                    'country':[country],
                    'region':[region],
                    'city':[city],
                    'district':[district],
                    'lat':[lat],
                    'lng':[lng],
                    'address':[address],
                    'rating':[rating],
                    'reviews':[reviews],
                    'occupancy_adults':[occupancy_adults],
                    'occupancy_children':[occupancy_children],
                    'room_code':[room_code],
                    'room_name':[room_name],
                    'bed_type':[bed_type]
            })

            df.to_sql('bcom_static', con=engine_local_db_hb_price, 
                    if_exists='append', method='multi')

            # with open(f'./_scripts/scrapers/bcom_price/crawled_data/hotel_static_{DATE_STR}.csv', 'a+', encoding="utf-8") as file: # {DATE_STR}
            #     files = csv.writer(file, delimiter='|')
            #     files.writerow([
            #         scrape_time,
            #         response.meta['index'],
            #         response.meta['hotel_id'],
            #         hotel_type,
            #         hotel_name,
            #         country,
            #         region,
            #         city,
            #         district,
            #         lat,
            #         lng,
            #         address,
            #         rating,
            #         reviews,
            #         occupancy_adults,
            #         occupancy_children,
            #         room_code,
            #         room_name,
            #         bed_type
            #     ])

                # yield print(response.meta['hotel_id'])