# %%
import pandas as pd
import os, sys

project_name = 'ooredoo'
os.chdir(os.getcwd().split(project_name)[0] + '/' + project_name)
sys.path.append(os.getcwd())

# pd.set_option('max_colwidth', 800)
'''
1. To filter advertised HB listings/ LIVE
2. Just to take a list of all Hotelbeds_ids that are hotel chains and to exclude them (static)

'''
# %%
file_path = './_scripts/scrapers/bcom_price/static_data/active_bcom_bing.csv'
active_bcom_bing = pd.read_csv(file_path, sep='|', header=None,
                names=['srape_time', 'proxy', 'index', 'hotel_id', 'response', 'main_website', 'main_title', 'profile_review_stars', 'profile_review_link', 'profile_review_ota', 'profile_hotel_stars', 'address', 'address_link_coordinates', 'phone_number', 'description', 'reviews_other_webs', 'tripadvisor_review_score', 'tripadvisor_link', 'tripadvisor_histogram_review', 'booking_com_review_score', 'booking_com_link', 'expedia_review_score', 'facebook_link', 'instagram_link', 'twitter_link', 'first_url_search_booking_com', 'first_url_search_tripadvisor', 'first_url_search_expedia', 'first_url_search_airbnb', 'first_title', 'first_website', 'first_description', 'first_subsection', 'first_contact_details'])
# %%
bcom_active_df_all = active_bcom_bing.copy()
bcom_active_df_all['bcom_hotel_url'] = bcom_active_df_all['first_url_search_booking_com'].combine_first(bcom_active_df_all['booking_com_link'])
# %%
bcom_active_df_all = bcom_active_df_all[bcom_active_df_all['bcom_hotel_url'].str.contains('html', na=False)]
bcom_active_df_all['bcom_hotel_url'] = [".".join(x.split(".")[0:3]) for x in bcom_active_df_all['bcom_hotel_url'].astype(str)]
bcom_active_df_all['bcom_hotel_url'] = bcom_active_df_all['bcom_hotel_url'] + '.en-gb.html'
bcom_active_df_all
# %%
bcom_active_df_all = bcom_active_df_all[['index', 'hotel_id', 'bcom_hotel_url']]
bcom_active_df_all
# %%
#####################################################
hb_hotel_static = pd.read_csv('./_scripts/scrapers/bcom_price/static_data/hb_all_hotels_static.csv', usecols=['code', 'countryCode', 'chainCode', 'city.content', 'accommodationTypeCode'])
hb_hotel_static
# %%
bcom_active_hotel = bcom_active_df_all.merge(hb_hotel_static, left_on='hotel_id', right_on='code')
# %%
#####################################################
hb_hotel_chains_size = hb_hotel_static.groupby('chainCode')['code'].count()
hb_hotel_chains_size = pd.DataFrame(hb_hotel_chains_size)
hb_hotel_chains_size = hb_hotel_chains_size[hb_hotel_chains_size['code'] > 0]
hb_hotel_chains_chain_list = hb_hotel_chains_size.index.to_list()
hb_hotel_chains_chain_list
# %%
bcom_active_hotel_nobig_chains = bcom_active_hotel[~bcom_active_hotel['chainCode'].isin(hb_hotel_chains_chain_list)]
bcom_active_hotel_nobig_chains

# %%
# filtered_countries = ['IT', 'PT', 'ES', 'FR', 'BG']

# bcom_active_filter_country = bcom_active_hotel_nobig_chains[
#                     (bcom_active_hotel_nobig_chains['countryCode'].isin(filtered_countries)) ]
# bcom_active_filter_country
# # # %%
# # restricted_types_list = ['APTHOTEL', 'APARTMENT']
# restricted_cities_list = ['Miami Beach', 'New York']
# bcom_active_us_uk_ae = bcom_active_us_uk_ae[
#                     # (~bcom_active_us_uk_ae['accommodationTypeCode'].isin(restricted_types_list))&
#                     (~bcom_active_us_uk_ae['city.content'].isin(restricted_cities_list))
#                     ]

# %%
bcom_active_hotel = bcom_active_hotel_nobig_chains
# bcom_active_hotel = bcom_active_filter_country

bcom_active_hotel.reset_index(inplace=True)

bcom_active_hotel = bcom_active_hotel[['index', 'hotel_id', 'bcom_hotel_url']]
                                            # ,'countryCode', 'accommodationTypeCode', 'chainCode', 'city.content']]
bcom_active_hotel
# %%
#####################################################
# bcom_active_hotel = bcom_active_hotel.loc[:10]

today = pd.to_datetime('today').strftime('%Y-%m-%d')
days_in_advance = 20
LOS = 1
CHECK_INS = pd.date_range(start=today, periods=days_in_advance, freq='D')

bcom_hotel_url_list = []
for check_in in CHECK_INS:
    check_out = check_in + pd.DateOffset(LOS)

    check_in = check_in.strftime('%Y-%m-%d')
    check_out = check_out.strftime('%Y-%m-%d')

    for index, row in bcom_active_hotel.iterrows():
        index = row['index']
        hotel_id = row['hotel_id']
        bcom_hotel_url = row['bcom_hotel_url']
        bcom_hotel_url_price = f'{bcom_hotel_url}?checkin={check_in};checkout={check_out};no_rooms=1;group_adults=2;group_children=0'

        bcom_hotel_url_list_dict = {
            'index':index, 
            'hotel_id':hotel_id, 
            'check_in':check_in,
            'bcom_hotel_url':bcom_hotel_url, 
            'bcom_hotel_url_price':bcom_hotel_url_price
            }
        bcom_hotel_url_list.append(bcom_hotel_url_list_dict)

# %%
bcom_active_hotel_price_urls = pd.DataFrame(bcom_hotel_url_list)
bcom_active_hotel_price_urls
# bcom_active_hotel.to_csv('~/Downloads/bcom_active_hotel.csv')

# %%
