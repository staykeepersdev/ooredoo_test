'''
1. HB Price and Static
2. Bcom Price and Static
3. Static Merge with Unique Rooms
4. Static Fuzzy Match

5. Merge static and hb price, then price based on ['check_in', 'bcom_room_code']
6. Ranking Clean
7. Mean Calculation for each hotel room
8. Filter based on matching and ranking criteria

9 Bcom Static (option to filter on columns by geo-location, rating, reviews etc. from Booking.com)
10. Filter for correct urls and Active hotels and Geo Location
'''
# %%
import os, sys, django
import pandas as pd

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price
from _scripts.sql_alchemy_settings.leap_ooredoo import engine_leap_ooredoo
from _scripts.scrapers.bcom_price.analysis.room_name_fuzzy_matching import main_fuzzy_clean_matching

# %%
scraped_time_start = '2021-06-09'
scraped_time_end = '2021-06-14'
country_tuple = ('IT', 'PT', 'ES', 'FR')
check_in_start = scraped_time_start
check_in_end = (pd.to_datetime(check_in_start) + pd.to_timedelta('90 days')).strftime('%Y-%m-%d')
# %%
#################################### HB PRICE & STATIC ####################################
sql = f"""
    SELECT * FROM public.hb_price_6
    WHERE scraped_time >= '{scraped_time_start}' AND scraped_time < '{scraped_time_end}'
	AND country_code IN {country_tuple}
	AND check_in BETWEEN '{check_in_start}' AND '{check_in_end}';
"""
hb_price = pd.read_sql(sql=sql,con=engine_local_db_hb_price)
# hb_price = pd.read_csv('~/Downloads/hb_price.csv')
# hb_price
# %%
hb_price = hb_price[['scraped_time', 'check_in', 'country_code', 'code', 'rooms_code', 'rates_net', 'rooms.name']]
hb_price = hb_price.rename(columns={'code': 'hb_hotel_id', 'rooms_code': 'hb_room_code', 'rooms.name': 'hb_room_name', 'rates_net': 'hb_price'})
hb_price['hb_price'] = hb_price['hb_price'].astype(float)
hb_price['hb_hotel_room_id'] = hb_price['hb_hotel_id'].astype(str) + hb_price['hb_room_code']
hb_price['hb_hotel_id'] = hb_price['hb_hotel_id'].astype(str)
hb_price['check_in'] = pd.to_datetime(pd.to_datetime(hb_price['check_in']).dt.date)
hb_price['scraped_date'] = pd.to_datetime(pd.to_datetime(hb_price['scraped_time']).dt.date)
hb_price
# %%
hb_static = hb_price.drop_duplicates('hb_hotel_room_id')
hb_static = hb_static[['hb_hotel_id', 'hb_room_code', 'hb_room_name', 'hb_hotel_room_id']]
hb_static
# %%
#################################### BCOM PRICE & STATIC ####################################
sql = f"""
    SELECT * FROM bcom_price
    WHERE srape_time >= '{scraped_time_start}' AND srape_time < '{scraped_time_end}'
	AND check_in BETWEEN '{check_in_start}' AND '{check_in_end}';
"""
bcom_price = pd.read_sql(sql=sql,con=engine_local_db_hb_price)
bcom_price
# %%
bcom_price = bcom_price[['srape_time', 'hotel_id', 'check_in', 'room_code', 'room_name', 'total_price']]
bcom_price = bcom_price.copy()
bcom_price.rename(columns={'room_code':'bcom_room_code', 'hotel_id':'hb_hotel_id', 'room_name':'bcom_room_name', 'room_code':'bcom_room_code', 'total_price': 'bcom_price'}, inplace=True)
bcom_price['hb_hotel_id'] = bcom_price['hb_hotel_id'].astype('int').astype('str')
bcom_price['check_in'] = pd.to_datetime(bcom_price['check_in'])
bcom_price['scraped_date'] = pd.to_datetime(pd.to_datetime(bcom_price['srape_time']).dt.date)
bcom_price = bcom_price.copy()
bcom_price['bcom_price'] = bcom_price['bcom_price'].astype(str).str.extract('(\d+)').astype('float')
bcom_price
# %%
bcom_static = bcom_price.drop_duplicates('bcom_room_code')
bcom_static = bcom_static[['hb_hotel_id', 'bcom_room_code', 'bcom_room_name']]
bcom_static
# %%
#################################### FUZZY MATCH ####################################
hb_bcom_static_merge = hb_static.merge(bcom_static, on='hb_hotel_id', how='left')
hb_bcom_static_merge = hb_bcom_static_merge[hb_bcom_static_merge['bcom_room_code'].notna()]
hb_bcom_static_merge
# %%
hb_bcom_match_ranking = main_fuzzy_clean_matching(df=hb_bcom_static_merge, hb_room_name_column='hb_room_name', bcom_room_name_column='bcom_room_name')

hb_bcom_match_ranking = hb_bcom_match_ranking.sort_values(by='match', ascending=False).drop_duplicates(subset='hb_hotel_room_id', keep='first')
hb_bcom_match_ranking
# %%
#################################### PRICE RANKING ####################################
hb_bcom_price_merge = hb_price.merge(hb_bcom_match_ranking, on='hb_hotel_room_id')
hb_bcom_price_merge = hb_bcom_price_merge.merge(bcom_price, on=['scraped_date', 'check_in', 'bcom_room_code'])

hb_bcom_price_merge.rename(columns={'bcom_room_name_y': 'bcom_room_name', 'hb_room_name_y': 'hb_room_name',
                                    'hb_room_code_x':'hb_room_code'}, inplace=True)
hb_bcom_price_ranking = hb_bcom_price_merge[['hb_hotel_id', 'hb_room_code', 'bcom_room_code', 'hb_room_name', 'bcom_room_name',
                                            'match', 'hb_price', 'bcom_price', 'hb_hotel_room_id']]

hb_bcom_price_ranking_mean_unique = pd.DataFrame( hb_bcom_price_ranking.groupby('hb_hotel_room_id', as_index=False)[['hb_price', 'bcom_price']].mean() )
hb_bcom_price_ranking_mean_unique.columns = ['hb_hotel_room_id', 'hb_price_mean', 'bcom_price_mean']
hb_bcom_price_ranking_mean = hb_bcom_price_ranking_mean_unique.merge(hb_bcom_price_ranking, on='hb_hotel_room_id', how='left').drop_duplicates('hb_hotel_room_id')
hb_bcom_price_ranking_mean = hb_bcom_price_ranking_mean[['hb_hotel_room_id', 'hb_hotel_id', 'hb_room_code', 'bcom_room_code','hb_room_name', 'bcom_room_name',
                          'match', 'hb_price_mean', 'bcom_price_mean']]
hb_bcom_price_ranking_mean
# %%
#################################### RANKING ####################################
hb_bcom_price_ranking_mean = hb_bcom_price_ranking_mean.copy()
hb_bcom_price_ranking_mean['price_diff_%'] = (hb_bcom_price_ranking_mean['bcom_price_mean'] - hb_bcom_price_ranking_mean['hb_price_mean'] ) / hb_bcom_price_ranking_mean['hb_price_mean']
hb_bcom_price_ranking_mean

hb_bcom_price_ranking_mean.sort_values(by='price_diff_%',ascending=False, inplace=True)
hb_bcom_price_ranking_mean['price_diff_%'] = round(hb_bcom_price_ranking_mean['price_diff_%'].astype(float),2)

hb_bcom_price_ranking_mean = hb_bcom_price_ranking_mean[hb_bcom_price_ranking_mean['match'] > 85]
hb_bcom_price_ranking_mean = hb_bcom_price_ranking_mean[(hb_bcom_price_ranking_mean['price_diff_%'] > 0.2) &
                                                        (hb_bcom_price_ranking_mean['price_diff_%'] < 2)]
hb_bcom_price_ranking_mean
# %%
#################################### FILTERS ####################################
query = '''
    SELECT 
        hotelbeds_hotel_id as hb_hotel_id,
        code as hb_room_code,
        CONCAT(hotelbeds_hotel_id, code) as hb_hotel_room_id
    FROM public.calendar_app_calendarroomtype;
'''

hb_django_listed_rooms = pd.read_sql_query(query,engine_leap_ooredoo)
hb_django_listed_rooms
hb_bcom_price_ranking_mean_filter = hb_bcom_price_ranking_mean[~hb_bcom_price_ranking_mean['hb_hotel_room_id'].isin(hb_django_listed_rooms['hb_hotel_room_id'].to_list())]
hb_bcom_price_ranking_mean_filter

# %%
sql = f"""
    SELECT DISTINCT(CAST(hotel_id AS text)) AS hb_hotel_id, 
    hotel_type, country, region, city, district, 
	CAST(case when rating='' then '0' else rating end AS DECIMAL), 
	REPLACE(reviews,',','')::NUMERIC AS reviews
FROM bcom_static
WHERE rating <> '';
"""
bcom_static_no_price = pd.read_sql(sql=sql,con=engine_local_db_hb_price)
bcom_static_no_price
# %%
hb_bcom_filter = hb_bcom_price_ranking_mean_filter.merge(bcom_static_no_price)
hb_bcom_filter = hb_bcom_filter[hb_bcom_filter['rating'] > 7.5]
hb_bcom_filter
# %%
hb_bcom_filter.groupby('country')['match'].count().nlargest()
# %%
country_filter_list = ['All country houses', 'Italy', 'Portugal', 'Spain', 'France', 'Bulgaria']
hb_bcom_filter = hb_bcom_filter[hb_bcom_filter['country'].isin(country_filter_list)]
hb_bcom_filter

# %%
hb_bcom_filter = hb_bcom_filter[['hb_hotel_id', 'hb_room_code', 'bcom_room_code','hb_room_name', 'bcom_room_name',
                          'match', 'hb_price_mean', 'bcom_price_mean', 'price_diff_%']]
hb_bcom_filter
# %%
hb_bcom_filter.to_csv('~/Downloads/hb_bcom_EU.csv')
# %%
