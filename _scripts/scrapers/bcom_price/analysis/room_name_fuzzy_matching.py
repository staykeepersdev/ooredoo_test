from fuzzywuzzy import fuzz
import pandas as pd

import re
import spacy
from ftfy import fix_text

def fuzzywuzzy_get_ratio(row, hb_room_name_column, bcom_room_name_column):
    name1 = row[hb_room_name_column]
    name2 = row[bcom_room_name_column]
    return fuzz.token_set_ratio(name1, name2)

def numbers_to_words_df(df_col ):
    num_to_word_dict = {'1': 'one', '2': 'two', '3': 'three', '4': 'four'}
    for k, v in num_to_word_dict.items():
        df_col = str(df_col).lower().replace(k, v)
    return df_col

# def remove_stopwords(df, hb_room_name_column, bcom_room_name_column):
#     nlp = spacy.load('en_core_web_sm')
#     stop_words = nlp.Defaults.stop_words
#     df[hb_room_name_column] = df[hb_room_name_column].apply(lambda x: ' '.join([word for word in str(x).split() if word not in (stop_words)]))
#     df[bcom_room_name_column] = df[bcom_room_name_column].apply(lambda x: ' '.join([word for word in str(x).split() if word not in (stop_words)]))
#     return df

def sentence_tokenize_clean(sentence):
    sentence = re.split("\W+", sentence)
    sentence = ' '.join(sentence)
    sentence = fix_text(sentence)
    sentence = sentence.encode("ascii", errors="ignore").decode()
    sentence = sentence.lower()
    chars_to_remove = [")","(",".","|","[","]","{","}","'"]
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
    sentence = re.sub(rx, '', sentence)
    sentence = sentence.replace('&', 'and')
    sentence = sentence.replace(',', ' ')
    sentence = sentence.replace('-', ' ')
    sentence = sentence.title()
    sentence = re.sub(' +',' ',sentence).strip()
    sentence = re.sub(r'[,-./]|\sBD',r'', sentence)
    sentence = sentence.split(" ")
    return ' '.join(sentence)

def main_fuzzy_clean_matching(df, hb_room_name_column, bcom_room_name_column):
    df = df.copy()
    df[hb_room_name_column] = df[hb_room_name_column].apply(numbers_to_words_df)
    df[bcom_room_name_column] = df[bcom_room_name_column].apply(numbers_to_words_df)

    df = df.copy()
    # df = remove_stopwords(df, hb_room_name_column, bcom_room_name_column)

    df[hb_room_name_column] = df[hb_room_name_column].apply(sentence_tokenize_clean)
    df[bcom_room_name_column] = df[bcom_room_name_column].apply(sentence_tokenize_clean)

    df = df.copy()
    df['match'] = df.apply(fuzzywuzzy_get_ratio, axis=1, hb_room_name_column = hb_room_name_column, bcom_room_name_column = bcom_room_name_column)
    return df
    