# %%
import requests
from bs4 import BeautifulSoup
import re
import json
import csv
import os.path

import datetime
import time
import threading
import concurrent.futures

# %%
file_urls = open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/batch_5_6.txt', 'r')
hotel_url = file_urls.read().splitlines()
# hotel_url = hotel_url[:50]
len(hotel_url)
# %%
csv_output_path = '/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/batch_5_6.csv'
f = open(csv_output_path, "w")
writer = csv.DictWriter(f, fieldnames=['hotel_url', 'scrape_time','not_available',
                    'main_page', 'hotel_id', 'hotel_name', 'room_href', 'room_ids',
                    'room_name', 'hotel_outer_html'], delimiter='|')
writer.writeheader()
f.close()
# %%
start = time.perf_counter()

scrape_ls = []

def hotel_scrape(hotel_url: str):
    # print(hotel_url) # url print
    page = requests.get(hotel_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    today = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    not_available = soup.select('p[rel*="this_hotel_is_not_bookable"]')
    main_page = soup.findAll('h2', attrs={'class': 'bui-segment-header'})
    try:
        hotel_id = soup.find('input', attrs={'name':'hotel_id'})['value']
        hotel_name = soup.find('div', attrs={'class':'hp__hotel-title'}).h2.contents[2].splitlines()[1]
    except TypeError:
        hotel_id = ""
        hotel_name = ""

    s = soup.findAll('a', attrs={'href': re.compile("^#RD")})
    room_id = re.findall("href=[\"\'](.*?)[\"\']", str(s))
    room_name = re.findall("data-room-name-en=[\"\'](.*?)[\"\']", str(s))

    d = [hotel_url, 
        today, 
        str(not_available), 
        str(main_page),
        hotel_id, 
        hotel_name,
        str(s),
        str(room_id), 
        str(room_name),
        str(soup)
        ]    
    # print(d)

    with open (csv_output_path, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter='|', lineterminator='\n')
        writer.writerow(d)

    scrape_ls.append(today)
    print(len(scrape_ls))

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(hotel_scrape, hotel_url)

end = time.perf_counter()
print( round(end - start, 2))
# scrape_ls
# %%
import pandas as pd
df = pd.read_csv(csv_output_path, delimiter='|')
df

# %%
df.drop('hotel_outer_html', 1, inplace=True)

# %%
df.to_csv(r'~/Downloads/bcom_HB_ALL_5_6_local.csv')

# %%
def extract(col):
    soup = BeautifulSoup(col, 'html.parser')
    s = soup.findAll('a', attrs={'href': re.compile("^#RD")})
    room_id = [hotel_url['href'] for i in s]
    # room_name = [hotel_url['data-room-name-en'] for i in s]
    return room_id
    # soup.find('input', attrs={'name':'hotel_id'})['value']

df['hotel_outer_html'].apply(extract)

# %%

soup = BeautifulSoup(df.iloc[0,2], 'html.parser')
soup.extract("^#RD")

# %%

# %%
df.to_json(r'~/Downloads/bcom_HB_ALL_b7_local1.json')
# %%
f = str(df.iloc[1,8])
with open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/test.txt', 'w') as text_file:
    text_file.write(f)

# %%
soup = BeautifulSoup(f, 'html.parser')
# soup
# %%
soup.select('div[class*="bba-sr-destinations-container"]')

# %%
df1 = pd.read_json(r'/Users/miro/Downloads/bcom_HB_ALL_b7_local.json')
df1
# %%
