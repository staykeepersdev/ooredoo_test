# %%
import requests
from bs4 import BeautifulSoup
import re
import json

import datetime
import time
import threading
import concurrent.futures

# %%
file_urls = open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/batch_1_2.txt', 'r')
hotel_url = file_urls.read().splitlines()
# hotel_url = hotel_url[:5]
len(hotel_url)
# %%
start = time.perf_counter()

scrape_ls = []

def hotel_scrape(hotel_url: str):
    # print(hotel_url) # url print
    page = requests.get(hotel_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    today = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    not_available = soup.select('p[rel*="this_hotel_is_not_bookable"]')
    main_page = soup.select('div[class*="filtercategory icon_filtercategory_container"]')
    # try:
    #     hotel_id = soup.find('input', attrs={'name':'hotel_id'})['value']
    #     hotel_name = soup.find('div', attrs={'class':'hp__hotel-title'}).h2.contents[2].splitlines()[1]
    # except TypeError:
    #     hotel_id = ""
    #     hotel_name = ""

    s = soup.findAll('a', attrs={'href': re.compile("^#RD")})
    # room_id = [hotel_url['href'] for i in s]
    # room_name = [hotel_url['data-room-name-en'] for i in s]

    d = {
        'hotel_url': hotel_url,
        'scrape_time': today,
        'not_available': not_available,
        'main_page': main_page,
        # 'hotel_id': hotel_id,
        # 'hotel_name': hotel_name,
        'room_href': str(s),
        # 'room_ids': room_id,
        # 'room_name': room_name,
        'hotel_outer_html': str(soup)
        }
    # print(d)
    with open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/batch_1_2_result.json', 'a', encoding='utf-8') as text_file:
        json.dump(d, text_file, ensure_ascii=True, indent=4 )
        text_file.write(', \n')
    scrape_ls.append(today)
    print(len(scrape_ls))

#    with open(csv_output_path, 'a', encoding='utf-8') as text_file:
#        json.dump(d, text_file, ensure_ascii=True, indent=4 )
#        text_file.write(', \n')

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(hotel_scrape, hotel_url)

end = time.perf_counter()
print( round(end - start, 2))

# %%
import pandas as pd

df = pd.DataFrame(scrape_ls)
df
# %%
df.to_json(r'~/Downloads/bcom_HB_ALL_b7_local_1_2.json')

# %%
def extract(col):
    soup = BeautifulSoup(col, 'html.parser')
    s = soup.findAll('a', attrs={'href': re.compile("^#RD")})
    room_id = [hotel_url['href'] for i in s]
    # room_name = [hotel_url['data-room-name-en'] for i in s]
    return room_id
    # soup.find('input', attrs={'name':'hotel_id'})['value']

df['hotel_outer_html'].apply(extract)

# %%

soup = BeautifulSoup(df.iloc[0,2], 'html.parser')
soup.extract("^#RD")

# %%

# %%
f = str(df.iloc[1,8])
with open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/test.txt', 'w') as text_file:
    text_file.write(f)

# %%
soup = BeautifulSoup(f, 'html.parser')
# soup
# %%
soup.select('div[class*="bba-sr-destinations-container"]')

# %%
df1 = pd.read_json(r'/Users/miro/Downloads/bcom_HB_ALL_b7_local.json')
df1
# %%
