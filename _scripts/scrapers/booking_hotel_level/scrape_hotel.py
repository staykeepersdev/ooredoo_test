# %%
import time
import pandas as pd
import glob, os
import json
import re
import csv
from ast import literal_eval
import concurrent.futures

import requests
from bs4 import BeautifulSoup
pd.set_option('display.max_colwidth', 500)
# %%
df = pd.concat(map(pd.read_csv, glob.glob(os.path.join(r"/Users/miro/Downloads/hote_level_all", "*.csv"))))
df.reset_index(inplace=True)

# %%
df['hotel_url'] = df['hotel_url'].str.split(".", n=3, expand=True).loc[:,:2].agg('.'.join, axis=1) + '.en-gb.html'
df
# %%
df['room_ids'] = df['room_ids'].apply(literal_eval)
df = df.explode('room_ids').reset_index()
df
# %%
df['room_ids'] = df['room_ids'].str.extract(r'(\d+)')

df['room_url'] = df['hotel_url'] + '#room_' + df['room_ids']
df
# %%
# room url level
df1 = df.dropna(subset=['room_ids'] )
df1 = df1['room_url']
df1
# %%
# hotel url level
room_url = df.dropna(subset=['room_ids'] )
room_url = room_url.drop_duplicates(subset=['hotel_id'] )
room_url = room_url['room_url'].reset_index()
room_url = room_url['room_url']
room_url
# %%
# df.to_csv(r'~/Downloads/room_url1.csv')

# %%
room_url = room_url[:15000]
# room_url
# %%
csv_output_path = '/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/bcom_url_images.csv'
f = open(csv_output_path, "w")
writer = csv.DictWriter(f, fieldnames=['hotel_id','associated_rooms', 'id', 'orientation', 'created', 'highres_url',
    'thumb_url','large_url','caption_right','thumb_contains',
    'thumb_className','contains','className','verticalAlign'], delimiter=',')
writer.writeheader()
f.close()

# %%
start = time.perf_counter()
count_ls = []
# for n, i in enumerate(test_url):
def room_scrape(room_url: str):
    # print(i)
    page = requests.get(room_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    s = str(soup)

    hotel_id = re.search(r"b_hotel_id: \'(.+)\'", s).group(1)

    j = re.findall(r"(?<=hotelPhotos: \[)[\S\s]*?(?=],)", s)
    j = ''.join(j)

    j = "[" + j + "]"
    j = re.sub(r"'", '"', j)
    j = re.sub(r"(\w.+): ", r'"\1":', j)
    j = re.sub(r"(class=).+", r'\1",', j)
    j = re.sub(r"(caption_left).+", r'\1": ""', j)
    j = re.sub(r"(caption_right).+", r'\1": ""', j)
    # print(j)
    t = json.loads(j)

    t = pd.json_normalize(t)
    t['hotel_id'] = hotel_id
    t = t.reindex(columns=['hotel_id','associated_rooms', 'id', 'orientation', 'created', 'highres_url',
    'thumb_url','large_url','caption_right','thumb_contains',
    'thumb_className','contains','className','verticalAlign'] )
    # print(t)
    with open(csv_output_path, 'a', newline='') as csvfile:
        t.to_csv(csvfile, header=False)
    
    count_ls.append(1)
    print(len(count_ls))

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(room_scrape, room_url)

end = time.perf_counter()
print( round(end - start, 2))

# %%

# %%
######################### Load all hotel urls
room_df = pd.concat(map(pd.read_csv, glob.glob(os.path.join(r"/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_hotels/bcom_image_urls", "*.csv") )))
room_df.reset_index(inplace=True)

# %%
room_df['associated_rooms'] = room_df['associated_rooms'].fillna('0')
room_df['associated_rooms'] = room_df['associated_rooms'].apply(literal_eval)
# %%
room_df = room_df.explode('associated_rooms').reset_index()
room_df

# %%
room_df.to_csv('~/Downloads/bcom_room_urls.csv')
# %%

# %%
################################# TEST #################################
page = requests.get(room_url[1])
soup = BeautifulSoup(page.content, 'html.parser')
s = str(soup)

hotel_id = re.search(r"b_hotel_id: \'(.+)\'", s).group(1)

j = re.findall(r"(?<=hotelPhotos: \[)[\S\s]*?(?=],)", s)
j = ''.join(j)
j

# b_hotel_id: \'(.+)\'
# %%
j = "[" + j + "]"
j = re.sub(r"'", '"', j)
j = re.sub(r"(\w.+): ", r'"\1":', j)
j = re.sub(r"(class=).+", r'\1",', j)
j = re.sub(r"(caption_left).+", r'\1": ""', j)
j = re.sub(r"(caption_right).+", r'\1": ""', j)
print(j)

# %%
# t = json.dumps(j)
t = json.loads(j)
t
# %%
t = pd.json_normalize(t)
t
# %%
t['hotel_id'] = hotel_id
t
# %%
t = t.reindex(columns=['hotel_id','associated_rooms', 'id', 'orientation', 'created', 'highres_url',
    'thumb_url','large_url','caption_right','thumb_contains',
    'thumb_className','contains','className','verticalAlign'] )
t

# %%
with open(csv_output_path, 'a', newline='') as csvfile:
    t.to_csv(csvfile, header=False)
    
# %%

# %%
