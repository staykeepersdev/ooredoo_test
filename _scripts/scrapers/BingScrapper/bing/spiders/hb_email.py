import scrapy
import re
from urllib.parse import unquote, quote
# from hotelbeds_data.scripts.hb_price_db.BingScrapper.hotel_names_cities import HOTELS

import pandas as pd

class BingScraperSpider(scrapy.Spider):
    name = 'hb_email'
    df = pd.read_csv('/home/miroslav/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/hb_price_db/BingScrapper/bing/spiders/hb_email_unique.csv')
    hb_emails_list = df['email_unique'].to_list()
    start_urls = [f'https://bing.com/search?q="{hb_email}' for hb_email in hb_emails_list]
    counter = 0
    def parse(self, response):
        self.counter +=1
        if self.counter % 100 == 0:
            print('Checked #', self.counter)
            print('Checked #', self.counter)
            print('Checked #', self.counter)
        all_links = response.css('.b_algo h2')

        searched_query = unquote(response.request.url.split('littlehotelier.com%2Fproperties+')[1])
        for link in all_links:
            href = link.css('a::attr(href)').get()
            name = ''.join(link.css('a ::text').extract()).strip()

            with open('bing_hb_email_unique.csv', 'a+') as file:
                file.write(f'{searched_query};{name};{href}\n')

# https://www.bing.com/search?q=encantodeitapoan