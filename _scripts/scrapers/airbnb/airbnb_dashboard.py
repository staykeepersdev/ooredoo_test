# %%
# import os
# project_dir = 'ooredoo'
# os.chdir(os.path.join(os.path.dirname(os.getcwd()).split(project_dir, 1)[0],project_dir))
import streamlit as st

import pandas as pd
import numpy as np
# %%
st.title('Test')
# %%
path = '/home/miroslav/Dev/wwpt_projects/ooredoo/_scripts/scrapers/airbnb/static/active_units_leap_merge.csv'
active_listings_leap_df = pd.read_csv(path, delimiter=',')
st.write(active_listings_leap_df)
# %%
map_data = active_listings_leap_df[['lat', 'lng']]
map_data.columns = ['lat', 'lon']
st.map(map_data)
# %%

# %%
st.write("Here's our first attempt at using data to create a table:")
st.write(pd.DataFrame({
    'first column': [1, 2, 3, 4],
    'second column': [10, 20, 30, 40]
}))
# %%
chart_data = pd.DataFrame(
     np.random.randn(20, 3),
     columns=['a', 'b', 'c'])

st.line_chart(chart_data)
# %%
map_data = pd.DataFrame(
    np.random.randn(1000, 2) / [50, 50] + [37.76, -122.4],
    columns=['lat', 'lon'])

st.map(map_data)
# %%
if st.checkbox('Show dataframe'):
    chart_data = pd.DataFrame(
       np.random.randn(20, 3),
       columns=['a', 'b', 'c'])

    chart_data
# %%
import matplotlib as plt

active_listings_leap_df['airbnb_id'].plot.line(x='airbnb_id')
st.pyplot()
# %%
left_column, right_column = st.beta_columns(2)
pressed = left_column.button('Press me?')
if pressed:
    right_column.write("Woohoo!")

expander = st.beta_expander("FAQ")
expander.write("Here you could put in some really, really long explanations...")