import scrapy
from scrapy.http.request import Request

import os, re, time, sys, django
from datetime import datetime

import pandas as pd

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.scrapers.airbnb.airbnb_active_ranking import df


class AirbnbRankingSpider(scrapy.Spider):
    name = 'airbnb_ranking'
    allowed_domains = ['airbnb.com']

    request_counter = 0
    bing_searches_total = len(df)

    def start_requests(self):
        self.request_counter +=1
        print (f'Checked #{self.request_counter} out of {self.bing_searches_total}.')
    
        for index, row in df.iterrows():
            df_index = index
            airbnb_search_id = row['airbnb_id']
            start_urls = row['airbnb_location_url']
            yield scrapy.Request(url=start_urls, 
                                    meta={'airbnb_search_id': airbnb_search_id,
                                        'df_index':df_index},
                                        dont_filter=True)

    def parse(self, response):
        # time.sleep(1)
        scrape_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        df_index = response.meta.get('df_index')

        airbnb_search_id = response.meta.get('airbnb_search_id')
        current_page_number = response.xpath('//*[@aria-label="Search results pagination"]/button/text()').extract_first()
        next_page_url = response.xpath('//*[@aria-label="Search results pagination"]/button/following-sibling::a/@href').extract_first()
        total_listings = response.xpath('//*[@class="_1snxcqc"]/text()').extract_first()
        total_listings = re.search('\d+',total_listings).group()

        fs = '/following-sibling::span'
        airbnb_listings = response.xpath('//div[@itemprop = "itemListElement"]')
        for listing in airbnb_listings:
            
            room_id = listing.xpath('.//*[@class="_8s3ctt"]/a/@target').re('([0-9].+)')[0]
            
            guests = listing.xpath('.//*[@class="_kqh46o"]/span/text()').extract_first()
            bedroom_type = listing.xpath(f'.//*[@class="_kqh46o"]/span{fs}{fs}/text()').extract_first()
            beds =listing.xpath(f'.//*[@class="_kqh46o"]/span{fs}{fs}{fs}{fs}/text()').extract_first()
            bathrooms = listing.xpath(f'.//*[@class="_kqh46o"]/span{fs}{fs}{fs}{fs}{fs}{fs}/text()').extract_first()

            rating = listing.xpath('.//*[@class="_10fy1f8"]/text()').extract_first()
            reviews = listing.xpath('.//*[@class="_1hxyyw3"]/span/@aria-label').extract_first() #.re(';( [0-9].+?)')[0]
            
            try:
                ranking_position_page = listing.xpath('.//*[@itemprop="position"]/@content').extract_first()
                ranking_position = int(ranking_position_page) + (int(current_page_number) * 20 - 20)
            except:
                ranking_position = listing.xpath('.//*[@itemprop="position"]/@content').extract_first()

            if str(airbnb_search_id) == str(room_id):
                df_scraped = pd.DataFrame({
                    'scrape_time':scrape_time,
                    'df_index':df_index,
                    'room_id':room_id,
                    'ranking_position':ranking_position,
                    'ranking_position_page':ranking_position_page,
                    'current_page_number':[current_page_number],
                    # 'guests':guests,
                    # 'bedroom_type':bedroom_type,
                    # 'beds':beds,
                    # 'bathrooms':bathrooms,
                    'rating':rating,
                    'reviews':reviews,
                    'total_listings':total_listings
                    # 'next_page_url':next_page_url
                })
                df_scraped.to_csv('/home/miroslav/Dev/wwpt_projects/ooredoo/_scripts/scrapers/airbnb/crawled/airbnb_ranking1.csv',
                                    mode='a', header=False, index=None, sep='|')
            else:
                pass
        
        
            
        if next_page_url:
            base_url = 'https://www.airbnb.com'
            absolute_next_page_url = base_url + next_page_url
            yield scrapy.Request(absolute_next_page_url, 
                                meta={'airbnb_search_id': airbnb_search_id,
                                    'df_index':df_index})                
        else:
            pass

    
    # def push_to_gbq(self):
    #     df_scraped = pd.read_csv('_scripts/scrapers/airbnb/crawled/airbnb_ranking.csv', delimiter='|')
    #     df_scraped.to_gbq(destination_table="leap.airbnb_ranking",
    #                             project_id = "xero-245302",
    #                             if_exists="replace")


