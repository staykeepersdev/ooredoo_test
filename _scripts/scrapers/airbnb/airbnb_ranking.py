# %%
import pandas as pd
import numpy as np

import os
project_dir = 'ooredoo'
os.chdir(os.path.join(os.path.dirname(os.getcwd()).split(project_dir, 1)[0],project_dir))

from _scripts.sql_alchemy_settings.leap_ooredoo import engine

# %%
q = '''
    SELECT 
	hostify_id,
	hostify_internal_code,
	hostify_child_id,
	request_data -> 'location_params' -> 'lat' as lat,
	request_data -> 'location_params' -> 'lng' as lng,

	request_data -> 'layout_params' -> 'bathrooms' as bathrooms,
	request_data -> 'layout_params' -> 'person_capacity' as person_capacity,

	request_data -> 'location_params' -> 'listing_type' as listing_type,
	request_data -> 'location_params' -> 'property_type' as property_type,
	request_data -> 'location_params' -> 'property_type_group' as property_type_group,

	request_data -> 'booking_restrictions_params' -> 'occupancy' as occupancy,
	request_data -> 'booking_restrictions_params' -> 'min_notice_hours' as min_notice_hours
	
    FROM public.calendar_app_calendarroom
    '''

calendar_room_df = pd.read_sql_query(q,engine)
calendar_room_df
# %%
import pandas as pd
path = '_scripts/scrapers/airbnb/static/active_leap_listings.csv'
active_listings_leap_df = pd.read_csv(path, delimiter=';')
active_listings_leap_df
# %%
calendar_room_df.dropna(subset=['hostify_child_id'], inplace=True)
calendar_room_df['hostify_child_id'] = calendar_room_df['hostify_child_id'].astype('int')
active_units_leap_merge = active_listings_leap_df.merge(calendar_room_df, on='hostify_child_id')
active_units_leap_merge
# %%
active_units_leap_merge.groupby(['min_notice_hours']).size()
# active_units_leap_merge.groupby(['listing_type','property_type']).size()
# active_units_leap_merge = active_units_leap_merge[['hostify_parent_id','hostify_child_id','airbnb_id','listing_type','lat','lng']]
# active_units_leap_merge
# %%
def find_max_min_coordinates(row):
	degrees = 0.005 # 0.005 (busy cities) AND 0.009 1km AND 0.045 5km
	lat_center = row['lat']
	long_center = row['lng']
	sw_lat = lat_center - degrees # min
	ne_lat = lat_center + degrees # max
	sw_lng = long_center - (degrees / np.cos(lat_center*np.pi/180) ) # min
	ne_lng = long_center + (degrees / np.cos(lat_center*np.pi/180) ) # max
	# print(sw_lat, sw_lng, ne_lat, ne_lng)
	return f'https://www.airbnb.com/s/homes?ne_lat={ne_lat}&ne_lng={ne_lng}&sw_lat={sw_lat}&sw_lng={sw_lng}'

# %%
active_units_leap_merge_ = active_units_leap_merge.copy()
active_units_leap_merge['airbnb_location_url'] = active_units_leap_merge.apply(find_max_min_coordinates, axis=1)
active_units_leap_merge
# %%
active_units_leap_merge.groupby('listing_type').size()
# %%
active_units_leap_merge.drop_duplicates(subset=['lat'])
# %%
active_units_leap_merge.to_csv('_scripts/scrapers/airbnb/static/active_units_leap_merge.csv')
# %%
pd.read_csv('_scripts/scrapers/airbnb/crawled/airbnb_ranking.csv', header=None).head(60)
# %%
# %%
'''
1. To scrape all districts from all existing listings
2. Create pagination search
3. then filter unique districts and set up district search to verify listing ranking

To create a circle or rectangular for location coordinates

- total number of properties in the location

- average reviews in the search
- average number of reviews in the area
- total number of photos
- superhost status
- type of beds, bedroom, bathroom and create comps set
'''
# %%

# %%
