# %%
import pandas as pd
import numpy as np

import os, sys, django
# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.leap_ooredoo import engine_leap_ooredoo

def find_max_min_coordinates(row):
	degrees = 0.005 # 0.005 (busy cities) AND 0.009 1km AND 0.045 5km
	lat_center = row['lat']
	long_center = row['lng']
	sw_lat = lat_center - degrees # min
	ne_lat = lat_center + degrees # max
	sw_lng = long_center - (degrees / np.cos(lat_center*np.pi/180) ) # min
	ne_lng = long_center + (degrees / np.cos(lat_center*np.pi/180) ) # max
	# print(sw_lat, sw_lng, ne_lat, ne_lng)
	return f'https://www.airbnb.com/s/homes?ne_lat={ne_lat}&ne_lng={ne_lng}&sw_lat={sw_lat}&sw_lng={sw_lng}'

# %%
def active_listings():
    q = '''
        SELECT 
        hostify_id AS hostify_parent_id,
        hostify_internal_code,
        hostify_child_id,
        channel_listing_id as airbnb_id,
        request_data -> 'location_params' -> 'lat' as lat,
        request_data -> 'location_params' -> 'lng' as lng,

        request_data -> 'layout_params' -> 'bathrooms' as bathrooms,
        request_data -> 'layout_params' -> 'person_capacity' as person_capacity,

        request_data -> 'location_params' ->> 'listing_type' as listing_type,
        request_data -> 'location_params' ->> 'property_type' as property_type,
        request_data -> 'location_params' ->> 'property_type_group' as property_type_group,

        request_data -> 'booking_restrictions_params' -> 'occupancy' as occupancy,
        request_data -> 'booking_restrictions_params' -> 'min_notice_hours' as min_notice_hours

        FROM public.calendar_app_calendarroom
        WHERE auto_pricing = 'true'
        '''

    calendar_room_df = pd.read_sql_query(q,engine_leap_ooredoo)

    active_listings_df = calendar_room_df.copy()
    active_listings_df['airbnb_location_url'] = active_listings_df.apply(find_max_min_coordinates, axis=1)

    active_listings_df = active_listings_df[['hostify_parent_id', 'hostify_child_id','airbnb_id','lat','lng','airbnb_location_url']]
    return active_listings_df
# %%
df = active_listings()
print(df)
# %%
