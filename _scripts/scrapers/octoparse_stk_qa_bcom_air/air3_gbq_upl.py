import json
import django
import os
import sys
import datetime
import math
import pandas as pd

# to review to delete them
import time
import hashlib
import requests

# check
from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Import Octo task ids
import octoparse.scripts.ab0_ids as auto_ids

########################################
# Auth Octoparse login
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)



########################################
# Scraper data import in BigQuery

# set up Google log-in
import csv
from google.oauth2 import service_account

import pygsheets

# import authentication from Google service account json stored on local
g_sheet = pygsheets.authorize(service_account_file='/home/miro/ooredoo/certificates/xero-245302-dd5fc26b4f42.json')





####################################### 2 LOS #######################################
# Scraper task to download all items in each page

r_d = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_2_LOS, 1000)

n = 1000
total = r_d['data']['total']
# print(total)
pages = math.ceil(total / n)
# print(pages)

scraped_data = []

for i in range(pages):
        i = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_2_LOS, 1000)
        # print(i)

        task_update = OctoClient.update_data_status(auto_ids.auto_STK_Air_2_LOS)
        # print(task_update)

        item = i['data']['dataList']
        # print(item)
        scraped_data.append(item)

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
scraped_data_ls = [val for sublist in scraped_data for val in sublist]

scrape_data_df = pd.DataFrame(scraped_data_ls)
# print(scrape_data_df)


########################################
# Scraper data import in BigQuery

# Upload Octo task data into BigQuery table
scrape_data_df.to_gbq(destination_table="octoparse.stk_airbnb_price",
                                        project_id = "xero-245302",
                                        if_exists="append")





####################################### 5 LOS #######################################
# Scraper task to download all items in each page

r_d = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_5_LOS, 1000)

n = 1000
total = r_d['data']['total']
# print(total)
pages = math.ceil(total / n)
# print(pages)

scraped_data = []

for i in range(pages):
        i = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_5_LOS, 1000)
        # print(i)

        task_update = OctoClient.update_data_status(auto_ids.auto_STK_Air_5_LOS)
        # print(task_update)

        item = i['data']['dataList']
        # print(item)
        scraped_data.append(item)

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
scraped_data_ls = [val for sublist in scraped_data for val in sublist]

scrape_data_df = pd.DataFrame(scraped_data_ls)
# print(scrape_data_df)


########################################
# Scraper data import in BigQuery

# Upload Octo task data into BigQuery table
scrape_data_df.to_gbq(destination_table="octoparse.stk_airbnb_price",
                                        project_id = "xero-245302",
                                        if_exists="append")





####################################### 10 LOS #######################################
# Scraper task to download all items in each page

r_d = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_10_LOS, 1000)

n = 1000
total = r_d['data']['total']
# print(total)
pages = math.ceil(total / n)
# print(pages)

scraped_data = []

for i in range(pages):
        i = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_10_LOS, 1000)
        # print(i)

        task_update = OctoClient.update_data_status(auto_ids.auto_STK_Air_10_LOS)
        # print(task_update)

        item = i['data']['dataList']
        # print(item)
        scraped_data.append(item)

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
scraped_data_ls = [val for sublist in scraped_data for val in sublist]

scrape_data_df = pd.DataFrame(scraped_data_ls)
# print(scrape_data_df)


########################################
# Scraper data import in BigQuery

# Upload Octo task data into BigQuery table
scrape_data_df.to_gbq(destination_table="octoparse.stk_airbnb_price",
                                        project_id = "xero-245302",
                                        if_exists="append")





####################################### 28 LOS #######################################
# Scraper task to download all items in each page

r_d = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_28_LOS, 1000)

n = 1000
total = r_d['data']['total']
# print(total)
pages = math.ceil(total / n)
# print(pages)

scraped_data = []

for i in range(pages):
        i = OctoClient.export_non_exported_data(auto_ids.auto_STK_Air_28_LOS, 1000)
        # print(i)

        task_update = OctoClient.update_data_status(auto_ids.auto_STK_Air_28_LOS)
        # print(task_update)

        item = i['data']['dataList']
        # print(item)
        scraped_data.append(item)

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
scraped_data_ls = [val for sublist in scraped_data for val in sublist]

scrape_data_df = pd.DataFrame(scraped_data_ls)
# print(scrape_data_df)


########################################
# Scraper data import in BigQuery

# Upload Octo task data into BigQuery table
scrape_data_df.to_gbq(destination_table="octoparse.stk_airbnb_price",
                                        project_id = "xero-245302",
                                        if_exists="append")




