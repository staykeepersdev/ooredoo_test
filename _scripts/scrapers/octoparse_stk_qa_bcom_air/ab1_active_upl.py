import django
import os
import sys
import math

import pandas as pd
import numpy as np

from django.utils import timezone

# set up Google log-in
import csv
from google.oauth2 import service_account
import pygsheets

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()


########################################
# Auth Octoparse login
from octoparse.models import Airbnb_Listing, Bcom_Listing
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)



#######################################
# To add Booking.com active url listings and to add to Postgres

# import authentication from Google service account json stored on local
bcom_g_sheet = pygsheets.authorize(service_account_file='/home/miro/ooredoo/certificates/xero-245302-dd5fc26b4f42.json' )

# automate url link
bcom_spreadsheet_url = \
"https://docs.google.com/spreadsheets/d/1UAUDoNzJTlHVU_UK5B8J8lq0mag_ZYcJaXqSpt9TJMQ/edit#gid=503799513"
bcom_test = bcom_spreadsheet_url.split('/d/')
bcom_id_ = bcom_test[1:][0].split('/edit')[0]
bcom_sheet = bcom_g_sheet.open_by_key(bcom_id_)

# select worksheet from Google sheet
bcom_wks = bcom_sheet.worksheet_by_title('Bcom_list')

# create a dataframe
bcom_df2 = bcom_wks.get_as_df()

# to remove empty column from Google sheet import
bcom_df2.drop(bcom_df2.columns[[4]], axis=1, inplace=True)

bcom_df2.replace("", np.nan, inplace=True)
bcom_df2.dropna(subset = ["bcom_url"], inplace=True)
bcom_df_ls = bcom_df2['bcom_url'].tolist()

# print(bcom_df_ls)

for row in bcom_df_ls:
        bcom_dataList = Bcom_Listing.objects.update_or_create(
                        bcom_id = row
                        )
        # print(row)





#######################################
# Download all active airbnb urls from Octo task (scraped)

# airbnb_listing_status = "cd2b1d87-a83f-4841-7ddd-423414bed9dc"
# airbnb_active_ids = OctoClient.get_data_by_offset(airbnb_listing_status,0,1000)
# # print(airbnb_active_ids)

# n = 1000
# total = airbnb_active_ids['data']['total']
# pages = math.ceil(total / n)
# # print(pages)

# scraped_data_1 = []

# for i in range(pages):
#         LIMIT = i * 1000
#         # print(LIMIT)
#         r = OctoClient.get_data_by_offset(airbnb_listing_status, LIMIT, n)
#         dataList = r['data']['dataList']
#         # print(i)
#         for item in dataList:
#                 # print(item)
#                 scraped_data_1.append(item)

# active_unq = pd.DataFrame(scraped_data_1)

# df_unique = active_unq[active_unq.Page_URL != 'https://www.airbnb.co.uk/s/homes']

# df_duplicate = df_unique.drop_duplicates(subset='Page_URL')

# df_regx = df_duplicate['airbnb_id']=df_duplicate['Page_URL'].str.extract(r'(?<=rooms/)(.*?)(?=[?])')

# df_ls = df_regx[0].tolist()
# # print(df_ls)


# # to record records in Postgres
# for i in df_ls:
#         dataList = Airbnb_Listing.objects.update_or_create(
#                         airbnb_id = i
#                         )
#         # print(i)



#######################################
# Download all airbnb urls from Google Sheet (raw)

# import authentication from Google service account json stored on local
g_sheet = pygsheets.authorize(service_account_file='/home/miro/ooredoo/certificates/xero-245302-dd5fc26b4f42.json')

# automate url link
spreadsheet_url = \
"https://docs.google.com/spreadsheets/d/1UAUDoNzJTlHVU_UK5B8J8lq0mag_ZYcJaXqSpt9TJMQ/edit#gid=503799513"
test = spreadsheet_url.split('/d/')
id_ = test[1:][0].split('/edit')[0]
sheet = g_sheet.open_by_key(id_)

# select worksheet from Google sheet
wks = sheet.worksheet_by_title('Air_list')

# create a dataframe
df2 = wks.get_as_df()

# filter only active and missing properties from ID_Register
df4 = df2[(df2.status == "Active") | (df2.status == "Missing")]

df3 = df4[['airbnb_id_active']]

airbnb_ids = df3.astype(int)

airbnb_ids_ls = airbnb_ids.values.flatten()

# to record records in Postgres
for i in airbnb_ids_ls:
        dataList = Airbnb_Listing.objects.update_or_create(
                        airbnb_id = i
                        )
        # print(i)

########################################
# To upload all Airbnb listings to Scraper to filter only active Airbnb listings

# fmt = 'https://www.airbnb.co.uk/rooms/{id:}'

# active_ls = []

# for i, k in airbnb_ids.iterrows():
#         id = k['airbnb_id_active']
#         url = fmt.format(id=id)
#         # print(url)
#         active_ls.append(url)


# # Update Octo task with Airbnb listings excluding inactive listings
airbnb_listing_status = "cd2b1d87-a83f-4841-7ddd-423414bed9dc"
# airbnb_active_ids = OctoClient.update_task_parameters(airbnb_listing_status, "RQBzajKh.UrlList", active_ls)
# # print(airbnb_active_ids)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(airbnb_listing_status)
print(start_task)


# # Clear the Octoparse task data
# clear_task_data = OctoClient.clear_data(airbnb_listing_status)



