# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
# set up Google log-in
import sys
import csv
from google.oauth2 import service_account
import pygsheets

import pandas as pd
import numpy as np


# %%
# import authentication from Google service account json stored on local
bcom_g_sheet = pygsheets.authorize(service_account_file='/Users/miro/Dev/certifications/xero-245302-dd5fc26b4f42.json')

# automate url link
bcom_spreadsheet_url = \
"https://docs.google.com/spreadsheets/d/1UAUDoNzJTlHVU_UK5B8J8lq0mag_ZYcJaXqSpt9TJMQ/edit#gid=503799513"
bcom_test = bcom_spreadsheet_url.split('/d/')
bcom_id_ = bcom_test[1:][0].split('/edit')[0]
bcom_sheet = bcom_g_sheet.open_by_key(bcom_id_)

# select worksheet from Google sheet
bcom_wks = bcom_sheet.worksheet_by_title('Bcom_list')

# create a dataframe
bcom_df2 = bcom_wks.get_as_df()

# %%
# to remove empty column from Google sheet import
bcom_df2.drop(bcom_df2.columns[[4]], axis=1, inplace=True)
print(bcom_df2)


# %%
bcom_df2.replace("", np.nan, inplace=True)
bcom_df2.dropna(subset = ["bcom_url"], inplace=True)
bcom_df_ls = bcom_df2['bcom_url'].to_list()
bcom_df_ls


# %%
df_new = pd.DataFrame (bcom_df_ls, columns=['bcom_url'])
df_new

# %%
### upload data into Google sheet

sheet1 = bcom_g_sheet.open_by_key('1UAUDoNzJTlHVU_UK5B8J8lq0mag_ZYcJaXqSpt9TJMQ')

# select worksheet from Google sheet
wks1 = sheet1.worksheet_by_title('test')

# Push "merge" data into a new Google sheet file

wks1.set_dataframe(df_new, start=(1,1), extend=True)

# %%

sys.exit()
df.to_gbq(destination_table="octoparse.test1",
                                        project_id = "xero-245302",
                                        if_exists="append")


