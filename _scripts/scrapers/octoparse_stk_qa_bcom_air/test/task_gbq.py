import json
import django
import os
import sys
import hashlib
import requests
import time
import datetime
import math

import pandas as pd

from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append('/Users/miro/Dev/staykeepersdev-ooredoo-ea9577a7e0dd/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Import Octo task ids
import octoparse.scripts.ab0_ids as auto_ids

from octoparse.models import Scrape
from octoparse.client import OctoClient

client = OctoClient (advanced_api=True)

task_scrape = "5e205935-7487-4a72-893e-3587e669bad8"

token_renew = client.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
client.set_access_token(token_new)

r_d = client.get_data_by_offset(auto_ids.auto_STK_bcom, 0, 1000)
print(r_d)
sys.exit() # STOP HERE

n = 1000
total = r_d['data']['total']
pages = math.ceil(total / n)
print(pages)

for i in range(1):
        LIMIT = i * 1000
        print(LIMIT)
        r = client.get_data_by_offset(auto_ids.auto_STK_bcom, LIMIT, n)
        dataList = r['data']['dataList']
        for item in dataList:
                item = i['data']['dataList']
                
                # create Dataframe to import to gbq
                scrape_data_df = pd.DataFrame(item)
                print(scrape_data_df)
                
                # import df to gbq
                scrape_data_df.to_gbq(destination_table="octoparse.stk_bcom_price",
                                                project_id = "xero-245302",
                                                if_exists="append")
                # print(item)
# Scrape task will offset until it downloads all task in a loop
