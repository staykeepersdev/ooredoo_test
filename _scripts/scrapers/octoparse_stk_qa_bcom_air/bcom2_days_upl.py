import json
import django
import os
import sys
import datetime
import math
import pandas as pd

# to review to delete them
import time
import hashlib
import requests

# check
from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Import Octo task ids
import octoparse.scripts.ab0_ids as auto_ids

########################################
# Auth Octoparse login
from octoparse.models import Bcom_Listing
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)



########################################
# To pull data from Postgres and to prepare in datasets

bcom_ids = Bcom_Listing.objects.values_list('bcom_id', flat=True)
print(bcom_ids)



####################################### 1,2,3,5,10,28 LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [1,2,3,5,10,28]
advance_days = 90

fmt = '{id:}?;checkin={date1:%Y-%m-%d};checkout={date2:%Y-%m-%d};group_adults=1'
                
base = datetime.datetime.today()
date_list = [base + datetime.timedelta(days=x) for x in range(advance_days)]
# print(date_list)

bcom_id_lst = []

for i in bcom_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        bcom_id_lst.append(url)


########################################
# Upload URL list to Octoparse text list                        

# Extract specific Octoparse task in json format

URL_lst = OctoClient.update_task_parameters(auto_ids.auto_STK_bcom,
                                                "PSRWshxx.UrlList",bcom_id_lst)
# print(URL_lst)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(auto_ids.auto_STK_bcom)
# print(start_task)




