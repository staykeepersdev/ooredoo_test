import json
import django
import os
import sys
import datetime
import math
import pandas as pd

# to review to delete them
import time
import hashlib
import requests

# check
from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Import Octo task ids
import octoparse.scripts.ab0_ids as auto_ids

########################################
# Auth Octoparse login
from octoparse.models import Airbnb_Listing
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)



########################################
# To pull data from Postgres and to prepare in datasets

airbnb_ids = Airbnb_Listing.objects.values_list('airbnb_id', flat=True)
# print(airbnb_ids)



########################################
# Octo tasks variables - task ids, advance_days and url formatting

advance_days = 30

base = datetime.date.today()

fmt = 'https://www.airbnb.co.uk/rooms/{id:}/?check_in={date1:%Y-%m-%d}&check_out={date2:%Y-%m-%d}&guests=1&adults=1'

date_list = [base + datetime.timedelta(days=x) for x in range(advance_days)]
# print(date_list)



####################################### 2 LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [2]

airbnb_id_lst = []

for i in airbnb_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        airbnb_id_lst.append(url)



########################################
# Upload URL list to Octoparse text list                        

# Extract specific Octoparse task in json format

URL_lst = OctoClient.update_task_parameters(auto_ids.auto_STK_Air_2_LOS,
                                                "PSRWshxx.UrlList",airbnb_id_lst)
# print(URL_lst)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(auto_ids.auto_STK_Air_2_LOS)
# print(start_task)





####################################### 5 LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [5]

airbnb_id_lst = []

for i in airbnb_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        airbnb_id_lst.append(url)



########################################
# Upload URL list to Octoparse text list                        

# Extract specific Octoparse task in json format

URL_lst = OctoClient.update_task_parameters(auto_ids.auto_STK_Air_5_LOS,
                                                "PSRWshxx.UrlList",airbnb_id_lst)
# print(URL_lst)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(auto_ids.auto_STK_Air_5_LOS)
# print(start_task)





####################################### 10 LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [10]

airbnb_id_lst = []

for i in airbnb_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        airbnb_id_lst.append(url)



########################################
# Upload URL list to Octoparse text list                        

# Extract specific Octoparse task in json format

URL_lst = OctoClient.update_task_parameters(auto_ids.auto_STK_Air_10_LOS,"PSRWshxx.UrlList",airbnb_id_lst)
# print(URL_lst)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(auto_ids.auto_STK_Air_10_LOS)
# print(start_task)





####################################### 28 LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [28]

airbnb_id_lst = []

for i in airbnb_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        airbnb_id_lst.append(url)



########################################
# Upload URL list to Octoparse text list                        

# Extract specific Octoparse task in json format

URL_lst = OctoClient.update_task_parameters(auto_ids.auto_STK_Air_28_LOS,"PSRWshxx.UrlList",airbnb_id_lst)
# print(URL_lst)

# start the Octoparse task to scrape
start_task = OctoClient.start_task(auto_ids.auto_STK_Air_28_LOS)
# print(start_task)