# %%
################################
import numpy as np
import pandas as pd

# batch LIVE listings Gospodin
bcom_price = pd.read_csv(r"/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/hotel_room_database_table/bcom_hotel_room_url.csv")
# %%
bcom_price['hotel_id'] = bcom_price['hotel_id'].astype(float).fillna(0).astype(int)
bcom_price['room_ids'] = bcom_price['room_ids'].astype(float).fillna(0).astype(int)
bcom_price
# %%
bcom_price['hote_id_count'] = bcom_price.groupby('hotel_id').cumcount()+1
bcom_price
# %%
bcom_price['room_name'] = bcom_price['room_name'].apply(lambda x: x[1:-1].split(','))
bcom_price
# %%
bcom_price_explode = bcom_price.explode('room_name')
bcom_price_explode['room_name_count'] = bcom_price_explode.groupby('room_ids').cumcount()+1
bcom_price_explode

# %%
bcom_price_explode['matches'] = bcom_price_explode['hote_id_count'] == bcom_price_explode['room_name_count']
bcom_price_explode = bcom_price_explode[bcom_price_explode['matches'] == True]
bcom_price_explode
# %%
bcom_price_explode['room_name'] = bcom_price_explode['room_name'].str.replace("'", '')
bcom_price_explode
# %%

# ['name','hotel_code','rooms.code','hb_room_type','b_hotel_name','b_hotel_id','b_room_id',
# 'bcom_room_type','Page_URL','hb_price','bcom_price','hb_price_rank','bcom_price_rank','match',
# 'price_diff_%','check_in','check_out','Current_Time','hb_scraped_time']

# %%
bcom_price_explode['hotel_room_id'] = bcom_price_explode['hotel_id'] + bcom_price_explode['room_ids']
bcom_price_explode.drop_duplicates(subset=['hotel_room_id'],inplace=True)
# %%
bcom_price_explode = bcom_price_explode[['hotel_name', 'hotel_id', 'room_ids', 'room_name', 'hotel_url', 'hotel_room_id']]
bcom_price_explode.columns = ['b_hotel_name','b_hotel_id','b_room_id', 'bcom_room_type','Page_URL', 'b_hotel_room_id']
bcom_price_explode
# %%
######################################################################## HB matching
hb_bcom_matching = pd.read_csv('/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/hotel_room_database_table/hb_bcom_matching.csv')
hb_bcom_matching
# %%
hb_bcom_matching['b_hotel_id'] = hb_bcom_matching['b_hotel_id'].astype(float).fillna(0).astype(int)
hb_bcom_matching['b_room_id'] = hb_bcom_matching['b_room_id'].astype(float).fillna(0).astype(int)
hb_bcom_matching
# %%
hb_bcom_matching['b_hotel_room_id'] = hb_bcom_matching['b_hotel_id'] + hb_bcom_matching['b_room_id']
# filter max unique room pair match between booking.com and hotelbeds
hb_bcom_matching = hb_bcom_matching.sort_values(by='match', ascending=False).drop_duplicates('b_hotel_room_id', keep='first')
# hb_bcom_matching.drop(columns=['b_hotel_room_id'], inplace=True)
hb_bcom_matching
# %%
hb_bcom_merge_right = bcom_price_explode.merge(hb_bcom_matching, 
                        on=['b_hotel_id','b_room_id','Page_URL'],
                        how='left')
hb_bcom_merge_right
hb_bcom_merge_right.info()
# %%
hb_bcom_merge_right = hb_bcom_merge_right.rename(columns={'b_hotel_name_x': 'b_hotel_name', 'bcom_room_type_x': 'bcom_room_type'})
hb_bcom_merge_right = hb_bcom_merge_right[['name','hotel_code','rooms.code','hb_room_type','b_hotel_name','b_hotel_id','b_room_id',
'bcom_room_type','Page_URL','hb_price','bcom_price','hb_price_rank','bcom_price_rank','match',
'price_diff_%','check_in','check_out','Current_Time','hb_scraped_time']]
hb_bcom_merge_right
hb_bcom_merge_right.info()
# %%
hb_bcom_merge_right = hb_bcom_merge_right[hb_bcom_merge_right['b_hotel_id'] != 0]
# %%
hb_bcom_merge_right.to_csv('~/Downloads/hb_bcom_matching_ALL_without_hb_info.csv')

# %%

# %%
