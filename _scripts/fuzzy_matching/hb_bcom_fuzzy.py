# %%
import numpy as np
import pandas as pd

import glob, os

pd.set_option('display.max_colwidth', 200)
# %%
# batch LIVE listings Gospodin
bcom_price = pd.concat(map(pd.read_csv, glob.glob(os.path.join(r"/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/_25_26_Jan_batch/bcom_price" , "*.csv"))))
bcom_price.reset_index(inplace=True)
bcom_price
# %%
hb_price= pd.read_csv('/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/_25_26_Jan_batch/hb_price.csv')
hb_bcom_hotel_unique = pd.read_excel('/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/_25_26_Jan_batch/hb_bcom_hotel_unique.xlsx')

# %%
# HB price clean
hb_price['hb_hotel_room_id'] = hb_price['code'].astype(str) + '|' + hb_price['rooms.code'] + ';'
hb_price.rename(columns={'rooms.name': 'hb_room_type', 'rates.net': 'hb_price', 'code': 'hotel_code', 'scraped_time': 'hb_scraped_time'}, inplace=True)
# hb_price['hb_price'] = hb_price['rates.net'].str.extract(r'(\d+.\d+)').astype(float) # GBP separate in a custom code

# cumulative count on hotel roomtypes from lowest to expensive price
hb_price.sort_values(by='hb_price', ascending=True, inplace=True)

hb_price.drop_duplicates(subset=['hb_hotel_room_id'], inplace=True) # remove duplicates
hb_price.reset_index(inplace=True)

hb_price['hb_price_rank'] = hb_price.groupby('hotel_code').cumcount()
hb_price
# %%
# Bcom price clean
# bcom_price['Page_URL'] = bcom_price['Field1_Link'].str.split('html', expand=True)[0] + 'html'
bcom_price['Page_URL'] = [".".join(x.split(".")[0:3]) for x in bcom_price['Page_URL'].astype(str)]
bcom_price.replace(r"\n+", "|", regex=True, inplace=True)

bcom_price['bcom_room_type'] = bcom_price['Text'].str.replace(r'|',"")
# bcom_price['b_hotel_id'] = bcom_price['b_hotel_id'].astype(int) # it is empty in the last Octo scraper

bcom_price['bcom_hotel_room_id'] = bcom_price['Page_URL'] + " - " + bcom_price['bcom_room_type'] # only URL us unique

bcom_price['Text2'] = bcom_price['Text2'].str.replace(r".", "").str.replace(r",", "")
bcom_price['bcom_price'] = bcom_price['Text2'].str.extract(r'(\d+)').astype(float)

# cumulative count on hotel roomtypes from lowest to expensive price
bcom_price.sort_values(by='bcom_price', ascending=True, inplace=True)

bcom_price.drop_duplicates(subset=['bcom_hotel_room_id'], inplace=True)
bcom_price.reset_index(inplace=True)

bcom_price['bcom_price_rank'] = bcom_price.groupby('Page_URL').cumcount() # only URL us unique

bcom_price['b_room_id'] = bcom_price['Field1_Link'].str.extract(r'#RD([0-9]*.)')
bcom_price

# %%
hb_bcom_hotel_unique['booking_link'] = [".".join(x.split(".")[0:3]) for x in hb_bcom_hotel_unique['booking_link'].astype(str)]
hb_bcom_hotel_unique = hb_bcom_hotel_unique[hb_bcom_hotel_unique['hb_hotel_id'].notnull()]
hb_bcom_hotel_unique['hb_hotel_id'] = hb_bcom_hotel_unique['hb_hotel_id'].astype(int)
hb_bcom_hotel_unique
# %%
bcom_merge = pd.merge(bcom_price, hb_bcom_hotel_unique, left_on='Page_URL', right_on='booking_link', how='outer')
bcom_merge
# %%

# %%
hb_bcom_room_outer = pd.merge(bcom_merge, hb_price, left_on='hb_hotel_id', right_on='hotel_code',how='outer')
hb_bcom_room_outer

# %%
hb_bcom_room_outer1 = hb_bcom_room_outer[['name', 'hotel_code', 'rooms.code', 'hb_room_type', 'hb_hotel_room_id',
                         'b_hotel_name','b_hotel_id', 'b_room_id' ,'bcom_room_type', 'bcom_hotel_room_id', 'Page_URL', 'hb_price',
                         'bcom_price', 'hb_price_rank', 'bcom_price_rank', 
                         'check_in', 'check_out', 'Current_Time', 'hb_scraped_time']]
hb_bcom_room_outer1
# %%
hb_bcom_room_outer1[['check_in', 'check_out','Current_Time', 'hb_scraped_time']] = \
                hb_bcom_room_outer1[['check_in', 'check_out','Current_Time', 'hb_scraped_time']].apply(pd.to_datetime, errors='coerce')
# %%
################################################################ Clean & Transform data for matching
# Convert digit numbers to word numbers

def Numbers_To_Words_df(df_col ):
  num_to_word_dict = {'1': 'one', '2': 'two', '3': 'three', '4': 'four'}
  for k, v in num_to_word_dict.items():
      df_col = str(df_col).lower().replace(k, v)
  return df_col

# %%
hb_bcom_room_outer1['hb_room_type'] = hb_bcom_room_outer1['hb_room_type'].apply(Numbers_To_Words_df)
hb_bcom_room_outer1['bcom_room_type'] = hb_bcom_room_outer1['bcom_room_type'].apply(Numbers_To_Words_df)

# %%
from nltk.corpus import stopwords
# remove stopwords

stop = stopwords.words('english')
hb_bcom_room_outer1['hb_room_type'] = hb_bcom_room_outer1['hb_room_type'].apply(lambda x: ' '.join([word for word in str(x).split() if word not in (stop)]))
hb_bcom_room_outer1['bcom_room_type'] = hb_bcom_room_outer1['bcom_room_type'].apply(lambda x: ' '.join([word for word in str(x).split() if word not in (stop)]))

# %%
import string
import re
from ftfy import fix_text
import nltk
#adjust text normalizing per row

def ngrams(string ): # , n=3 / , n='...?'
    tokenizer = nltk.RegexpTokenizer(r"\w+")
    string = tokenizer.tokenize(string)
    string = ' '.join(string) # 
    string = fix_text(string) # fix text encoding issues
    string = string.encode("ascii", errors="ignore").decode() #remove non ascii chars
    string = string.lower() #make lower case
    chars_to_remove = [")","(",".","|","[","]","{","}","'"]
    rx = '[' + re.escape(''.join(chars_to_remove)) + ']'
    string = re.sub(rx, '', string) #remove the list of chars defined above
    string = string.replace('&', 'and')
    string = string.replace(',', ' ')
    string = string.replace('-', ' ')
    string = string.title() # normalise case - capital at start of each word
    string = re.sub(' +',' ',string).strip() # get rid of multiple spaces and replace with a single space
    # string = ' '+ string +' ' # pad names for ngrams...
    string = re.sub(r'[,-./]|\sBD',r'', string)

    string = string.split(" ")
    
    # ls = [re.findall(f'{n}',j) for i in range(1) for j in string]
    # flat_list = [item for sublist in ls for item in sublist]
    # ngrams = zip(*[string[i:] for i in range(n)])
    # return [''.join(ngram) for ngram in ngrams]
    
    return ' '.join(string)
    # return string

ngrams('Deluxe with;"c in Room, 1 King |BED!') # test
# %%
hb_bcom_room_outer1['hb_room_type'] = hb_bcom_room_outer1['hb_room_type'].apply(ngrams)
hb_bcom_room_outer1['bcom_room_type'] = hb_bcom_room_outer1['bcom_room_type'].apply(ngrams)

# %%
################################################################ Fuzzy Matching
from fuzzywuzzy import fuzz

def get_ratio(row):
    name1 = row['hb_room_type']
    name2 = row['bcom_room_type']
    return fuzz.token_set_ratio(name1, name2)

hb_bcom_room_outer1['match'] = hb_bcom_room_outer1.apply(get_ratio, axis=1)

# %%
################################################################ Hotelbeds Filter
hb_bcom_room_outer1[['match', 'hb_room_type', 'bcom_room_type', 'hotel_code' ]]

# filter max unique room pair match between booking.com and hotelbeds
hb_bcom_room_outer2 = hb_bcom_room_outer1.sort_values(by='match', ascending=False).drop_duplicates('hb_hotel_room_id', keep='first')
hb_bcom_room_outer2

# %%

# hb_bcom_room_outer1[hb_bcom_room_outer1['hotel_code'] == 368322][['match', 'hb_room_type', 'bcom_room_type', 'hotel_code' ]]

# %%
hb_bcom_room_outer2['price_diff_%'] = (hb_bcom_room_outer2['bcom_price'] - hb_bcom_room_outer2['hb_price'] ) / hb_bcom_room_outer2['hb_price']
# %%
hb_bcom_room_outer2.sort_values(by='price_diff_%',ascending=False, inplace=True)
hb_bcom_room_outer2['price_diff_%'] = round(hb_bcom_room_outer2['price_diff_%'].astype(float),2)
hb_bcom_room_outer2

# %%
room_types_allowed = ['APT', 'BED', 'DBL', 'DBT', 'DUS', 'FAM', 'HOM',
                        'JSU', 'QUA', 'ROO', 'SGL', 'STU', 'SUI', 'TPL', 'TWN']
hb_bcom_room_outer2 = hb_bcom_room_outer2[hb_bcom_room_outer2['rooms.code'].str.rsplit('.').str[0]\
                        .str.contains('|'.join(room_types_allowed))]
# hb_bcom_room_outer2
# %%
# reorganise the columns
hb_bcom_room_outer2 = hb_bcom_room_outer2[['name', 'hotel_code', 'rooms.code', 'hb_room_type', 'hb_hotel_room_id', 
                        'b_hotel_name', 'b_hotel_id', 'b_room_id', 'bcom_room_type', 'bcom_hotel_room_id', 'Page_URL', 
                        'hb_price', 'bcom_price', 'hb_price_rank', 'bcom_price_rank', 'match', 'price_diff_%',
                        'check_in', 'check_out', 'Current_Time', 'hb_scraped_time']]

# %%
hb_bcom_room_outer2['Page_URL'] = hb_bcom_room_outer2['Page_URL'] + '.en-gb.html'
hb_bcom_room_outer2
# %%
################################## to sort from high to low match and to filter unique b
# %%
hb_bcom_room_outer2.to_csv(r'~/Downloads/hb_bcom_room_outer_diff1.csv')

# %%
################################################################ Bcom Filter
bcom_room_outer2 = hb_bcom_room_outer1
bcom_room_outer2['b_hotel_id'] = bcom_room_outer2['b_hotel_id'].astype(str)
bcom_room_outer2['b_hotel_room_id'] = bcom_room_outer2['b_hotel_id'] + bcom_room_outer2['b_room_id']
bcom_room_outer2
# %%

# filter max unique room pair match between booking.com and hotelbeds
bcom_room_outer2 = bcom_room_outer2.sort_values(by='match', ascending=False).drop_duplicates('b_hotel_room_id', keep='first')
bcom_room_outer2
# %%
# %%
# %%
# %%

"""
# %%
from sklearn.model_selection import train_test_split
# %%
X_train, X_test, y_train, y_test = train_test_split(X, y)
# %%
# Train the classifier
svm = rl.SVMClassifier()
svm.fit(room_pairs_train, room_matches_index)
# %%
# Predict the match status for all record pairs
result_svm = svm.predict(df_ml_X)

len(result_svm)
# %%

rl.confusion_matrix(pairs1_test, result_svm, len(df_ml_X))
# %%

# The F-score for this classification is
rl.fscore(pairs1_test, result_svm)
# %%


# %%
# Expedia room name vectorizasition
from sklearn.feature_extraction.text import CountVectorizer
# %%
bow_transformer = CountVectorizer().fit(df1['Expedia'])
dc = bow_transformer.vocabulary_ # dict with bag of words
# Print total number of vocab words
print(bow_transformer.vocabulary_)
print(len(bow_transformer.vocabulary_))

# %%
df_row = df1['Expedia'][2]
df_row

# %%
bow4 = bow_transformer.transform([df_row])
print(bow4)
print(bow4.shape)
# %%
print(bow_transformer.get_feature_names()[341])
# %%
messages_bow = bow_transformer.transform(df1['Expedia'])
# %%
print('Shape of Sparse Matrix: ', messages_bow.shape)
print('Amount of Non-Zero occurences: ', messages_bow.nnz)
# %%
from sklearn.feature_extraction.text import TfidfTransformer

tfidf_transformer = TfidfTransformer().fit(messages_bow)
tfidf4 = tfidf_transformer.transform(bow4)
print(tfidf4)
# %%
print(tfidf_transformer.idf_[bow_transformer.vocabulary_['u']])
# %%
messages_tfidf = tfidf_transformer.transform(messages_bow)
print(messages_tfidf.shape)
# %%

# %%
from sklearn.naive_bayes import MultinomialNB
spam_detect_model = MultinomialNB().fit(messages_tfidf, df1['Booking.com'])
# %%
print('predicted:', spam_detect_model.predict(tfidf4)[0])
print('expected:', df1['Booking.com'][1])
# %%
all_predictions = spam_detect_model.predict(messages_tfidf)
print(all_predictions)
# %%

from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
vec1 = np.array([[1,1,0,1,1]])
vec2 = np.array([[1,1,0,1,0]])
#print(cosine_similarity([vec1, vec2]))
print(cosine_similarity(vec1, vec2))

"""
# %%
