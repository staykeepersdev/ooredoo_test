# %%
import glob, os

import pandas as pd
import numpy as np

pd.set_option('display.max_colwidth', 200)
# %%
################################ scrape_2
path = r'/Volumes/GoogleDrive/Shared drives/Dashboards (KPIs)/POWER BI/Leap_HotelBeds_&_Airbnb/KN_hb_MATCHING_bing_URL/hb_bcom_matching/bcom_room_url_scrape/1-8-2'
all_files = glob.glob(os.path.join(path, "*.csv"))

df_from_each_file = (pd.read_csv(f,error_bad_lines=False) for f in all_files)
scrape_2 = pd.concat(df_from_each_file, ignore_index=True)
scrape_2
# %%
scrape_2['room_id'] = scrape_2['Page_URL'].str.extract(r'room_([0-9]*.)')
scrape_2
# %%
scrape_2['room_id'] = scrape_2['room_id'].astype(str)
scrape_2['Field_count'] = scrape_2['Field'].str.len()
# %%
scrape_2_len = scrape_2[scrape_2['Field_count'] > 2000 ]
scrape_2_len.reset_index(inplace=True)
scrape_2_len
# %%
scrape_2_len = scrape_2_len[['Page_URL', 'Field', 'room_id']]
scrape_2_len
# %%
# scrape_2_len.to_csv('~/Downloads/scrape_2_len.csv')

# %%
################################ scrape_1
scrape_1 = pd.read_csv('/Users/miro/Downloads/bcom_room_html.csv')

# %%
scrape_1['room_id'] = scrape_1['room_id'].astype(str)
scrape_1['Field_count'] = scrape_1['Field'].str.len()
# %%
scrape_1_len = scrape_1[scrape_1['Field_count'] > 2000 ]
scrape_1_len.reset_index(inplace=True)
scrape_1_len
# %%
################################ bcom_room_html_all
bcom_room_html_all = pd.concat([scrape_1_len, scrape_2_len])
bcom_room_html_all.drop_duplicates(subset=['room_id'],inplace=True)
bcom_room_html_all.reset_index(inplace=True)
bcom_room_html_all
# %%
bcom_room_html_all = bcom_room_html_all[['Page_URL', 'Field', 'room_id']]
bcom_room_html_all.info()
# %%
bcom_room_html_all
# %%
################################ hb_bcom_matching_ALL_without_hb_info
hb_bcom_matching_ALL_without_hb_info = pd.read_csv('/Users/miro/Downloads/hb_bcom_matching_ALL_without_hb_info.csv')
hb_bcom_matching_ALL_without_hb_info
# %%
hb_bcom_matching_ALL_without_hb_info = hb_bcom_matching_ALL_without_hb_info[[
       'Page_URL', 'b_room_id'
       ]]
# %%
hb_bcom_matching_ALL_without_hb_info['b_room_id'] = hb_bcom_matching_ALL_without_hb_info['b_room_id'].astype(str)
hb_bcom_matching_ALL_without_hb_info['Page_URL'] = hb_bcom_matching_ALL_without_hb_info['Page_URL'] + '#room_' + hb_bcom_matching_ALL_without_hb_info['b_room_id']
hb_bcom_matching_ALL_without_hb_info
# %%
################################ bcom_room_without_content
bcom_room_without_content = hb_bcom_matching_ALL_without_hb_info.merge(bcom_room_html_all, left_on=['Page_URL', 'b_room_id'], right_on=['Page_URL', 'room_id'], how='left')
bcom_room_without_content.info()
# %%
bcom_room_without_content = bcom_room_without_content[bcom_room_without_content['Field'].isna()].reset_index()
bcom_room_without_content
# %%
# bcom_room_without_content.to_csv('~/Downloads/bcom_room_without_content.csv')
# %%
################################ bcom_room_without_content for not scraped urls
bcom_room_without_content = pd.read_csv('/Users/miro/Downloads/bcom_room_without_content.csv')
# %%
bcom_room_without_content = bcom_room_without_content[['Page_URL', 'b_room_id']]
bcom_room_without_content.reset_index(inplace=True)
# %%
bcom_room_without_content['b_room_id'] = bcom_room_without_content['b_room_id'].astype(str)
bcom_room_without_content
# %%

# %%
################################ scrape 3
path = r'/Users/miro/Downloads/bcom_scrape_3'
all_files = glob.glob(os.path.join(path, "*.csv"))

df_from_each_file_3 = (pd.read_csv(f,error_bad_lines=False) for f in all_files)
scrape_3 = pd.concat(df_from_each_file_3, ignore_index=True)
scrape_3
# %%
scrape_3['b_room_id'] = scrape_3['Page_URL'].str.extract(r'room_([0-9]*.)').astype(str)
scrape_3
# %%
# scrape_3['Field_count'] = scrape_3['Field'].str.len()
scrape_3 = scrape_3[ scrape_3['Field'].str.len() > 2000 ]
scrape_3.reset_index(inplace=True)
scrape_3
# %%
scrape_3 = scrape_3[['Page_URL', 'Field', 'b_room_id']]
scrape_3.rename(columns={'b_room_id': 'room_id'}, inplace=True)
scrape_3
# %%
# scrape_3.to_csv('~/Downloads/scrape_3.csv')
# %%
scrape_3_merge = bcom_room_without_content.merge(scrape_3, on='b_room_id', how='left')
scrape_3_merge
# %%
bcom_room_without_content_3 = scrape_3_merge[scrape_3_merge['Field'].isnull()]
bcom_room_without_content_3
# %%
# bcom_room_without_content_3.to_csv('~/Downloads/bcom_room_without_content_3.csv')
# %%
################################ 
"""
    to filter for errors:
    1) hotel not available
    2) Room not existing anymore with this url
    3) Rooms with a lot of photos (delay?)
    4) to remove search URL from
"""
# %%
################################ scrape 4
path = r'/Users/miro/Downloads/bcom_scrape_4'
all_files = glob.glob(os.path.join(path, "*.csv"))

df_from_each_file_4 = (pd.read_csv(f,error_bad_lines=False) for f in all_files)
scrape_4 = pd.concat(df_from_each_file_4, ignore_index=True)
scrape_4
# %%
scrape_4['b_room_id'] = scrape_4['Page_URL'].str.extract(r'room_([0-9]*.)').astype(str)
scrape_4
# %%

# %%
# %%
# scrape_4['Field_count'] = scrape_4['Field'].str.len()
scrape_4 = scrape_4[ scrape_4['Field'].str.len() > 2000 ]
scrape_4.reset_index(inplace=True)
scrape_4
# %%
scrape_4 = scrape_4[['Page_URL', 'Field', 'b_room_id']]
scrape_4.rename(columns={'b_room_id': 'room_id'}, inplace=True)
scrape_4.drop_duplicates(subset=['room_id'], inplace=True)
scrape_4
# %%
# scrape_4.to_csv('~/Downloads/scrape_4.csv')

# %%

# %%

# %%
# %%
# %%
# %%
# %%
total = np.arange(0,10)
n = 5
c = -1
total_ls = []
for i in total:
    c += 1
    if c == n:
        c = 0 
    else:
        c
    total_ls.append(c)
total_ls
# %%
pd.DataFrame(total_ls)
# %%
np.array(total_ls)
# %%
# %%
# %%
################################ Restructure Booking.com url
df = pd.read_csv('~/Downloads/bcom_room_html1.csv')
df
# %%
df['room_id'] = df['room_id'].astype(str)
# %%
df['Page_URL_en_gb'] = df['Page_URL'].str.extract(r'(en-gb)')
df
# %%
df_en_gb = df[df['Page_URL_en_gb'].notna()]
df_en_gb

# %%
df_en_gb['Page_URL'] = [".".join(x.split(".")[0:3]) for x in df_en_gb['Page_URL'].astype(str)]
df_en_gb
# %%
df_en_gb['Page_URL'] = df_en_gb['Page_URL'] + '.en-gb.html#room_' + df_en_gb['room_id'].astype(str)
df_en_gb.reset_index(inplace=True)
df_en_gb
# %%
df_en_gb = df_en_gb[['Page_URL', 'Field', 'room_id', ]]
df_en_gb

# %%
# df_en_gb.to_csv('~/Downloads/bcom_room_html.csv')

# %%

# %%
# %%
# %%
t = df['b_hotel_outer_body'][0]
# %%
# df['b_hotel_outer_body'].str.extract(r'')

# %%
# extract room ids
from bs4 import BeautifulSoup
soup = BeautifulSoup(t, 'html.parser')
# room_id = [a['data-room-id'] for a in soup.select('a[data-room-id]')]
room_href = [a['href'] for a in soup.select('a[href]')]
room_href = [k for k in room_href if 'RD' in k]
room_href
# %%
t = soup.findAll('a', attrs={'class':'jqrt togglelink'})
# %%
t['class']
# %%

# %%

# %%
d = df.iloc[:1,:]
# %%
d['new'] = pd.Series(room_href)
d
# %%
pd.concat([d,pd.Series(room_href)], axis=1)
# %%
################################ extract room images
import codecs
f = codecs.open('/Users/miro/Dev/wwpt_projects/ooredoo/hotelbeds_data/scripts/bcom_rooms/room_34609411.html')
soup = BeautifulSoup(f, 'html.parser')
soup

# %%
for t in soup.select('img[class*=""]'): #filter img with "empty" class
    # soup.select['data-room-id']
    print(t)
    print(t['src'])
# %%
soup.find['div', 'data-room-id']
# %%
# soup.findAll('div', attrs={'data-room-id'=''})
# %%
soup.select('div[data-room-id*=""]')
# %%
soup.select('input[class*="feedback-loop__view-feedback"]')['value']
# %%
