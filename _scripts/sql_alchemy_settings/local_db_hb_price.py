import sys, os, django
from sqlalchemy import create_engine

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# from django.conf import settings

# set up sqlAlchemy database access
user = 'postgres'
password = '04079'
database_name = 'hb_price'
host = 'localhost'
port = 5432

database_url = f'postgresql://{user}:{password}@{host}:{port}/{database_name}'

engine = create_engine(database_url, echo=False)

engine_local_db_hb_price = engine

# connection = engine.connect()

# import pandas as pd
# df = pd.DataFrame({'Test': [100]})
# print(df)
# # push to PostgreSQL
# df.to_sql('test1243', con=engine, if_exists='replace', 
#             method='multi')