import sys, os, django
from sqlalchemy import create_engine

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.conf import settings

# set up sqlAlchemy database access
user = settings.DATABASES['default']['USER']
password = settings.DATABASES['default']['PASSWORD']
database_name = settings.DATABASES['default']['NAME']
host = settings.DATABASES['default']['HOST']
port = 5432

database_url = f'postgresql://{user}:{password}@{host}:{port}/{database_name}'

engine = create_engine(database_url, echo=False)

engine_leap_m_ooredoo = engine