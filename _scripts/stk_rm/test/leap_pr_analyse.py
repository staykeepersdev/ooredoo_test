# %%
import sys, os, django

import pandas as pd
import numpy as np
import plotly.express as px

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from ooredoo.sql_alchemy_settings import database_url, engine

# %%
query ='''SELECT * FROM hb_hf_price'''
df_sql = pd.read_sql(query, con=engine)
df_sql

# %%

analyse = df_sql.groupby('hostify_id')['price'].agg(['max', 'min', 'mean', 'std', 'var', 'count', 'unique'])
analyse
# analyse.to_csv(r'~/Downloads/analyse.csv')

# %%
t = df_sql.query("date > '12/10/2020'") # 31947 hostify_id == 31947 and 
t
# t.plot(x='start_date', y='price', c= "red", marker='.', linestyle='-' ,figsize=(12,6))

# px.scatter(t, x='start_date', y='price' )#.update_traces(mode='lines+markers')

# %%
px.parallel_coordinates(t, color='price' )

# %%

px.scatter(t, x='date_hour', y='price', color='start_date', trendline='ols', marginal_x='violin', marginal_y='box')

# %%
analyse.to_csv(r'~/Downloads/analyse_.csv')
# %%
t = t.sort_values(by='date_hour')
t['date_hour'] = t['date_hour'].astype(str)
px.scatter(t, x='date', y='price', size_max=60, animation_frame='date_hour' )


# %%

# %%
#convert columns to datetime
# t['index1'] = t.index
# df5 = pd.melt(t[['index1', 'date_diff', 'start_date','end_date']], id_vars=['index1', 'date_diff'], value_name='date_exp')
# df5
# # %%%
# df5['date_exp'] = pd.to_datetime(df5['date_exp'])
# df5.set_index('date_exp', inplace=True)
# df5.drop('variable', axis=1, inplace=True)
# df5
# %%


