# %%
import re
import pandas as pd
import numpy as np
import pathlib

import sys, os, django

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from ooredoo.sql_alchemy_settings import engine

# %%
directory = '/Users/miro/Dev/wwpt_projects/ooredoo/ooredoo_price_logs/hb_price_logs_updated/text_files'
#directory = '/Users/miro/Dev/wwpt_projects/ooredoo/ooredoo_price_logs/hb_price_logs_updated/test1'

sample = []

for path in pathlib.Path(directory).iterdir():
    with open(path) as myfile:
        for line in myfile.readlines():
                sample.append(line)


df = pd.DataFrame(sample, columns=['data'])
# df = pd.json(sample)
df

# %%
df = df[df['data'] != '\n']

df['date_hour'] = "{'date_hour':'" + df['data'].astype(str).str[:19] + "',"
df['date'] = "'date':'" + df['data'].astype(str).str[:10] + "',"

df['hostify_id'] = "'hostify_id':" + df['data'].str.extract("(\d+)\s{'calendar'") + ","
df['calendar'] = df['data'].str.extract("(?='calendar':)(.*?)(?=\n)")

df['calendar'] = df['calendar'].astype(str)
df['calendar'] = df['calendar'].replace('nan',np.nan)
df = df.dropna()

df

# %%
df['json'] = df['date_hour'] + df['date'] + df['hostify_id'] + df['calendar']

df['json'] = df['json'].map(eval)
df

# %%
j = df['json']

df_unnest = pd.json_normalize(j, 'calendar', ['hostify_id', 'date_hour', 'date'])
df_unnest

# %%
# format all columns for analysis
# df_unnest.dropna()
df_unnest['id'] = df_unnest['date_hour'] + '_' + df_unnest['hostify_id'].astype(str)

# %%
df_unnest['start_date'] = pd.to_datetime(df_unnest['start_date'])
df_unnest['end_date'] = pd.to_datetime(df_unnest['end_date'])
df_unnest['date_hour'] = pd.to_datetime(df_unnest['date_hour'])
df_unnest['date'] = pd.to_datetime(df_unnest['date'])
df_unnest['date_hour'] = pd.to_datetime(df_unnest['date_hour'])
df_unnest['hostify_id'] = df_unnest['hostify_id'].astype(int)


# %%
# Create a calendar table in SQL
cal = np.arange(np.datetime64('2020-01-01'), np.datetime64('2022-01-01'))
df_cal = pd.DataFrame(cal, columns=['date'])

df_cal.to_sql('cal', con=engine, if_exists='replace', method='multi')
# %%
# Import tables in PostgreSQL for CROSS JOIN
df_unnest.to_sql('df_a', con=engine, if_exists='replace', index=False, method='multi', chunksize=10000)

# %%
query ='''SELECT * FROM df_a
            JOIN cal c ON c.date between df_a.start_date and df_a.end_date'''
df_sql = pd.read_sql(query, con=engine)
df_sql

# %%
# solve for duplicate date column
df_sql.columns = ['start_date', 'end_date', 'is_available', 'price', 'hostify_id', 'date_hour', 'date', 'id', 'index', 'date1']
df_sql

# %%
df_sql.to_sql('hb_hf_price', con=engine, if_exists='replace', index=False, method='multi', chunksize=10000)

# %%
from google.oauth2 import service_account
import pygsheets
bcom_g_sheet = pygsheets.authorize(service_account_file='/Users/miro/Dev/certifications/xero-245302-dd5fc26b4f42.json')

df_sql.to_gbq(destination_table="octoparse.hb_hf_price",
                                        project_id = "xero-245302",
                                        if_exists="replace",
                                        progress_bar=True)

# %%


# %%


# %%

