
# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import django
import os
import sys
import math
from datetime import datetime

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from django.utils import timezone

# set up Google log-in
import csv
from google.oauth2 import service_account
import pygsheets

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('Users/miro/Dev/staykeepersdev-ooredoo-ea9577a7e0dd', 1)[0],
        'Users/miro/Dev/staykeepersdev-ooredoo-ea9577a7e0dd'    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()


# %%
### Outline script logic below

from hostify_api.calendar_api_client import CalendarAPIClient

HF_cal = CalendarAPIClient()

# variables
listing_id = [15460, 75509]
start_date = datetime(2020,1,1)
end_date = datetime(2020,12,1)
per_page = 2000

cal_price_all = []

for i in listing_id:
    cal_price = HF_cal.get_calendar_day(
        params = {
            'per_page': per_page,
            'listing_id': i,
            'start_date':  start_date.strftime("%Y-%m-%d"),
            'end_date': end_date.strftime("%Y-%m-%d")
        })
    # print(cal_price['calendar'])

    cal_price_all.append(cal_price['calendar'])


# flatten list
flat_list = [item for sublist in cal_price_all for item in sublist]
# print(flat_list)

pd = pd.DataFrame(flat_list)
print(pd)


import numpy as np
import matplotlib.pyplot as plt

# evenly sampled time at 200ms intervals
t = np.arange(0., 5., 0.2)

# red dashes, blue squares and green triangles
plt.plot(t, t, 'r--', t, t**2, 'bs', t, t**3, 'g^')


# %%





# %%

print("test")


# %%

# %%
