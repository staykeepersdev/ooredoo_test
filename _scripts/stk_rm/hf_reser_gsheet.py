import sys, os, django
import math

import pandas as pd
import numpy as np

# set up Google log-in
from google.oauth2 import service_account
import pygsheets

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hostify_api.reservations_api_client import ReservationsApiClient
res_get = ReservationsApiClient()

from hostify_leap_api.reservations_api_client import ReservationsApiClient
res_leap_get = ReservationsApiClient()

# import authentication from Google service account json stored on local
g_account_file = os.path.dirname(__file__).split('ooredoo',1)[0] + 'ooredoo/_scripts/auth_certificates/xero-245302-dd5fc26b4f42.json'
sheet_url = pygsheets.authorize(service_account_file = g_account_file)
# automate url link
sheet_id = '17fau4d1MO5TJ1zp27azIptrX728dLdlYdcPe_xH4Zvk'
sheet = sheet_url.open_by_key(sheet_id)



def stk_confirmed_reservations():
    today_date = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date = f' "{today_date}" '

    per_page = 1000

    response = res_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "confirmed_at",
                        "operator": ">=", 
                        "value": {start_date}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })

    print(response['total'])


    df_stk_confirmed_reservations = pd.json_normalize(response, 'reservations')


    # filter columns
    df_stk_confirmed_reservations = df_stk_confirmed_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id', 'custom_fields']
    ]]
    df_stk_confirmed_reservations.sort_values(by='confirmed_at', ascending=False, inplace=True)
    df_stk_confirmed_reservations['time_update'] = pd.to_datetime('now')
    return df_stk_confirmed_reservations



def leap_confirmed_reservations():
    today_date = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date = f' "{today_date}" '

    per_page = 1000

    response = res_leap_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "confirmed_at",
                        "operator": ">=", 
                        "value": {start_date}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })

    print(response['total'])


    df_leap_confirmed_reservations = pd.json_normalize(response, 'reservations')


    # filter columns
    df_leap_confirmed_reservations = df_leap_confirmed_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id', 'custom_fields']
    ]]
    df_leap_confirmed_reservations.sort_values(by='confirmed_at', ascending=False, inplace=True)
    df_leap_confirmed_reservations['time_update'] = pd.to_datetime('now')

    return df_leap_confirmed_reservations



def stk_check_in_reservations():
    today_date1 = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date1 = f' "{today_date1}" '

    per_page = 1000

    response1 = res_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "checkIn", 
                        "operator": ">=", 
                        "value": {start_date1}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })


    print(response1['total'])


    df_stk_check_in_reservations = pd.json_normalize(response1, 'reservations')


    # filter columns
    df_stk_check_in_reservations = df_stk_check_in_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id' 
    ]]
    df_stk_check_in_reservations.sort_values(by=['checkIn','confirmed_at'], ascending=False, inplace=True)
    return df_stk_check_in_reservations



def leap_check_in_reservations():
    today_date1 = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date1 = f' "{today_date1}" '

    per_page = 1000

    response1 = res_leap_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "checkIn", 
                        "operator": ">=", 
                        "value": {start_date1}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })


    print(response1['total'])


    df_leap_check_in_reservations = pd.json_normalize(response1, 'reservations')


    # filter columns
    df_leap_check_in_reservations = df_leap_check_in_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id' 
    ]]
    df_leap_check_in_reservations.sort_values(by=['checkIn','confirmed_at'], ascending=False, inplace=True)

    return df_leap_check_in_reservations



def stk_check_out_reservations():
    today_date2 = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date2 = f' "{today_date2}" '

    per_page = 1000

    response2 = res_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "checkOut", 
                        "operator": ">=", 
                        "value": {start_date2}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })


    print(response2['total'])


    df_stk_check_out_reservations = pd.json_normalize(response2, 'reservations')


    # filter columns
    df_stk_check_out_reservations = df_stk_check_out_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id' 
    ]]
    df_stk_check_out_reservations.sort_values(by=['checkOut','confirmed_at'], ascending=False, inplace=True)

    return df_stk_check_out_reservations



def leap_check_out_reservations():
    today_date2 = (pd.to_datetime('today') - pd.to_timedelta('8 days')).strftime('%Y-%m-%d')
    start_date2 = f' "{today_date2}" '

    per_page = 1000

    response2 = res_leap_get.get_reservations(
                params={
                'per_page': per_page,
                "filters": f'''[
                    {{
                        "field": "checkOut", 
                        "operator": ">=", 
                        "value": {start_date2}
                    }},
                    {{
                        "field": "status", 
                        "operator": "in", 
                        "value": "accepted,extended"
                    }}
                ]'''
            })


    print(response2['total'])


    df_leap_check_out_reservations = pd.json_normalize(response2, 'reservations')


    # filter columns
    df_leap_check_out_reservations = df_leap_check_out_reservations[['confirmation_code', 'listing_nickname',  'checkIn', 'checkOut',
        'nights', 'revenue', 'price_per_night', 'source', 'status', 
        'confirmed_at','created_at', 'guest_name', 'guests', 
        # 'currency',  'base_price', 'security_price', # 'extras_price', 'extras_price_ex', 'extras_price_inc', # 'cancellation_fee', 'tax_amount', 'cleaning_fee', 'channel_commission', # 'service_charge', 'subtotal',  'transaction_fee', # 'sum_refunds', 'net_revenue', # 'advance_days','notes', 'cleaning_notes', 'listing_id', 'parent_listing_id' 
    ]]
    df_leap_check_out_reservations.sort_values(by=['checkOut','confirmed_at'], ascending=False, inplace=True)

    return df_leap_check_out_reservations



df_stk_confirmed_reservations = stk_confirmed_reservations()
df_leap_confirmed_reservations = leap_confirmed_reservations()
df_concat_confirmed_reservations = pd.concat([df_stk_confirmed_reservations,df_leap_confirmed_reservations])
sheet_wks = sheet.worksheet_by_title('confirmed_data')
sheet_wks.clear(start='A', end='M')
sheet_wks.set_dataframe(df_concat_confirmed_reservations, start=(1,1))


df_stk_check_in_reservations = stk_check_in_reservations()
df_leap_check_in_reservations = leap_check_in_reservations()
df_concat_check_in_reservations = pd.concat([df_stk_check_in_reservations,df_leap_check_in_reservations])
sheet_wks = sheet.worksheet_by_title('check_in_data')
sheet_wks.clear(start='A', end='M')
sheet_wks.set_dataframe(df_concat_check_in_reservations, start=(1,1))


df_stk_check_out_reservations = stk_check_out_reservations()
df_leap_check_out_reservations = leap_check_out_reservations()
df_concat_check_out_reservations = pd.concat([df_stk_check_out_reservations,df_leap_check_out_reservations])
sheet_wks = sheet.worksheet_by_title('check_out_data')
sheet_wks.clear(start='A', end='M')
sheet_wks.set_dataframe(df_concat_check_out_reservations, start=(1,1))
