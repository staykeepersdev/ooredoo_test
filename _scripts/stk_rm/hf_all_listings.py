import os
import sys
import django
import math
import pandas as pd

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

from _scripts.sql_alchemy_settings.leap_m_ooredoo import engine_leap_m_ooredoo

# Initial/Dummy Request ... just to get the totals
response = LISTING_API_CLIENT.listings(page=1, per_page=1)
if not response['success']:
    print('ERROR: INITIAL REQUEST')

# Get count .. compute total pages needed
total_listings = response['total']
print(f'Total listings: {total_listings}')
PER_PAGE = 1000
PAGES = math.ceil(total_listings / PER_PAGE)

# For quick/dummy tests
# PER_PAGE = 100
# PAGES = 100

hf_all_listings = []
for page in range(1, PAGES + 1):
    response = LISTING_API_CLIENT.listings(page=page, per_page=PER_PAGE)
    # print(response)
    hf_all_listings.append(response['listings'])

# flatten list
flat_list = [item for sublist in hf_all_listings for item in sublist]

df = pd.DataFrame(flat_list)

# filter only id and nickname column
df = df[['id', 'nickname']]
df = df.rename(columns={'id':'hostify_parent_id', 'nickname':'listing_nickname'})

# Transform dataframe including unique id
df[['listing_nickname', 'unique_id']
       ] = df['listing_nickname'].str.rsplit('-', 1, expand=True)

df['unique_id'] = pd.to_numeric(df['unique_id'], errors='coerce')
df = df.dropna(subset=['unique_id'])
df['unique_id'] = df['unique_id'].astype(int)
print(df)

# push to PostgreSQL
df.to_sql('hf_listings_all', con=engine_leap_m_ooredoo, if_exists='replace', 
            method='multi')
