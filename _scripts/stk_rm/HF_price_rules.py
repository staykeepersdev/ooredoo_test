import os

import numpy as np
import pandas as pd
import pygsheets

import holidays


today = pd.to_datetime('today')

class HF_MC_manual:

    def __init__(self):
        pass
        # print("in init")


    def week_price(self, df, start_date, price):
        df['week_day'] = df[start_date].dt.weekday

        mon_price = 0.85
        tue_price = 0.90
        wed_price = 1.00
        thu_price = 1.10
        fri_price = 1.20
        sat_price = 1.15
        sun_price = 0.80

        cond_lst = [
            df['week_day'] == 0,
            df['week_day'] == 1,
            df['week_day'] == 2,
            df['week_day'] == 3,
            df['week_day'] == 4,
            df['week_day'] == 5,
            df['week_day'] == 6
        ]

        choice_lst = [
            df['price'] * sun_price,
            df['price'] * mon_price,
            df['price'] * tue_price,
            df['price'] * wed_price,
            df['price'] * thu_price,
            df['price'] * fri_price,
            df['price'] * sat_price
        ]

        df['week_price'] = np.select(cond_lst,choice_lst)

        return df.head(7)

    
    def before_today_discount(self, df, start_date, is_available):
        bf_3 = 3
        bf_7 = 7
        bf_12 = 12

        bf_d_3 = 0.05
        bf_d_7 = 0.08
        bf_d_12 = 0.10

        bf_3d = df[ (df[start_date] < today) &
                (df[start_date] >= (today - pd.DateOffset(days=bf_3))) ][is_available].sum()

        bf_7d = df[ (df[start_date] < today) &
                (df[start_date] >= (today - pd.DateOffset(days=bf_7))) ][is_available].sum()

        bf_12d = df[ (df[start_date] < today) &
                (df[start_date] >= (today - pd.DateOffset(days=bf_12))) ][is_available].sum()

        if bf_12d >= bf_12:
            bf_discount = bf_d_12
        elif bf_7d >= bf_7:
            bf_discount = bf_d_7
        elif bf_3d >= bf_3:
            bf_discount = bf_d_3
        else:
            bf_discount = 0

        return bf_discount

    
    def after_today_discount(self, df, start_date, is_available):
        af_7 = 7
        af_14 = 14
        af_30 = 30
        af_45 = 45
        af_60 = 60

        af_d_7 = 0.15
        af_d_14 = 0.12
        af_d_30 = 0.09
        af_d_45 = 0.07
        af_d_60 = 0.05

        # The logic below checks that there is no reservation in the future period.
        today_avail = df[ (df[start_date] == today) ][is_available].sum()

        af_7d = df[ (df[start_date] > today) &
                (df[start_date] >= (today - pd.DateOffset(days=af_7))) ][is_available].sum()

        af_14d = df[ (df[start_date] > today) &
                (df[start_date] >= (today - pd.DateOffset(days=af_14))) ][is_available].sum()

        af_30d = df[ (df[start_date] > today) &
                (df[start_date] >= (today - pd.DateOffset(days=af_30))) ][is_available].sum()

        af_45d = df[ (df[start_date] > today) &
                (df[start_date] >= (today - pd.DateOffset(days=af_45))) ][is_available].sum()

        af_60d = df[ (df[start_date] > today) &
                (df[start_date] >= (today - pd.DateOffset(days=af_60))) ][is_available].sum()

        if today_avail == 0:
            af_discount = 0

        elif af_60d >= af_60:
            af_discount = af_d_60
        elif af_45d >= af_45:
            af_discount = af_d_45
        elif af_30d >= af_30:
            af_discount = af_d_30
        elif af_14d >= af_14:
            af_discount = af_d_14
        elif af_7d >= af_7:
            af_discount = af_d_7
        else:
            af_discount = 0
        
        return af_discount

    def today_gradual_discount(self, df, days, max_disc):
        today = np.datetime64(pd.to_datetime('today').strftime('%Y-%m-%d'))

        day_disc = max_disc / days
        day_disc_n = np.random.choice([day_disc], size=(days))
        date_range = pd.date_range(today, periods=days, freq='D')

        df_disc = pd.DataFrame({
                            'start_date':   date_range,
                            'disc':         day_disc_n
                            })
        
        df_disc['disc'] = 1 - ( df_disc.loc[::-1, 'disc'].cumsum()[::-1] )
        
        df = pd.merge(df, df_disc, how='left', on='start_date')
        
        df = df.fillna(value=1)
        df['week_price'] = df['week_price'] * df['disc']
        df = df.drop(columns='disc')
        
        return df

    def gap_days_calc(self, df ):
        df['days_gap'] = (df['is_available'].diff() != 0).cumsum()
        
        mask = (df['is_available'] == 0)
        df.loc[mask, 'days_gap'] = 0

        # Filter days only after and including today
        df = df[df.start_date >= today - pd.to_timedelta('1 days')]
    
    def gap_days_groupby(self, df ):
        void_days_table = df.groupby('days_gap')['price'].count()

        df = df.merge(void_days_table, left_on='days_gap', right_index=True)

        # Drop reserved days
        df = df[df.is_available == 1]

        # Set 1 day gaps to be unavailable
        if not df[df['listing_nickname'].str.contains('Exeter')].empty:
            pass
        else:
            df.loc[df['price_y'] == 1, 'is_available'] = 0

        return df
    
    def gap_days_discount(self, df ):
        gap_3 = 3
        gap_5 = 5
        gap_7 = 7
        gap_14 = 14
        gap_28 = 28

        gap_3_d = 0.25
        gap_5_d = 0.20
        gap_7_d = 0.15
        gap_14_d = 0.10
        gap_28_d = 0.05
    
        cond_lst = [
            df['price_y'] <= gap_3,
            df['price_y'] <= gap_5,
            df['price_y'] <= gap_7,
            df['price_y'] <= gap_14,
            df['price_y'] <= gap_28,
            df['price_y'] != -1
        ]

        choice_lst = [
            df['week_price'] * (1 - gap_3_d),
            df['week_price'] * (1 - gap_5_d),
            df['week_price'] * (1 - gap_7_d),
            df['week_price'] * (1 - gap_14_d),
            df['week_price'] * (1 - gap_28_d),
            df['week_price'] * 1
        ]

        df['week_price'] = np.select(cond_lst,choice_lst)

        return df

    def holiday_days_markup(self, df ):
        df_date = []
        for date, name in sorted(holidays.England( years=[2020,2021,2022]).items()):
            df_date.append([date, name])

        df_cal = pd.DataFrame(df_date, columns=['start_date', 'name'])
        df_cal['start_date'] = pd.to_datetime(df_cal['start_date'])
        df_cal['mark_up'] = 1.5

        # Manually added holiday calendar dates
        df_other_holiday = pd.DataFrame({
                        'start_date': np.array(['2020-12-29', '2020-12-30', '2020-12-31'], 
                        dtype='datetime64[D]'
        ),
                        'mark_up': pd.to_numeric([1.5, 2, 2], downcast='float')
        })

        df_cal = df_cal.merge(df_other_holiday, on=['start_date', 'mark_up'], how='outer')

        # Mark-up prices for holiday days
        df = df.merge(df_cal, on='start_date', how='left')
        df = df.fillna(value=1)
        df['week_price'] = df.mark_up * df.week_price

        return df

    def occupancy_discount(self, df ):
        # df['week_price'] = 100
        cond_lst = [
            df['occ_perc'] >= 0.75,
            df['occ_perc'] >= 0.50,
            df['occ_perc'] >= 0.30,
            df['occ_perc'] >= 0.15,
            df['occ_perc'] >= 0
        ]

        choice_lst = [
            df['week_price'] * 0.80,
            df['week_price'] * 0.90,
            df['week_price'] * 0.85,
            df['week_price'] * 0.80,
            df['week_price'] * 0.90
        ]

        df['week_price'] = np.select(cond_lst,choice_lst)
        return df




    def id_register(self):

        ################################# Pull ID_Register target price and listings by Dec 2021
        g_account_file = os.path.dirname(__file__).split('ooredoo',1)[0] + 'ooredoo/_scripts/auth_certificates/xero-245302-dd5fc26b4f42.json'
        sheet_url = pygsheets.authorize(service_account_file = g_account_file)
        sheet_id = '16qJYFJeclX7SfrlcYZLJn-nVaScPMnGrKZ3Ike75S9g'
        sheet = sheet_url.open_by_key(sheet_id)
        sheet_wks = sheet.worksheet_by_title('ID Register')
        df = sheet_wks.get_as_df()
        # to remove empty column from Google sheet import
        df.drop(df.columns[[4]], axis=1, inplace=True)

        df_register = df[['property unique number', 'neighborhood']]

        df1 = df[['property unique number', 'status']]
        df = df[['property unique number','oct_20', 'nov_20', 'dec_20', '_jan_21', '_feb_21', 
        '_mar_21', '_apr_21', '_may_21', '_jun_21', '_jul_21', '_aug_21', '_sep_21', '_oct_21', '_nov_21', '_dec_21']]
        
        df.columns = ['property unique number','oct_20', 'nov_20', 'dec_20', 'jan_21', 'feb_21', 
        'mar_21', 'apr_21', 'may_21', 'jun_21', 'jul_21', 'aug_21', 'sep_21', 'oct_21', 'nov_21', 'dec_21']

        # unpivot ID_Register monthly target
        id_reg = pd.melt(df, id_vars=['property unique number'])

        # create start and end date for each month
        id_reg[['month', 'day']] = id_reg['variable'].str.split('_', expand=True)
        id_reg['start_month'] = pd.to_datetime("01-" + id_reg['month'] + "-" + id_reg['day'])
        id_reg['end_month'] = id_reg['start_month'] - pd.tseries.offsets.MonthEnd(n=0)
        id_reg['date_diff'] = id_reg['end_month'] - id_reg['start_month']
        id_reg['date_diff'] = id_reg['date_diff'].dt.days + 1

        # transform text and blank spaces then calculate days difference
        id_reg['month_target_price'] = id_reg['value'].replace(r'^\s*$', np.nan, regex=True)
        id_reg['month_target_price'] = id_reg['month_target_price'].astype('float64')
        id_reg['day_target_price'] = id_reg['month_target_price'] / id_reg['date_diff']

        return id_reg, df1, df_register
        