import django
import os
import sys
import math
import json

from datetime import datetime, timedelta

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.express as px
# import datapane as dp

from django.utils import timezone

# set up Google log-in
import csv
from google.oauth2 import service_account
import pygsheets

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.leap_m_ooredoo import engine_leap_m_ooredoo

from hostify_api.listing_api_client import ListingAPIClient
from hostify_api.calendar_api_client import CalendarAPIClient
from hostify.models import NonLeapListingInfo

from _scripts.stk_rm.HF_price_rules import HF_MC_manual

HF_MC_manual = HF_MC_manual()
HF_cal = CalendarAPIClient()
HF_list = ListingAPIClient()



################################# Pull Hostify listings information
# get all listing parent ids from Hostify
query = '''SELECT hostify_parent_id, listing_nickname, unique_id 
            FROM hf_listings_all 
            WHERE listing_nickname NOT LIKE 'LP_%' '''
hf_ids = pd.read_sql_query(query, engine_leap_m_ooredoo)



################################# Pull ID_Register target price and listings by Dec 2021
id_reg, df1, df_register = HF_MC_manual.id_register()



################################# Combine Hostify static listing parent data with ID_Register target price
listing_target = hf_ids.merge(id_reg, left_on='unique_id', right_on='property unique number')

stk_listing_ids = listing_target

stk_listing_ids['MS'] = stk_listing_ids['listing_nickname'].str.contains(r'MS\b')
stk_listing_ids = stk_listing_ids.drop_duplicates(subset=['hostify_parent_id'])
stk_listing_ids = stk_listing_ids.merge(df1, on='property unique number')

stk_listing_ids = stk_listing_ids[ stk_listing_ids['status'] == 'Active' ]
single_listing_ids = stk_listing_ids[ (stk_listing_ids['MS'] == False) ] # excluding group listings
group_listing_ids = stk_listing_ids[ (stk_listing_ids['MS'] == True) ] # only group/ master listings

stk_listing_ids_ls = stk_listing_ids['hostify_parent_id'].to_list()
single_listing_ids_ls = single_listing_ids['hostify_parent_id'].to_list()
group_listing_ids_ls = group_listing_ids['hostify_parent_id'].to_list()



################################# Loop to assign listings ID_Register target price to its actual availability

# variables
#hostify_id = [ 104277 ] #[ 82033, 82034, 94159, 99585, 99582, 99588, 99577, 99574, 23988, 90643, 90087, 90091, 14382, 14386, 14421, 14441, 14805, 14807, 15460, 15494, 15506, 15508, 16476, 18463, 19743, 19745, 19750, 19752, 19753, 19762, 19764, 19771, 19772, 19773, 19774, 19777, 19778, 19779, 19780, 19781, 19782, 19783, 19784, 19788, 19789, 19790, 21578, 21772, 21774, 21775, 22263, 22264, 22265, 22266, 22357, 22359, 22363, 22365, 22382, 22387, 22442, 22443, 22444, 22445, 22446, 22447, 22448, 22449, 22450, 22451, 22452, 22453, 22454, 22673, 22768, 22799, 22800, 22801, 22802, 22803, 23366, 23367, 23368, 23370, 23372, 23373, 23375, 23376, 23377, 23378, 23379, 23380, 23381, 23384, 23385, 23386, 23387, 23482, 23483, 23484, 23485, 23486, 23494, 23984, 23998, 24041, 24047, 24064, 24065, 24078, 24084, 24086, 24087, 24101, 24102, 24103, 24781, 26066, 26996, 26998, 27002, 27004, 27005, 27007, 27008, 34892, 56607, 56608, 56609, 56625, 56626, 56640, 56641, 56854, 56882, 56883, 56891, 56892, 57017, 57018, 57215, 57216, 80166, 80167, 81154, 81188, 81204, 81221, 86028, 86340, 86594, 86595, 86596, 86597, 86598, 86599, 86600, 87786, 87972, 88244, 88245, 88621, 88622, 88674, 88675, 88676, 88677, 88709, 88710, 88711, 88727, 88728, 88729, 88730, 88731, 88732, 88733, 88734, 88735, 88736, 88737, 88738, 88739, 88740, 88747, 88748, 88749, 88750, 88751, 88758, 88759, 88760, 88761, 88762, 88763, 88764, 88765, 88766, 88767, 88768, 88769, 88770, 89043, 89748, 89749, 89751, 89754, 89878, 89880, 89881, 89960, 89968, 89969, 89970, 89972, 89975, 89976, 89977, 89978, 89979, 89980, 90042, 90046, 90047, 90048, 90049, 90064, 90067, 90070, 90071, 90076, 90085, 90088, 90090, 90092, 90093, 90301, 90302, 90303, 90304, 90305, 90306, 90307, 90559, 90560, 90562, 90598, 90599, 90600, 90601, 90602, 90603, 90604, 90605, 90606, 90607, 90608, 90609, 90612, 90613, 90614, 90615, 90617, 90618, 90619, 90620, 90621, 90622, 90624, 90625, 90626, 90627, 90628, 90629, 90631, 90633, 90634, 90635, 90636, 90637, 90641, 90642, 90645, 90646, 90647, 90649, 90650, 90651, 90652, 90653, 90654, 90786, 90788, 90789, 90790, 90791, 90792, 90799, 90801, 90995, 91013, 91014, 91017, 91562, 92562, 92565, 92568, 92571, 92574, 92577, 94164, 94170, 94173, 94176, 94179, 94185, 94188, 94191, 94197, 94200, 94203, 94209, 94212, 94218, 94221, 94395, 94402, 94405, 94408, 94411, 94432, 94514, 94517, 94520, 94523, 94526, 94529, 94532, 94535, 94538, 94541, 94544, 94547, 94550, 94553, 94556, 94559, 94562, 94565, 94568, 94571, 94574, 94577, 94580, 94583, 94586, 94915, 94918, 95276, 95279, 95282, 95288, 95330, 95333, 95336, 95339, 95342, 95345, 95348, 95351, 95354, 95357, 95360, 95363, 95366, 95369, 95372, 95375, 95378, 95381, 95384, 95387, 95390, 95393, 95396, 95399, 95402, 95405, 95408, 95411, 95414, 95417, 95420, 95423, 95438, 95450, 95451, 95568, 95569, 95958, 95961, 95964, 95967, 95982, 95998, 96002, 96003, 96004, 96005, 96006 ]
hostify_id = single_listing_ids_ls
start_date = datetime.today() - timedelta(days=14)
end_date = datetime.today() + timedelta(days=120) #datetime.today() + timedelta(days=90)
per_page = 1000

# parent_list = HF_list.listings(page=2, per_page=100)
# print(parent_list)

# get calendar price per date per listing
# cal_price_all = []

for i in hostify_id:
    cal_price = HF_cal.get_calendar_day(
        params={
            'per_page': per_page,
            'listing_id': i,
            'start_date':  start_date.strftime('%Y-%m-%d'),
            'end_date': end_date.strftime('%Y-%m-%d')
        })
    # print(cal_price)
    
    # cal_price_all.append(cal_price['calendar'])
    
    # flatten list
    # flat_list = [item for sublist in cal_price for item in sublist]
    # print(flat_list)
    
    # assign calendar format 
    df = pd.DataFrame(cal_price['calendar'])
    
    df['end_date'] = df['date']
    df['note'] = datetime.today().strftime('A''%d''/''%m')
    
    df_hf = df.drop(columns=['currency', 'base_price', 'auto_pricing', 'reservation_id', 'id'])
    
    # set when calendar is free 1 and when calendar is booked, unavailable or at £1,000 then 0
    df_hf = df_hf.replace('available',1)
    df_hf = df_hf.replace(['booked', 'unavailable'], 0)

    mask2 = (df_hf['price'] == 1000)
    df_hf.loc[mask2, 'status'] = 0

    df_hf = df_hf.rename(columns={'status': 'is_available', 'date': 'start_date'})

    column_names = ['start_date','end_date','is_available','price','note','listing_id']
    df_hf = df_hf.reindex(columns=column_names)

    

    ################################# Add ID_Register target price to Hostify live/ dynamic availability

    listing_target1 = listing_target.loc[listing_target['hostify_parent_id'] == i]
    
    df_hf['start_date'] = pd.to_datetime(df_hf.loc[:, 'start_date'] )
    
    a = df_hf.start_date.values
    bh = listing_target1.end_month.values
    bl = listing_target1.start_month.values

    v, j = np.where((a[:, None] >= bl) & (a[:, None] <= bh))

    df = pd.DataFrame(
        np.column_stack([df_hf.values[v], listing_target1.values[j]]),
        columns=df_hf.columns.append(listing_target1.columns)
        )
    
    df['start_month'] = pd.to_datetime(df.start_month)
    df['end_month'] = pd.to_datetime(df.end_month)
    
    # replace live price with id_reg target price per day for each month
    df['MC_price'] = df['price']
    # if ID_Register has price less > £10 or missing projections then the price will be set to £100
    df['price'] = np.where(df['day_target_price'] > 1 , 
                            df['day_target_price'],
                            100)
    
    # Loop names to add to price chart
    property_unique_number = df.iloc[0]['property unique number']
    listing_nickname = df.iloc[0]['listing_nickname']
    
    ################################# Create DF calculation based on MC pricing

    df['start_date'] = pd.to_datetime(df['start_date'])
    
    # week prices distribution
    HF_MC_manual.week_price( df=df, start_date='start_date', price='price')
    
    
    # Discount for the first 1-6 empty days (gradual discount to assure 100% occ. within 5 days)
    df = HF_MC_manual.today_gradual_discount(df=df, days=45, max_disc=0.20)
    # df.to_csv(r'/Users/miro/Downloads/test11.csv')
    
    
    # BEFORE Today date void number of days and respective discount percentage
    bf_discount = HF_MC_manual.before_today_discount(df=df, start_date='start_date', is_available='is_available')
    

    # AFTER Today date void number of days and respective discount percentage
    af_discount = HF_MC_manual.after_today_discount(df=df, start_date='start_date', is_available='is_available')
    
    
    # Filter days only after and including today
    HF_MC_manual.gap_days_calc(df=df)


    # filter only from today in the future days and remove empty days
    today = pd.to_datetime('today').strftime('%Y-%m-%d')
    df = df[df['start_date'] >= today]

    df = df[df['is_available'] != 0]
    
    if df.empty:
        continue
    else:
        pass

    # First gap period apply combined discount for before and after
    
    df['week_price'] = np.where( df['days_gap'] == np.float( df[(df['start_date'] >= today) & 
                                ( df['is_available'] == 1)]['days_gap'][:1] ), 
                                df['week_price'] * (1 - (af_discount + bf_discount)), 
                                df['week_price'] )
    
    
    # Gaps groupby for count of available days and merge again the result
    df = HF_MC_manual.gap_days_groupby(df=df)


    # To discount future booking gaps based on the number of days gap
    HF_MC_manual.gap_days_discount(df=df)

    
    # To add holiday days mark_up
    df = HF_MC_manual.holiday_days_markup(df=df)
    

    # To create a chart visualisation for Target, HF_MC and actual price
    # fig = px.scatter(df, x='start_date', y=['day_target_price', 'week_price', 'MC_price'], title=f'HF ID - {i}, UN ID - {property_unique_number}, {listing_nickname}').update_traces(mode='lines+markers')
    # fig.show()
    # dp.Report(dp.Plot(fig)).publish(name='test')
    
    # Drop day prices where the current MC_price price is equal "week_price"/the alghorithm changed price
    df = df[df['MC_price'] != df['week_price'].round(0)]
    

    # To prepare the data for Hostify POST request
    df = df.drop(columns=['name', 'mark_up','MC_price', 'price_x', 'listing_id', 'week_day', 'days_gap','price_y', 'hostify_parent_id',
                            'listing_nickname', 'unique_id', 'property unique number', 'variable',
                            'value', 'month', 'day', 'start_month', 'end_month', 'date_diff',
                            'month_target_price', 'day_target_price', 'MS'])

    df = df.rename(columns={'week_price': 'price'})
    df['start_date'] = df['start_date'].astype(str)
    
    print(bf_discount)
    print(af_discount)
    
    
    ################################# Import DF to HF Json
    if not df.empty:
        df_json = df.to_json(orient='records')
        parsed = json.loads(df_json)
        parsed
        
        calendar_day_params = {"calendar": parsed}

        cal_price = HF_cal.put_calendar_days(i, calendar_day_params)

        print(cal_price)
    else:
        continue

