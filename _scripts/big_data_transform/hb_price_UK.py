# %%
from operator import index
import sys, os, django
import time
import pandas as pd
import numpy as np

import dask.dataframe as dd
import dask.array as da
from dask.distributed import Client

import multiprocessing
import threading

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price, database_url
# %%
################################# Dask Dashboard
# client = Client(n_workers=4)
# client

# %%


# %%

sql = "SELECT * FROM hb_price LIMIT 10000000"
def csv_chunk():
    for index, chunk in enumerate(pd.read_sql_query(sql , engine_local_db_hb_price, chunksize=10**6)):
        df = pd.DataFrame(chunk)
        print(df)
        df.to_csv(f'~/Downloads/hb_price__{index}.csv')

# %%
from multiprocessing.pool import ThreadPool
sql = "SELECT * FROM hb_price LIMIT 10000000"
CHUNKSIZE = 1000000
MAX_POOL = 6

def process_frame(df):
        # process data frame
        return len(df)

if __name__ == '__main__':
    reader = pd.read_sql_query(sql, engine_local_db_hb_price, chunksize=CHUNKSIZE)
    pool = multiprocessing.Pool(MAX_POOL) # use 4 processes

    for counter, df in enumerate(reader):
        # process each data frame
        f = pool.apply_async(process_frame,[df])
        df.to_csv(f'~/Downloads/hb_price__{counter}.csv')
        print(df)
                
        # result = 0
        # for f in funclist:
        #         result += f.get(timeout=10) # timeout in 10 seconds

        # print (f"There are {result} rows of data")
# %%

# %%
################################# 
query = '''
        SELECT * FROM public."hb_price_UK"
        LIMIT 10000000
        '''
df = pd.read_sql_query(query, engine_local_db_hb_price) #, chunksize=100000
df
# %%

# %%
df_dd = dd.read_sql_table('hb_price', database_url, index_col='scraped_time' ) # npartitions=10**6
df_dd
# df_dd.groupby('rates.net').mean().compute()

# %%
df_dd.to_csv('/Users/miro/Downloads/hb_price_csv/hb_price_*.csv')
# %%
# %%
# %%
test = dd.read_parquet('~/Downloads/hb_price_parq_new') # , engine_local_db_hb_price='pyarrow'
test

# %%
test1 = test.head(60)
# %%
# new_df = pd.DataFrame({'scraped_time': [pd.to_datetime('2021-03-05')]})
new_df = dd.from_pandas(test1, chunksize=1000000)
new_df
# %%
################################# Read from paquet file
dd.to_parquet(new_df, '~/Downloads/test', 
                append=True, 
                ignore_divisions=True,
                partition_on=['check_in'] )
# ,write_metadata_file=False ,append=True, ignore_divisions=True 

# %%
# filter_data = [('check_in', "=", pd.to_datetime('2021-02-18 00:00:00'))]
new = dd.read_parquet('~/Downloads/test', ) #filters=filter_data
new.head() #npartitions=2
# len(new)
# %%
# start = time.perf_counter()

# test.groupby('rates.net').mean().compute()

# end = time.perf_counter()
# f'{round((end - start)/ 60, 2)} mins'
# %%
test_array = test.to_dask_array() # lengths=True will take longer to calculate the chunk sizes 1.5 min more
test_array
# %%
test_array_np = test_array.astype('int64')
test_array_np
# %%
import h5py
f = h5py.File('myfile.hdf5')
d = f.require_dataset('/data', shape=test_array_np.shape, dtype=test_array_np.dtype)
da.store(test_array_np, d)
# %%
da.to_hdf5('myfile.hdf5', '/output', test_array_np)
# %%


# %%
################################# Read from HDF5 & csvs
test.to_csv('/Users/miro/Downloads/hb_price_csv/hb_price_*.csv')
# dd.to_hdf(path='~/Downloads/hb_price_hdf5.hdf', key='index', df=df_dd)

# %%
df_csv = dd.read_csv('/Users/miro/Downloads/hb_price_csv/hb_price_*.csv')
df_csv

# %%

# %%
