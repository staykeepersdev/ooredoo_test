# %%
import time
import pandas as pd
import numpy as np

import vaex

# %%
################################# Vaex
# import vaex
# df_vaex = vaex.open('/Users/miro/Downloads/hb_price_csv/hb_price_*.csv', convert='hb_price.hdf5')
# df_vaex

# %%
df_read_vaex = vaex.open('/Users/miro/Downloads/hb_price_hdf5/hb_price_*.csv.hdf5')
df_read_vaex

# %%
df_read_vaex['hotel_room_id'] = df_read_vaex['code'].astype(str) + df_read_vaex['rooms.code']
df_read_vaex
# %%
df_read_vaex['scraped_time'] = df_read_vaex['scraped_time'].astype('datetime64[ns]')
df_read_vaex
# %%
# df_read_vaex.plot(x='scraped_time', y='rates.net')
# %%
df_read_vaex.groupby(df_read_vaex.code, agg='sum')
# %%
df_read_vaex.mean('rates.net')
# %%
# df_read_vaex.plot(df_read_vaex['rates.net'], df_read_vaex['code'])
# %%
df_vaex = vaex.open('/Users/miro/Downloads/hb_price_csv/hb_price_*.csv')
df_vaex
# %%
