# %%
import sys, os, django
import pandas as pd
import math

import dask.dataframe as dd
import datetime
import time

sys.path.append('/home/miroslav/Dev/wwpt_projects/ooredoo/')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from _scripts.sql_alchemy_settings.local_db_hb_price import engine_local_db_hb_price, database_url
# %%
################################# Dask Dashboard
from dask.distributed import Client
# Client()
client = Client(n_workers=8,
                threads_per_worker=1,
                memory_limit='8GB')
                # processes=False)
# %%
df_dd = dd.read_sql_table('hb_price_3', database_url, index_col='check_in') #, bytes_per_chunk="1000 MiB") # npartitions=10**6
df_dd['scraped_date'] = df_dd.scraped_time.dt.date
# df_dd = df_dd.set_index('scraped_time')
print(df_dd)

# %%
path = '/media/miroslav/Storage/Dev/parquet_storage/hb_price_worldwide_daily/'
# path = '/home/miroslav/Downloads/hb_price_worldwide_daily_1/'
# path = '/home/miroslav/Downloads/test1/'
dd.to_parquet(df_dd, path,
                write_metadata_file=False,
                # append=True,
                ignore_divisions=True,
                engine='pyarrow',
                partition_on=['scraped_date', 'country_code'] ) # 'scraped_date', 'destinationCode'] )

# %%
# %%
# path = '/media/miroslav/My Book Duo/datasets/parquet/test' #hb_price_worldwide_daily'
# path = '/media/miroslav/My Book Duo/datasets/parquet/hb_price_worldwide_daily_1'
path = '/media/miroslav/Storage/Dev/parquet_storage/hb_price_worldwide_daily_1/scraped_date=2021-05-19'
# path = '/home/miroslav/Downloads/hb_price_worldwide_daily_1/scraped_date=2021-05-07/'
filter_data = [
            # ('scraped_date', ">", '2021-04-19'),
            # ('check_in', "=", '2021-04-09'),
            # ('check_in', "<", '2021-05-21'),
            ('country_code', "=", 'AU'),
            # ('destinationCode', "=", 'ADL')
            ]
new = dd.read_parquet(path, engine='pyarrow') #, filters=filter_data) # columns=['index'], engine='pyarrow' # fastparquet
new
# %%
len(new)
# new.compute()
# %%
# %%
################################################################################
df_dd = dd.read_sql_table('hb_price_4', database_url, index_col='check_in')
df_dd['scraped_date'] = df_dd.scraped_time.dt.date
print(df_dd)
# %%
from dask_sql import Context

c = Context()
c.create_table("data_table", df_dd)
# %%
today_str = '2021-05-07' #pd.to_datetime('today').strftime('%Y-%m-%d')
tomorrow_str = '2021-05-08' #(pd.to_datetime('today') + pd.DateOffset(1)).strftime('%Y-%m-%d')
# %%
# result = c.sql(f"""
#     SELECT
#         *
#     FROM data_table
#     WHERE scraped_time >= '{today_str}' AND scraped_time < '{tomorrow_str}'
# """)
result = c.sql(f"""
CREATE TABLE today_table AS
SELECT * FROM data_table
WHERE scraped_time >= '{today_str}' AND scraped_time < '{tomorrow_str}'
""")
result
# %%
# path = '/media/miroslav/Storage/Dev/parquet_storage/hb_price_worldwide_daily_1/'
path = '/home/miroslav/Downloads/hb_price_worldwide_daily_1/'
dd.to_parquet(result, path,
                write_metadata_file=False,
                # append=True,
                ignore_divisions=True,
                engine='pyarrow',
                partition_on=['scraped_date', 'country_code'] ) # 'scraped_date', 'destinationCode'] )
# %%
# %%
################################################################################
# from blazingsql import BlazingContext
# import cudf
# import dask_cudf
# import dask
# from dask.distributed import Client

today_str = '2021-05-07' #pd.to_datetime('today').strftime('%Y-%m-%d')
tomorrow_str = '2021-05-08' #(pd.to_datetime('today') + pd.DateOffset(1)).strftime('%Y-%m-%d')

connection = engine_local_db_hb_price.connect()
my_query = f"""
        CREATE TABLE today_table AS
        SELECT * FROM public.hb_price_4
        WHERE scraped_time >= '{today_str}' AND scraped_time < '{tomorrow_str}';
"""
connection.execute(my_query)

# %%
df_dd = dd.read_sql_table('today_table', database_url, index_col='check_in') #, bytes_per_chunk="1000 MiB") # npartitions=10**6
df_dd['scraped_date'] = df_dd.scraped_time.dt.date
# df_dd = df_dd.set_index('scraped_time')
print(df_dd)

# %%
path = '/media/miroslav/Storage/Dev/parquet_storage/hb_price_worldwide_daily_1/'
# path = '/home/miroslav/Downloads/hb_price_worldwide_daily_1/'
# path = '/home/miroslav/Downloads/test1/'
dd.to_parquet(df_dd, path,
                write_metadata_file=False,
                # append=True,
                ignore_divisions=True,
                engine='pyarrow',
                partition_on=['scraped_date', 'country_code'] )
# %%
connection = engine_local_db_hb_price.connect()
my_query = f"""
        DROP TABLE today_table;
"""
connection.execute(my_query)
# %%
'''
total = pd.read_sql('SELECT MAX(index) FROM hb_price', engine_local_db_hb_price)
total = total.iloc[0,0]
index_fetch = 10
start_offset_range = 0 - index_fetch
for i in range(0, (math.ceil(total/index_fetch)) ):
    start_offset_range += index_fetch
    # fetched_rows = n
    end_offset_range = start_offset_range + index_fetch - 1
    print(i)
    print(f'From {start_offset_range} to {end_offset_range}')
    
    query = f"SELECT * FROM hb_price WHERE index between {start_offset_range} and {end_offset_range};"
    df = pd.read_sql(query, engine_local_db_hb_price) #, index_col='index')
    df['scraped_date'] = df['scraped_time'].dt.date
    print(len(df))
    df_dd = dd.from_pandas(df, npartitions=8) #processors
    del df
    
    df_dd['scraped_time'] = dd.to_datetime(df_dd.scraped_time,unit='ns')
    df_dd['scraped_date'] = df_dd.scraped_time.dt.date
    # df_dd = df_dd.set_index('check_in')
    print(df_dd)

    path = '/media/miroslav/My Book Duo/datasets/parquet/hb_price_worldwide_daily'
    dd.to_parquet(df_dd, path,
                    append=True,
                    ignore_divisions=True,
                    partition_on=['scraped_date', 'country_code', 'destinationCode'] )
'''

# %%

    

# %%
