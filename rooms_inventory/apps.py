from django.apps import AppConfig


class RoomsInvetoryConfig(AppConfig):
    name = 'rooms_inventory'
