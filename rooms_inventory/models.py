from django.db import models
from hotels.models import HotelRoom

class InventoryDay(models.Model):
    hotel_room = models.ForeignKey(
        HotelRoom,
        on_delete=models.CASCADE,
        related_name='inventory',
    )
    date = models.DateField()
    rate = models.PositiveIntegerField(blank=True, null=True)
    release = models.PositiveIntegerField()
    allotment = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.hotel_room} - {self.date}'

class PriceDay(models.Model):
    
    ## This should be tested if it oculd be OneToOne
    inventory_day = models.ForeignKey(
        InventoryDay,
        on_delete=models.CASCADE,
        related_name='prices'
    )
    price_per_pax = models.BooleanField(default=False)
    net_price = models.FloatField(blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    specific_rate = models.IntegerField(blank=True, null=True)
    base_board = models.CharField(max_length=2, blank=True, null=True)
    amount = models.FloatField(blank=True, null=True)

    def __str__(self):
        return f'{self.inventory_day}'

