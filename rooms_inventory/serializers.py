from rest_framework import serializers
from rest_framework import pagination

from .models import (
    InventoryDay,
    PriceDay
)


class PriceDaySerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceDay
        exclude = ('id',)

class InventoryDaySerializer(serializers.ModelSerializer):
    prices = PriceDaySerializer(many=True)
    class Meta:
        model = InventoryDay
        fields = (
            'hotel_room',
            'date',
            'rate',
            'release',
            'allotment',
            'prices'
        )