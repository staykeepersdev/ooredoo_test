from django.contrib import admin
from .models import InventoryDay, PriceDay
# Register your models here.

class PriceDaysInline(admin.TabularInline):
    model = PriceDay
    extra = 0
    show_change_link = True

@admin.register(InventoryDay)
class InventoryDayAdmin(admin.ModelAdmin):
    inlines = [PriceDaysInline]
    list_display = ('hotel_room', 'date', 'rate', 'release', 'allotment')
    list_filter = ('date',)

@admin.register(PriceDay)
class PriceDayAdmin(admin.ModelAdmin):
    list_display = ('inventory_day', 'price_per_pax', 'net_price', 'price', 'specific_rate', 'base_board', 'amount')

