from django.urls import path
from .views import (
    HotelRoomsInventory,
)

urlpatterns = [
    # Future endpoints - Inventory related
    # path('<int:hotel_room_id>', HotelRoomsInventory.as_view(), name='hotel-room-inventory'),
]