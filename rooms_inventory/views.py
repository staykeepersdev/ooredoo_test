from django.shortcuts import render

from rest_framework import generics, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny

# DRF Yet Another Swagger Generator 
from drf_yasg.utils import swagger_auto_schema

from .models import InventoryDay, PriceDay
from .serializers import (
    InventoryDaySerializer,
)

# Hotel rooms related
class HotelRoomsInventory(generics.ListAPIView):
    permission_classes = (IsAuthenticated,) 
    serializer_class = InventoryDaySerializer
    # ordering_fields = ('default_price', 'rating')

    def get_queryset(self, **kwargs):
        hotel_room_id = self.kwargs['hotel_room_id']
        return InventoryDay.objects.filter(hotel_room_id=hotel_room_id)
    
    @swagger_auto_schema(
      operation_summary='GET /inventory/{hotel-room-id}/',
      operation_description="<b>Returns a single hotel room price and availability</b> - based on <b>hotel room unique id</b>.",
      responses={status.HTTP_200_OK: InventoryDaySerializer}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)