from django.apps import AppConfig


class HostifyConfig(AppConfig):
    name = 'hostify'
