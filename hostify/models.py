from django.contrib.postgres.fields import JSONField
from django.db import models
from hotelbeds_data.models import APICountry

# class ExtraGuestPrice(models.Model):

#     country = models.OneToOneField(
#         APICountry,
#         related_name='extra_guest_price',
#         on_delete=models.CASCADE
#     )

#     price = models.PositiveSmallIntegerField(default=0)


class NonLeapListingInfo(models.Model):

    hostify_parent_id = models.PositiveIntegerField()
    listing_nickname = models.CharField(max_length=255)
    
    # Stores AirBnb children count & first child
    count_airbnb_children = models.PositiveSmallIntegerField(default=0)
    hostify_child_id_airbnb = models.PositiveIntegerField(null=True)
    id_airbnb = models.BigIntegerField(null=True)

    # Stores Booking.com children count & first child
    count_booking_com_children = models.PositiveSmallIntegerField(default=0)
    hostify_child_id_booking_com = models.PositiveIntegerField(null=True)
    id_booking_com = models.BigIntegerField(null=True)

    count_other_children = models.PositiveSmallIntegerField(default=0)
    count_total_children = models.PositiveSmallIntegerField(default=0)

    # Store all children as json
    children = JSONField(null=True, blank=True)

class HB_Price(models.Model):
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    is_available = models.CharField(max_length=1)
    price = models.FloatField()
    hostify_id = models.CharField(max_length=10)
    date_hour = models.DateTimeField()
    date = models.CharField(max_length=10)
    id = models.primary_key=True
    date_diff = models.DateTimeField()
    index1 = models.CharField(max_length=20)
    date_exp = models.DateTimeField()


