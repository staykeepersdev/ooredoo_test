# Generated by Django 2.2.7 on 2020-08-25 22:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('octoparse', '0009_auto_20200825_1423'),
    ]

    operations = [
        migrations.RenameField(
            model_name='scrape',
            old_name='los',
            new_name='night_stay',
        ),
    ]
