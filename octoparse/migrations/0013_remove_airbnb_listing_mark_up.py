# Generated by Django 2.2.7 on 2020-09-21 16:56

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('octoparse', '0012_airbnb_listing_bcom_listing'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='airbnb_listing',
            name='mark_up',
        ),
    ]
