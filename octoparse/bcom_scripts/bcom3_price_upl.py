#!/usr/bin/env python
# %%
import django, os, sys
import math
import pandas as pd
import numpy as np

# check
from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from ooredoo.sql_alchemy_settings import database_url, engine

# %%
########################################
# Auth Octoparse login
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)


# %%
########################################
# Scraper data import in BigQuery

# set up Google log-in
import csv
from google.oauth2 import service_account

import pygsheets

# import authentication from Google service account json stored on local
g_sheet = pygsheets.authorize(service_account_file='/Users/miro/Dev/wwpt_projects/ooredoo/ooredoo/xero-245302-dd5fc26b4f42.json')


# %%
########################################
# Scraper task to download all items in each page

# r_d = OctoClient.export_non_exported_data(octo_task, 1000)

# n = 1000
# total = r_d['data']['total']
# # print(total)
# pages = math.ceil(total / n)
# # print(pages)

# for i in range(pages):
#         i = OctoClient.export_non_exported_data(octo_task, 1000)
#         # print(i)

#         task_update = OctoClient.update_data_status(octo_task)
#         # print(task_update)

#         item = i['data']['dataList']
        
#         # create Dataframe to import to gbq
#         scrape_data_df = pd.DataFrame(item)
#         print(scrape_data_df)
#         # sys.exit()
#         # import df to gbq
#         scrape_data_df.to_gbq(destination_table="octoparse.bcom_price",
#                                         project_id = "xero-245302",
#                                         if_exists="append")
#         # print(item)
        

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
# scraped_data_ls = [val for sublist in scraped_data for val in sublist]



# %%
octo_task = '7b27ae9e-dbf1-4da1-b5ca-920c7aad619a'
r_d = OctoClient.get_data_by_offset(octo_task, 0, 1000)
print(r_d)
# %%
# n = 1000
# total = r_d['data']['total']
# pages = math.ceil(total / n)
# # print(pages)
# print(total)

# for i in range(pages):
#         LIMIT = i * 1000
#         print(LIMIT)
#         r = OctoClient.get_data_by_offset(octo_task, LIMIT, n)
#         dataList = r['data']['dataList']
#         # for item in dataList:
#         #         print(item)

# %%
for i in range( len(r_d['data']['dataList']) ): # 920 & 186?
    df1 = pd.DataFrame(r_d['data']['dataList'])
    # df2 = df1[['Current_Time', 'Page_URL']].loc[0:]
    print(i)
    

    sample = str(df1['Field'][i]).replace('\n', '')
    sample = str(df1['Field'][i]).replace('</span>', '| </span>')


    # sample = sample.replace("'", "")
    # sample = sample.replace(",", "")
    # sample = sample.replace('"', '')
    # re.sub('<[^<]+?>', '', sample)
    # sample.append(sample[i].rstrip('\n').split(','))
    sample

    
    from bs4 import BeautifulSoup
    soup = BeautifulSoup(sample, 'html.parser')
    room_id = [a['data-room-id'] for a in soup.select('a[data-room-id]')]
    room_href = [a['href'] for a in soup.select('a[href]')]
    room_href = [k for k in room_href if 'RD' in k]
    room_href

    
    # pd.set_option('max_colwidth', 1000)
    if not sample:
        continue
    ls = pd.read_html(sample, attrs = {'id': 'hprt-table'} )[0] # , attrs = {'id': 'hprt-table'} 
    ls
    df = pd.DataFrame(ls)
    df
    if df.empty:
        continue
    df['Current_Time'] = df1['Current_Time'][i]
    df['Page_URL'] = df1['Page_URL'][i]
    df
    
    df.rename(columns={
        df.columns[0] : 'room_type', df.columns[1] : 'max_person', 
        df.columns[2] : 'price', df.columns[3] : 'policy'}, inplace=True)
    df
    print(df)
    # df.dropna(subset=['price'], inplace=True)
    df = df[df['max_person'].str.contains('Max')].reset_index()

    df['max_person'] = df['max_person'].str.extract(r'(\d)', expand=True)
    df
    try:    
        df['room_type'] = (df['room_type'].str.split('|', expand=True)).iloc[:,:1]
        df
    except ValueError:
        pass   
    df["price"] = df["price"].str.split(' ', expand=True).iloc[:,:1]
    df
    
    
    df = df[['room_type','max_person', "price", 'policy', 'Current_Time', 'Page_URL']]
    df

    
    df['room_id'] = pd.Series(room_id)
    df['room_href'] = pd.Series(room_href)
    df

    print(df)
    
    ########################################
    # To push data from Postgres and to prepare in datasets
    df.to_sql('bcom_price', 
                    engine,
                    # chunksize=10000,
                    method='multi',
                    if_exists='append')

    
    
# %%

# %%

# %%

# %%
