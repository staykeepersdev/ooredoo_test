## !/usr/bin/env python
# %%
import django
import os, sys
import json
import datetime
import math
import pandas as pd
import numpy as np

# to review to delete them
import time
import hashlib
import requests

# check
from django.utils import timezone

import importlib

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()


# %%
########################################
# Auth Octoparse login
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)


# %%
########################################
# Scraper data import in BigQuery

# set up Google log-in
import csv
from google.oauth2 import service_account

import pygsheets

# import authentication from Google service account json stored on local
g_sheet = pygsheets.authorize(service_account_file='/Users/miro/Dev/wwpt_projects/ooredoo/ooredoo/xero-245302-dd5fc26b4f42.json')


# %%
########################################
# Scraper task to download all items in each page

KN_bcom_ids = ['e630217f-8bc9-414f-8ec7-e2bd92744c13', 'e06dae37-80c9-49a5-bc4f-208553e13225', 
                '123199ba-303f-4d58-b742-1f9dcab2227b', '7f3c7305-8a80-4951-b41b-553a832bd509']

for j in KN_bcom_ids:
    r_d = OctoClient.get_data_by_offset(KN_bcom_ids, 0, 1000)

    n = 1000
    total = r_d['data']['total']
    # print(total)
    pages = math.ceil(total / n)
    # print(pages)

    for i in range(pages):
            LIMIT = i * 1000
            print(LIMIT)
            r = OctoClient.get_data_by_offset(KN_bcom_ids, LIMIT, n)
            dataList = r['data']['dataList']

            # this is for not exported tasks
            # i = OctoClient.export_non_exported_data(KN_bcom_ids, 1000)
            # # print(i)
            # task_update = OctoClient.update_data_status(KN_bcom_ids)
            # # print(task_update)
            # item = i['data']['dataList']
            
            # create Dataframe to import to gbq
            scrape_data_df = pd.DataFrame(dataList)
            scrape_data_df.replace("\n", '|', regex=True, inplace=True)
            # print(scrape_data_df)
            
            # import df to gbq
            scrape_data_df.to_gbq(destination_table="leap.bcom_hotel_scrape",
                                            project_id = "xero-245302",
                                            if_exists="append")
            # print(item)
            # sys.exit()

# the data is nested in two list and list comprehension below unnest one of the dictionaries so the data could be uploaded to the Dataframe
# scraped_data_ls = [val for sublist in scraped_data for val in sublist]



# %%


# %%
