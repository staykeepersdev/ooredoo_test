# %%
import django, os, sys
import datetime

import numpy as np
import pandas as pd

# check
from django.utils import timezone

# sys.path.append('/srv/www/ooredoo/')
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from ooredoo.sql_alchemy_settings import database_url, engine

# %%
########################################
# Auth Octoparse login
from octoparse.models import Bcom_Listing
from octoparse.client import OctoClient

OctoClient = OctoClient(advanced_api=True)

token_renew = OctoClient.new_token("miro@staykeepers.com","Ivomiro123!")
token_new = list(token_renew.values())[0]
OctoClient.set_access_token(token_new)



# %%
########################################
# To pull data from GBQ
# query = """
#         SELECT * FROM leap.scrape_results
#         """

# df = pd.read_gbq(query, 
#             project_id = "xero-245302", 
#             dialect='standard')

# %%
########################################
# To push data from Postgres and to prepare in datasets
# df.to_sql('scrape_results', 
#                 engine,
#                 # chunksize=10000,
#                 method='multi',
#                 if_exists='replace')

# %%
# To pull data from PSQL
query = '''
        SELECT booking_link 
        FROM scrape_results 
        WHERE booking_link LIKE '%www.booking.com/hotel%'
        '''
df = pd.read_sql(query, engine)
df
# %%
# pd.set_option('max_colwidth', 2000)
df['booking_link'] = df['booking_link'].str.split('html', expand=True)[0]

bcom_ids = df['booking_link'].to_list()

# %%
####################################### LOS #######################################
# To create a url with dynamic check-in and out dates

LOS = [1]
advance_days = 1

fmt = '{id:}html?;checkin={date1:%Y-%m-%d};checkout={date2:%Y-%m-%d};group_adults=1'
                
base = datetime.datetime.today() + datetime.timedelta(days=5) # to remove 
date_list = [base + datetime.timedelta(days=x) for x in range(advance_days)]
# print(date_list)

bcom_id_set = set()

for i in bcom_ids:
        id = i
        # print(i)
                
        for j in LOS:
                delta_days = j
                # print(delta_days)
                for i in date_list:
                        date1 = i
                        date2 = i + datetime.timedelta(days=j)
                        url = fmt.format(id=id, date1=date1, date2=date2)
                        # print(url)
                        bcom_id_set.add(url)
len(bcom_id_set)
bcom_id_set = list(bcom_id_set)

# %%
########################################
# Upload URL list to Octoparse text list                        
bcom_price_auto = ['7b27ae9e-dbf1-4da1-b5ca-920c7aad619a', 'f9d85ff4-35d0-479d-b1b1-5dac54853a68',
                'fd4885cd-e455-41ba-a9a0-3fd59b0ad8f4', 'd934c5da-255e-4b92-b7ee-555a2e301948',
                '38b1bb25-5add-4996-a9a3-bfd9c073d829','1f64f501-2cd8-4310-a08a-ad3c0bce1fa2']

n = 10000
# ls = []
for i, j in zip(range(0, len(bcom_id_set), n), bcom_price_auto):
        # print(i)
        # print(j)
        # Extract specific Octoparse task in json format
        URL_lst = OctoClient.update_task_parameters(task_id=j,
                                                name="BkfDehZ4.UrlList",
                                                value=bcom_id_set[i:i + n])
        # print(URL_lst)
        # ls.append(bcom_id_set[i:i + n])
        # print(bcom_id_set[i:i + n][0:10])

        # start the Octoparse task to scrape
        start_task = OctoClient.start_task(j)
        print(start_task)

# len(ls)
# sum([ len(ls[i]) for i in range(0, 6) ])
# %%
