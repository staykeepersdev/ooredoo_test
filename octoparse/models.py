from django.db import models

# Create your models here.

class Scrape(models.Model):
    Current_Time = models.CharField(max_length=300)
    airbnb_id = models.CharField(max_length=1000)
    Page_URL = models.CharField(max_length=500)
    error_dates = models.TextField(blank=True)
    per_night = models.CharField(max_length=300)
    price_block = models.CharField(max_length=500)
    LOS = models.CharField(max_length=100)
    accommodation_name = models.CharField(max_length=100)
    accommodation_fare = models.CharField(max_length=100)
    column1 = models.CharField(max_length=100)
    column2 = models.CharField(max_length=100)
    column3 = models.CharField(max_length=100)
    total_price = models.CharField(max_length=100)

# class Name(models.Model):
#     airbnb_id = models.CharField('Airbnb Name', max_length=120)

#     def Meta(self):
#         verbose_name='airbnb_id'

#     def __str__(self):
#         return self.airbnb_id

class Airbnb_Listing(models.Model):
    airbnb_id = models.CharField(max_length=300)
    mark_up = models.FloatField(max_length=4)


class Bcom_Listing(models.Model):
    bcom_id = models.CharField(max_length=300)
