from django.contrib import admin

from .models import Airbnb_Listing, Bcom_Listing

@admin.register(Airbnb_Listing)
class Airbnb_Listing(admin.ModelAdmin):

    fileds = ['airbnb_id', 'mark_up']

    list_display = ['airbnb_id']
    
    list_filter = ['mark_up']

    list_display = ['airbnb_id', 'mark_up']

    list_editable = ['mark_up']
