from django.apps import AppConfig


class OctoparseConfig(AppConfig):
    name = 'octoparse'
