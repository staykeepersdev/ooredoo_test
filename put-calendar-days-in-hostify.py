import datetime
import django
import os
import logging

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Get an instance of a logger
logger = logging.getLogger('global')

from django.utils import timezone

from calendar_app.models import (
    CalendarHotel,
    CalendarRoom,
    CalendarRoomType,
    CalendarDay
)

from hostify_api.calendar_api_client import CalendarAPIClient

CALENDAR_API_CLIENT = CalendarAPIClient()

hotel_room_types = CalendarRoomType.objects.filter(
    calendar_rooms__hostify_id__isnull=False
).filter(calendar_rooms__auto_pricing=True).distinct()

today = datetime.date.today()
now = timezone.now()
requests_counter = 0
request_params = []
for hotel_room_type in hotel_room_types:
    calendar_days = CalendarDay.objects.filter(
        calendar_hotel_room=hotel_room_type,
        date__gte=today,
        updated_at_hostify=False
    )

    if calendar_days.exists():
        # Optimize to get values_list for all calendar days at once

        calendar_days_params = []
        last_day = {}
        for calendar_day in calendar_days.order_by('date'):
            date = calendar_day.date.strftime("%Y-%m-%d")
            
            if calendar_day.is_available:
                is_available = 1
            else:
                is_available = 0
            calendar_day_params = {
                "start_date": date,
                "end_date": date,
                "is_available": is_available,
            }
            if calendar_day.price and calendar_day.is_available:
                price = float(calendar_day.price)
                if price < 10:
                    price = 10
                calendar_day_params["price"] = price  

            ## Check for calendar days grouping
            # get day difference
            if last_day:
                last_day_date = datetime.datetime.strptime(last_day['end_date'], "%Y-%m-%d").date()
                days_delta = calendar_day.date - last_day_date
            if (
                last_day
                and days_delta.days == 1
                and last_day['is_available'] == calendar_day_params['is_available']
                and last_day.get('price','') == calendar_day_params.get('price','')
            ):

                last_calendar_day_params = calendar_days_params[-1]
                last_calendar_day_params['end_date'] = date
                last_day = last_calendar_day_params

            else:
                calendar_days_params.append(calendar_day_params)
                last_day = calendar_day_params

        params ={
            'calendar': calendar_days_params
        }
        # Get multiple ids per room type
        hostify_ids = list(
            hotel_room_type.calendar_rooms.filter(
                auto_pricing=True
            ).filter(
                hostify_id__isnull=False
            ).distinct().values_list('hostify_id', flat=True)
        )

        all_passed=True
        
        for hostify_id in hostify_ids:
            if hostify_id:
                requests_counter+=1
                request_params.append(f'{hostify_id} {str(params)}')
                response = CALENDAR_API_CLIENT.put_calendar_days(hostify_id.strip(), params)
                
                # If any id doesnt succeed days wont be marked as updated
                if not response['success']:
                    all_passed = False
                    logger.error(f'HOSTIFY-ERROR : {response}')
                    
                    CalendarRoom.objects.filter(hostify_id=hostify_id).update(
                        auto_pricing=False,
                        hostify_status='deactivated',
                        errors=response,
                    )
        
        if all_passed:    
            calendar_days.update(updated_at_hostify=True, updated_at=now)

## Reuqests logger
with open('logs/requests-counter.txt','a+') as file:
    file.write(f'{now} requests: {requests_counter}\n')

examples_file_name = f'logs/requests_params_{ now.strftime("%Y_%m_%d_%H_%M_%S") }.txt'
with open(examples_file_name,'a+') as file:
    for params in request_params:
        file.write(f'{now} {params}\n\n')