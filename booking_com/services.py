import csv
from datetime import datetime, timedelta

from django.apps import apps
from hotels.models import Hotel

DEFAULT_CHECK_IN_OUT_TIMES = {
    'check_in_start': '15:00:00',
    'check_in_end': '22:00:00',
    'checkout': '10:00:00',
}

def normalize_str(string):
    return string.replace('\n','').replace('\r','')

# input time format: hhmm
def normalize_times(room_line, time_point):
    time_str = room_line[time_point]

    # validate length
    if len(time_str) != '4':
        return DEFAULT_CHECK_IN_OUT_TIMES[time_point]

    # validate nums only
    if not time_str.isdigit():
        return DEFAULT_CHECK_IN_OUT_TIMES[time_point]

    # output time format: hh:mm:00
    return f'{time_str[:2]}:{time_str[2:4]}:00'


def enforce_3_hours_difference(checkin_start, check_in_end):
    MINIMUM_MINUTES = 180

    d_checkin_start = datetime.strptime(checkin_start, '%H:%M:%S')
    d_check_in_end = datetime.strptime(check_in_end, '%H:%M:%S')
    checkin_start_since_midnight = d_checkin_start.hour * 60 + d_checkin_start.minute
    checkin_end_since_midnight = d_check_in_end.hour * 60 + d_check_in_end.minute

    checkin_start_end_diff = checkin_end_since_midnight - checkin_start_since_midnight
    if checkin_start_end_diff < MINIMUM_MINUTES:
        minimum_minutes = MINIMUM_MINUTES - checkin_start_end_diff
        d_check_in_end = d_check_in_end + timedelta(minutes=minimum_minutes)

    return d_checkin_start.strftime("%H:%M:%S"), d_check_in_end.strftime("%H:%M:%S")


def extract_booking_com_json_data(BookingComData, csv_file):

    BookingComHotel = apps.get_model('booking_com', 'BookingComHotel')
    BookingComHotelRoom = apps.get_model('booking_com', 'BookingComHotelRoom')

    room_lines = csv.DictReader((line.decode('utf8') for line in csv_file), delimiter=",")
    for room_line in room_lines:
        b_com_hotel, _ = BookingComHotel.objects.get_or_create(
            booking_com_id = int(normalize_str(room_line['b_com_hotel_id'])),
            defaults={
                'booking_com_data': BookingComData,
                'title': normalize_str(room_line['name']),
                'country': normalize_str(room_line['country']),
                'city': normalize_str(room_line['city']),
                'address': normalize_str(room_line['address']),
                "description": room_line['transit'],
                'page_url': room_line['page_url']
            }
        )

        room_type = normalize_str(room_line['room_type'])
        room_id = normalize_str(room_line['room_id'])

        if room_type or room_id:
            checkin_start = normalize_times(room_line, 'check_in_start')
            check_in_end = normalize_times(room_line, 'check_in_end')
            checkout = normalize_times(room_line, 'checkout')

            # AirBnb requires minimum of 2 hours difference - but lets be on the safe side
            checkin_start, check_in_end = enforce_3_hours_difference(checkin_start, check_in_end)

            BookingComHotelRoom.objects.get_or_create(
                booking_com_hotel = b_com_hotel,
                room_type = room_type,
                booking_com_id = room_id,
                defaults={
                    "raw_data": room_line,
                    "checkin_start": checkin_start,
                    "checkin_end": check_in_end,
                    "checkout": checkout,
                }
            )
