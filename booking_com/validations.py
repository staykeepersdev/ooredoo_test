import csv

def validate_mandatory_hotel_level_csv_fields(csv_file):
    # note: current_time is always 1st column when scrapped
    HOTEL_LEVEL_REQUIRED_FIELDS = [
        'b_com_hotel_id',
        'name',
        'country',
        'city',
        'address',
        'transit',
        'page_url',
        'extra_guest_fee'
    ]

    rooms_dict = csv.DictReader([line.decode('utf8') for line in csv_file], delimiter=",")
    csv_fields = rooms_dict.fieldnames

    # sort
    csv_fields.sort(), HOTEL_LEVEL_REQUIRED_FIELDS.sort()

    missing_fields = [f for f in HOTEL_LEVEL_REQUIRED_FIELDS if f not in set(csv_fields)]
    return missing_fields

# @todo
def validate_optional_room_level_csv_fields(room_line):
    # optional: required fields for room level
    ROOM_LEVEL_REQUIRED_FIELDS = [
        'room_type',
        'check_in_start',
        'check_in_end',
        'checkout'
    ]
