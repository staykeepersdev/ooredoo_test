from django.contrib import admin

from .models import BookingComData, BookingComHotel, BookingComHotelRoom

class BookingComHotelInline(admin.TabularInline):
    model = BookingComHotel
    extra = 0
    raw_id_fields = ('hotel',)
    exclude = ('booking_com_data_id',)
    show_change_link = True


@admin.register(BookingComData)
class BookingComDataAdmin(admin.ModelAdmin):
    inlines = [BookingComHotelInline]
    list_display= ('pk', 'data_file', 'uploaded_at', 'hotels_count')

    def hotels_count(self,obj):
        return obj.b_com_hotels.count()


class BookingComHotelRoomInline(admin.TabularInline):
    model = BookingComHotelRoom
    extra = 0
    raw_id_fields = ('hotel_room',)
    exclude = ('booking_com_hotel_id',)
    show_change_link = True


@admin.register(BookingComHotel)
class BookingComHotelAdmin(admin.ModelAdmin):
    inlines = [BookingComHotelRoomInline]
    list_display = ('booking_com_id', 'title', 'hotel', 'city', 'country', 'page_url')
    list_filter = ('city', 'country')
    search_fields = ('booking_com_id', 'title')
    raw_id_fields = ('hotel',)


@admin.register(BookingComHotelRoom)
class BookingComHotelRoomAdmin(admin.ModelAdmin):
    raw_id_fields = ('hotel_room',)
    list_display = ('booking_com_hotel','room_type', 'booking_com_id')