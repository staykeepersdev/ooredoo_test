from django.db import models

from django.contrib import messages
from django.contrib.postgres.fields import JSONField

from hotels.models import (
    Hotel,
    HotelRoom
)
from .validations import validate_mandatory_hotel_level_csv_fields
from .services import extract_booking_com_json_data

class BookingComData(models.Model):
    data_file = models.FileField(upload_to='booking_com')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__data_file_name = self.data_file.name

    def save(self, *args, **kwargs):
        extract_json = False
        if not self.id:
            extract_json = True            
        elif self.data_file.name and self.data_file.name != self.__data_file_name:
            extract_json = True
        
        super().save(*args, **kwargs)
        
        if extract_json:
            with self.data_file.open('rb') as csv_file:

                # Validate for missing or incorrect fields
                missing_fields = validate_mandatory_hotel_level_csv_fields(csv_file)
                if len(missing_fields):
                    print(f"Missing fields: {missing_fields}")
                    # @todo messages.add_message(request, f"Missing fields: {missing_fields}", level=messages.ERROR)

                extract_booking_com_json_data(self, csv_file)
    
    class Meta():
        verbose_name_plural='Booking com data'


class BookingComHotel(models.Model):

    booking_com_data = models.ForeignKey(
        BookingComData,
        on_delete=models.CASCADE,
        related_name='b_com_hotels'
    )

    hotel = models.OneToOneField(
        Hotel,
        on_delete=models.CASCADE,
        related_name='b_com_hotel',
        blank=True,
        null=True
    )

    title = models.CharField(max_length=2048,)
    booking_com_id = models.CharField(max_length=255)
    country = models.CharField(max_length=2048)
    city = models.CharField(max_length=2048)
    address = models.CharField(max_length=2048, blank=True, null=True)
    description = models.TextField(blank=True,null=True)

    page_url = models.CharField(max_length=2048, blank=True, null=True)

    def __str__(self):
        return self.title
    

class BookingComHotelRoom(models.Model):

    booking_com_hotel = models.ForeignKey(
        BookingComHotel,
        on_delete=models.CASCADE,
        related_name='b_com_rooms'
    )

    hotel_room = models.OneToOneField(
        HotelRoom,
        on_delete=models.CASCADE,
        related_name='b_com_room',
        blank=True,
        null=True,
    )

    room_type = models.CharField(max_length=255, blank=True, null=True)
    booking_com_id = models.CharField(max_length=255, blank=True, null=True)
    checkin_start = models.CharField(max_length=16, blank=True, null=True)
    checkin_end = models.CharField(max_length=16, blank=True, null=True)
    checkout = models.CharField(max_length=16, blank=True, null=True)
    description = models.TextField(blank=True,null=True)
    raw_data = JSONField(null=True, blank=True)
