from django.apps import AppConfig


class BookingComConfig(AppConfig):
    name = 'booking_com'
