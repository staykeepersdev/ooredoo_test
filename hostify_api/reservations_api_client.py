from .base import HostifyAPIClient


class ReservationsApiClient(HostifyAPIClient):
    def __init__(self):
        super().__init__(endpoint='reservations/')

    def get_reservations(self, params=None):
        return self.get_list(params=params)

    def get_reservation(self, reservation_id: int, params=None):
        self.endpoint = 'reservations/'
        return self.get_item(reservation_id, params=params)
    
    def accept_reservation(self, data=None):
        self.endpoint = 'reservations/accept'
        return self.post_item(data=data)

    
    def decline_reservation(self, data=None):
        self.endpoint = 'reservations/decline'
        return self.post_item(data=data)

    
    def pre_approve_reservation(self, data=None):
        self.endpoint = 'reservations/pre_approve'
        return self.post_item(data=data)
    
    def special_offer(self, data=None):
        self.endpoint = 'reservations/special_offer'
        return self.post_item(data=data)
    
    def update_custom_field(self, data=None):
        self.endpoint = 'reservations/custom_field_update'
        return self.post_item(data=data)
        
        # FOR TESTS
        # return {
        #     "success": True,
        #     "error": "Reservation status does not allow updates!"
        # }