from .base import HostifyAPIClient


class ListingAPIClient(HostifyAPIClient):

    def __init__(self):
        super().__init__(endpoint='/listings/')    

    # Users related
    def user_add_listing(self, data):
        self.endpoint = 'users/add_listing'
        return self.post_item(data=data)

    # Create listing related
    def process_location(self, data):
        self.endpoint = 'listings/process_location'
        return self.post_item(data=data)
    
    def process_layout(self, data):
        self.endpoint = 'listings/process_layout'
        return self.post_item(data=data)
    
    def process_amenities(self, data):
        self.endpoint = 'listings/process_amenities'
        return self.post_item(data=data)

    def process_translations(self, data):
        self.endpoint = 'listings/process_translations'
        return self.post_item(data=data)

    def process_booking_restrictions(self, data):
        self.endpoint = 'listings/process_booking_restrictions'
        return self.post_item(data=data)

    def process_photos(self, data):
        self.endpoint = 'listings/process_photos'
        return self.post_item(data=data)


    # Listings related
    def listings(self, page=1, per_page=100):
        self.endpoint = f'listings?page={page}&per_page={per_page}'
        return self.get_list()

    def get_listing(self, listing_id):
        self.endpoint = f'listings/'
        return self.get_item(listing_id)

    def get_listing_children(self, hostify_parent_id):
        self.endpoint = f'listings/children/{hostify_parent_id}'
        return self.get_list()


    # AirBnb related
    def clone_listing(self, data):
        self.endpoint = 'listings/clone'
        return self.post_item(data=data)

    def toggle_listing_on_airbnb(self, data):
        self.endpoint = 'listings/channel_list'
        return self.post_item(data=data)

    def get_clone_listing_state(self, job_id):
        self.endpoint = f'listings/clone/{job_id}'
        return self.get_list()

    def update_listing_on_airbnb(self, data):
        self.endpoint = 'listings/update'
        return self.post_item(data=data)


    # Custom fields related
    # @link https://app.hostify.com/docs#set-values-custom-fields
    def set_custom_fields_values(self, data):
        self.endpoint = 'custom_fields/set_values'
        return self.post_item(data=data)
