from .base import HostifyAPIClient


class CalendarAPIClient(HostifyAPIClient):

    def __init__(self):
        super().__init__(endpoint='/calendar/bulk_listings/')

    def put_calendar_days(self, identifier, data):
        self.endpoint='/calendar/bulk_listings/'
        return self.put_item(identifier=identifier, data=data)
    
    def get_calendar_day(self, params=None):
        self.endpoint= '/calendar/'
        return self.get_list(params=params)