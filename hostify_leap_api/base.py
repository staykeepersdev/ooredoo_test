from urllib.parse import urljoin

import json
import requests

from django.conf import settings


BASE_URL = 'https://pmsapi.staykeepers.co'
API_KEY = 'KuNFAC2y1mQd82wd9aJ3adM9Xmgks3ei' #'uOHXyOd0uiegVb7BfqT4uq0U6AeZmdOu'

class HostifyAPIClient:
    def __init__(self, endpoint=None,):
        self.url = BASE_URL
        self.api_key = API_KEY
        self.endpoint = endpoint
        self.headers = {
            'Content-Type': 'application/json',
            'x-api-key': self.api_key
        }
        
    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers, params=params)
        return json.loads(response.content)

    def _post_json_response(self, url: str, data=None):
        response = requests.post(url=url, headers=self.headers, json=data)
        return json.loads(response.content)
    
    def _put_json_response(self, url: str, data=None):
        response = requests.put(url=url, headers=self.headers, json=data)
        try:
            response_json = json.loads(response.content)
        except:
            response_json = {
                'success': '"success":true' in str(response.content),
                'content': response.content
            }
        return response_json

    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def detail_url(self, identifier: int):
        return urljoin(self.list_url(), str(identifier))
    
    def get_list(self, params=None):
        url = self.list_url()
        return self._get_json_response(url=url, params=params)

    def get_item(self, identifier: int,  params=None):
        url = self.detail_url(identifier)
        return self._get_json_response(url=url, params=params)
               
    def post_item(self, data=None):       
        url = self.list_url()
        return self._post_json_response(url=url, data=data)
    
    def put_item(self, identifier: int, data=None):       
        url = self.detail_url(identifier=identifier)
        return self._put_json_response(url=url, data=data)

