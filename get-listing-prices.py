import datetime
import json
import requests
import threading
import time
import os

import django
from timeit import default_timer as timer
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from collections import defaultdict as ddict
from django.core.paginator import Paginator

from calendar_app.models import (
    CalendarHotel,
    CalendarRoomType,
    CalendarDay
)

from hotelbeds_api.services import get_hotels_availability

def get_params(check_in, check_out, hotel_ids, room_codes, adults=2):
    params = {
        "stay": {
            "checkIn": check_in.strftime("%Y-%m-%d"),
            "checkOut": check_out.strftime("%Y-%m-%d")
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": adults,
                "children": 0
            }
        ],
        "hotels": {
            "hotel": hotel_ids
        },
        "rooms": {
            "included": True,
            "room": room_codes
        },
        "filter": {
            "paymentType": 'AT_WEB',
            "packaging": False
        }
    }
    return params

def check_if_day_in_database(
    calendar_days_to_add,
    calendar_days_to_update,
    hotel_calendar_days_list, 
    check_in, 
    room_code,
    room_id,
    is_available=False,
    price=0,
    response_hotel_currency='',
    
):
   
    # Check if we its new or we have to update it
    new_day = True
    for flat_day in hotel_calendar_days_list:
        if flat_day[0] == room_code:
            new_day = False

            # Update only if there is a difference
            if not(price == flat_day[1] and is_available == flat_day[2]):
                # check if price difference is lower than 3%
                if flat_day[1] != 0 and price != flat_day[1] and is_available and flat_day[2]:
                    percentage_diff = (abs(flat_day[1] - price) / flat_day[1]) * 100.0
                    if percentage_diff < 2:
                        break
                
                calendar_day_object = CalendarDay.objects.get(
                    date=check_in,
                    calendar_hotel_room_id=room_id
                )
                calendar_day_object.price = price
                calendar_day_object.is_available = is_available
                calendar_day_object.updated_at_hostify = False
                
                calendar_days_to_update.append(calendar_day_object)
            
            # WE have the Room/Date object.
            break

        
    if new_day:
        calendar_days_to_add.append(
            CalendarDay(
                date=check_in,
                calendar_hotel_room_id=room_id,
                price=price,
                currency = response_hotel_currency,
                is_available=is_available
            )
        )

def get_hotels_prices(calendar_hotels_queryset, adults=2):
    hotel_ids = list( calendar_hotels_queryset.values_list('hotelbeds_api_hotel__code', flat=True) )

    room_codes = list( CalendarRoomType.objects.filter(
        calendar_hotel__in=calendar_hotels_queryset
    ).filter(
        calendar_rooms__auto_pricing=True
    ).values_list('code', flat=True).distinct('code') )

    hotel_rooms_list = CalendarRoomType.objects.filter(
        calendar_hotel__in=calendar_hotels_queryset
    ).filter(
        calendar_rooms__auto_pricing=True
    ).values_list('hotelbeds_hotel_id','code','id').distinct()

    # Hotels and their rooms with ids and code
    hotel_rooms_dict = ddict(list)
    for hotel_id, room_code, room_id in hotel_rooms_list:
        hotel_rooms_dict[hotel_id].append({
            'room_code': room_code,
            'id': room_id,
        })


    starting_date = datetime.date.today()
    for day_in_advance in range(1,91):

        check_in = starting_date + datetime.timedelta(days=day_in_advance)
        check_out = check_in + datetime.timedelta(days=1)

        # All Object for this date
        calendar_days_queryset = CalendarDay.objects.filter(
            date=check_in
        ).all()

        params = get_params(check_in, check_out, hotel_ids, room_codes, adults)
        
        ## Get Availability for all hotels and the specified room_types  for 1 date only
        # Patch for exceeded limit 
        while True:
            hb_response =  get_hotels_availability(params)
            if 'error' in hb_response:
                time.sleep(2)
            else:
                break
        try:
            hotels_raw_data = hb_response.get('hotels').get('hotels')
        except Exception as e:            
            hotels_raw_data = dict()
            break
        
        # ## Execution timer
        # # start = timer()
        not_available_hotels_ids = hotel_ids.copy()

        calendar_days_to_add = []
        calendar_days_to_update = []
        if hotels_raw_data:
            for hotel_response in hotels_raw_data:
                
                response_hotel_id = hotel_response['code']
                not_available_hotels_ids.remove(response_hotel_id)

                # Prefetched list of calendar_days for each hotel. We will split calendar object to: create/update
                hotel_calendar_days_list = list(calendar_days_queryset.filter(
                    calendar_hotel_room__hotelbeds_hotel_id=response_hotel_id
                ).values_list('calendar_hotel_room__code', 'price', 'is_available'))
                
                
                response_hotel_currency = hotel_response['currency']
                response_rooms = hotel_response['rooms']
                
                hotel_rooms = hotel_rooms_dict[str(response_hotel_id)]

                # Loop over database specified rooms
                for room in hotel_rooms:
                    room_id = room['id'] 
                    room_code = room['room_code']
                    room_found = False

                    ## Check all response hotel rooms for a match with the selected DB Room
                    for response_room in response_rooms:
                        if response_room['code'] == room_code:
                            
                            # Get the minimum price returned
                            prices = []
                            for rate in response_room['rates']:
                                prices.append(float(rate['net']))
                            price = min(prices)
                            is_available=True

                            check_if_day_in_database(
                                calendar_days_to_add,
                                calendar_days_to_update,
                                hotel_calendar_days_list,
                                check_in,
                                room_code,
                                room_id,
                                is_available,
                                price,
                                response_hotel_currency,
                            )
                            
                            # Room is found | We will continue with the next room
                            room_found = True
                            break
                    
                    ## Room isnt in the response. Room is unavailable
                    if not room_found:       
                        check_if_day_in_database(
                            calendar_days_to_add,
                            calendar_days_to_update,
                            hotel_calendar_days_list,
                            check_in,
                            room_code,
                            room_id
                        )

        ## If there are hotels with not a single empty room
        for hotel_id in not_available_hotels_ids:

            # Prefetched list of calendar_days for each hotel. We will split calendar object to: create/update
            hotel_calendar_days_list = list(calendar_days_queryset.filter(
                calendar_hotel_room__hotelbeds_hotel_id=hotel_id
            ).values_list('calendar_hotel_room__code', 'price', 'is_available'))
            hotel_rooms = hotel_rooms_dict[str(hotel_id)]
            for room in hotel_rooms:
                room_id = room['id']
                room_code = room['room_code']
                check_if_day_in_database(
                    calendar_days_to_add,
                    calendar_days_to_update,
                    hotel_calendar_days_list,
                    check_in,
                    room_code,
                    room_id
                )

        # End Timer
        # end = timer()
        # print(end - start)
        
        # Bulk Create
        if calendar_days_to_add:
            CalendarDay.objects.bulk_create(calendar_days_to_add, ignore_conflicts=True)
            
        # Bulk Update
        if calendar_days_to_update:
            CalendarDay.objects.bulk_update(calendar_days_to_update, ['price', 'is_available', 'updated_at_hostify'])

def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

def loop_over_paginated_queryset(paginator, adults=2):
    for page in range(1, paginator.num_pages + 1):
        # Check running threads before starting new one
        while threading.active_count() >= MAX_THREADS:
            time.sleep(2)
        calendar_hotels_queryset = paginator.page(page).object_list
        run_threaded(get_hotels_prices, (calendar_hotels_queryset, adults) )

HOTELS_PER_THREAD = 1000
MAX_THREADS = 8 # Includes the main thread as as well (-1)

calendar_hotels = CalendarHotel.objects.filter(
    calendar_room_types__calendar_rooms__auto_pricing=True
).filter(hotelbeds_api_hotel__price_for_one=False).order_by('id').distinct()

calendar_hotels_priced_one = CalendarHotel.objects.filter(
    calendar_room_types__calendar_rooms__auto_pricing=True
).filter(hotelbeds_api_hotel__price_for_one=True).order_by('id').distinct()

paginator = Paginator(calendar_hotels, HOTELS_PER_THREAD)

paginator_priced_one = Paginator(calendar_hotels_priced_one, HOTELS_PER_THREAD)

loop_over_paginated_queryset(paginator)
loop_over_paginated_queryset(paginator_priced_one, adults=1)