import django
import os
import re
import sys
import zipfile

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from scripts.cache_api.utils import (
    get_cache_files,
    extract_zip
)

rates_folder, zip_filename, version = get_cache_files(rates='update')

print(f"""

# ZIP FILE NAME: {zip_filename} 
Version: {version}

""")
extract_zip(rates_folder, zip_filename, version)

# ZIP FILE NAME: id_7803-20200722183339.zip
# Version: 1595392395