import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

if len(sys.argv) != 2:
    sys.exit('MISSING VERSION !')

# Init API Client
from hotelbeds_api.clients.cache_api_client import CacheFileAPIClient
CACHE_FILE_API_CLIENT = CacheFileAPIClient()

# Get the Version as command-line argument
VERSION = sys.argv[1]

# Confirm Cache Version
response = CACHE_FILE_API_CLIENT.confirm_version(VERSION)
if response.status_code == 200:
    print('\n# Successfully Confirmed Version !\n')
    print(f'Response: {response.content}')