import os
import re

# Blocks regex
def get_blocks(rates_content):
    regex = re.compile(r'''
        \{(....)\}
        (.*?)
        \{/....\}
        ''', re.VERBOSE | re.DOTALL) # re.MULTILINE might be needed

    blocks = regex.findall(rates_content)

    # remove white space for each block
    blocks_dict = { code:block.strip() for code, block in dict(blocks).items() }
    return blocks_dict


def read_blocks_from_file(filepath):
    with open(filepath, 'r') as f:
        rates_content = f.read()
        return get_blocks(rates_content)


def find_contract_by_id_and_hotel_code(in_folder, contract_id_lookup, hotel_code_lookup):
    
    # Iterate through all contract files
    for subdir, dirs, files in os.walk(in_folder):
        for filename in files:
            office_code, contact_id, payment_model, f, hotel_code = filename.split('_')
            contact_id, hotel_code = int(contact_id), int(hotel_code)
            
            # Skip empty contract files
            if hotel_code == 'EMPTY': continue

            # We found matches ...
            if hotel_code == hotel_code_lookup and contact_id == contract_id_lookup:
                print(filename)
                filepath = os.path.join(subdir, filename)
                return filepath
