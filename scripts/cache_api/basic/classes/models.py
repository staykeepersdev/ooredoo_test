class BaseRoomDataModel:
    
    def __init__(self):
        self.full_room_code = f'{self.room_code}-{self.characteristic}'

        super().__init__()


class RoomType(BaseRoomDataModel):
    
    def __init__(self, raw_room_data):
        self.raw_room_data = raw_room_data
        
        # Convert to Human-Readable format        
        room_data = self.raw_room_data.strip().split(':')

        self.room_code = room_data[0]
        self.characteristic = room_data[1]
        
        self.standard_capacity = int(room_data[2] or 0)
        self.minimum_pax = int(room_data[3] or 0)
        self.maximum_pax = int(room_data[4] or 0)

        self.maximum_adults = int(room_data[5] or 0)
        self.maximum_children = int(room_data[6] or 0)
        self.maximum_infants = int(room_data[7] or 0)
        
        # New fields
        self.minimum_adults = int(room_data[8] or 0)
        self.minimum_children = int(room_data[9] or 0)

        # Assume we need at least 1 room
        self.rooms_required = 1

        super().__init__()

    # Magic stuff
    def __eq__(self, other):
        if isinstance(other, str): return

        # Assume equality
        equality = True
        
        if self.full_room_code != other.full_room_code: equality = False
        if self.standard_capacity != other.standard_capacity: equality = False
        
        if self.minimum_pax != other.minimum_pax: equality = False
        if self.maximum_pax != other.maximum_pax: equality = False
        
        if self.minimum_adults != other.minimum_adults: equality = False
        if self.maximum_adults != other.maximum_adults: equality = False

        return equality