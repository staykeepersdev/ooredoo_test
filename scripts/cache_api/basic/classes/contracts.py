from scripts.cache_api.basic.classes.models import RoomType

class ContractBase:

    def __init__(self, blocks):
        self._blocks = blocks

        # Do some initial work 
        self.init_room_types()

    def init_room_types(self):
        self.ROOM_TYPES = [ RoomType(raw_room_data) for raw_room_data in self.CNHA.splitlines() ]
        self.ROOM_CODES = [ room_type.full_room_code for room_type in self.ROOM_TYPES ]

    # Magic stuff
    def __contains__(self, key):
        return key in self.ROOM_CODES

    def find_room_type_by_code(self, room_code):
        for room_type in self.ROOM_TYPES:
            if room_type.full_room_code == room_code:
                return room_type

    @property
    def CCON(self): return self._blocks['CCON']
    
    @property
    def CNHA(self): return self._blocks['CNHA']

    @property
    def CNIN(self): return self._blocks['CNIN']

    @property
    def CNCT(self): return self._blocks['CNCT']

    @property
    def CNSR(self): return self._blocks['CNSR']

    @property
    def CNSU(self): return self._blocks['CNSU']

    @property
    def CNCF(self): return self._blocks['CNCF']

    def get_block(self, block):
        return self._blocks[block]

class ContractBefore(ContractBase):
    pass


class ContractNow(ContractBase):
    pass