from itertools import zip_longest
from difflib import unified_diff
from pprint import pprint

from scripts.cache_api.basic.classes.contract_header import ContractHeader
from scripts.cache_api.basic.classes.models import RoomType

class ContractDiffer:
    UPDATE_TYPES = ['micro', 'daily', 'data']
    TODO = {
        'ACTIVATE_ROOMS': set(),
        'DEACTIVATE_ROOMS': set(),
        'RECOMPUTE_ROOMS': set()
    }
    
    # What update level is required
    FORCE_ROOMS_UPDATE = None
    # FORCE_BOARDS_UPDATE = None
    # FORCE_FULL_UPDATE = None

    def __init__(self, contract_before, contract_now, existing_rooms_only):
        self.CONTRACT_BEFORE = contract_before
        self.CONTRACT_NOW = contract_now
        self.EXISTING_ROOMS_ONLY = existing_rooms_only

        self.CONTRACT_BLOCKS_RETAINED = {
            'CCON': None,
            'CNHA': None,
            'CNIN': None,
            'CNCT': None,
            'CNSR': None,
            'CNSU': None,
            'CNCF': None
        }

    def are_all_blocks_retained(self):
        # Assumme all blocks remain the same ...
        # Mostly true when the updates are happening every 2 hours
        ALL_BLOCKS_RETAINED = True

        # Basic blocks check
        for block in self.CONTRACT_BLOCKS_RETAINED.keys():
            if self.CONTRACT_BEFORE.get_block(block) == self.CONTRACT_NOW.get_block(block):
                self.CONTRACT_BLOCKS_RETAINED[block] = True
            else:
                self.CONTRACT_BLOCKS_RETAINED[block] = False
                ALL_BLOCKS_RETAINED = False

        return ALL_BLOCKS_RETAINED

    
    # Check all updated blocks on a 1-by-1 basis ...
    def detect_updated_blocks(self):
        CONTRACT_BLOCKS_RETAINED = self.CONTRACT_BLOCKS_RETAINED
        
        # 1. Detect Contract Header updates - {CCON}
        if not CONTRACT_BLOCKS_RETAINED['CCON']:
            self.detect_contract_header_updates()
        
        # 2. Detect Room Type updates - {CNHA}
        if not CONTRACT_BLOCKS_RETAINED['CNHA']:
            self.detect_room_types_updates()


    def detect_contract_header_updates(self):
        # CONTRACT_BEFORE_HEADER = ContractHeader( self.CONTRACT_BEFORE.CCON )
        # CONTRACT_NOW_HEADER = ContractHeader( self.CONTRACT_NOW.CCON )

        IGNORE_INDEXES_AFTER = 26 # Ignore Contract Header values after index 26
        RATE_DEPENDENT_INDEXES = {
            '9': 'Initial date',
            '10': 'End date',
        }
        
        header_before_list = self.CONTRACT_BEFORE.CCON.split(':')[:IGNORE_INDEXES_AFTER]
        header_now_list = self.CONTRACT_NOW.CCON.split(':')[:IGNORE_INDEXES_AFTER]

        index, updated_indexes = 0, []
        for before, now in zip(header_before_list, header_now_list):
            # Add all non-matching values
            if before != now:
                updated_indexes.append(index)
            
            index += 1

        print(f'\n# CONTRACT HEADER UPDATES : updated values {len(updated_indexes)}')
        for updated_index in updated_indexes:
            print(f'Updated: {RATE_DEPENDENT_INDEXES[str(updated_index)]}')
            return True


    def detect_room_types_updates(self):
        # Shortcuts
        existing_rooms_only = self.EXISTING_ROOMS_ONLY
        room_types_before = self.CONTRACT_BEFORE.ROOM_TYPES
        room_types_now = self.CONTRACT_NOW.ROOM_TYPES

        print(f'\n# ROOM TYPES UPDATES')
        for existing_room_code in existing_rooms_only:
            
            # Get temporal room state
            room_before = 'PRESENT' if existing_room_code in room_types_before else 'REMOVED'
            room_now = 'PRESENT' if existing_room_code in room_types_now else 'REMOVED'

            # Define actions based on room state progression ...
            if room_before == 'PRESENT' and room_now == 'REMOVED':
                self.TODO['DEACTIVATE_ROOMS'].add(existing_room_code)
                self.FORCE_ROOMS_UPDATE = True
                continue

            if room_before == 'REMOVED' and room_now == 'PRESENT':
                self.TODO['ACTIVATE_ROOMS'].add(existing_room_code)
                self.FORCE_ROOMS_UPDATE = True
                continue

            if room_before == 'PRESENT' and room_now == 'PRESENT':
                # Check if room data is updated
                room_type_before = room_types_before.find_room_type_by_code(existing_room_code)
                room_type_now = room_types_now.find_room_type_by_code(existing_room_code)

                if room_type_before != room_type_now:
                    self.TODO['RECOMPUTE_ROOMS'].add(existing_room_code)
                continue

            if room_before == 'REMOVED' and room_now == 'REMOVED':
                # Ensure these rooms are Deactivated
                self.TODO['DEACTIVATE_ROOMS'].add(existing_room_code)
                self.FORCE_ROOMS_UPDATE = True
                continue
