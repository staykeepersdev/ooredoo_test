import os
import sys

import django
sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from scripts.cache_api.basic.utils import (
    find_contract_by_id_and_hotel_code,
    read_blocks_from_file
)

from scripts.cache_api.basic.classes.contracts import ContractBefore, ContractNow
from scripts.cache_api.basic.classes.contract_differ import ContractDiffer

# Compare Update Rates ...
VERSION_BEFORE = 1595929232
VERSION_NOW = 1596001232

DIR_UPDATE_RATES = 'extracted/production/update_rates/'

# Before & after
DIR_UPDATE_RATES_BEFORE = f'{DIR_UPDATE_RATES}VERSION_{VERSION_BEFORE}/DESTINATIONS/'
DIR_UPDATE_RATES_NOW = f'{DIR_UPDATE_RATES}VERSION_{VERSION_NOW}/DESTINATIONS/'


# Find contract of specific hotel

## Multiple contracts per hotel within a single payload
## HOTEL_CODE = 211993 # 386271 

## Single contract per hotel within a single payload
HOTEL_CODE = 200168 # 293635, 200168, 472841, 163917, 293709, 186093, 585906, 117173, 587094, 601818
CONTRACT_ID = 10014

# For debugging
print(f'''
VERSION_BEFORE: {VERSION_BEFORE}
VERSION_NOW: {VERSION_NOW}
HOTEL_CODE: {HOTEL_CODE}
''')


# Get filepaths to contracts
contract_filepath_before = find_contract_by_id_and_hotel_code(DIR_UPDATE_RATES_BEFORE, CONTRACT_ID, HOTEL_CODE)
contract_filepath_now = find_contract_by_id_and_hotel_code(DIR_UPDATE_RATES_NOW, CONTRACT_ID, HOTEL_CODE)

# Print filepaths for testing
print(contract_filepath_before, contract_filepath_now, sep='\n')

# Real all blocks from each contract
blocks_before = read_blocks_from_file(contract_filepath_before)
blocks_now = read_blocks_from_file(contract_filepath_now)

# Create Contract Objects
CONTRACT_BEFORE = ContractBefore(blocks_before)
CONTRACT_NOW = ContractNow(blocks_now)

# Create a Differ object to compare both contracts
existing_rooms_only = ['DBL-OM', 'DBL-ST']
CONTRACT_DIFFER = ContractDiffer(CONTRACT_BEFORE, CONTRACT_NOW, existing_rooms_only)
if CONTRACT_DIFFER.are_all_blocks_retained():
    print('The contract is not updated !')
    # Continue with the next contract ...
    sys.exit()


# Check all updated blocks on a 1-by-1 basis:
CONTRACT_DIFFER.detect_updated_blocks()