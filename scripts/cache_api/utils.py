import os
import sys
import zipfile

from django.conf import settings
HOTELBEDS_ENVIRONMENT = settings.HOTELBEDS_ENVIRONMENT

from hotelbeds_api.services import get_filename_from_content_disposition

# Init API Client
from hotelbeds_api.clients.cache_api_client import CacheFileAPIClient
CACHE_FILE_API_CLIENT = CacheFileAPIClient()

def get_cache_files(rates='full'):
    rates_folder = f'{HOTELBEDS_ENVIRONMENT}/{rates}_rates/'

    try:
        if rates == 'full': file_response = CACHE_FILE_API_CLIENT.get_full_rates()
        if rates == 'update': file_response = CACHE_FILE_API_CLIENT.get_update_rates()

        version = file_response.headers.get('X-Version', None)
        if not version:
            print('\n# ERROR: NO VERSION\n')
            
            try:
                json_response = file_response.json()
                print(
                    f'Status: {json_response["status"]}',
                    f'Error: {json_response["error"]}',
                    f'Message: {json_response["message"]}',
                    sep='\n'
                )
            except:
                print('# ERROR: UNABLE TO JSON PARSE')
                sys.exit()

            sys.exit()

        # Create VERSION-TAGGED folder name
        rates_folder += f'VERSION_{version}/'
        try:
            os.mkdir(f'downloads/{rates_folder}')
        except OSError:
            print (f'Creation of the {rates_folder} failed')

        zip_filename = get_filename_from_content_disposition(file_response.headers.get('content-disposition'))
        if not zip_filename:
            print('ERROR: no file')
            sys.exit()

        # Save zip on disk
        with open(f'downloads/{rates_folder}{zip_filename}', 'wb') as file:
            file.write(file_response.content)

        return rates_folder, zip_filename, version
    
    except Exception as e:
        print(f'EXCEPTION: {str(e)}')
        sys.exit()


def get_file_size(filepath):
    return os.stat(filepath).st_size


def extract_hotel_code_from_contract(contract_file):
    lines = contract_file.readlines()
    contract_header = lines[1]
    
    hotel_code = contract_header.split(':')[7]
    return hotel_code


# Appends _HOTEL_CODE or _EMPTY to each extracted contract file name
def suffix_filename(filepath, hotel_code=None):
    suffix = hotel_code if hotel_code else 'EMPTY'
    os.rename(filepath, f'{filepath}_{suffix}')


def extract_zip(rates_folder, zip_filename, version):
    downloads_folder = f'downloads/{rates_folder}'
    zip_path = downloads_folder + zip_filename

    contract_count = 0
    with zipfile.ZipFile(zip_path, 'r') as archive:
        for file in archive.namelist():
            archive.extract(file, f'extracted/{rates_folder}')
            extracted_contract_path = f'extracted/{rates_folder}{file}'

            if get_file_size(extracted_contract_path) > 0: # Valid file

                hotel_code = None
                with open(extracted_contract_path, 'r') as contract_file:
                    hotel_code = extract_hotel_code_from_contract(contract_file)

                if hotel_code: suffix_filename(extracted_contract_path, hotel_code)
            
            else: # Empty file
                suffix_filename(extracted_contract_path)

            contract_count += 1
    
    print('Contract count: ', contract_count)