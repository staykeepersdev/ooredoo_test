import django
import os
import re
import sys

from django.utils import timezone


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom
from calendar_app.static_data.hostify_custom_fields_ids import HOSTIFY_CUSTOM_FIELDS_IDS

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()


# Get list of all rooms listed at AIRBNB
calendar_rooms = CalendarRoom.objects.all().filter(hostify_child_id__isnull=False)
now = timezone.now()
for calendar_room in calendar_rooms:

    response = ''

    try:
        # Bulk update of date listed in Django
        if not calendar_room.listed_at:
            if calendar_room.cloning_updated_at:
                calendar_room.listed_at = calendar_room.cloning_updated_at
            else:
                calendar_room.listed_at = timezone.now()

        # Bulk update of date listed in Hostify - via Listing custom field
        response = LISTING_API_CLIENT.set_custom_fields_values({
            "custom_field_id": HOSTIFY_CUSTOM_FIELDS_IDS['AIRBNB_DATE_LISTED'],
            "value": calendar_room.listed_at.strftime("%Y-%m-%d"),
            "listing_ids": [calendar_room.hostify_id]
        })

        calendar_room.save()
    except Exception as e:
        print(e)
        # print(
        #     f'# ERROR: HOSTIFY CUSTOM FIELD UPDATE\n',
        #     f'For calendar room: {calendar_room.id} - {calendar_room.title}\n',
        #     f'Response: {response}\n\n'
        # )
        continue