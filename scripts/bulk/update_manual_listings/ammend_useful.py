import os
import random
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Consts
from calendar_app.static_data.nickname_adjectives import ADJECTIVES
from calendar_app.static_data.zones import ZONES

# Listing API Client - for direct updates
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Models
from calendar_app.models import CalendarRoom
from hotels.models import HotelRoom

from calendar_app.services import (
    get_booking_restrictions_params,
    process_part
)

# Hostify IDs
MANUAL_LISTINGS_WITH_ENQUIRIES_RESERVATIONS = [26109, 24343, 24680, 24691, 24692, 24693, 24694, 24695, 24697, 24836, 25368, 25374, 25375, 25377, 25378, 25379, 25673, 25674, 25675, 25676, 25677, 25950, 26047, 26054, 26061, 26091, 26181, 26192, 26196, 26197, 26198, 25678, 25711]

def build_translation_name(hotelbeds_api_hotel, hotel, room):
    zone_number = str(hotelbeds_api_hotel.data.get('zoneCode'))
    zone_name = ZONES.get(hotel.destination_code).get(zone_number)

    name = f'{random.choice(ADJECTIVES)} {room.description} at {zone_name}'.title()

for manual_listing in MANUAL_LISTINGS_WITH_ENQUIRIES_RESERVATIONS:
    try:
        calendar_room = CalendarRoom.objects.get(hostify_id=manual_listing)
        hostify_id = manual_listing

        print(f'Update Manual Listing ... with room ID {calendar_room.id} ... with Hostify ID {manual_listing}')

        # Kitchen sink
        calendar_room_type = calendar_room.calendar_room_type
        calendar_hotel = calendar_room_type.calendar_hotel
        hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
        hotel = hotelbeds_api_hotel.hotel
        
        room_code = calendar_room_type.code
        hotel_name = hotel.name

        # 1. Auto-create request parameters for translations and booking restrictions

        ## Set the listing name in the Location Params - not sure if this can be updated currently ?
        # calendar_room.request_data['location_params']['name'] = calendar_room.title

        ## Set listing ID, title & description
        room = HotelRoom.objects.filter(hotel=hotel, room_code=room_code).first()
        # calendar_room.request_data['translations_params']['name'] = build_translation_name(hotelbeds_api_hotel, hotel, room)
        ## We already have ID & description for all Manual Listings - should we overwrite these ?
        
        ## Set booking restrictions, max capacity, and extra guest fee
        b_com_room, amenities = None, []
        calendar_room.request_data['booking_restrictions_params'] = {}
        calendar_room.request_data['booking_restrictions_params'] = get_booking_restrictions_params(
            hostify_id,
            room,
            b_com_room,
            amenities
        )
        # calendar_room.request_data['booking_restrictions_params']['checkin_start'] = calendar_room.request_data['booking_restrictions_params']['checkin_start'][:5]
        # calendar_room.request_data['booking_restrictions_params']['checkin_end'] = calendar_room.request_data['booking_restrictions_params']['checkin_end'][:5]
        # calendar_room.request_data['booking_restrictions_params']['checkout'] = calendar_room.request_data['booking_restrictions_params']['checkout'][:5]
        
        # 2. Save to Django
        calendar_room.save()


        # 3. NOT NEEDED: Apply Updates in Hostify
        
        ## 3.1. Apply Translations updates in Hostify
        # response, errors = process_part(calendar_room.hostify_id, 'translations_params', calendar_room.request_data['translations_params'])
        # if not response['success']:
        #     print(f'# ERROR: Translations updates of {calendar_room.id}: {errors}')
        #     break

        ## 4. DIRECT AirBnb Update
        if calendar_room.hostify_id and calendar_room.hostify_id != '0':
            update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                'listing_id': calendar_room.hostify_id,

                # Additional values provided by Hostify
                # 'price_markup': # optional
                # 'extra_person': # optional
                # 'guests_included': # optional
                # 'security_deposit': # optional
                # 'cancel_policy': # optional
                # 'instant_booking': # optional

                # 'min_notice_hours': # optional
                # 'max_notice_days': # optional

                "checkin_start": calendar_room.request_data['booking_restrictions_params']['checkin_start'],
                "checkin_end": calendar_room.request_data['booking_restrictions_params']['checkin_end'],
                "checkout": calendar_room.request_data['booking_restrictions_params']['checkout']
            })
            
            if not update_response['success']:
                print(f'# ERROR: DIRECT AIRBNB Booking Restrictions updates of {calendar_room.id} / Hostify Id {calendar_room.hostify_id}: {str(update_response)}')

        ## 5. Apply Booking Restrictions updates in Hostify
        response, errors = process_part(int(calendar_room.hostify_id), 'booking_restrictions_params', calendar_room.request_data['booking_restrictions_params'])
        if not response['success']:
            print(f'# ERROR: HOSTIFY Booking Restrictions updates of {calendar_room.id} / Hostify Id {calendar_room.hostify_id}: {errors}')

        # sys.exit()

    except Exception as e:
        print(f'EXCEPTION with Calendar Room {calendar_room.id} / Hostify Id {calendar_room.hostify_id}: {str(e)}')
        # break
