import os
import re
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Calendar App
from calendar_app.services import process_part
from calendar_app.models import CalendarRoom

calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['amenities_params', 'translations_params']
).all().order_by('id')

SEARCH_FOR = ['Hot Tub', 'Hot tub', 'hot tub', 'Jacuzzi', 'jacuzzi']

# Utils
def replace_jacuzzi(string, is_amenity=False):
    if is_amenity:
        for needle in SEARCH_FOR:
            string = string.replace(needle, 'Bathroom')

        return string

    string = string.replace('Hot Tub', 'Shower or Bath').replace('Hot tub', 'Shower or bath').replace('hot tub', 'shower or bath')
    string = string.replace('Jacuzzi', 'Shower or Bath').replace('jacuzzi', 'shower or bath')

    return string

# Results
updated_room_amenities = set()
updated_room_name_summary = set()
error_listings = {}
exception_listings = {}
for calendar_room in calendar_rooms:
    print(f'# Remove Jacuzzi from room {calendar_room.id} ... Hostify ID {calendar_room.hostify_id} ... Hostify Child Id {calendar_room.hostify_child_id}')
    try:
        # Locations params ... do nothing

        # 1. Amenities params+
        FOUND_IN_AMENITIES = None
        if calendar_room.request_data.get('amenities_params', False):
            amenities_params = calendar_room.request_data['amenities_params']
            room_amenities = amenities_params.get('amenities', [])
            
            # Step 1.0: Update in Django
            for idx, room_amenity in enumerate(room_amenities):
                if any(substring in room_amenity for substring in SEARCH_FOR):
                    FOUND_IN_AMENITIES = True
                    room_amenities[idx] = replace_jacuzzi(room_amenities[idx], is_amenity=True)

            calendar_room.request_data['amenities_params']['amenities'] = list(set(room_amenities))
            calendar_room.save()

            if FOUND_IN_AMENITIES:
                # 1.1. Update Parent in Hostify
                if calendar_room.hostify_id and calendar_room.hostify_id != '0':
                    response11, errors = process_part(calendar_room.hostify_id, 'amenities', calendar_room.request_data['amenities_params'])
                    
                    if response11['success']:
                        updated_room_amenities.add(calendar_room.id)
                    else:
                        error = f'# ERROR - AMENITIES: Update in Hostify Parent of {calendar_room.id}: {str(errors)}'
                        error_listings[calendar_room.id] = error
                        print(error)

                # 1.2. Update Child in Hostify
                if calendar_room.hostify_child_id != None and int(calendar_room.hostify_child_id) != 0:
                    response12, errors = process_part(calendar_room.hostify_child_id, 'amenities', calendar_room.request_data['amenities_params'])

                    if response12['success']:
                        updated_room_amenities.add(calendar_room.id)
                    else:
                        error = f'# ERROR - AMENITIES: Update in Hostify Child of {calendar_room.id}: {str(errors)}'
                        error_listings[calendar_room.id] = error
                        print(error)

        # 2. Translations params+
        FOUND_IN_TRANSLATIONS = None
        if calendar_room.request_data.get('translations_params', False):
            translations_params = calendar_room.request_data['translations_params']
            
            # Step 2.0: Update in Django
            if translations_params.get('name', False):
                name = translations_params['name']
                name_replaced = replace_jacuzzi(name)

                if len(name) != len(name_replaced):
                    FOUND_IN_TRANSLATIONS = True
                    calendar_room.request_data['translations_params']['name'] = name_replaced

            if translations_params.get('summary', False):
                summary = translations_params['summary']
                summary_replaced = replace_jacuzzi(summary)

                if len(summary) != len(summary_replaced):
                    FOUND_IN_TRANSLATIONS = True
                    calendar_room.request_data['translations_params']['summary'] = summary_replaced

            calendar_room.save()

            if FOUND_IN_TRANSLATIONS:
                # Step 2.1: Update Hostify parent
                if calendar_room.hostify_id and calendar_room.hostify_id != '0':
                    response21, errors = process_part(calendar_room.hostify_id, 'translations', calendar_room.request_data['translations_params'])

                    if response21['success']:
                        updated_room_name_summary.add(calendar_room.id)
                    else:
                        error = f'# ERROR - TRANSLATIONS: Update in Hostify Parent of {calendar_room.id}: {str(errors)}'
                        error_listings[calendar_room.id] = error
                        print(error)

                # Step 2.2: Update Hostify child & Listing on AirBnb ... if we have Hostify Child ID
                if calendar_room.hostify_child_id and calendar_room.hostify_child_id != '0':
                    hotel_summary = calendar_room.request_data['translations_params']['summary']
                    
                    response22 = LISTING_API_CLIENT.update_listing_on_airbnb({
                        'listing_id': calendar_room.hostify_child_id,
                        'description': {"summary": hotel_summary}
                    })                
                    
                    if response22['success']:
                        updated_room_name_summary.add(calendar_room.id)
                    else:
                        error = f'# ERROR - TRANSLATIONS: Update in Hostify Child of {calendar_room.id}: {str(errors)}'
                        error_listings[calendar_room.id] = error
                        print(error)

    except Exception as e:
        print(f'# EXCEPTION of {calendar_room.id}: {str(e)}')
        exception_listings[calendar_room.id] = str(e)
        continue

print(f'\n# UPDATED ROOM AMENITIES listings: {list(updated_room_amenities)}\n')
print(f'# UPDATED ROOM NAME/SUMMARY listings: {list(updated_room_name_summary)}\n')

print(f'# ERROR listings: {error_listings}\n')
print(f'# EXCEPTION LISTINGS: {exception_listings}\n')