import os
import sys
import django
import math

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Initial/Dummy Request ... just to get the totals
response = LISTING_API_CLIENT.listings(page=1, per_page=1)
if not response['success']:
    print('ERROR: INITIAL REQUEST')
    print(response)
    sys.exit()

# Get count .. compute total pages needed
total_listings = response['total'] # 26758
print(f'Total listings: {total_listings}')

PER_PAGE = 1000
PAGES = math.ceil(total_listings / PER_PAGE)

# For quick/dummy tests
# PER_PAGE = 100
# PAGES = 100

ALL_NON_LEAP_PARENT_ONLY = set()
for page in range(1, PAGES+1):
    response = LISTING_API_CLIENT.listings(page=page, per_page=PER_PAGE)
    
    if not response['success']:
        print(f'ERROR: PAGE {page} REQUEST')
        print(response)
        sys.exit()

    # Iterate through all listings
    for idx, listing in enumerate(response['listings']):
        if not listing['nickname'].startswith('LP_'):
            ALL_NON_LEAP_PARENT_ONLY.add(listing['id'])

# Batch 1
USER1_ID = 1535 # viktoria@staykeepers.com
USER2_ID = 1932 # bojana@winwinnkeeper.net

# Batch 2
USER3_ID = 1934 # admin@staylivegrow.com

# Batch 3
USER4_ID = 1938 # vi@winwinnkeeper.net
USER5_ID = 1939 # tj@winwinnkeeper.net
USER6_ID = 1940 # plamenaormandjieva@winwinnkeeper.net

# Batch 4
USER7_ID = 1945 # lazar@winwinnkeeper.net
USER8_ID = 1946 # silviatoncheva@winwinnkeeper.net

# Batch 5
USER9_ID = 1952 # anna.i@winwinnkeeper.net
USER10_ID = 1953 # hassan.m@winwinnkeeper.net
USER11_ID = 1954 # margarette.r@winwinnkeeper.net

# Status vars
success_listings = []
error_listings = {}
exception_listings = {}

# ALL_NON_LEAP_PARENT_ONLY = [26626, 18457, 28697, 18463, 18467, 40995, 18474, 14382, 14386, 14394, 35833, 35834, 26066, 24031, 18508, 18509, 28102, 34895, 34897, 34900, 14421, 16476, 24673, 24674, 24675, 24677, 24678, 24679, 26072, 14441, 24682, 24683, 24684, 24685, 24686, 24687, 24688, 24689, 24690, 26073, 26074, 26075, 26076, 26077, 26078, 26079, 16525, 26080, 22673, 28819, 14493, 28851, 28853, 28854, 28855, 28856, 28857, 28860, 28864, 28865, 28868, 28869, 28871, 28872, 28873, 24781, 41186, 41187, 14567, 14572, 22768, 14579, 14581, 14587, 14589, 14599, 14604, 22799, 22800, 22801, 22802, 22803, 37148, 37150, 37151, 26986, 26987, 26988, 26989, 26990, 26991, 26992, 26993, 26994, 26995, 26996, 26997, 26998, 27001, 27002, 27003, 27004, 27005, 27006, 27007, 27008, 27009, 27012, 22923, 14805, 14807, 14809, 23021, 23022, 23023, 27118, 27119, 22244, 27120, 27121, 27122, 27123, 20983, 20984, 27124, 27125, 27126, 27127, 14846, 23987, 14996, 21163, 15076, 15079, 19176, 15082, 25342, 25376, 27450, 27451, 27452, 27453, 27454, 23366, 23367, 23368, 23369, 23370, 23371, 23372, 23373, 23374, 23375, 23376, 21329, 23377, 23378, 23379, 23380, 23381, 23382, 23383, 23384, 23385, 23386, 23387, 17268, 17269, 17270, 41891, 15275, 23482, 23483, 23484, 23485, 23486, 23488, 23494, 23495, 23496, 23497, 23498, 23499, 23506, 19436, 31724, 31725, 31726, 19444, 19445, 19448, 19449, 19450, 19451, 19452, 19453, 19454, 19455, 19456, 19457, 19458, 19459, 19460, 19461, 19462, 19463, 23592, 23593, 23594, 23595, 17462, 17463, 17464, 17466, 35901, 21578, 33871, 35920, 33874, 33877, 15446, 33878, 33882, 15453, 15455, 15458, 15460, 15469, 15471, 15472, 15474, 15475, 15476, 15477, 15478, 15479, 15480, 17528, 17530, 15483, 15481, 15485, 15482, 21631, 21632, 21633, 21634, 21635, 27774, 15494, 15495, 15501, 23992, 15506, 15508, 15522, 15534, 21686, 21694, 21695, 21696, 21697, 21698, 21699, 21700, 21701, 21702, 21703, 21704, 21705, 21706, 21729, 21730, 21731, 21732, 21733, 21734, 21735, 21736, 21737, 21738, 23985, 21771, 21772, 21773, 21774, 21775, 19742, 19743, 19744, 19745, 19746, 19747, 23986, 19749, 19750, 19751, 19752, 19753, 19754, 19755, 19756, 19757, 19758, 19759, 19760, 19761, 19762, 19763, 19764, 19765, 19766, 19767, 19768, 19769, 19770, 19771, 19772, 19773, 19774, 19775, 19776, 19777, 19778, 19779, 19780, 19781, 19782, 19783, 19784, 19785, 19786, 19787, 19788, 19789, 19790, 19791, 23988, 23989, 23990, 23948, 23949, 23950, 23965, 23966, 23967, 23968, 23991, 23972, 23973, 23974, 23975, 23976, 23980, 23981, 23982, 23983, 23984, 21937, 21938, 21939, 21940, 21941, 21942, 21943, 21944, 21945, 21946, 21947, 21948, 21949, 21950, 21951, 21952, 21953, 21954, 21955, 21956, 21957, 21958, 21959, 21960, 21961, 21962, 21963, 21964, 21965, 21966, 23993, 24007, 24008, 24009, 24010, 24011, 24012, 24013, 24014, 24015, 24016, 24017, 24018, 24019, 24020, 24021, 24022, 24023, 24024, 24025, 24026, 24027, 24028, 24029, 24030, 23994, 24032, 24035, 24036, 24037, 24038, 24039, 24040, 24041, 24043, 24044, 24045, 24046, 24047, 24048, 24051, 24052, 24053, 24054, 24055, 24056, 24058, 24060, 24061, 24062, 23995, 24063, 24064, 24065, 24066, 24067, 24068, 24069, 24070, 24078, 19987, 24084, 24086, 24087, 19993, 23996, 24091, 24093, 24094, 24095, 24096, 20001, 20002, 24097, 24098, 24099, 24100, 24101, 24102, 24103, 24104, 20018, 20019, 20020, 20021, 23997, 20025, 20026, 24121, 20028, 24122, 24123, 24124, 24125, 24126, 24127, 24128, 20036, 24129, 24130, 24131, 24134, 24135, 24136, 24137, 23998, 24138, 24139, 24140, 24153, 24154, 24155, 24156, 24157, 24158, 24159, 24160, 24161, 24162, 24163, 24164, 23999, 24165, 24166, 24167, 20073, 24168, 24170, 24171, 24172, 24173, 24174, 24175, 24176, 24177, 24179, 24184, 24185, 24000, 24210, 24211, 24212, 24213, 24215, 24216, 24217, 24218, 40631, 22223, 20183, 20184, 20185, 20186, 20189, 20190, 20192, 22240, 22242, 22241, 20196, 20197, 20198, 22243, 20200, 20201, 20202, 20203, 20204, 20205, 20206, 20207, 20208, 20209, 20210, 22263, 22264, 22265, 22266, 24323, 24324, 24325, 24326, 24327, 24328, 24329, 24330, 24331, 24332, 24333, 24334, 24335, 24336, 24337, 24338, 24339, 24340, 24341, 22349, 22350, 22351, 22352, 22353, 22354, 22355, 22356, 22357, 22358, 22359, 22360, 22361, 22362, 16219, 16220, 20316, 22363, 22364, 22365, 22366, 22367, 22368, 22369, 22370, 22371, 16231, 22372, 22373, 22375, 22376, 22377, 22378, 22379, 22380, 22382, 22383, 22384, 22385, 22386, 22387, 22389, 22390, 22391, 22392, 22393, 22394, 22395, 22396, 22397, 22398, 22399, 22400, 22401, 22402, 22403, 22404, 22405, 22406, 22407, 22408, 22409, 22410, 22411, 22412, 22413, 22414, 22415, 22416, 22417, 22418, 22419, 22420, 22421, 22422, 22423, 22424, 22426, 22427, 22428, 22429, 22430, 22431, 22432, 22433, 22434, 22435, 22436, 22437, 16294, 22438, 22439, 22440, 22441, 22442, 22443, 22444, 22445, 22446, 22447, 22448, 22449, 22450, 22451, 22452, 22453, 22454, 22455, 22456, 24001, 24002, 24003, 20417, 20418, 24004, 28609, 24005, 24006, 20433, 20434, 22381]
print(f'ALL_NON_LEAP_PARENT_ONLY: {ALL_NON_LEAP_PARENT_ONLY}')
# sys.exit()

print('\n### BULK RE-ASSIGN OF NON-LEAP LISTINGS')
for hostify_parent_id in list(ALL_NON_LEAP_PARENT_ONLY):
    print(f'Hostify ID: {hostify_parent_id}')

    try:
        # 1st: make sure the Parents are updated
        response1 = LISTING_API_CLIENT.user_add_listing({'user_id': USER1_ID, 'listing_id': hostify_parent_id})
        response2 = LISTING_API_CLIENT.user_add_listing({'user_id': USER2_ID, 'listing_id': hostify_parent_id})
        response3 = LISTING_API_CLIENT.user_add_listing({'user_id': USER3_ID, 'listing_id': hostify_parent_id})
        response4 = LISTING_API_CLIENT.user_add_listing({'user_id': USER4_ID, 'listing_id': hostify_parent_id})
        response5 = LISTING_API_CLIENT.user_add_listing({'user_id': USER5_ID, 'listing_id': hostify_parent_id})
        response6 = LISTING_API_CLIENT.user_add_listing({'user_id': USER6_ID, 'listing_id': hostify_parent_id})
        response7 = LISTING_API_CLIENT.user_add_listing({'user_id': USER7_ID, 'listing_id': hostify_parent_id})
        response8 = LISTING_API_CLIENT.user_add_listing({'user_id': USER8_ID, 'listing_id': hostify_parent_id})
        response9 = LISTING_API_CLIENT.user_add_listing({'user_id': USER9_ID, 'listing_id': hostify_parent_id})
        response10 = LISTING_API_CLIENT.user_add_listing({'user_id': USER10_ID, 'listing_id': hostify_parent_id})
        response11 = LISTING_API_CLIENT.user_add_listing({'user_id': USER11_ID, 'listing_id': hostify_parent_id})

        success = (
            response1['success'] and response2['success'] and response3['success'] and
            response4['success'] and response5['success'] and response6['success'] and
            response7['success'] and response8['success'] and
            response9['success'] and response10['success'] and response11['success']
        )

        if success:
            success_listings.append(hostify_parent_id)
        else:
            error = (
                response1.get('error', '_') + response2.get('error', '_') + response3.get('error', '_') +
                response4.get('error', '_') + response5.get('error', '_') + response6.get('error', '_') +
                response7.get('error', '_') + response8.get('error', '_') +
                response9.get('error', '_') + response10.get('error', '_') + response11.get('error', '_')
            )
            error_listings[hostify_parent_id] = error.replace('Listing has already been added to this user.', '+')

        # 2nd: make sure the Children are updated
        # @ignore Not sure if needed

    except Exception as e:
        exception_listings[hostify_parent_id] = str(e)
        # print(f'# EXCEPTION LISTING: {str(e)}\n')
        continue

print(f'\n\n# SUCCESS listings: {success_listings}\n')
print(f'# Error listings: {error_listings}\n')
print(f'# EXCEPTION LISTINGS: {exception_listings}\n')

