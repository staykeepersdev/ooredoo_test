import os
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get Users's data
from scripts.bulk_reassign_listings.listings import USER_ID, USER_LISTINGS

for hostify_parent_id, hostify_children_ids in USER_LISTINGS.items():
    
    result = LISTING_API_CLIENT.get_listing_children(hostify_parent_id)
    if result['success']:
        children = result['listings']
        children_ids = [child['id'] for child in children]
    
        # Fill them up :)
        USER_LISTINGS[hostify_parent_id] = children_ids
    else:
        print(f'ERROR: {hostify_parent_id}')
    
    break

print('USER_LISTINGS: ', USER_LISTINGS)
