import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Models
from calendar_app.models import CalendarRoom

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get list of all rooms
LIMIT_START = 23000
LIMIT_END = 24000

# Batch 1
USER1_ID = 1918 # jovie@winwinnkeeper.net
USER2_ID = 1922 # jowie@winwinneeper.net
USER3_ID = 1919 # margarette@winwinnkeeper.net
USER4_ID = 1792 # hassan@staykeepers.com

# Batch 2
USER5_ID = 1925 # anna@winwinnkeeper.net
USER6_ID = 1935 # pavlinageorgieva@winwinnkeeper.net

# Start
calendar_rooms = CalendarRoom.objects.filter(
    hostify_id__isnull=False
).all().order_by('id')[LIMIT_START:LIMIT_END]

print(f'\n### BULK RE-ASSIGN OF LEAP LISTINGS: From {LIMIT_START} to {LIMIT_END} - {len(calendar_rooms)}\n')

success_listings = []
error_listings = {}
exception_listings = {}
for calendar_room in calendar_rooms:
    print(f'# Listing {calendar_room.id}')
    calendar_room_id = calendar_room.id
    hostify_parent_id = calendar_room.hostify_id

    try:
        # 2 Batches
        # 1st: make sure the Parents are updated
        response1 = LISTING_API_CLIENT.user_add_listing({'user_id': USER1_ID, 'listing_id': hostify_parent_id})
        response2 = LISTING_API_CLIENT.user_add_listing({'user_id': USER2_ID, 'listing_id': hostify_parent_id})
        response3 = LISTING_API_CLIENT.user_add_listing({'user_id': USER3_ID, 'listing_id': hostify_parent_id})
        response4 = LISTING_API_CLIENT.user_add_listing({'user_id': USER4_ID, 'listing_id': hostify_parent_id})
        response5 = LISTING_API_CLIENT.user_add_listing({'user_id': USER5_ID, 'listing_id': hostify_parent_id})
        response6 = LISTING_API_CLIENT.user_add_listing({'user_id': USER6_ID, 'listing_id': hostify_parent_id})
        success = response1['success'] and response2['success'] and response3['success'] and response4['success'] and response5['success'] and response6['success']
        
        if success:
            success_listings.append(calendar_room_id)
        else:
            error = response1.get('error', '_') + response2.get('error', '_') + response3.get('error', '_') + response4.get('error', '_') + response5.get('error', '_') + response6.get('error', '_')
            error_listings[calendar_room_id] = error.replace('Listing has already been added to this user.', '+')

        # 2nd: make sure the Children are updated
        # @ignore Not sure if needed
        
    except Exception as e:
        exception_listings[calendar_room_id] = str(e)
        continue

print(f'# SUCCESS listings: {success_listings}\n')
print(f'# ERROR listings: {error_listings}\n')
print(f'# EXCEPTION LISTINGS: {exception_listings}\n')

# For file
# with open(f'scripts/bulk/reassign_listings/LEAP_{LIMIT_START}_{LIMIT_END}.txt', 'a') as f:
#     print(f'\n### BULK RE-ASSIGN OF LEAP LISTINGS: From {LIMIT_START} to {LIMIT_END} - {len(calendar_rooms)}\n', file=f)
#     print(f'\n\n# SUCCESS listings: {success_listings}\n\n', file=f)

#     print(f'# ERROR listings: {error_listings}\n', file=f)
#     print(f'# EXCEPTION LISTINGS: {exception_listings}\n', file=f)