# Rebecca Mackay‑Roberts
# Email: rmackay-roberts@crm-students.com
REBECCA_USER_ID = 1807
REBECCA_LISTINGS = {
  56938: [56953, 56954, 63237],
  56941: [56960, 56961, 64213],
  56942: [56962, 56963, 64234],
  56973: [56997, 56998, 64233],
  56974: [56999, 57000, 64235],
  56975: [57001, 57002, 64236],
  56976: [57013, 57014, 64237],
  56991: [57020, 57021, 64238],
  56992: [57022, 57023, 64239],
  56993: [57024, 57025, 64240],
  56994: [57026, 57027],
  56995: [57028, 57029],
  56996: [57030, 57031],
  57057: [57068, 57070],
  57058: [57069, 57076],
  57059: [57071, 57072],
  57060: [57074, 57077],
  57061: [57073, 57078],
  57062: [57075, 57080],
  57063: [57079, 57081],
  63238: [63315, 63316],
  63239: [63317, 63318],
  63240: [63319, 63323],
  63241: [63324, 63325],
  63242: [63327, 63330],
  63243: [63328, 63333],
  63244: [63320, 63329],
  63245: [63322, 63331],
  63246: [63332, 63344],
  63247: [63321, 63356],
  63248: [63326, 63334],
  63249: [63335, 63361],
  63250: [63336, 63367],
  63251: [63337, 63424],
  63252: [63338, 63359],
  63253: [63339, 63346],
  63254: [63340, 63348],
  63255: [63341, 63358],
  63257: [63355, 63438],
  63258: [63363, 63384],
  63259: [63345, 63351],
  63261: [63347, 63353],
  63262: [63411, 63464],
  63263: [63342, 63523],
  63264: [63343, 63551],
  63256: [63443, 63565],
  63267: [63349, 63365],
  63268: [63368, 63370],
  63269: [63360, 63376],
  63270: [63364, 63374],
  63271: [63352, 63372],
  63272: [63357, 63587],
  63273: [63350, 63379],
  63274: [63362, 63602],
  63275: [63482, 63509],
  63276: [63366, 63397],
  63277: [63354, 63616],
  63278: [63507, 63535],
  63279: [63380, 63381],
  63280: [63378, 63383],
  63281: [63373, 63650],
  63282: [63377, 63680],
  63283: [63541, 63694],
  63284: [63369, 63630],
  63285: [63561, 63708],
  63286: [63596, 63729],
  63287: [63375, 63743],
  63260: [63495, 63854],
  63288: [63371, 64026],
  63289: [63610, 64036],
  63290: [63410, 64040],
  63291: [63575, 64059],
  63292: [63627, 64079],
  63293: [63521, 64080],
  63294: [63382, 63952],
  63295: [63429, 64039],
  63296: [63469, 64037],
  63297: [63664, 64134],
  63266: [63669, 64164],
  63304: [63684, 64154],
  63305: [63702, 63986],
  63306: [63719, 64155],
  63307: [63642, 64157],
  63308: [63733, 64158],
  63309: [63840, 64159],
  63310: [64034, 64162],
  63311: [64035, 64038],
  63312: [64033, 64156],
  63313: [63988, 64161],
  63314: [63961, 64160]
}

# Katherine Austin - same as David Hudghton
# Email: kaustin@collegiate-ac.com
KATHERINE_USER_ID = 1921
KATHERINE_LISTINGS = {
  56607: [56658, 56895],
  56608: [],
  56609: [],
  56625: [],
  56626: [],
  56640: [],
  56641: [],
  56854: [],
  56882: [],
  56883: [],
  56891: [],
  56892: [],
  56645: [],
  56648: [],
  56646: [],
  56650: [],
  56651: [],
  56655: [],
  56672: [],
  56673: [],
  56898: [],
  56899: [],
  56900: [],
  56914: []
}

# David Hudghton - same as Katherine Austin
# Email: dhudghton@collegiate-ac.com
DAVID_USER_ID = 1920
DAVID_LISTINGS = KATHERINE_LISTINGS

# Anna Hue Ly
# Email: anna.ly@axostudent.co.uk
ANNA_HUE_LY_USER_ID = 1923
ANNA_HUE_LY_LISTINGS = {
  56956: [56979, 56982],
  56957: [],
  33874: [],
  35834: [],
  57003: [],
  57004: [],
  57037: [],
  34897: [],
  37148: [],
  57045: [],
  57046: [],
  57047: [],
  34895: [],
  37150: [],
  57008: [],
  57009: [],
  57010: [],
  57011: [],
  57012: [],
  57211: [],
  57212: [],
  57213: [],
  37151: [],
  33882: [],
  57214: [],
  57215: [],
  57216: [],
  57015: [],
  57016: [],
  57017: [],
  57018: [],
  57019: [],
  57194: [],
  40631: [],
  57082: [],
  41186: [],
  57083: [],
  57084: [],
  57085: [],
  57086: [],
  57196: [],
  57195: [],
  35833: [],
  56955: [],
  34900: [],
  41187: [],
  57087: [],
  57088: [],
  57089: [],
  57090: [],
  57091: [],
  57197: [],
  57198: [],
  57199: [],
  57200: [],
  57201: [],
  57202: [],
  57203: [],
  57204: [],
  57205: [],
  57206: [],
  57207: [],
  57208: [],
  57209: [],
  40995: [],
  41891: [],
  57112: [],
  57113: [],
  57114: [],
  57115: [],
  57116: [],
  57193: [],
  33877: [],
  35901: [],
  33878: [],
  35920: [],
  57127: []
}

# Thomas Bush - same as Anna Hue Ly listings
# Email: thomas.b@axostudent.co.uk
THOMAS_BUSH_USER_ID = 1924
THOMAS_BUSH_LISTINGS = ANNA_HUE_LY_LISTINGS

# Export
USER_ID = THOMAS_BUSH_USER_ID
USER_LISTINGS = THOMAS_BUSH_LISTINGS