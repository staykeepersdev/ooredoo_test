import os
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get Users's data
from scripts.bulk.reassign_listings.data.NON_LEAP_STOYAN import USER_ID, USER_LISTINGS

for hostify_parent_id, hostify_children_ids in USER_LISTINGS.items():
    try:
        # 1st: make sure the Parent is updated
        result = LISTING_API_CLIENT.user_add_listing({
            'user_id': USER_ID,
            'listing_id': hostify_parent_id
        })
        
        # if result['success']: print(f'SUCCESS: Parent {hostify_parent_id} updated')
        if not result['success']: print(f'ERROR: Parent {hostify_parent_id}')

        '''
        # 2nd: make sure the Children are updated
        for child_id in hostify_children_ids:
            result = LISTING_API_CLIENT.user_add_listing({
                'user_id': USER_ID,
                'listing_id': child_id
            })

            # if result['success']: print(f'SUCCESS: Child {child_id} updated')
            if not result['success']: print(f'ERROR: Child {child_id}')
        '''

    except Exception as e:
        print(f'# EXCEPTION: With listing {hostify_parent_id} or its children: ', str(e))
        print('\n')
        continue
