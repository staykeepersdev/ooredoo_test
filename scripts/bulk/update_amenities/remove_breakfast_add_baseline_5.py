import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.services import process_part
from calendar_app.models import CalendarRoom

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get list of all rooms
LIMIT_START = 0
LIMIT_END = 1000
calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['amenities_params']
).all().order_by('id')[LIMIT_START:LIMIT_END]

print(f'\n### BULK UPDATE OF LISTINGS: From {LIMIT_START} to {LIMIT_END}')

updated_rooms = []
error_rooms = []
exception_rooms = {}
for calendar_room in calendar_rooms:
    print(f'Room ID - Hostify ID - Child ID: {calendar_room.id} - {calendar_room.hostify_id} - {calendar_room.hostify_child_id}')
    
    try:
        room_amenities = calendar_room.request_data['amenities_params']['amenities']

        # One more time - get rid of this pesky breakfast :)
        has_at_least_one_breakfast = None
        for idx, room_amenity in enumerate(room_amenities):
            if 'breakfast' in room_amenity.lower():
                has_at_least_one_breakfast = True
                del room_amenities[idx]

        # Enforce additional amenities
        if not 'Towels' in room_amenities: room_amenities.append('Towels')
        if not 'Bed Linens' in room_amenities: room_amenities.append('Bed Linens')
        if not 'Hot Water' in room_amenities: room_amenities.append('Hot Water')
        if not 'Bathroom' in room_amenities: room_amenities.append('Bathroom')
        if not 'Essentials' in room_amenities: room_amenities.append('Essentials')

        # Nothing to update ...
        # if not has_at_least_one_breakfast:
        #     continue

        # Step 1: Update in Django
        calendar_room.request_data['amenities_params']['amenities'] = room_amenities
        calendar_room.save()

        # Step 2: Update in Hostify Parent
        if calendar_room.hostify_id != None and int(calendar_room.hostify_id) != 0:
            response, errors = process_part(calendar_room.hostify_id, 'amenities', calendar_room.request_data['amenities_params'])
            
            if not response['success']:
                error_rooms.append(calendar_room.id)
                print(f'# ERROR: Update in Hostify Parent of {calendar_room.id}: ', errors)

        # Step 3: Update in Hostify Child
        if calendar_room.hostify_child_id != None and int(calendar_room.hostify_child_id) != 0:
            response, errors = process_part(calendar_room.hostify_child_id, 'amenities', calendar_room.request_data['amenities_params'])

            if not response['success']:
                error_rooms.append(calendar_room.id)
                print(f'# ERROR: Update in Hostify Child of {calendar_room.id}: ', errors)

        # Step 4: Update in AirBnb
        # It seems Step 3 is also updating in AirBnb
        updated_rooms.append(calendar_room.id)

    except Exception as e:
        exception_rooms[calendar_room.id] = str(e)
        continue

print('Updated rooms: ', updated_rooms, '\n\n')
print('Error rooms: ', error_rooms, '\n\n')
print('EXCEPTIONS: ', exception_rooms)