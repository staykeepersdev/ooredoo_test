import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.services import process_part
from calendar_app.models import CalendarRoom

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get list of all rooms
LIMIT_START = 15000
LIMIT_END = 20000
calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['amenities_params']
).all().order_by('id')[LIMIT_START:LIMIT_END]

print(f'\n### BULK UPDATE OF LISTINGS: From {LIMIT_START} to {LIMIT_END}')

updated_rooms = []
exception_rooms = {}
for calendar_room in calendar_rooms:
    has_at_least_one_breakfast = None

    room_amenities = calendar_room.request_data['amenities_params']['amenities']

    try:
        for idx, room_amenity in enumerate(room_amenities):
            if 'breakfast' in room_amenity.lower():
                has_at_least_one_breakfast = True
                del room_amenities[idx]

        # Nothing to update ...
        # if not has_at_least_one_breakfast:
        #     continue

        # Step 1: Update in Django
        if has_at_least_one_breakfast:
            calendar_room.request_data['amenities_params']['amenities'] = room_amenities
            calendar_room.save()

        # Step 2: Update in Hostify Parent
        if int(calendar_room.hostify_id or 0) > 0:
            response, errors = process_part(calendar_room.hostify_id, 'amenities', calendar_room.request_data['amenities_params'])
            
            if not response['success']:
                print('# ERROR: Update in Hostify Parent: ', calendar_room.id)
                print('Error message: ', errors)

        # Step 3: Update in Hostify Child
        if int(calendar_room.hostify_child_id or 0) > 0:
            response, errors = process_part(calendar_room.hostify_child_id, 'amenities', calendar_room.request_data['amenities_params'])

            if not response['success']:
                print('# ERROR: Update in Hostify Child: ', calendar_room.id)
                print('Error message: ', errors)

        # Step 4: Update in AirBnb
        # It seems Step 3 is also updating in AirBnb
        updated_rooms.append(calendar_room.id)

    except Exception as e:
        exception_rooms[calendar_room.id] = str(e)
        continue

print('Updated rooms: ', updated_rooms, '\n\n')
print('EXCEPTIONS: ', exception_rooms)