import django
from django.db.models import Q
import os
import re
import sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.services import (
    get_hb_matched_rooms,
    get_manual_matched_rooms,
    process_part
)
from calendar_app.static_data.all_room_types import ALL_HOTEL_ROOM_TYPES
from hotelbeds_data.models import APIHotel

from calendar_app.models import CalendarRoom
from hotels.models import HotelRoom

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()



# Get list of all rooms with layout_params
calendar_rooms = CalendarRoom.objects.filter(request_data__has_any_keys=['layout_params']).all()
print(calendar_rooms.count())
counter = 0
for calendar_room in calendar_rooms:
    counter+=1
    # STEP 1: GET HB hotel_code & hotel #
    # Get HB hotel_code
    try:
        calendar_hotel = calendar_room.calendar_room_type.calendar_hotel
        hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
        hotel_code = hotelbeds_api_hotel.code
    except:
        print(f'Unable to extract HB hotel_code from room: {calendar_room.id} - {calendar_room.title}')
        continue

    # Get Hotel
    api_hotel = APIHotel.objects.filter(code=int(hotel_code)).first()
    try:
        hotel = api_hotel.hotel # REQUIRED BELOW ...
    except:
        print(f'HB Hotel with code {hotel_code} doesn\'t exists.')
        continue
    
    
    # STEP 2: GET room_code & room_type #
    # Get Room Code
    room_code = calendar_room.calendar_room_type.code # The original CSV approach is based on csv_dict_line['hb_room_code'] -> ROO.SU    
    
    # Get static room code ... then room type ...
    static_room_code = re.sub(r'\-\d+$', '', room_code) # From SUI.1B-1 to SUI.1B
    room_type = ALL_HOTEL_ROOM_TYPES.get(static_room_code) # REQUIRED BELOW ...
    

    # STEP 3: We need the correct Room object to extract room_stays ... #
    hotel_room = HotelRoom.objects.filter(hotel=hotel, room_code=room_code).first()

    
    # STEP 4: Get the rooms layout & person_capacity ... based on REQUIRED hotel_room & room_type #
    if hotel_room.room_stays:
        rooms, person_capacity = get_hb_matched_rooms(hotel_room.room_stays)
        if not person_capacity:
            rooms, person_capacity = get_manual_matched_rooms(room_type)
    else:
        rooms, person_capacity = get_manual_matched_rooms(room_type)


    # MID-WAY DEBUGGING :)
    # print(f'Person capacity: {person_capacity}')


    # TODO
    
    ## TODO STEP 1: Update person_capacity ... rooms should be ignored
    layout_params = calendar_room.request_data['layout_params']
    if layout_params['person_capacity'] != person_capacity:
        print(counter,calendar_room.id, calendar_room.calendar_room_type.code)
        
        layout_params['person_capacity'] = person_capacity

        calendar_room.request_data['layout_params'] = layout_params
        calendar_room.save()

        
        
        ## TODO STEP 2: Update at Hostify all CalendarRooms EXCLUDING hostify_status in ('Ready For Listing', 'Information_error')
        if calendar_room.hostify_status in ['ready', 'info_errors', 'not_listed', 'waiting', 'updating', 'processed']: 
            continue

        updated_layout_params = layout_params.copy()
        
        # # EXCLUDE ROOMS !
        del updated_layout_params['rooms']
        response, errors = process_part(calendar_room.hostify_id, 'layout', updated_layout_params)

    # Step # is not needed for this case
    # TODO STEP 3: Update at AirBnB all listings with hostify_child_id   ...   BONUS: Testing the new Update AirBnB endpoint
    # if not calendar_room.hostify_child_id: continue

    # try:
    #   update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
    #       'listing_id': calendar_room.hostify_child_id,
    #       'extra_person': ???
    #   })
    # except:
    #     print(
    #         f'# ERROR: HOSTIFY UPDATE LISTING\n',
    #         f'For calendar room: {calendar_room.id} - {calendar_room.title}\n',
    #         f'Response: {update_response}\n\n'
    #     )

    





# Docs

## Update Listing
## @link https://app.hostify.com/docs#update-listing
