import os
import re
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Calendar App
from calendar_app.services import process_part
from calendar_app.models import CalendarRoom

MIN_NOTICE_HOURS = 168 # Increased from 1 day to 1 week

# LIMIT
LIMIT_START = 0
LIMIT_END = 10

# Start
calendar_rooms = CalendarRoom.objects.filter(
    hostify_id__isnull=False,
    hostify_child_id__isnull=False,
    request_data__has_any_keys=['booking_restrictions_params']
).all().order_by('id') # [LIMIT_START:LIMIT_END]

success_listings = []
error_listings = {}
exception_listings = {}
for calendar_room in calendar_rooms:
    print(f'# Room {calendar_room.id} ... Hostify ID {calendar_room.hostify_id} ... Hostify Child Id {calendar_room.hostify_child_id}')

    try:
        if calendar_room.request_data.get('booking_restrictions_params', False):
            booking_restrictions_params = calendar_room.request_data['booking_restrictions_params']
            booking_restrictions_params['min_notice_hours'] = MIN_NOTICE_HOURS

            if calendar_room.hostify_child_id and calendar_room.hostify_child_id != '0':
                response = LISTING_API_CLIENT.update_listing_on_airbnb({
                    'listing_id': calendar_room.hostify_child_id,
                    'min_notice_hours': MIN_NOTICE_HOURS
                })

                # Save to Django upon success
                if response['success']:
                    calendar_room.request_data['booking_restrictions_params'] = booking_restrictions_params
                    calendar_room.save()
                    success_listings.append(calendar_room.id)
                else:
                    print(f'ERROR with room {calendar_room.id}: {response["error"]}')
                    error_listings[calendar_room.id] = str(response["error"])

    except Exception as e:
        print(f'# EXCEPTION of {calendar_room.id}: {str(e)}')
        exception_listings[calendar_room.id] = str(e)
        continue

print(f'\n# UPDATED MIN_NOTICE_HOURS listings: {success_listings}\n')
print(f'# ERROR listings: {error_listings}\n')
print(f'# EXCEPTION LISTINGS: {exception_listings}\n')