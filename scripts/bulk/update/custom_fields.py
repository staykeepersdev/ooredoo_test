import django
import os
import sys
import threading
import time

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.core.paginator import Paginator

from calendar_app.models import CalendarRoom
from calendar_app.services import update_hostify_custom_fields

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

def update_custom_fields(calendar_rooms):
    global hostify_listings_updated

    for calendar_room in calendar_rooms:
        # Update custom fields in Hostify
        response, error = update_hostify_custom_fields(calendar_room)
        
        # Print the responses only when there are exceptions
        if not response:
            print(f'Django ID# {calendar_room.pk} | {error}')

        hostify_listings_updated += 1
        if hostify_listings_updated % 100 == 0:
            print(f'Updated Rooms : {hostify_listings_updated}')

def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

def loop_over_paginated_queryset(paginator):
    for page in range(1, paginator.num_pages + 1):
        # Check running threads before starting new one
        while threading.active_count() >= MAX_THREADS:
            time.sleep(2)
        calendar_rooms_queryset = paginator.page(page).object_list
        run_threaded(update_custom_fields, (calendar_rooms_queryset,) )

ROOMS_PER_THREAD = 1000
MAX_THREADS = 11 # Includes the main thread as as well (-1)

hostify_listings_updated = 0

calendar_rooms = CalendarRoom.objects.filter(
    hostify_id__isnull=False
).filter(
    updated_custom_fields=False
).order_by('pk')
paginator = Paginator(calendar_rooms, ROOMS_PER_THREAD)

loop_over_paginated_queryset(paginator)
