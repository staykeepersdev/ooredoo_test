import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.services import process_part
from calendar_app.models import CalendarRoom

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

ALLOWED_HOSTIFY_STATUSES = ['listed', 'listed_with_errors']
ALLOWED_CLONING_STATUSES = ['cloned']

# Get list of all rooms
LIMIT_START = 0
LIMIT_END = 1000
calendar_rooms = CalendarRoom.objects.filter(
    # id=15233,
    # request_data__has_any_keys=['location_params'], # required for 1st pass
    request_data__has_keys=['location_params', 'layout_params'], # required for 2nd pass
    hostify_child_id__isnull=False
).all().order_by('id')[LIMIT_START:LIMIT_END]

# SINGLE LISTING TEST ...

## LP_CBB Gran Hotel Cochabamba STU.ST 1 01 - 12087 -> Cloned & Listed
## TEST_HOSTIFY_PARENT_ID = 31257
## TEST_HOSTIFY_CHILD_ID = 32172
## calendar_rooms = CalendarRoom.objects.filter(hostify_id=TEST_HOSTIFY_PARENT_ID).all()

# deprecated
# hostify_parent_listings_updated = 0
# hostify_child_listings_updated = 0
airbnb_listings_updated = 0

listings_of_type_APT = []
listings_of_type_APT_with_bedroom = []

print(f'\n### BULK UPDATE OF LISTINGS: From {LIMIT_START} to {LIMIT_END}')

# Utils
def print_room_data(calendar_room, property_type, property_type_group):
    print(f'Calendar room: {calendar_room.title} - {calendar_room.id}')
    print(f'Property type: {property_type}')
    print(f'Property type group: {property_type_group}\n')


PHASE = 2

for calendar_room in calendar_rooms:
    # All Phases: Property Type & Property Type Group: hostify without _ / airbnb with _
    property_type = 'guest suite'
    property_type_group = 'secondary units'
    
    # 2nd Phase: Check if the Room Type is given as Apartment from HB
    if PHASE == 2:
        is_apartment = False
        
        rooms_layout = calendar_room.request_data['layout_params']['rooms']
        if 'APT' in calendar_room.calendar_room_type.code:
            listings_of_type_APT.append(calendar_room.id)

            # Check if we have at least 1 room of type 'bedroom'
            for room in rooms_layout:
                if room['room_type'] == 'bedroom':
                    is_apartment = True
                    listings_of_type_APT_with_bedroom.append(calendar_room.id)

        if is_apartment:
            property_type = 'apartment'
            property_type_group = 'apartments'
        else:
            continue

    # For debugging
    # print_room_data(calendar_room, property_type, property_type_group)
    # continue

    # 1. Update property_type in Django
    calendar_room.request_data['location_params']['property_type'] = property_type
    calendar_room.request_data['location_params']['property_type_group'] = property_type_group
    calendar_room.request_data['location_params']['listing_type'] = 'entire home' # required by Hostify !
    calendar_room.save()


    '''
    @deprecated
    # 2. Update property_type in Hostify parent
    if calendar_room.hostify_status in ALLOWED_HOSTIFY_STATUSES:
        try:
            updated_location_params = calendar_room.request_data['location_params']
            response, errors = process_part(calendar_room.hostify_id, 'location', updated_location_params)

            if response['success']:
                hostify_parent_listings_updated += 1
            else:
                print('# ERROR: Hostify parent update: ', calendar_room.id)

        except Exception as e:
            print('# EXCEPTION: Hostify parent update', str(e))
            print(f'# Problem listing: ', calendar_room.id)
            print('\n')
            continue
    '''


    '''
    @deprecated
    # 3. The parent->child property_type sync in Hostify ... is now shaky ...
        try:
            updated_location_params = calendar_room.request_data['location_params']
            response, errors = process_part(calendar_room.hostify_child_id, 'location', updated_location_params)

            if response['success']:
                hostify_child_listings_updated += 1
            else:
                print('# ERROR: Hostify child update: ', calendar_room.id)

        except Exception as e:
            print('# EXCEPTION: Hostify child update', str(e))
            print(f'# Problem listing: ', calendar_room.id)
            print('\n')
            continue
    '''

    # 4. Update property_type in AirBnb ... seems to be working, the endpoint returns success
    if calendar_room.cloning_status in ALLOWED_CLONING_STATUSES:
        try:
            update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                'listing_id': calendar_room.hostify_child_id,
                'property_type': property_type,
                'property_type_group': property_type_group
            })

            if update_response['success']:
                airbnb_listings_updated += 1
            else:
                print('# ERROR: AirBnb update: ', calendar_room.id)

        except Exception as e:
            print(f'# EXCEPTION: AirBnb update listing', str(e))
            print(f'# Problem listing: ', calendar_room.id)
            print('\n')
            continue

# 1st Pass ... deprecated
# print('Hostify parent listings updated: ', hostify_parent_listings_updated)
# print('Hostify child listings updated: ', hostify_child_listings_updated)

# 2nd Pass
print('Listings of type APT: ', listings_of_type_APT)
print('Listings of type APT with bedroom: ', listings_of_type_APT_with_bedroom)

# Always
print('AirBnb listings updated: ', airbnb_listings_updated)