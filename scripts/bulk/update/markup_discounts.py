import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom
from calendar_app.bulk_services import update_price_markup

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Find all rooms marked with 02 
calendar_rooms = CalendarRoom.objects.filter(
    title__contains=' 02 '
).order_by('id').all()

PRICE_MARKUP = 100
WEEKLY_DISCOUNT = 25
MONTHLY_DISCOUNT = 35

rooms_count = len(calendar_rooms)
rooms_count_half = rooms_count//2

print( f'Calendar rooms count: {rooms_count}' )
# sys.exit()

calendar_rooms1 = calendar_rooms[:rooms_count_half]
calendar_rooms2 = calendar_rooms[rooms_count_half:]

for calendar_room in calendar_rooms1:
    
    try:

        if calendar_room.hostify_id:
            # We only need to update the Child listing. Parent doesn't have markup.
            if calendar_room.hostify_child_id and calendar_room.hostify_child_id != '0':

                # Set calendar_room markup & discounts
                response = LISTING_API_CLIENT.update_listing_on_airbnb({
                    'listing_id': calendar_room.hostify_child_id,
                    'price_markup': PRICE_MARKUP,
                    'weekly_price_discount': WEEKLY_DISCOUNT,
                    'monthly_price_discount': MONTHLY_DISCOUNT
                })

                print(f'Response: {response}')
                
                if response['success']:
                    # Update in Django as well
                    calendar_room.markup = PRICE_MARKUP
                    calendar_room.weekly_discount = WEEKLY_DISCOUNT
                    calendar_room.monthly_discount = MONTHLY_DISCOUNT
                    calendar_room.save()

                # sys.exit()

    except Exception as e:
        print(f'# EXCEPTION: {str(e)}')
        # sys.exit()