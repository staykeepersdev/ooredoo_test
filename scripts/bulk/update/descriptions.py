import django
import random
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Hotels App
from hotels.models import HotelRoom

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Calendar App
from calendar_app.models import CalendarRoom
from calendar_app.services import (
    replace_all_hotel_related_occurences,
    add_hotel_summary_notes,
    process_part
)
from calendar_app.static_data.nickname_adjectives import ADJECTIVES
from calendar_app.static_data.zones import ZONES

# Get list of all rooms
LIMIT_START = 27000
LIMIT_END = 30000

print(f'\n### BULK UPDATE OF LISTINGS DESCRIPTIONS: From {LIMIT_START} to {LIMIT_END}')

# For testing purposes
# ID_OF_ROOM_LISTED_AT_AIRBNB = 00000
# calendar_rooms = CalendarRoom.objects.filter(id=ID_OF_ROOM_LISTED_AT_AIRBNB).all().order_by('id')[LIMIT_START:LIMIT_END]

calendar_rooms = CalendarRoom.objects.all().order_by('id')[LIMIT_START:LIMIT_END]

def get_summary(calendar_room, hotel):

    if 'translations_params' in calendar_room.request_data:
        return calendar_room.request_data['translations_params'].get('summary', hotel.description)
    else:
        return hotel.description


manually_listed_rooms_without_hostify_ids = []
cannot_update_parent_listings = []

directly_updated_child_listings = []
cannot_update_child_listings = {}
exception_listings = {}

for calendar_room in calendar_rooms:
    try:
        # Get Hotel data
        calendar_room_type = calendar_room.calendar_room_type
        calendar_hotel = calendar_room_type.calendar_hotel
        hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
        hotel = hotelbeds_api_hotel.hotel
        hotel_name = hotel.name
        
        # Get Hotel summary
        hotel_summary = get_summary(calendar_room, hotel)

        # Find and replace all mentions of the actual Hotel name within the hotel summary
        hotel_summary = replace_all_hotel_related_occurences(hotel_summary, hotel_name)

        # Additional summary notes
        hotel_summary = add_hotel_summary_notes(hotel_summary, hotelbeds_api_hotel)
        
        
        # FOR TESTING:
        # print(f'# CALENDAR ROOM: {calendar_room.id}')
        # print('Hotel name: ', hotel_name, '\n')

        
        # Step 1: Update in Django
        '''
        if calendar_room.hostify_status == 'manually':
            
            # 1.1. Enrich all Manually Listed ... add translations_params to the request_data
            if calendar_room.hostify_id:
                calendar_room.request_data['translations_params'] = {
                    'listing_id': calendar_room.hostify_id,
                    'summary': hotel_summary
                }
                calendar_room.save()
            
            else:
                print(f"# ERROR: Manually listed room {calendar_room.id} without Hostify Id !!!")
                manually_listed_rooms_without_hostify_ids.append(calendar_room.id)

        else:

            # 1.2. Directly update
            calendar_room.request_data['translations_params']['summary'] = hotel_summary
            calendar_room.save()

        # Step 2: Update Hostify parent ... if we have Hostify ID
        updated_translation_params = calendar_room.request_data['translations_params']
        if calendar_room.hostify_id:
            response, errors = process_part(calendar_room.hostify_id, 'translations', updated_translation_params)

            if errors != '':
                print(
                    f'# ERROR: Hostify parent update:\n',
                    f'Calendar room: {calendar_room.id}\n',
                    f'Response: {response}\n',
                    f'Errors: {errors}\n',
                )
                cannot_update_parent_listings.append(calendar_room.id)
        '''

        # Step 3: Update Hostify child & Listing on AirBnb ... if we have Hostify Child ID
        if calendar_room.hostify_child_id and calendar_room.hostify_child_id != '0':
            update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                'listing_id': calendar_room.hostify_child_id,
                'description': {
                    "summary": hotel_summary
                }
            })
            
            if update_response['success']:
                directly_updated_child_listings.append(calendar_room.id)
            
            else:
                cannot_update_child_listings[calendar_room.id] = str(update_response)


    except Exception as e:
        exception_listings[calendar_room.id] = str(e)
        continue


# print('# Manually listed rooms without hostify ids: ', manually_listed_rooms_without_hostify_ids)
# print('# Cannot update parent listings: ', cannot_update_parent_listings)

print('# Directly updated child listings: ', directly_updated_child_listings, '\n')
print('# CANNOT UPDATE child listings: ', cannot_update_child_listings, '\n')
print(f'# EXCEPTION LISTINGS: ', exception_listings, '\n')