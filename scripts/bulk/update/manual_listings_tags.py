import django
import os
import sys

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom
from calendar_app.services import process_part
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

rooms = CalendarRoom.objects.filter(hostify_status='manually').exclude(request_data__location_params__has_any_keys=['tags'])
counter = 1
for room in rooms:
    print(counter,room.pk)
    counter +=1
    
    api_hotel = room.calendar_room_type.calendar_hotel.hotelbeds_api_hotel
    hotel = api_hotel.hotel

    hotel_code = api_hotel.code
    room_code = room.calendar_room_type.code
    country = hotel.country.description
    city = hotel.city.title()

    tags = [str(hotel_code),room_code, country, hotel.destination_code, city, 'LEAP','MANUALLY_CREATED']

    location_params = room.request_data.get('location_params','')
    location_params['tags'] = tags

    room.request_data['location_params'] = location_params
    room.save()
    update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
        'listing_id': room.hostify_id,
        'tags': tags
    })
    if room.hostify_child_id:
        update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
            'listing_id': room.hostify_child_id,
            'tags': tags
        })
