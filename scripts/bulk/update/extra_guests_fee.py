import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()


from django.db.models import Q

from calendar_app.services import process_part
from calendar_app.models import CalendarRoom
from hotels.models import HotelRoom


from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

GUESTS_INCLUDED = 2

# Get list of all rooms with layout_params
calendar_rooms = CalendarRoom.objects.filter(request_data__has_any_keys=['booking_restrictions_params']).all()

print(calendar_rooms.count())
counter = 0
for calendar_room in calendar_rooms:
    booking_restrictions_params = calendar_room.request_data['booking_restrictions_params']
    if calendar_room.request_data['layout_params']['person_capacity'] > GUESTS_INCLUDED:
        try:
            counter+=1
            calendar_room_type = calendar_room.calendar_room_type
            hotel_room = HotelRoom.objects.filter(
                hotel__api_hotel__code=calendar_room_type.hotelbeds_hotel_id,
                room_code=calendar_room_type.code
            ).first()

            if hotel_room and hotel_room.extra_guest_fee > 0:
                extra_guest_price = hotel_room.extra_guest_fee                
                
                booking_restrictions_params['occupancy'] = GUESTS_INCLUDED
                booking_restrictions_params['extra_guest_price'] = extra_guest_price
                calendar_room.request_data['booking_restrictions_params'] = booking_restrictions_params
                calendar_room.save()        
                    
                ## TODO STEP 2: Update at Hostify all CalendarRooms EXCLUDING hostify_status in ('Ready For Listing', 'Information_error')
                if calendar_room.hostify_status in ['ready', 'info_errors', 'not_listed', 'waiting', 'updating', 'processed']: 
                    continue    

                
                response, errors = process_part(calendar_room.hostify_id, 'booking_restrictions', booking_restrictions_params)
                child_id = calendar_room.hostify_child_id
                if child_id and child_id != 0:
                    booking_restrictions_params['listing_id'] = child_id
                    print(counter, extra_guest_price, child_id)
                    response, errors = process_part(calendar_room.hostify_child_id, 'booking_restrictions', booking_restrictions_params)
                    update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                        'listing_id': child_id,
                        'extra_person': extra_guest_price,
                        'guests_included': GUESTS_INCLUDED 
                    })
            else:
                if not hotel_room:
                    print(f'Missing room type- {calendar_room_type}')
        except Exception as e:
            print(e)


    





# Docs

## Update Listing
## @link https://app.hostify.com/docs#update-listing
