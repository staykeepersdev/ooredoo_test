import django
import random
import os
import sys

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Get Listings Data
PAGE = 28
PER_PAGE = 1000

FROM = PER_PAGE * (PAGE-1)
TO = PER_PAGE * PAGE

response = LISTING_API_CLIENT.listings(page=PAGE, per_page=PER_PAGE)
if not response['success']:
    print('ERROR')
    sys.exit()

# Get count
total_listings = response['total'] # 26758

print(f'\n### GET NON-LEAP LISTINGS: From {FROM} to {TO}\n')

# Iterate through all listings
listings = response['listings']


leap_listings = []
non_leap_listings = []

for idx, listing in enumerate(listings):
    if listing['nickname'].startswith('LP_'):
        leap_listings.append(listing['id'])
    else:
        non_leap_listings.append(listing['id'])

    # OPTIONAL: Skip all LEAP listings
    # if listing['nickname'].startswith('LP_'):
    #     continue

    '''
    # Get detailed listings information

    listing_id = listing['id']
    name, nickname = listing['name'], listing['nickname']
    integration_id, integration_name = listing['integration_id'], listing['integration_name'] # integration_nickname
    country, city = listing['country'], listing['city']
    tags = listing['tags'] if listing['tags'] else ''

    # Listing: ID, Nickname, Country, City, Integration ID, Integration Name, Tags
    print(f'{listing_id},{nickname},{country},{city},{integration_id},{integration_name},{str(tags)}', file=f)
    '''

with open(f'listings{PAGE}.txt', 'w') as f:
    for non_leap_listing in non_leap_listings:
        # print(f'NON LEAP LISTING: {non_leap_listing}')
        print(non_leap_listing, file=f)
