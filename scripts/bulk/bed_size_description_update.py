import django
import random
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Hotels App
from hotels.models import HotelRoom

# Listing API Client
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

# Calendar App
from calendar_app.models import CalendarRoom
from calendar_app.services import (
    replace_all_hotel_related_occurences,
    add_hotel_summary_notes,
    process_part
)
from calendar_app.static_data.nickname_adjectives import ADJECTIVES
from calendar_app.static_data.zones import ZONES

# Get list of all rooms
LIMIT_START = 0
LIMIT_END = 3000

calendar_rooms = CalendarRoom.objects.all()[LIMIT_START:LIMIT_END]


def get_summary(calendar_room, hotel):

    if 'translations_params' in calendar_room.request_data:
        return calendar_room.request_data['translations_params'].get('summary', hotel.description)
    else:
        return hotel.description

counter = 0
for calendar_room in calendar_rooms:
    try:
        # Get Hotel data
        calendar_room_type = calendar_room.calendar_room_type
        calendar_hotel = calendar_room_type.calendar_hotel
        hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
        hotel = hotelbeds_api_hotel.hotel
        hotel_name = hotel.name
        
        # Get Hotel summary
        hotel_summary = get_summary(calendar_room, hotel)

        # Find and replace all mentions of the actual Hotel name within the hotel summary
        hotel_summary = replace_all_hotel_related_occurences(hotel_summary, hotel_name)

        # Additional summary notes
        hotel_summary = add_hotel_summary_notes(hotel_summary, hotelbeds_api_hotel)
        
        
        # FOR TESTING:
        # print(f'# LISTED ROOM: {calendar_room.id}', '\n')
        # print('Hotel name: ', hotel_name, '\n')
        # print('Hotel summary: ', hotel_summary, '\n')

        
        # Step 1:
        if calendar_room.hostify_status == 'manually':
            
            # 1.1. Enrich all Manually Listed ... add translations_params to the request_data
            print(f"# ERROR: Manually listed room {calendar_room.id} without Hostify Id !!!")

            calendar_room.request_data['translations_params'] = {
                'listing_id': calendar_room.hostify_id,
                'summary': hotel_summary
            }
            calendar_room.save()

        else:

            # 1.2. Update in Django
            calendar_room.request_data['translations_params']['summary'] = hotel_summary
            calendar_room.save()


        # Step 3: Update Hostify parent ... if we have Hostify ID
        updated_translation_params = calendar_room['translations_params']
        if calendar_room.hostify_id:
            response, errors = process_part(calendar_room.hostify_id, 'translations', updated_translation_params)


        # Step 4: Update Hostify child & Listing on AirBnb ... if we have Hostify Child ID
        if calendar_room.hostify_child_id and calendar_room.hostify_child_id != '0':
            update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                'listing_id': calendar_room.hostify_child_id,
                'description': {
                    "summary": hotel_summary
                }
            })
            print('Update response: ', update_response)
        
    except Exception as e:
        print(f'# EXCEPTION: {str(e)}')
        print(f'Counter: {counter}')
        print(f'Problem listing: {calendar_room.hostify_child_id}')

    counter += 1
