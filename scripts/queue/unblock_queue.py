import os
import sys
import django

sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from background_task.models import Task

# Find all Blocked Tasks
tasks = Task.objects.filter(locked_by__isnull=False).all()

# Unblock the Queue
for task in tasks:
	task.locked_by = None
	task.locked_at = None
	task.save()