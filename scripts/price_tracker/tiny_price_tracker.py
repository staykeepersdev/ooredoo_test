from datetime import datetime

def parse(string):
    ALLOTMENTS_INDEX = string.find("{'calendar'")

    date_time_received = datetime.strptime(string[0:18], '%Y-%m-%d %H:%M:%S')
    listing_id = int(string[32:ALLOTMENTS_INDEX].strip())
    allotments = string[ALLOTMENTS_INDEX:]
    
    tokens = {
        "{'calendar': [{": '',
        "'start_date': '": '',
        "', 'end_date': '": ':',
        "', 'is_available': ": ':',
        ", 'price': ": ':',
        "}, {": '\n',
        "}]}": ''
    }
    for token, replacement in tokens.items():
        allotments = allotments.replace(token, replacement)

    return date_time_received, listing_id, allotments

LISTING_ID = 45097
print(f'# LISTING: {LISTING_ID}')

f = open(f'scripts/price_track/LISTING_{LISTING_ID}_DATA.txt', 'r')
for row in f:
    date_time_received, _, allotments = parse(row)
    print(f'## Received: {date_time_received}\n')
    print(f'Allotments: \n{allotments}')


'''
NOTE:
To collect the data you can use the following grep command - from ooredoo root:
grep -h LISTING_ID logs/*.txt > scripts/price_track/LISTING_DATA.txt

Some examples:
grep -h 26879 logs/*.txt > scripts/price_track/LISTING_26879_DATA.txt
grep -h 32343 logs/*.txt > scripts/price_track/LISTING_32343_DATA.txt
grep -h 45097 logs/*.txt > scripts/price_track/LISTING_45097_DATA.txt
grep -h 50215 logs/*.txt > scripts/price_track/LISTING_50215_DATA.txt
grep -h 68676 logs/*.txt > scripts/price_track/LISTING_68676_DATA.txt
'''