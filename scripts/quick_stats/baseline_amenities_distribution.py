import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom

# Get list of all rooms
# LIMIT_START = 0
# LIMIT_END = 20000
calendar_rooms = CalendarRoom.objects.filter(
    airbnb_status='listed',
    request_data__has_any_keys=['amenities_params']
).all().order_by('id') # [LIMIT_START:LIMIT_END]

print(f'\nQuick amenities stats')
print(f'Rooms count: {len(calendar_rooms)}')

# has_soap = set() # No way to search for Soap !!!
has_towel = set() # Towels and bed linen
has_bed_linens = set() # Towels and bed linen
has_pillow = set()
has_pillow_menu = set()

has_hot_water = set() # No way to search for hot water !!!
has_bathroom = set()
has_private_bathroom = set()

has_washer = set() # Washer
has_dishwasher = set() # Dishwasher
has_essentials = set()
has_air_conditioning = set()

for calendar_room in calendar_rooms:
    room_id = calendar_room.id
    room_amenities = calendar_room.request_data['amenities_params']['amenities']

    try:
        for idx, room_amenity in enumerate(room_amenities):
            # if 'soap' in room_amenity.lower(): has_soap.add(room_id)
            if 'towel' in room_amenity.lower(): has_towel.add(room_id)
            if 'bed linens' in room_amenity.lower(): has_bed_linens.add(room_id)
            if 'pillow' in room_amenity.lower(): has_pillow.add(room_id)
            if 'pillow menu' in room_amenity.lower(): has_pillow_menu.add(room_id)

            if 'hot water' in room_amenity.lower(): has_hot_water.add(room_id)
            if 'bathroom' in room_amenity.lower(): has_bathroom.add(room_id)
            if 'private bathroom' in room_amenity.lower(): has_private_bathroom.add(room_id)
            
            if 'washer' in room_amenity.lower(): has_washer.add(room_id) # Washer = Dishwasher
            if 'dishwasher' in room_amenity.lower(): has_dishwasher.add(room_id)
            if 'essentials' in room_amenity.lower(): has_essentials.add(room_id)
            if 'air conditioning' in room_amenity.lower(): has_air_conditioning.add(room_id)
    
    except Exception as e:
        print(f'EXCEPTION {str(e)}')
        continue

print(
    f'Towel: {len(has_towel)}\n'
    f'Bed Linens: {len(has_bed_linens)}\n'
    f'Pillow: {len(has_pillow)}\n'
    f'Pillow Menu: {len(has_pillow_menu)}\n\n'

    f'Hot Water: {len(has_hot_water)}\n'
    f'Bathroom: {len(has_bathroom)}\n'
    f'Private bathroom: {len(has_private_bathroom)}\n\n'

    f'Washer: {len(has_washer)}\n'
    f'Dishwasher: {len(has_dishwasher)}\n'
    f'Essentials: {len(has_essentials)}\n'
    f'Air conditioning: {len(has_air_conditioning)}\n'
)