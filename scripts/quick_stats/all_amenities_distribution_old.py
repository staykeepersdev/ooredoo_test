import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom
from hotelbeds_data.models import APIFacility

from scripts.quick_stats.data_amenities.DJANGO_AMENITIES_LISTINGS import DJANGO_AMENITIES_LISTINGS

calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['amenities_params']
).all().order_by('id')

# ALL_AVAILABLE_HOSTIFY_AMENITIES = [] # Collect if needed

'''
# One-time extraction
def extract_all_available_django_amenities():
    ALL_AVAILABLE_DJANGO_AMENITIES = set()
    
    for calendar_room in calendar_rooms:
        room_amenities = calendar_room.request_data['amenities_params']['amenities']
        ALL_AVAILABLE_DJANGO_AMENITIES |= set(room_amenities)

    print(f'# ALL_AVAILABLE_DJANGO_AMENITIES: {ALL_AVAILABLE_DJANGO_AMENITIES}')

extract_all_available_django_amenities()
'''

################

'''
def fill_amenities_dict_with_listings():
    for calendar_room in calendar_rooms:
        room_amenities = calendar_room.request_data['amenities_params']['amenities']

        for room_amenity in room_amenities:
            DJANGO_AMENITIES_LISTINGS[room_amenity].append(calendar_room.id)

    # Print to file - the list is big
    with open('django_amenities_listings.txt', 'w') as f:
        print(DJANGO_AMENITIES_LISTINGS, file=f)

fill_amenities_dict_with_listings()
'''

################

def amenities_listings_or_count(count_only=True):
    
    with open('amenities_listings.csv', 'w') as f:
        for amenity_django, listings in DJANGO_AMENITIES_LISTINGS.items():
            count_or_listings = len(listings) if count_only else str(listings).replace(",", " ")

            api_facility = APIFacility.objects.filter(description=amenity_django).first()
            amenity_hostify = api_facility.hostify_description if api_facility else 'API FACILITY NOT FOUND'

            print(f'{amenity_django},{count_or_listings},{amenity_hostify}', file=f)

amenities_listings_or_count()
