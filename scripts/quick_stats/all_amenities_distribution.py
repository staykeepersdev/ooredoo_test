import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom
from hotelbeds_data.models import APIFacility

from scripts.quick_stats.data_amenities.HOSTIFY_AMENITIES_LISTINGS import HOSTIFY_AMENITIES_LISTINGS

HOSTIFY_AMENITIES = HOSTIFY_AMENITIES_LISTINGS.keys()

# Gets all presently used amenities
# amenities = APIFacility.objects.exclude(hostify_description__contains='--')

calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['amenities_params']
).all().order_by('id')

'''
def fill_amenities_dict_with_listings():
    for calendar_room in calendar_rooms:
        room_amenities = calendar_room.request_data['amenities_params']['amenities']

        for room_amenity in room_amenities:
            if room_amenity in HOSTIFY_AMENITIES:
                HOSTIFY_AMENITIES_LISTINGS[room_amenity].append(calendar_room.id)

    # Print to file - the list is big
    with open('hostify_amenities_listings.txt', 'w') as f:
        print(HOSTIFY_AMENITIES_LISTINGS, file=f)

fill_amenities_dict_with_listings()
'''

def amenities_listings_or_count(count_only=True):
    
    with open('amenities_listings.csv', 'w') as f:
        for amenity_hostify, listings in HOSTIFY_AMENITIES_LISTINGS.items():
            count_or_listings = len(listings) if count_only else str(listings).replace(",", " ")

            print(f'{amenity_hostify},{count_or_listings}', file=f)

amenities_listings_or_count()