from datetime import timedelta, datetime
import re

strftime = datetime.strftime

from hotels.models import Hotel
from scripts.cache_api_certification.exercises import CURRENT_EXERCISE_DATA
from scripts.cache_api_certification.contract_header import CONTRACT_HEADER
from scripts.cache_api_certification.consts import PRINT_MODE

# Blocks regex
def get_blocks(rates_content):
    regex = re.compile(r'''
        \{(....)\}
        (.*?)
        \{/....\}
        ''', re.VERBOSE | re.DOTALL) # re.MULTILINE might be needed

    matches = regex.findall(rates_content)
    return dict(matches)


## Get hotel object by hotel_code
def get_hotel(line) -> Hotel:
    content = line.strip().split(':')
    hotel_code = content[7]

    try:
        hotel = Hotel.objects.get(api_hotel__code=hotel_code)
    except:
        hotel = None
    
    return hotel


# Paxes & Rooms utils
def paxes_to_counts(paxes_list):
    INFANT_BOUNDARY_AGE = CONTRACT_HEADER.infant_boundary_age
    ADULT_BOUNDARY_AGE = CONTRACT_HEADER.adult_boundary_age

    count_paxes = len(paxes_list)
    count_infants = len([pax for pax in paxes_list if pax['age'] <= INFANT_BOUNDARY_AGE])
    count_children = len([pax for pax in paxes_list if pax['age'] > INFANT_BOUNDARY_AGE and pax['age'] <= ADULT_BOUNDARY_AGE])
    count_adults = len([pax for pax in paxes_list if pax['age'] > ADULT_BOUNDARY_AGE])

    return count_paxes, count_adults, count_children, count_infants


def count_paxes_within_age_bounds(paxes_list, minimum_age, maximum_age):
    count = 0
    for pax in paxes_list:
        if minimum_age <= pax['age'] <= maximum_age:
            count += 1
    
    return count


# https://stackoverflow.com/questions/1060279/iterating-through-a-range-of-dates-in-python
def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)


# https://stackoverflow.com/questions/37868117/overlapping-dates-between-two-date-ranges-in-python
def continous_range(d1, d2):
    delta = d2 - d1
    return set([d1 + timedelta(days=i) for i in range(delta.days + 1)])


def print_booking_info():
    if not PRINT_MODE: return

    exercise_id = CURRENT_EXERCISE_DATA["ID"]
    booking_date = strftime(CURRENT_EXERCISE_DATA["BOOKING_DATE"], '%d %m %Y')
    booking_from = strftime(CURRENT_EXERCISE_DATA["FROM"], '%d %m %Y')
    booking_to = strftime(CURRENT_EXERCISE_DATA["TO"], '%d %m %Y')
    booking_duration = (CURRENT_EXERCISE_DATA["TO"] - CURRENT_EXERCISE_DATA["FROM"]).days
    count_paxes, count_adults, count_children, count_infants = paxes_to_counts(CURRENT_EXERCISE_DATA['PAXES'])    

    print(
        f'\n# EXERCISE NO: {exercise_id}\n\n'

        f'BOOKING INITIATED ON: {booking_date}\n'
        f'Booking from: {booking_from}\n'
        f'Booking to: {booking_to}\n'
        f'Booking duration: {booking_duration}\n\n'

        f'Count paxes: {count_paxes}\n'
        f'Count adults: {count_adults}\n'
        f'Count children: {count_children}\n'
        f'Count infants: {count_infants}\n'
    )


def show_exercise_error(error_message, type='ERROR'):
    exercise_id = CURRENT_EXERCISE_DATA['ID']
    exercise_filename = CURRENT_EXERCISE_DATA['RATES_FILENAME']
    
    print(f'\n# EXERCISE {type}\n')

    print(f'Exercise id: {exercise_id}')
    print(f'Exercise filname: {exercise_filename}')

    print(f'\n{error_message}\n')