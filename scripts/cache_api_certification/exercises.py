import datetime
str_to_datetime = datetime.datetime.strptime

# The example Exercise doesn't have Inventory included
EXERCISE_NO_0 = {
    'ID': 0,
    'RATES_FILENAME': 'example_exercise.txt',
    'BOOKING_DATE': str_to_datetime('01/06/2018', '%d/%m/%Y'), # This is fabricated
    'FROM': str_to_datetime('17/07/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('22/07/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40 + 2 children at age 5 & 11
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "CH",
            "age": 5
        }
    ]
}

EXERCISE_NO_1 = {
    'ID': 1,
    'RATES_FILENAME': 'exercise1-1_101263_M_F.txt',
    'BOOKING_DATE': str_to_datetime('01/04/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('09/05/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('15/05/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40 + 2 children at age 5 & 11
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "CH",
            "age": 5
        },
        {
            "type": "CH",
            "age": 11
        }
    ]
}

EXERCISE_NO_2 = {
    'ID': 2,
    'RATES_FILENAME': 'exercise2-1_102675_M_F',
    'BOOKING_DATE': str_to_datetime('01/04/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('15/06/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('20/06/2018', '%d/%m/%Y'),
    'PAXES': [ # 4 Adults at age 40
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        }
    ]
}

EXERCISE_NO_3 = {
    'ID': 3,
    'RATES_FILENAME': 'exercise3-195_3324_M_F',
    'BOOKING_DATE': str_to_datetime('05/04/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('17/05/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('18/05/2018', '%d/%m/%Y'),
    'PAXES': [ # 4 Adults at age 40
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        }
    ]
}

EXERCISE_NO_4 = {
    'ID': 4,
    'RATES_FILENAME': 'exercise4-138_15964_M_F',
    'BOOKING_DATE': str_to_datetime('21/03/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('15/04/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('22/04/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        }
    ]
}

EXERCISE_NO_5 = {
    'ID': 5,
    'RATES_FILENAME': 'exercise5-1_99303_O_F',
    'BOOKING_DATE': str_to_datetime('04/09/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('27/09/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('01/10/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        }
    ]
}

EXERCISE_NO_6 = {
    'ID': 6,
    'RATES_FILENAME': 'exercise6-148_12520_M_F',
    'BOOKING_DATE': str_to_datetime('02/10/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('03/10/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('09/10/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40 + 2 children at age 5 & 0
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "CH",
            "age": 5
        },
        {
            "type": "CH",
            "age": 0
        }
    ]
}

EXERCISE_NO_7 = {
    'ID': 7,
    'RATES_FILENAME': 'exercise7-148_20767_M_F-solved',
    'BOOKING_DATE': str_to_datetime('02/04/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('14/05/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('20/05/2018', '%d/%m/%Y'),
    'PAXES': [ # 1 Adults at age 40 + 2 children at age 5 & 7
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "CH",
            "age": 5
        },
        {
            "type": "CH",
            "age": 7
        }
    ]
}

EXERCISE_NO_8 = {
    'ID': 8,
    'RATES_FILENAME': 'exercise8-47_47338_M_F',
    'BOOKING_DATE': str_to_datetime('05/04/2018', '%d/%m/%Y'),
    'FROM': str_to_datetime('17/05/2018', '%d/%m/%Y'),
    'TO': str_to_datetime('18/05/2018', '%d/%m/%Y'),
    'PAXES': [ # 2 Adults at age 40
        {
            "type": "AD",
            "age": 40
        },
        {
            "type": "AD",
            "age": 40
        }
    ]
}

CURRENT_EXERCISE_DATA = EXERCISE_NO_5

CACHE_API_CERTIFICATION_FOLDER = './scripts/cache_api_certification/data/'
CACHE_API_RATES_FILEPATH = CACHE_API_CERTIFICATION_FOLDER + CURRENT_EXERCISE_DATA['RATES_FILENAME']





'''
# Hotelbeds CERTIFICATION REQUIREMENTS:

Step 1: Look for a rate plan that satisfies the requested occupancy and stay dates.
Step 2: Check booking rules for selected rate plans.
Step 3: For each selected rate plan look for the rates that match the dates of stay to get the base price.
Step 4: For each selected rate plan look for the applicable board offers to get all the possible board prices.
Step 5: For each selected contract look for free offers that apply per stay.
Step 6: For each selected rate plan look for offers that apply once per pax per night. For each night of stay apply the discount to the base amount and add the result to the accumulated final price, once per pax covered by the offer.
Step 7: For each selected rate plan look for offers that apply per night and per room. For each night of stay apply the discount to the base amount and add the result to the accumulated final price.
Step 8: For each selected rate plan look for offers that apply per stay. For each pax or room apply the discount to the base amount and add the result to the accumulated final price.
Step 9: For each pax apply the Free offers discount to the base amount and add the result to the accumulated final price.
'''