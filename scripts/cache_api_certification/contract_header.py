import datetime
strptime = datetime.datetime.strptime

from scripts.cache_api_certification.consts import (
    PRINT_MODE,
    BOARD_TYPES
)

# Non-Enforced Singleton Pattern
class ContractHeaderSingleton:

    def init_data(self, raw_line):
        data_items = raw_line.strip().split(':')

        # Contract Header Data:
        # @link https://developer.hotelbeds.com/docs/read/apitude_cache/files/File_Specification

        # One value is too much ???

        # External Inventory are contracts with Hotel Suppliers & Hotel Chains like: Hilton, Derbysoft, etc...
        # Internal Inventory are direct contracts with Single Hotels
        self.external_inventory = True if data_items[0] == 'Y' else False
        self.internal_inventory = not self.external_inventory
        self.destination_code = data_items[1]

        # Contract number and office code are unique-together identifier for a contract
        # Contract numbers can repeat for different office codes
        self.office_code = int(data_items[2])
        self.contract_number = int(data_items[3])
        
        # Used for Validation purposes
        self.contract_name = data_items[4]
        
        # Meal Plans (board) and Room Type descriptions are dependent on company code ... but HOW ?
        self.company_code = data_items[5]
        self.type_of_service = data_items[6] # For now it's just H. In future it could be: Transfers, Excursions, etc...

        # HB & Giata codes
        self.hotel_code = int(data_items[7])
        self.giata_hotel_code = data_items[8] # Unreliable, can be null
        
        # Travel start-end dates
        self.initial_date = strptime(data_items[9], '%Y%m%d')
        self.end_date = strptime(data_items[10], '%Y%m%d')
        
        # WHEN NO_HOTEL = 'Y' -> It is Roulette contract with no specific accommodation code defined
        # WHEN NO_HOTEL = 'N' -> The Hotel Code should always be provided
        self.no_hotel = True if data_items[0] == 'Y' else False
        self.is_roulette_contract = self.no_hotel
        
        '''
        # Roulette Contracts:
        
        Roulette contracts mean that the client can be allocated in any of the resorts of the category and zone chosen.
        The name of the resort the client will be staying in will be confirmed a few days before arrival.

        You can exclude this from your account, so that these contracts are blocked if you do not want to sell them.
        There are no Roulette Contracts in the certification exercise file !
        '''

        # The currency can be either: GBP, EUR or USD
        self.currency = data_items[12]

        # This seems to be a default Base Board set for the whole contract
        self.general_base_board = data_items[13] 

        # The available Contract Classifications are listed in consts.py
        # For Certification - only NOR is used
        self.classification = data_items[14]
        # The classifications we do not plan to sell can be excluded
        # We will have to notify HB which ones we're not planning to use
        
        # Either M or P: Merchant model or Direct Payment model
        # When M: Gross price appears only when Recommended Retail Price (RRP) is required .. Net price always appears
        # When P: Gross price only .. Net price missing
        self.payment_model = data_items[15]
        # As we are paying directly to HB this will always be set to M
        
        # Y: Daily price is based on the first night of stay i.e. taken from the first night and multiplied by the number of nights 
        # N: Daily price is based on a nightly basis (pro-rata) i.e. taken from each night of the stay
        self.daily_price = True if data_items[16] == 'Y' else False
        
        # N: Release days values are taken for release for first day of booking. Check release date only for the first night of the stay
        # Y: Check release date for each night of the stay
        self.release_days = True if data_items[17] == 'Y' else False

        # Minimum age a pax is considered child for price purposes
        # Age below (non-inclusive) is considered an infant
        self.minimum_child_age = int(data_items[18])
        
        # Maximum child age when define the number of adults and children in a room for checking the occupancy restrictions
        # If 12 a passenger above 12 (non-inclusive) is considered an adult
        self.maximum_child_age = int(data_items[19])

        # B: This contract can be sold as hotel only or packaged
        # P: This contract can be sold only when packaging
        # Package discount in the CNSU section appears, it needs to be applied when packaging
        self.opaque = data_items[20]
        self.hotel_only_or_packaged = True if self.opaque == 'B' else False
        self.packaged_only = True if self.opaque == 'P' else False

        # Rate type 
        # Y: Fix rate. Will be supported in future. 
        # N: Variable rate
        self.fix_rate = True if data_items[21] == 'Y' else False
        self.variable_rate = not self.fix_rate
        
        # According to HB this should Always be U :)
        self.contract_type = data_items[22]
        
        # Maximum rooms allowed for booking
        self.maximum_rooms_allowed_for_booking = int(data_items[23]) if data_items[23] != '' else None 
        
        # For roulette contracts inform which hotel code to take the content description from
        self.reference_hotel_code_for_roulette_contracts = data_items[24] # In HB this field is called hotel_content
        
        # Y: Price appearing at [CNCT.Price] is the mandatory final price for this product when sold to public 
        # N: Price appearing at [CNCT.Price] is not mandatory
        self.selling_price = True if data_items[25] == 'Y' else False
        
        # These seem to be internal fields only
        self.internal_field = True if data_items[26] == 'Y' else False
        self.internal_field_data = data_items[27]
        self.internal_classification = data_items[28]
        
        # External Inventory only
        # This seems to be related to {SIAP} blocks
        self.is_total_price_per_stay = True if data_items[29] == 'Y' else False
        
        # These seem to be internal fields only
        self.internal_field_1 = data_items[30] if len(data_items) == 31 else None
        self.internal_field_2 = data_items[31] if len(data_items) == 32 else None
        
        # Time from destination until you can do the availability request in order to get proper availability.
        # Example: Release 0 days until 13H means availability till the same day of entry till 13:00 (destination time)
        # Release 1 day until 10H means availability till the day before the entry till 10:00 (destination time)
        # self.release_per_hours = data_items[32]

    @property
    def infant_boundary_age(self):
        return self.minimum_child_age

    @property
    def adult_boundary_age(self):
        return self.maximum_child_age

    
    # Print related ...
    @property
    def inventory_type(self):
        return 'INTERNAL' if self.internal_inventory else 'EXTERNAL'

    @property
    def contract_can_be_sold_as(self):
        if self.hotel_only_or_packaged: return 'Hotel only or packaged'
        if self.packaged_only: return 'Packaged only'
        return ''

    @property
    def payment_model_is(self):
        if self.payment_model == 'M': return 'Merchant model'
        if self.payment_model == 'P': return 'Direct payment'
        return ''

    @property
    def selling_price_is(self):
        if self.selling_price:
            return '[CNCT.Price] is the mandatory final price for this product when sold to public'
        else:
            return '[CNCT.Price] is not mandatory'

    def pretty_print(self):
        if not PRINT_MODE: return

        print(
            f'\n# CONTRACT HEADER\n\n',

            f'Inventory type: {self.inventory_type} \n',
            f'Destination: {self.destination_code}\n\n',

            f'Payment model: {self.payment_model_is} - {self.payment_model}\n',
            f'Currency: {self.currency}\n\n',

            f'Contract can be sold as: {self.contract_can_be_sold_as}\n',
            f'Maximum rooms allowed for booking: {self.maximum_rooms_allowed_for_booking}\n\n',

            f'General Base Board: {BOARD_TYPES[self.general_base_board]} {self.general_base_board}\n',
            f'Selling Price Is: {self.selling_price_is}\n\n',

            f'Infant Boundary Age: {self.infant_boundary_age}\n',
            f'Child/Adult Boundary Age: {self.adult_boundary_age}\n\n',
        )


    # For debugging
    def props(self):
        x = self
        return dict((key, getattr(x, key)) for key in dir(x) if key not in dir(x.__class__))


CONTRACT_HEADER = ContractHeaderSingleton()