import os
import sys

import django
sys.path.append('/srv/www/ooredoo/')
sys.path.append('C:\\server\\winwin\\ooredoo\\')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

# Contract Header
from scripts.cache_api_certification.contract_header import CONTRACT_HEADER

# Model Managers
from scripts.cache_api_certification.model_managers import (
    RoomTypeManager,
    RoomInventoryManager,
    RoomsRatesManager,
    BoardSupplementsAndDiscountsManager,
    SupplementsAndDiscountsManager
)

# Utils
from scripts.cache_api_certification.utils import (
    get_blocks,
    get_hotel,
    print_booking_info,
    show_exercise_error
)

# Exercises
from scripts.cache_api_certification.exercises import CACHE_API_RATES_FILEPATH

# Main
with open(CACHE_API_RATES_FILEPATH, 'r') as f:
    rates_content = f.read()

blocks = get_blocks(rates_content)
# print(blocks)


# Contract header
raw_contract_header_line = blocks['CCON']
CONTRACT_HEADER.init_data(raw_contract_header_line)
# hotel = get_hotel(raw_contract_header_line)

# Room types
raw_room_types = blocks['CNHA']
ROOM_TYPE_MANAGER = RoomTypeManager(raw_room_types)

# Room inventories
raw_stop_sales = blocks['CNPV'] # @todo for Live implementation !
raw_minimum_maximum_stay = blocks['CNEM'] # @todo @required
raw_room_inventory = blocks['CNIN']
ROOM_INVENTORY_MANAGER = RoomInventoryManager(raw_stop_sales, raw_minimum_maximum_stay, raw_room_inventory)

# Room prices
raw_room_prices = blocks['CNCT']
ROOMS_RATES_MANAGER = RoomsRatesManager(raw_room_prices)

# Board supplements and discounts
raw_board_supplements_and_discounts = blocks['CNSR']
BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER = BoardSupplementsAndDiscountsManager(raw_board_supplements_and_discounts)

# Supplements and discounts
raw_supplements_and_discounts = blocks['CNSU']
SUPPLEMENTS_AND_DISCOUNTS_MANAGER = SupplementsAndDiscountsManager(raw_supplements_and_discounts)


# ACTUAL REQUIREMENTS

# Step 0: Print Booking info & Contract Header
print_booking_info()
CONTRACT_HEADER.pretty_print()

## Step 1. Check which room types cover the Occupancy & Dates requirements:
ROOM_TYPE_MANAGER.count_rooms_required()
ROOM_TYPE_MANAGER.print_rooms_of_type_with_required_count()

ROOM_INVENTORY_MANAGER.set_room_type_manager(ROOM_TYPE_MANAGER)
ROOM_INVENTORY_MANAGER.init_available_inventory()
ROOM_INVENTORY_MANAGER.print_available_inventory()


## Step 2: Check booking rules for selected rate plans.
## These are done intrinsically in Step 1


## Step 3: For each selected rate plan look for the rates that match the dates of stay to get the base price.
ROOMS_RATES_MANAGER.set_only_available_room_types( ROOM_INVENTORY_MANAGER.available_room_types )
ROOMS_RATES_MANAGER.init_room_rates()
ROOMS_RATES_MANAGER.print_room_rates()


## Step 4: For each selected rate plan look for the applicable board offers to get all the possible board prices.
# BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER.set_room_rates_manager( ROOMS_RATES_MANAGER )
BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER.init_applicable_board_offers()
BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER.print_map_board_code_daily_rates()


## Step 5: IGNORE -> All {CNGR} blocks with Free Offers Per Stay are luckily empty :)
## @todo This will have to be included for the Live implementation


## Step 6 & Step 7: 
SUPPLEMENTS_AND_DISCOUNTS_MANAGER.init_applicable_offers()


### Step 6 - 2nd pass: @todo
ROOMS_RATES_MANAGER.set_supplements_and_discounts_manager(SUPPLEMENTS_AND_DISCOUNTS_MANAGER)
ROOMS_RATES_MANAGER.apply_base_board_offers()


### Step 7 - 2nd pass: @todo
BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER.set_supplements_and_discounts_manager(SUPPLEMENTS_AND_DISCOUNTS_MANAGER)
# BOARD_SUPPLEMENTS_AND_DISCOUNTS_MANAGER.apply_board_offers()