PRINT_MODE = True

# @link https://developer.hotelbeds.com/docs/read/apitude_cache/files/File_Specification
BLOCKS_BASIC = {
    'CCON': 'Contract header',
    'CNHA': 'Room Types',
    'CNNH': 'No hotel contracts',
    'CNPR': 'Promotions',
    'CNHF': 'Handling fees',
    'ATAX': 'Tax breakdown',
    'CNCL': 'Valid Markets',
}

# @link https://developer.hotelbeds.com/docs/read/apitude_cache/files/file_specification/Internal_Inventory_Process
BLOCKS_INTERNAL_INVENTORY = {
    'CNIN': 'Room inventory',
    'CNCT': 'Room prices',
    'CNSR': 'Board supplements and discounts',
    'CNSU': 'Supplements and discounts',
    'CNPV': 'Stop sales',
    'CNGR': 'Frees',
    'CNOE': 'Combinable offers',
    'CNEM': 'Minimum and maximum stay',
    'CNTA': 'Rate codes',
    'CNES': 'Check in and check out',
    'CNCF': 'Cancellation fees'
}

BOARD_TYPES = {
    'RO': 'Room Only',
    'SC': 'Self Catering',
    'BB': 'Bed and Breakfast',
    'HB': 'Half Board',
    'FB': 'Full Board',
    'AI': 'All Inclusive',
    'UAI/PAI': 'Ultra/Premium All inclusive'
}

CONTRACT_CLASSIFICATIONS = {
    # Standard Pricing for normal contracts and rates.
    'NOR': 'Standard Pricing',
    
    # Non Refundable Rate: This classification identifies the contracts with
    # restrictive cancelation fees for the whole duration - 100% stay from the moment of booking.
    'NRF': 'Non Refundable Rate',

    # Contracts for special offers. Not all the offers will be classified as SPE,
    # only the contracts with long term offers (entire contract).
    'SPE': 'Contracts for special offers',

    'CDI': 'Direct Contract',

    # Hotel Beds preferential product
    'GAR': 'Guaranteed rates and allotment',

    'OFE': 'Offer',

    # This is the regular classification for packages - ski, activities, excursions, spa sessions, dinners,
    # + other extra services usually supplied by the hotel, and sold together with the room.
    'PAQ': 'Packages',

    'NRP': 'Non-refundable package',

    # Spefici destinations: Andorra and Pyrenees
    'P01': 'Ski packages in specific destinations',

    # Rates valid only for Senior citizens. Be aware that guests may be requested to show their ID
    # or passport at the hotel reception desk to benefit from this offer.
    'SEN': 'Senior citizen offer',

    # Rates valid only for Young people, limited to a certain age. Be aware that guests may be
    # requested to show their ID or passport at the hotel reception desk to benefit from this offer.
    'JUN': 'Young people offer',
 
    # This classification identifies contracts where the client can be allocated in any of the resorts of the
    # category and zone chosen. The name of the definitive property is confirmed a few days before arrival.
    'ROU': 'Roulette Contract',

    # Only used for Hotelopia. It identifies the contract that has to be sold with
    # hotel name and details hidden. Once the booking is made the name of the hotel is revealed.
    'CNF': 'Confidential rates',

    # Only used when a contract is exclusive for a client, with own allotment.
    # Only this client will see this contract and identify these specific hotels to display them first or promote them.
    'CUP': 'Own-Allotment touroperator',

    # Contracts with discount for large families.
    # The property is entitled to request proof on arrival.
    'FAM': 'Large family discounts',

    # The property will request proof, in this case proof of marriage.
    'HNO': 'Honeymoon rates',


    # RESIDENT classifications that remain active ...
    'BAL': 'Discount for residents of Balearic Islands',
    'CAN': 'Discount for residents of Canary Islands',
    'YUK': 'Discount for residents of Yukatan',

    # FLASH classifications that remain active (only Hotelopia):
    'FLA': 'Flash sales'
}

SUPPLEMENTS_AND_DISCOUNTS_TYPES = {
    'B': 'Early booking',
    'C': 'Additional/Extra Bed',
    'E': 'Fixed stay',
    'F': 'Infant',
    'G': 'General',
    'I': 'Double for individual use ',
    'K': 'Turbo early booking',
    'L': 'Long stay',
    'M': 'Minimum stay',
    'N': 'Child discount',
    'O': 'Operation days',
    'Q': 'Only available for Airline discounts',
    'U': 'Last minute',
    'V': 'Apply supplement based on arrival date times number of nights if arrival date is in the Travel Start / End date period'
}

SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES = {
    'A': 'Absolute amount',
    'B': 'On base board',
    'M': 'Absolute amount plus board supplement',
    'N': 'Per night, base board plus supplement',
    'R': 'On board supplement',
    'T': 'Only once. Check in date must be within the travel start and end date. Apply once to check in date.',
    'U': 'Only once type if any date of the booking overlaps with the period. The day to apply the offer is the first one that overlaps.'
}