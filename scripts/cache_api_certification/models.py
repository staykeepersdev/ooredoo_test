import datetime
import numpy as np
import re

strptime = datetime.datetime.strptime

from scripts.cache_api_certification.utils import paxes_to_counts
from scripts.cache_api_certification.allotments_managers import (
    AllotmentsManager,
    PriceAllotmentsManager
)
from scripts.cache_api_certification.mixins import ExerciseDataMixin
from scripts.cache_api_certification.contract_header import CONTRACT_HEADER
from scripts.cache_api_certification.consts import (
    SUPPLEMENTS_AND_DISCOUNTS_TYPES,
    SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES
)

class BaseRoomDataModel(ExerciseDataMixin):
    
    def __init__(self):
        self.full_room_code = f'{self.room_code}-{self.characteristic}'

        super().__init__()


class RoomType(BaseRoomDataModel):
    
    def __init__(self, raw_room_data):
        room_data = raw_room_data.strip().split(':')

        self.room_code = room_data[0]
        self.characteristic = room_data[1]
        
        self.standard_capacity = int(room_data[2] or 0)
        self.minimum_pax = int(room_data[3] or 0)
        self.maximum_pax = int(room_data[4] or 0)

        self.maximum_adults = int(room_data[5] or 0)
        self.maximum_children = int(room_data[6] or 0)
        self.maximum_infants = int(room_data[7] or 0)
        
        # New fields
        self.minimum_adults = int(room_data[8] or 0)
        self.minimum_children = int(room_data[9] or 0)

        # Assume we need at least 1 room
        self.rooms_required = 1

        super().__init__()

    # Count how many rooms we will actually need for accomodation
    def count_rooms_required(self, paxes_list):
        count_paxes, allotted_adults, allotted_children, allotted_infants = paxes_to_counts(paxes_list)

        if self.maximum_pax > 0:

            # Reset allotted to 0 when there is no specific Room Type limitation
            if self.maximum_adults == 0: allotted_adults = 0
            if self.maximum_children == 0: allotted_children = 0
            if self.maximum_infants == 0: allotted_infants = 0

            rooms_required = 0
            while (allotted_adults > 0 or allotted_children > 0 or allotted_infants > 0):
                allotted_adults -= self.maximum_adults
                allotted_children -= self.maximum_children
                allotted_infants -= self.maximum_infants

                rooms_required += 1
        else:
            rooms_required = 1

        self.rooms_required = rooms_required
        


class RoomInventory(BaseRoomDataModel):
    def __init__(self, raw_inventory_data):
        inventory_data = raw_inventory_data.strip().split(':')

        self.start_date = strptime(inventory_data[0], '%Y%m%d')
        self.end_date = strptime(inventory_data[1], '%Y%m%d')

        self.room_code = inventory_data[2]
        self.characteristic = inventory_data[3]
        self.rate_code = inventory_data[4] or None

        self.raw_allotments = inventory_data[5]
        self.allotments_manager = AllotmentsManager(
            self.raw_allotments,
            self.start_date,
            self.end_date
        )

        super().__init__()


class RoomRate(BaseRoomDataModel):
    def __init__(self, raw_prices_data):
        prices_data = raw_prices_data.strip().split(':')

        self.start_date = strptime(prices_data[0], '%Y%m%d')
        self.end_date = strptime(prices_data[1], '%Y%m%d')

        self.room_code = prices_data[2]
        self.characteristic = prices_data[3]
        self.generic_rate = float(prices_data[4] or 0)
        self.market_price = float(prices_data[5] or 0)
        
        self.raw_allotments = prices_data[6]
        self.allotments = []
        self.price_allotments_manager = PriceAllotmentsManager(
            self.raw_allotments,
            self.start_date,
            self.end_date
        )

        super().__init__()
        # print( self.props(), '\n' )


class Board(ExerciseDataMixin):
    def __init__(self, raw_board_data):
        board_data = raw_board_data.strip().split(':')

        self.start_date = strptime(board_data[0], '%Y%m%d')
        self.end_date = strptime(board_data[1], '%Y%m%d')

        self.board_code = board_data[2]
        self.is_per_pax = True if board_data[3] == 'Y' else False
        self.is_per_room = not self.is_per_pax
        
        # Amount & Percentage: when Positive is Supplement ... when Negative is Discount
        self.amount = float(board_data[4] or 0)
        self.percentage = float(board_data[5] or 0)
        self.is_neutral = True if (self.amount == 0 and self.percentage == 0) else False

        self.rate = board_data[6]
        self.room_type = board_data[7]
        self.characteristic = board_data[8]

        # Age dependent
        self.minimum_age = int(board_data[9] or 0)
        self.maximum_age = int(board_data[10] or 0)
        self.is_age_bounded = True if (self.minimum_age or self.maximum_age) else False
        self.is_age_all = not self.is_age_bounded
        self.is_child_only = self.check_is_child_only()
        self.is_adult_only = self.check_is_adult_only()
        self.count_paxes_within_age_bounds = self.counting_paxes_within_age_bounds()

        # Weekday dependent
        self.allowed_on_monday = True if board_data[11] == 'Y' else False
        self.allowed_on_tuesday = True if board_data[12] == 'Y' else False
        self.allowed_on_wednesday = True if board_data[13] == 'Y' else False
        self.allowed_on_thursday = True if board_data[14] == 'Y' else False
        self.allowed_on_friday = True if board_data[15] == 'Y' else False
        self.allowed_on_saturday = True if board_data[16] == 'Y' else False
        self.allowed_on_sunday = True if board_data[17] == 'Y' else False
        self.is_day_limited = self.check_is_day_limited()

        self.internal_field = float(board_data[18])
        self.net_price = float(board_data[19] or 0)
        self.price = float(board_data[20] or 0)
        self.market_price = board_data[21]

    # Weekday related ...
    def check_is_day_limited(self):
        days_allowed = [value for prop, value in self.props().items() if prop.startswith('allowed_on_')]
        return all(days_allowed)

    def is_blocked_on(self, day_name):
        return not getattr(self, f'allowed_on_{day_name.lower()}')

    # @todo REQUIRED/SIMPLIFIED ONLY FOR CERTIFICATION ... in Live Mode the boards can be in all kinds of Age Boundaries
    def check_is_child_only(self):
        if self.is_age_all: return None

        if self.minimum_age <= CONTRACT_HEADER.adult_boundary_age:
            return True
        else:
            return False

    # @todo REQUIRED/SIMPLIFIED ONLY FOR CERTIFICATION ... in Live Mode the boards can be in all kinds of Age Boundaries
    def check_is_adult_only(self):
        if self.is_age_all: return None

        if self.minimum_age > CONTRACT_HEADER.adult_boundary_age:
            return True
        else:
            return False

    def counting_paxes_within_age_bounds(self):
        # If no age boundaries - include all paxes within this Room Board
        if self.is_age_all:
            return len(self.booking_paxes)

        if self.is_age_bounded:
            # If age boundaries - include only bounded paxes within this Room Board
            count = 0
            for pax in self.booking_paxes:
                if self.minimum_age <= pax['age'] <= self.maximum_age:
                    count += 1

            return count

    def compute_board_rate(self):
        if not self.amount: return 0

        if self.is_per_room:
            MULTIPLIER = 1
        
        if self.is_per_pax:
            MULTIPLIER = self.count_paxes_within_age_bounds

        return MULTIPLIER * self.amount

        # @todo this should be further improved after certification:
        # if self.percentage:

        return 0

# Supplement & Discount = fancy name for Offer
class SupplementAndDiscount(ExerciseDataMixin):
    
    def __init__(self, raw_data):
        super().__init__()

        data = raw_data.strip().split(':')

        self.start_date = strptime(data[0], '%Y%m%d')
        self.end_date = strptime(data[1], '%Y%m%d')

        self.application_start_date = strptime(data[2], '%Y%m%d') if data[2] != '' else None
        self.application_end_date = strptime(data[3], '%Y%m%d') if data[3] != '' else None
        
        # The Discount Code
        self.code = data[4]

        # Possible options for Type:
        self.of_type = data[5]
        self.of_type_info = SUPPLEMENTS_AND_DISCOUNTS_TYPES.get(self.of_type, '')

        self.is_per_pax = True if data[6] == 'Y' else False
        self.is_per_room = not self.is_per_pax

        self.opaque = True if data[7] == 'Y' else False
        self.packaging_only = self.opaque
        self.order = int(data[8] or 0)

        # Application type:
        self.application_type = data[9]
        self.application_type_info = SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES.get(self.application_type, '')

        # Amount & Percentage: when Positive is Supplement ... when Negative is Discount
        self.amount = float(data[10] or 0)
        self.percentage = float(data[11] or 0)
        self.is_neutral = True if (self.amount == 0 and self.percentage == 0) else False
        self.is_cumulative = True if data[12] == 'Y' else False

        self.rate = int(data[13] or 0)
        self.room_type = data[14]
        self.characteristic = data[15]
        self.board_code = data[16]

        # Minimum adults to apply the rule
        self.adults = int(data[17] or 0)

        # Indicates adult to which the rule applies: N and C
        self.pax_order = data[18]

        # Age dependent
        self.minimum_age = int(data[19] or 0)
        self.maximum_age = int(data[20] or 0)
        self.is_age_bounded = True if (self.minimum_age or self.maximum_age) else False
        self.is_age_all = not self.is_age_bounded

        # Refer to valuation process ?
        self.number_of_days = int(data[21] or 0)
        self.length_of_stay = int(data[22] or 0)
        
        # Refer to valuation process ?
        self.limit_date = strptime(data[23], '%Y%m%d') if data[23] != '' else ''

        # Weekday dependent
        self.allowed_on_monday = True if data[24] == 'Y' else False
        self.allowed_on_tuesday = True if data[25] == 'Y' else False
        self.allowed_on_wednesday = True if data[26] == 'Y' else False
        self.allowed_on_thursday = True if data[27] == 'Y' else False
        self.allowed_on_friday = True if data[28] == 'Y' else False
        self.allowed_on_saturday = True if data[29] == 'Y' else False
        self.allowed_on_sunday = True if data[30] == 'Y' else False

        self.net_price = float(data[19] or 0)
        self.price = float(data[20] or 0)
        self.market_price = data[21]

    def is_within_booking_range(self):
        return self.start_date <= self.booking_start < self.booking_end <= self.end_date

    def is_enclosing_booking_date(self):
        # Note: application_start_date & application_end_date can be null
        if self.application_start_date:
            if not self.application_start_date <= self.booking_date:
                return False

        if self.application_end_date:
            if not self.booking_date <= self.application_end_date:
                return False

        return True

    # Wrapper for all conditions required to activate an offer
    # By default assume it is applicable
    def is_applicable(self, per):
        if self.is_neutral: return False
        
        within_booking_range = self.is_within_booking_range()
        if not within_booking_range: return False

        enclosing_booking_date = self.is_enclosing_booking_date()
        if not enclosing_booking_date: return False

        per_field = getattr(self, f'is_{per}')
        if not per_field: return False

        # Minimum adults to apply the rule
        if self.booking_adults_count < self.adults:
            return False

        return True

    # Utils
    def print_offer(self, index=None):
        per = 'PER ROOM' if self.is_per_room else 'PER PAX'
        
        print(f'\n# APPLICABLE {per} OFFER:')
        
        if index: print(f'Index: {index}')
        for prop, value in self.props().items():
            print(f'{prop.replace("_", " ").capitalize()}: {value}')