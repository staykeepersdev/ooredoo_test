# What does the board basis mean?

## RO (Room Only)
No meals are included.

## SC (Self Catering)
No meals are included; however, your accommodation will be provided with catering facilities for you to cook light meals.

## BB (Bed and Breakfast)
Breakfast is included.

## HB (Half Board)
Breakfast and evening meals are included.
In some cases, you can choose to receive lunch instead of breakfast – the hotel will confirm this on arrival.

## FB (Full Board)
Breakfast, lunch and evening meals are included.

## AI (All Inclusive)
All meals and locally produced drinks are included.

## UAI/PAI (Ultra/Premium All inclusive)
As well as your meals, drinks and snacks, your resort may also provide daily re-stocked minibar, use of spa facilities and many other goodies.