import datetime
import re
import sys

from scripts.cache_api_certification.mixins import ExerciseDataMixin
from scripts.cache_api_certification.utils import paxes_to_counts
from scripts.cache_api_certification.consts import PRINT_MODE

class BaseAllotmentsManager(ExerciseDataMixin):
    def __init__(self, raw_allotments, start_date, end_date):
        self.allotments_start_date = start_date
        self.allotments_end_date = end_date
        self.allotments_duration = (end_date - start_date).days
        
        self.raw_allotments = raw_allotments
        self.all_allotments = []
        self.filtered_allotments = []

        self._init_allotments()

    def get_relative_indexes(self, booking_start_date, booking_end_date):
        booking_start_date_index = (booking_start_date - self.allotments_start_date).days
        booking_duration = (booking_end_date - booking_start_date).days
        booking_end_date_index = booking_start_date_index + booking_duration

        return booking_start_date_index, booking_end_date_index


class AllotmentsManager(BaseAllotmentsManager):

    def _init_allotments(self):
        allotments = re.findall(r'\((.*?)\)', self.raw_allotments)

        for counter, raw_allotment in enumerate(allotments):
            current_date = self.allotments_start_date + datetime.timedelta(days=counter)
            release_and_allotment = raw_allotment.split(',') # split into release & allotment

            self.all_allotments.append({
                'date': current_date,
                'release': int(release_and_allotment[0]),
                'allotment': int(release_and_allotment[1])
            })

    
    # Check if Booking-Range is a subset of Allotment-Range
    def is_booking_range_within_allotment_range(self, booking_start_date, booking_end_date):
        return self.allotments_start_date <= booking_start_date < booking_end_date <= self.allotments_end_date

    
    def has_availability(self, booking_start_date, booking_end_date, booking_rooms_required):
        if not self.is_booking_range_within_allotment_range(booking_start_date, booking_end_date):
            # @todo Raise exception for testing purposes
            return False, []

        # Get filtered allotment range
        booking_start_date_i, booking_end_date_i = self.get_relative_indexes(booking_start_date, booking_end_date)
        self.filtered_allotments = self.all_allotments[booking_start_date_i:booking_end_date_i]

        if len(self.filtered_allotments) == 0:
            # @todo Raise exception for testing purposes
            return False, self.filtered_allotments

        # Assume we have availability
        has_availability = True
        
        for allotment_day in self.filtered_allotments:
            if booking_rooms_required > allotment_day['allotment']:
                has_availability = False
                break

        return has_availability, self.filtered_allotments

    def print_filtered_allotments(self):
        if not PRINT_MODE: return
        
        for day in self.filtered_allotments:
            print(
                f'Date: {day["date"].strftime("%d %m %Y")}',
                f'\tAllotment: {day["allotment"]}'
                # f'\tRelease: {day["release"]}'
            )


class PriceAllotmentsManager(BaseAllotmentsManager):
    
    def _init_allotments(self):
        allotments = re.findall(r'\((.*?)\)', self.raw_allotments)
        
        for counter, raw_allotment in enumerate(allotments):
            current_date = self.allotments_start_date + datetime.timedelta(days=counter)
            price_allotment_list = raw_allotment.split(',') # split into price allotment

            if price_allotment_list[0] == 'Y':
                is_price_per_pax, is_price_per_room = True, False
            else:
                is_price_per_pax, is_price_per_room = False, True

            self.all_allotments.append({
                'date': current_date,
                'is_price_per_pax': is_price_per_pax,
                'is_price_per_room': is_price_per_room,
                'net_price': float(price_allotment_list[1]),
                'price': float(price_allotment_list[2]),
                'specific_rate': int(price_allotment_list[3]),
                'base_board': price_allotment_list[4],
                'amount': float(price_allotment_list[5])
            })

    def get_booking_rates(self, booking_start_date, booking_end_date):
        booking_start_date_i, booking_end_date_i = self.get_relative_indexes(booking_start_date, booking_end_date)
        self.filtered_allotments = self.all_allotments[booking_start_date_i:booking_end_date_i]

        return self.filtered_allotments

    @property
    def total_booking_rates_amount(self):
        count_paxes, count_adults, count_children, count_infants = paxes_to_counts(self.booking_paxes)
        paying_paxes = count_paxes + count_adults + count_children

        total = 0
        for allotment in self.filtered_allotments:
            if allotment['is_price_per_pax']:
                total += allotment['amount'] * count_adults

            if allotment['is_price_per_room']:
                total += allotment['amount']

        return total