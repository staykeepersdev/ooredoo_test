from scripts.cache_api_certification.exercises import CURRENT_EXERCISE_DATA
from scripts.cache_api_certification.utils import paxes_to_counts

class ExerciseDataMixin:

    EXERCISE = CURRENT_EXERCISE_DATA

    def __init__(self):
        (
            self.booking_paxes_count,
            self.booking_adults_count,
            self.booking_children_count,
            self.booking_infants_count
        ) = paxes_to_counts(self.booking_paxes)

    # Exercise related getters ...
    @property
    def booking_date(self):
        return self.EXERCISE['BOOKING_DATE']

    @property
    def booking_start(self):
        return self.EXERCISE['FROM']

    @property
    def booking_end(self):
        return self.EXERCISE['TO']

    @property
    def booking_duration(self):
        return (self.booking_end - self.booking_start).days

    @property
    def booking_paxes(self):
        return self.EXERCISE['PAXES']

    # For debugging
    def props(self):
        x = self
        return dict((key, getattr(x, key)) for key in dir(x) if key not in dir(x.__class__))
