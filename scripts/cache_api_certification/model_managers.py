import datetime
from datetime import date, timedelta
strftime = datetime.datetime.strftime

from scripts.cache_api_certification.contract_header import CONTRACT_HEADER

# Models
from scripts.cache_api_certification.models import (
    RoomType,
    RoomInventory,
    RoomRate,
    Board,
    SupplementAndDiscount
)
from scripts.cache_api_certification.mixins import ExerciseDataMixin
from scripts.cache_api_certification.consts import (
    PRINT_MODE,
    BOARD_TYPES,
    SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES
)
from scripts.cache_api_certification.utils import (
    date_range,
    count_paxes_within_age_bounds
)

class BaseManager(ExerciseDataMixin):

    def __init__(self, raw_lines):
        self.objects = []

        for raw_line in raw_lines.split('\n'):
            raw_data = raw_line.strip()

            if raw_data != '':
                self.objects.append( self.Model(raw_data) )

        super().__init__()

    def find_room_data_by_full_code(self, full_room_code):
        for room_data in self.objects:
            if room_data.full_room_code == full_room_code:
                return room_data


class RoomTypeManager(BaseManager):

    Model = RoomType
    
    def count_rooms_required(self):
        booking_paxes = self.booking_paxes

        # Track how many Rooms-of-Type we need to accommodate the required_paxes ... at least 1 and maybe more
        for room_type in self.objects:
            room_type.count_rooms_required( booking_paxes )

    def get_room_types(self):
        return self.objects

    def print_rooms_of_type_with_required_count(self):
        if not PRINT_MODE: return

        print('\n# ROOM TYPES -> REQUIRED ROOMS:')
        for room_type in self.objects:
            print(f'\n# {room_type.full_room_code} -> {room_type.rooms_required}')
            
            # Very important to compute Standard per-pax Price
            print('Standard capacity: ', room_type.standard_capacity, '\n')

            print('Minimum pax: ', room_type.minimum_pax)
            print('Maximum pax: ', room_type.maximum_pax, '\n')
            
            print('Minimum adults: ', room_type.minimum_adults)
            print('Maximum adults: ', room_type.maximum_adults, '\n')
            
            print('Minimum children: ', room_type.minimum_children)
            print('Maximum children: ', room_type.maximum_children, '\n')
            
            print('Maximum infants: ', room_type.maximum_infants)

class RoomInventoryManager(BaseManager):

    Model = RoomInventory

    def __init__(self, raw_stop_sales, raw_minimum_maximum_stays, raw_room_inventory):
        self.stop_sales = []
        self.minimum_maximum_stays = []

        # Init Stop Sales - @todo Live Data
        # for raw_stop_sale in raw_stop_sales.split('\n'):
        #     raw_data = raw_stop_sale.strip()

        #     if raw_data != '':
        #         self.stop_sales.append( StopSale(raw_data) ) @todo Add StopSale Model

        # Init Minimum-Maximum stay - @todo required now
        # for raw_minimum_maximum_stay in raw_minimum_maximum_stays.split('\n'):
        #     raw_data = raw_minimum_maximum_stay.strip()

        #     if raw_data != '':
        #         self.minimum_maximum_stays.append( MinimumMaximumStay(raw_data) ) @todo Add MinimumMaximumStay Model

        super().__init__(raw_room_inventory)


    def set_room_type_manager(self, ROOM_TYPE_MANAGER):
        self.ROOM_TYPE_MANAGER = ROOM_TYPE_MANAGER

    def init_available_inventory(self):
        # Init availability list
        self.available_room_types = []

        for room_inventory in self.objects:

            inventory_start = room_inventory.start_date
            booking_start, booking_end = self.booking_start, self.booking_end
            inventory_end = room_inventory.end_date

            # Initial validation
            if not self.is_booking_range_within_inventory_range(inventory_start, inventory_end):
                continue

            # Match Room Inventory & Room Types
            room_type_found = self.ROOM_TYPE_MANAGER.find_room_data_by_full_code(room_inventory.full_room_code)
            if not room_type_found: # @todo This should raise exception !
                continue

            # Get required number of rooms of this type
            booking_rooms_required = room_type_found.rooms_required
            
            # Validate if there is a Contract-Level limitation for allowed number of rooms for booking
            if CONTRACT_HEADER.maximum_rooms_allowed_for_booking != None:
                if booking_rooms_required > CONTRACT_HEADER.maximum_rooms_allowed_for_booking:
                    continue

            # Check if we have allotment for the whole stay with the required number of rooms
            room_has_availability, allotment_days_list = room_inventory.allotments_manager.has_availability(booking_start, booking_end, booking_rooms_required)

            if room_has_availability:
                if PRINT_MODE: print(f'\nSUCCESS: Room Inventory (Allotments) found: {room_type_found.full_room_code}')
                self.available_room_types.append(room_type_found.full_room_code)
            else:
                if PRINT_MODE: print(f'\nERROR: No Room Inventory availability (Allotments) for: {room_type_found.room_code}-{room_type_found.characteristic}')
                
            room_inventory.allotments_manager.print_filtered_allotments()

    # Check if Booking-Range is a subset of Inventory-Range
    def is_booking_range_within_inventory_range(self, inventory_start, inventory_end):
        return inventory_start <= self.booking_start < self.booking_end <= inventory_end

    def print_available_inventory(self):
        if not PRINT_MODE: return

        print('\nAvailable Inventory for room types:')
        print(self.available_room_types)


class RoomsRatesManager(BaseManager):

    Model = RoomRate

    # Dependency injection
    def set_supplements_and_discounts_manager(self, SUPPLEMENTS_AND_DISCOUNTS_MANAGER):
        self.SUPPLEMENTS_AND_DISCOUNTS_MANAGER = SUPPLEMENTS_AND_DISCOUNTS_MANAGER

    def set_only_available_room_types(self, only_available_room_types):
        self.only_available_room_types = only_available_room_types
    
    def is_room_rate_available(self, room_rate):
        return room_rate.full_room_code in self.only_available_room_types
    
    def init_room_rates(self):
        self.available_room_rates = []

        # More than one record is possible for every combination of room type and characteristic.
        # One record per rate. 
        for room_rate in self.objects:
            if not self.is_room_rate_available(room_rate):
                continue

            booking_start, booking_end = self.booking_start, self.booking_end
            booking_rates = room_rate.price_allotments_manager.get_booking_rates(booking_start, booking_end)
            total_booking_rates_amount = room_rate.price_allotments_manager.total_booking_rates_amount

            self.available_room_rates.append({
                'full_room_code': room_rate.full_room_code,
                'base_board': booking_rates[0]['base_board'], # The Base Board is repeated multiple times
                'booking_start': booking_start,
                'booking_end': booking_end,
                'booking_rates': booking_rates,
                'total_booking_rates_amount': total_booking_rates_amount
            })

    def get_available_room_rates(self):
        return self.available_room_rates

    # B Offers: On base board
    def apply_base_board_offers(self):
        b_offers_on_base_board = self.SUPPLEMENTS_AND_DISCOUNTS_MANAGER.get_offers_by_application_type('B')
        if not len(b_offers_on_base_board): return

        # print('Available B Offers')
        
        # @todo print('Apply Base Board Offers: ', b_offers_on_base_board)
        # for available_room_rate in self.available_room_rates:
        #     print(available_room_rate)
        #     available_room_rate['b_offer_on_base_board'] # Apply Offer

    # N Offers: Per night, base board plus supplement 
    def apply_per_night_base_board_plus_supplement(self):
        n_offers_per_night_base_board_plus_supplement = self.SUPPLEMENTS_AND_DISCOUNTS_MANAGER.get_offers_by_application_type('N')
        if not len(n_offers_per_night_base_board_plus_supplement): return

        # @todo print('Apply Per Night, Base board plus supplement: ', n_offers_per_night_base_board_plus_supplement)
        # for available_room_rate in self.available_room_rates:
        #     print(available_room_rate)
        #     available_room_rate['n_offer_per_night_base_board_plus_supplement'] # Apply Offer

    # 2nd pass: Apply offers based on Supplements & Discounts {CNSU} block
    def apply_offers(self):
        self.apply_base_board_offers()
        self.apply_per_night_base_board_plus_supplement()

        # What about:
        # A: Absolute amount
        # T:
        # U:

    def print_room_rates(self):
        if not PRINT_MODE: return

        print('\n# AVAILABLE ROOM RATES')

        for room_rate in self.available_room_rates:
            full_room_code = room_rate["full_room_code"]
            base_board = room_rate["base_board"]
            board_name = BOARD_TYPES[base_board]
            booking_rates = room_rate["booking_rates"]

            print(f'\n## Room Rates for: {full_room_code} - {board_name} {base_board}')
            
            for booking_rate in booking_rates:
                print(f'Date: {strftime(booking_rate["date"], "%d %m %Y")}', f'\tAmount: {booking_rate["amount"]}')

            total_booking_rates_amount = round(room_rate["total_booking_rates_amount"], 2)
            print(f'BASE BOARD TOTAL: \t {total_booking_rates_amount}')


class BoardSupplementsAndDiscountsManager(BaseManager):

    Model = Board

    # def set_room_rates_manager(self, ROOMS_RATES_MANAGER):
    #     self.ROOMS_RATES_MANAGER = ROOMS_RATES_MANAGER

    # Dependency injection
    def set_supplements_and_discounts_manager(self, SUPPLEMENTS_AND_DISCOUNTS_MANAGER):
        self.SUPPLEMENTS_AND_DISCOUNTS_MANAGER = SUPPLEMENTS_AND_DISCOUNTS_MANAGER

    def init_unique_board_codes(self):
        self.unique_board_codes = set()

        for room_board in self.objects:
            self.unique_board_codes.add(room_board.board_code)

    def get_boards_by_code(self, board_code):
        return [room_board for room_board in self.objects if room_board.board_code == board_code]
    
    # Compute all daily rates for specific board_code
    def compute_booking_daily_rates(self, board_code):
        # Find specific room boards - for example: only HB or BB
        boards_by_code = self.get_boards_by_code(board_code)
        
        # Init dict with all Booking Days as keys
        booking_daily_rates = {}
        for booked_day in date_range(self.booking_start, self.booking_end):
            booked_day_str = strftime(booked_day, "%d %m %Y")

            booking_daily_rates[booked_day_str] = []
            for room_board in boards_by_code:
                
                # print(f'ROOM BOARD: Amount {room_board.amount} / Percentage {room_board.percentage}')

                # If Booked Day within this specific Room Board date range
                if room_board.start_date <= booked_day <= room_board.end_date:

                    # Skip: If Nothing to Add or Substract from the base price ...
                    if room_board.is_neutral:
                        # Then No Daily Rate is included for this day
                        booking_daily_rates[booked_day_str].append(None)
                        continue

                    # Skip: If room_board Is Blocked for specific day names .. examples: Monday or Tuesday, etc..
                    if room_board.is_day_limited and room_board.is_blocked_on(booked_day.strftime("%A")):
                        booking_daily_rates[booked_day_str].append(None)
                        continue

                    # Finally compute the day rate
                    single_day_rate = room_board.compute_board_rate()

                    # Init single day
                    booking_daily_rates[booked_day_str].append({
                        'is_per_pax': room_board.is_per_pax,
                        'is_per_room': room_board.is_per_room,
                        'is_age_all': room_board.is_age_all,
                        'is_child_only': room_board.is_child_only,
                        'is_adult_only': room_board.is_adult_only,
                        'is_age_bounded': room_board.is_age_bounded,
                        'amount': room_board.amount,
                        'single_day_rate': single_day_rate
                    })

        return booking_daily_rates


    def init_map_board_code_daily_rates(self):
        self.map_board_code_daily_rates = {}
        
        for board_code in self.unique_board_codes:
            self.map_board_code_daily_rates[board_code] = self.compute_booking_daily_rates(board_code)


    def init_applicable_board_offers(self):

        # Init unique Board Codes
        self.init_unique_board_codes()

        # Init Board Rates by code
        self.init_map_board_code_daily_rates()

    # 2nd pass based on Supplements & Discounts ...
    def apply_board_offers(self):
        offers = self.SUPPLEMENTS_AND_DISCOUNTS_MANAGER.get_all_offers_by_application_type('base_board_only')
        
        self.apply_on_board_supplements(offers['R'])
        self.apply_per_night_base_board_plus_supplement(offers['N'])
        self.apply_only_once(offers['T'])
        self.apply_only_once_overlapping(offers['U'])
        self.apply_absolute_amount_board_supplement(offers['M'])

    def apply_on_board_supplements(self, offers):

        # Go through all Board Codes and their matching days + booking_daily_rates
        for board_code, days_booking_daily_rates in self.map_board_code_daily_rates.items():
            # print(board_code, daily_rates)

            # Go through all R Offers
            for offer in offers:
                # Not applicable offer
                if board_code != offer.board_code: continue

                # offer.print_offer()

                for day, booking_daily_rates in days_booking_daily_rates.items():
                    for booking_daily_rate in booking_daily_rates:

                        if offer.is_per_pax and booking_daily_rate['is_per_pax']:

                            # if offer.type == 'N':
                            # @todo Compute on Per Pax level
                            pass

                        if offer.is_per_room and booking_daily_rate['is_per_room']:
                            # @todo Compute on Per Room level
                            pass
                            
                        # Check if age dependent
                        # print(booking_daily_rate)

    def apply_per_night_base_board_plus_supplement(self, offers):
        pass

    def apply_only_once(self, offers):
        pass

    def apply_only_once_overlapping(self, offers):
        pass

    def apply_absolute_amount_board_supplement(self, offers):
        pass


    def print_map_board_code_daily_rates(self):
        if not PRINT_MODE: return

        for board_code, days_daily_rates in self.map_board_code_daily_rates.items():
            print('\n\n# BOARD CODE: ', board_code)

            for day, daily_rates in days_daily_rates.items():
                print('\n# SINGLE DATE: ', day)

                for index, daily_rate in enumerate(daily_rates):
                    
                    if daily_rate:
                        print(f'=> Daily rate: {index+1}')
                        
                        print(
                            f'Type: {"Per Pax" if daily_rate["is_per_pax"] else "Per Room"}\n'
                            f'Is all ages: {daily_rate["is_age_all"]}\n'
                            f'Is child only: {daily_rate["is_child_only"]}\n'
                            f'Is adult only: {daily_rate["is_adult_only"]}\n'
                            f'Amount: {daily_rate["amount"]}\n'
                            f'Single Day Rate: {daily_rate["single_day_rate"]}\n'
                            '\n'
                        )
                    else:
                        print('No matching daily rate')


class SupplementsAndDiscountsManager(BaseManager):

    Model = SupplementAndDiscount

    def init_unique_offer_codes(self):
        self.unique_offer_codes = set()

        # For debugging {CNSU}
        # i = 2
        for offer in self.objects:
            # offer.print_offer(i)
            self.unique_offer_codes.add(offer.code)
            # i += 1

    def init_offers_per_pax_per_night(self):

        self.offers_per_pax_per_night = []
        for offer in self.objects:
            if not offer.is_applicable('per_pax'): continue

            # offer.print_offer()
            self.offers_per_pax_per_night.append(offer)

    def init_offers_per_room(self):
        
        self.offers_per_room = []
        for offer in self.objects:
            if not offer.is_applicable('per_room'): continue

            # offer.print_offer()
            self.offers_per_room.append(offer)

    def init_applicable_offers(self):

        # Init unique Discount Codes
        self.init_unique_offer_codes()
        
        # Init offers per pax per night
        self.init_offers_per_pax_per_night()

        # Init offers per room
        self.init_offers_per_room()

    
    # Get Ordered offers by Application type ... see consts -> SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES
    def get_offers_by_application_type(self, application_type):
        offers_by_application_type = [offer for offer in self.objects if offer.application_type == application_type]
        return sorted(offers_by_application_type, key=lambda x: x.order)

    # @todo This should be deprecated soon - we should be more specific for each individual application type
    def get_all_offers_by_application_type(self, filter_by_application_group=None):
        offers_by_application_type = {}
        
        base_rates_only_codes = ['B', 'N', 'A', 'T', 'U']
        base_board_only_codes = ['R', 'N', 'T', 'U', 'M']

        for code, name in SUPPLEMENTS_AND_DISCOUNTS_APPLICATION_TYPES.items():
            if filter_by_application_group == 'base_rates_only' and code not in base_rates_only_codes: continue
            if filter_by_application_group == 'base_board_only' and code not in base_board_only_codes: continue

            offers_by_application_type[code] = self.get_offers_by_application_type(code)

        return offers_by_application_type


    # Utils ...
    
    # Get offers by code ... ordered in ASC mode
    def get_offers_by_code(self, code):
        offers_by_code = [offer for offer in self.objects if offer.code == code]
        return sorted(offers_by_code, key=lambda x: x.order)

