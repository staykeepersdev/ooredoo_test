import time, hashlib
import json
import requests

import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIRoom

# Your API Key and secret
apiKey = "6zuee8yw6ff55qmp96k9egma"
Secret = "f6PqqhUtG5"

sigStr = "%s%s%d" % (apiKey,Secret,int(time.time()))
signature = hashlib.sha256(sigStr.encode('utf-8')).hexdigest()

endpoint = "https://api.test.hotelbeds.com/hotel-content-api/1.0/types/rooms"

headers = {
    "X-Signature": signature,
    "Api-Key": apiKey,
    "Accept": "application/json"

}
for i in range(1,10):
    from_param = '%s' % ( 1 + (i-1)*1000 )
    to_param = '%s' % ( i*1000 )
    params = {
        "fields" : "all",
        "from": from_param,
        "to": to_param

    }
    # Reading response and print-out
    response = requests.get(url=endpoint, headers=headers, params=params)
    rooms = json.loads(response.content).get('rooms')

    for room in rooms:
        code = room.get('code')
        APIRoom.objects.get_or_create(code=code, defaults={'data': room})

