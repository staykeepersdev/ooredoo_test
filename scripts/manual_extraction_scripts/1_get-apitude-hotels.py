import time, hashlib
import json
import requests

import os, sys

import django
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel

# Your API Key and secret
apiKey = "6zuee8yw6ff55qmp96k9egma"
Secret = "f6PqqhUtG5"

sigStr = "%s%s%d" % (apiKey,Secret,int(time.time()))
signature = hashlib.sha256(sigStr.encode('utf-8')).hexdigest()

endpoint = "https://api.test.hotelbeds.com/hotel-content-api/1.0/hotels"

headers = {
    "X-Signature": signature,
    "Api-Key": apiKey,
    "Accept": "application/json"

}

def get_hotels_from_page(_from_hotel, _to_hotel):
    global from_hotel
    global to_hotel
    global current_hotel   

    params ={
        "destinationCode": "KTM",
        "fields" : "all",
        "from": _from_hotel,
        "to": _to_hotel
    }
    response = requests.get(url=endpoint, headers=headers, params=params)
    response_data = json.loads(response.content)

    total_hotels = int(response_data.get('total'))   

    hotels_data = response_data.get('hotels')   

    for hotel in hotels_data:
        code = hotel.get('code')
        APIHotel.objects.get_or_create(code=code, defaults={'data': hotel})
        print('Hotel #', str(current_hotel))
        current_hotel +=1
    
    if total_hotels > to_hotel:
        from_hotel = from_hotel + hotels_per_page
        to_hotel = to_hotel + hotels_per_page
        get_hotels_from_page(from_hotel,to_hotel)

current_hotel = 1
hotels_per_page = 100
from_hotel = 1
to_hotel = hotels_per_page
get_hotels_from_page(from_hotel, to_hotel)

