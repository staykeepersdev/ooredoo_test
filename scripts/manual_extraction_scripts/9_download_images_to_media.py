import requests
import tempfile
import os
import threading

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.core import files
from django.core.paginator import Paginator
from hotels.models import HotelImage

images = HotelImage.objects.filter(image='').order_by('id')

def download_images(queryset, q_name):
    counter = 0
    for image in queryset:
        counter +=1
        if counter % 50 == 0:
            print(q_name, counter)
        image_url = 'http://photos.hotelbeds.com/giata/bigger/' + image.giata_path
        # Steam the image from the url
        request = requests.get(image_url, stream=True)

        # Was the request OK?
        if request.status_code != requests.codes.ok:
            # Nope, error handling, skip file etc etc etc
            continue

        # Get the filename from the url, used for saving later
        file_name = image_url.split('/')[-1]

        # Create a temporary file
        lf = tempfile.NamedTemporaryFile()

        # Read the streamed image in sections
        for block in request.iter_content(1024 * 8):

            # If no more file then stop
            if not block:
                break

            # Write image block to temporary file
            lf.write(block)

        # Create the model you want to save the image to

        # Save the temporary image to the model#
        # This saves the model so be sure that is it valid
        image.image.save(file_name, files.File(lf))


def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

paginator = Paginator(images, 5000)
for page in range(1, paginator.num_pages + 1):
    run_threaded(download_images, (paginator.page(page).object_list, f'Q{page}'))