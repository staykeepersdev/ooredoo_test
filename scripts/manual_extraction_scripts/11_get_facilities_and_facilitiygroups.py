import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.utils import timezone
from hotelbeds_data.models import APIFacility, APIFacilityGroup
from hotelbeds_api.clients.facilities_api_client import FacilitiesAPIClient
from hotelbeds_api.clients.facilitiygroups_api_client import FacilityGroupsAPIClient

FACILITIES_CLIENT = FacilitiesAPIClient()
GROUP_FACILITIES_CLIENT = FacilityGroupsAPIClient()
now = timezone.now()

facility_groups  = GROUP_FACILITIES_CLIENT.get_all_facilitiy_groups().get('facilityGroups')
facility_groups_objects = []
for facility_group in facility_groups:
    
    try:
        facility_groups_objects.append(
            APIFacilityGroup(
                data=facility_group,
                code=facility_group['code'],
                description=facility_group['description']['content'],
                updated_at=now
            )
        )
    except:
        pass

APIFacilityGroup.objects.bulk_create(facility_groups_objects)

facilities = FACILITIES_CLIENT.get_all_facilities().get('facilities')
facility_objects = []
for facility in facilities:
    try:
        facility_objects.append(
            APIFacility(
                data=facility,
                code=facility['code'],
                description=facility['description']['content'],
                api_facility_group=APIFacilityGroup.objects.get(code=facility['facilityGroupCode']),
                updated_at=now
            )
        )
    except Exception as e:
        print(e)

APIFacility.objects.bulk_create(facility_objects)
