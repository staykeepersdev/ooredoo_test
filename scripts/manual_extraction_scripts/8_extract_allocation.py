import datetime
import hashlib
import json
import os
import re
import time


import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotels.models import Hotel, HotelRoom
from rooms_inventory.models import InventoryDay, PriceDay

hotel = check_next_line = next_line_type =  None


## Get hotel object by Hotel Code
def get_hotel(line) -> Hotel:
    content = line.split(':')
    hotel_code = content[7]
    try:
        hotel = Hotel.objects.get(api_hotel__code=hotel_code)
    except:
        hotel = None
    
    return hotel

def get_initial_room_content(hotel,line):
    content = line.split(':')
    start_date = datetime.datetime.strptime(content[0], '%Y%m%d')
    end_date = datetime.datetime.strptime(content[1], '%Y%m%d')

    room_type = content[2]
    characteristic_code = content[3]
    rate = content[4] or None

    hotel_room = HotelRoom.objects.filter(
        hotel=hotel,
        room_type=room_type,
        characteristic_code=characteristic_code
    ).first()

    return content, start_date, end_date, rate, hotel_room

def add_room_invetory(hotel, line):
    content, start_date, end_date, rate, hotel_room = get_initial_room_content(hotel,line)
    
    allotments = re.findall(r'\((.*?)\)', content[5]) 

    if hotel_room:
        current_date = start_date
        for counter, allotment in enumerate(allotments):
            current_date = start_date + datetime.timedelta(days=counter)
            allotment = allotment.split(',')
            InventoryDay.objects.update_or_create(
                hotel_room=hotel_room,
                date=current_date,
                rate=rate,
                defaults={
                    'release': int(allotment[0]),
                    'allotment': int(allotment[1])
                }                
            )

        if current_date != end_date:
            print('we have an Inventory error')

def add_room_prices(hotel, line):
    content, start_date, end_date, rate, hotel_room = get_initial_room_content(hotel,line)
    allotments = re.findall(r'\((.*?)\)', content[6]) 
    if hotel_room:
        current_date = start_date
        for counter, allotment in enumerate(allotments):
            current_date = start_date + datetime.timedelta(days=counter)
            inventory_day = InventoryDay.objects.filter(
                hotel_room=hotel_room,
                date=current_date,
                rate=rate
            ).first()

            if inventory_day:
                allotment = allotment.split(',')
                if allotment[0] == 'Y':
                    price_per_pax = True
                else:
                    price_per_pax = False
                PriceDay.objects.update_or_create(
                    inventory_day=inventory_day,
                    defaults={
                        'price_per_pax': price_per_pax,
                        'net_price': float(allotment[1]),
                        'price': float(allotment[2]),
                        'specific_rate': allotment[3] or None,
                        'base_board': allotment[4],
                        'amount': float(allotment[5]),
                    }                
                )

        if current_date != end_date:
            print('we have a Price error')


with os.scandir('extracted/DESTINATIONS/D_doh') as scanned_dir:
    for file in scanned_dir:
        if file.is_file():
            print(file)
            with open(file,'r') as current_file:
                for line in current_file:                    
                        
                    ##  Check current live and move on
                    if check_next_line:  
                        ## Stop current check if end of lines_type
                        if line.startswith('{/'):
                            check_next_line = False
                            continue            
                        
                        ## Check line type
                        if next_line_type == 'ccon':
                            hotel = get_hotel(line)
                            if not hotel:
                                break
                        elif next_line_type == 'cnin':
                            add_room_invetory(hotel, line)
                        elif next_line_type == 'cnct':
                            add_room_prices(hotel, line)
                        else:
                            pass
                        
                        continue
                        
                    ## Mark next line for checking
                    if line.startswith('{CCON}'):
                        check_next_line = True
                        next_line_type = 'ccon'
                    elif line.startswith('{CNIN}'):
                        check_next_line = True
                        next_line_type = 'cnin'
                    elif line.startswith('{CNCT}'):
                        check_next_line = True
                        next_line_type = 'cnct'
                    else:
                        check_next_line = False
