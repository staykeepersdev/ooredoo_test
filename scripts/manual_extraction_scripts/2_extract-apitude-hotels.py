import time, hashlib
import json
import requests

import os, sys

import django
sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel
from hotels.models import Hotel

codes = ['SYD','GYD','REP','KOS','PNH','ON','JAV','AMI','PLW','MES','BTH','ID5','KOE','SOC','BAI','BDO','MLG','ID4','SRG','MBA','NAI','LEK','KSI','MKZ','KUL','BKI','PEN','BKI','RAK','FEZ','AGA','ZGO','MEK','AKL','WLG','CUZ','OPO','LIS','FAO','MRU','MOW','CPT','JHG','CPT','KZN','ICN','TPE','ZNZ','ARK','DAE','XX6','TH7','USM','PHZ','HRE','VF','MVZ','PRG','REK','SCL','ANU','BUE','RIO','SYD','ON','KZN','CPT','VA','ASU','AGT','ENO','CUZ','VVI','LPB','CBB']

api_hotels = APIHotel.objects.filter(data__destinationCode__in=codes).exclude(hotel__isnull=False)

counter = 1
for api_hotel in api_hotels:
    print (str(counter), '#', str(api_hotel.pk))
    counter+=1
    data = api_hotel.data
    try:
        longitude = data.get('coordinates',).get('longitude')
        latitude = data.get('coordinates').get('latitude')
    except:
        longitude = latitude = 0
    hotel = Hotel.objects.get_or_create(api_hotel=api_hotel, 
    defaults={
        'name' : data.get('name').get('content'),
        'description' : data.get('description').get('content') if data.get('description') else '',
        'country_code' : data.get('countryCode'),
        'destination_code' : data.get('destinationCode'),
        'longitude' : longitude,
        'latitude' : latitude,
        'category_code': data.get('categoryCode'),
        'accommodation_type_code' :  data.get('accommodationTypeCode'),
        'address' : data.get('address').get('content'),
        'postal_code' : data.get('postalCode'),
        'city' : data.get('city').get('content'),
        'email' : data.get('email'),
        'web' : data.get('web'),
        'ranking' : data.get('ranking')
    })
print(counter)

