import time, hashlib
import datetime
import json
import requests

import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel

# Your API Key and secret
# Live API key
apiKey = "6zuee8yw6ff55qmp96k9egma"
Secret = "f6PqqhUtG5"

sigStr = "%s%s%d" % (apiKey, Secret, int(time.time()))
signature = hashlib.sha256(sigStr.encode("utf-8")).hexdigest()

endpoint = "https://api.hotelbeds.com/hotel-content-api/1.0/hotels"

headers = {"X-Signature": signature, "Api-Key": apiKey, "Accept": "application/json"}
api_hotels = APIHotel.objects.all()


def get_hotels_from_page(_from_hotel, _to_hotel):
    global from_hotel
    global to_hotel
    print(to_hotel)

    params = {"fields": "all", "from": _from_hotel, "to": _to_hotel}
    response = requests.get(url=endpoint, headers=headers, params=params)
    response_data = json.loads(response.content)

    total_hotels = int(response_data.get("total"))

    hotels_data = response_data.get("hotels")
    hotels_objects = []
    for hotel in hotels_data:
        code = hotel.get("code")
        try:
            api_hotels.get(code=code)
        except:
            date_str = hotel.get("lastUpdate")
            hotels_objects.append(
                APIHotel(
                    code=code,
                    updated_at=datetime.datetime.strptime(date_str, "%Y-%m-%d"),
                    data=hotel,
                )
            )
    APIHotel.objects.bulk_create(hotels_objects)
    if total_hotels > to_hotel:
        from_hotel = from_hotel + hotels_per_page
        to_hotel = to_hotel + hotels_per_page
        get_hotels_from_page(from_hotel, to_hotel)


hotels_per_page = 500
from_hotel = 65500
to_hotel = 66001
get_hotels_from_page(from_hotel, to_hotel)
