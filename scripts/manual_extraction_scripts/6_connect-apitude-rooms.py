import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIRoom
from hotels.models import HotelRoom

hotel_rooms = HotelRoom.objects.filter(api_room=None)
for hotel_room in hotel_rooms:
    try:
        api_room = APIRoom.objects.get(code=hotel_room.room_code)
        
        hotel_room.api_room = api_room
        hotel_room.min_pax = api_room.data.get('minPax')
        hotel_room.max_pax = api_room.data.get('maxPax')
        hotel_room.max_adults = api_room.data.get('maxAdults')
        hotel_room.max_children = api_room.data.get('maxChildren')
        hotel_room.min_adults = api_room.data.get('minAdults')

        hotel_room.description = api_room.data.get('description')
        hotel_room.type_description = api_room.data.get('typeDescription').get('content')
        hotel_room.characteristic_description = api_room.data.get('characteristicDescription').get('content')

        hotel_room.save()
    except Exception as e:
        print(e)
        pass

