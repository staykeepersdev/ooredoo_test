import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel
from hotels.models import Hotel, HotelRoom

hotels = Hotel.objects.filter(rooms__isnull=True)
api_hotels = APIHotel.objects.filter(hotel__in=hotels)

counter = 1
for api_hotel in api_hotels:
    rooms_data = api_hotel.data.get('rooms')
    hotel = api_hotel.hotel
    print(counter)
    counter +=1
    if rooms_data:
        for room_data in rooms_data:
            HotelRoom.objects.get_or_create(
                hotel=hotel,
                room_code=room_data.get('roomCode'),
                room_type=room_data.get('roomType'),
                characteristic_code=room_data.get('characteristicCode'),
                defaults={
                    "room_stays" : room_data.get('roomStays', None)
                }
            )


