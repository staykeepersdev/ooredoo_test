import time, hashlib
import json
import re
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from decimal import Decimal
from timeit import default_timer as timer

from currencies.models import ExchangeRate

from hotels.models import Hotel
from hotelbeds_data.models import APIRateComment
from hotelbeds_scraper.models import ScrapedData

# 30 days
# 2. City to focus (unordered list)
# Goa
# Palm Springs
# London
# Bali, Indonesia
# Antalya, Turkey
# Dubrovnik, Croatia
# Budapest, Hungary
# Dublin
# Edinburgh
# Dubai
# Phuket
# Sofia
# Bilbao

print('Starting...')
start = timer()

euro_gbp_rate = ExchangeRate.objects.get(base__name='EUR', currency__name='GBP').rate
euro_usd_rate = ExchangeRate.objects.get(base__name='EUR', currency__name='USD').rate
gbp_euro_rate = ExchangeRate.objects.get(base__name='GBP', currency__name='EUR').rate
gbp_USD_rate = ExchangeRate.objects.get(base__name='GBP', currency__name='USD').rate

scraped_data = ScrapedData.objects.filter(parsed_data__isnull=True)

hotel_codes = []

def get_hotels_types_and_adresses(json_data, hotel_codes):
    hotel_objects = list(Hotel.objects.filter(
        api_hotel__code__in=hotel_codes
    ).values_list('api_hotel__code','address', 'accommodation_type_code', 'api_hotel_category__simple_code'))
    if hotel_objects:
        for rate in json_data:
            rate_hotel_code = rate.get('hotel_code','')            

            for hotel_object in hotel_objects:
                if int(hotel_object[0]) == int(rate_hotel_code):
                    rate['hotel_address'] = hotel_object[1]
                    rate['hotel_type'] = hotel_object[2]
                    rate['hotel_stars'] = hotel_object[3]

                    break    
    return json_data  



def get_rate_comments_from_db(json_data, rate_comments_id, check_in, check_out):
    rate_objects = list(APIRateComment.objects.filter(
        code__in=rate_comments_id,                   
        date_start__lte=check_in,
        date_end__gte=check_out,
    ).values_list('incoming','code','rate_codes', 'description'))

    # Add each comment to the final result
    if rate_objects:
        for rate in json_data:
            rate_comments_id = rate.get('rate_comment','')            
            if rate_comments_id:
                rate_description = ''
                rate_comment_split = rate_comments_id.split('|')

                for rate_object in rate_objects:
                    if (int(rate_comment_split[0]) == rate_object[0] and
                            rate_comment_split[1] == rate_object[1] and
                            int(rate_comment_split[2]) in rate_object[2]):

                            rate_description += rate_object[3]

                rate['rate_comment'] = rate_description
    return json_data

def parse_data(scraped_data_obj):
    global hotel_codes
    rate_comments_id = hotel_codes = []
    
    json_data = []

    check_in = scraped_data_obj.check_in
    check_out = scraped_data_obj.check_out
    country = scraped_data_obj.country
    city = scraped_data_obj.city
    created_at = scraped_data_obj.created_at
    scraped_time = created_at.strftime("%Y-%m-%d %H:%M:%S")
    rounded_hour = created_at.strftime("%Y-%m-%d %H:00:00")
    hotels_data = scraped_data_obj.raw_data
    
    for hotel in hotels_data:
        hotel_name = hotel['name']
        hotel_code = hotel['code']
        hotel_codes.append(hotel_code)
        hotel_type = ''
        hotel_stars = hotel['categoryName']
        hotel_adress = ''
        hotel_currency = hotel['currency']
        for room in hotel['rooms']:
            room_type = room['name']
            for rate in room['rates']:

                # Get rates_ids
                rate_comments_id_string = rate.get('rateCommentsId','')
                if rate_comments_id_string:
                    rate_comment_id = rate_comments_id_string.split('|')[1]
                    rate_comments_id.append(rate_comment_id)

                board_type = rate['boardName']

                # Get 4 prices
                price = rate['net'] + hotel_currency
                if hotel_currency == 'EUR':
                    price_in_EUR = rate['net']
                    price_in_GBP = str(round(Decimal(rate['net']) * euro_gbp_rate,2))
                    price_in_USD = str(round(Decimal(rate['net']) * euro_usd_rate,2))
                elif hotel_currency == 'GBP':
                    price_in_EUR = str(round(Decimal(rate['net']) * gbp_euro_rate,2))
                    price_in_GBP = rate['net']
                    price_in_USD = str(round(Decimal(rate['net']) * gbp_USD_rate,2))
                else:
                    price_in_EUR = ''
                    price_in_GBP = ''
                    price_in_USD = ''

                # Get cancelation policies
                cancelation_policies = ''
                try:
                    cancellation_policies_list = rate['cancellationPolicies']
                    for policy in cancellation_policies_list:
                        cancelation_policies += f"{policy['amount']} {hotel_currency} from: {policy['from']} "
                except:
                    pass
                # Get offers
                offers= ''
                try:
                    offers_policies_list = rate['offers']
                    for offer in offers_policies_list:
                        offers += f"{offer['name']} {offer['amount']} {hotel_currency} "
                except:
                    pass

                rate_data = {
                    'rounded_hour': rounded_hour,
                    'scraped_time': scraped_time,
                    'hotel_code': hotel_code,
                    'check_in': check_in,
                    'check_out': check_out,
                    'country': country,
                    'city': city,
                    'hotel_name': hotel_name,
                    'hotel_type': hotel_type,
                    "hotel_stars": hotel_stars,
                    'hotel_address': hotel_adress,
                    'room_type': room_type,
                    'board_type': board_type,
                    'original_price': price,
                    'price_in_EUR': price_in_EUR,
                    'price_in_GBP': price_in_GBP,
                    'price_in_USD': price_in_USD,
                    'rate_comment': rate_comments_id_string,
                    'cancellation_policies': cancelation_policies,
                    'offers': offers
                }
                json_data += [rate_data]
                
    print('Extracting rate comments', str(timer()-start) )
    json_data = get_rate_comments_from_db(json_data, rate_comments_id, check_in, check_out)
    print('Finished extracting rate comments',  str(timer()-start))

    print('Extracting Hotels info', str(timer()-start) )
    json_data = get_hotels_types_and_adresses(json_data, hotel_codes)
    print('Finished Hotels info',  str(timer()-start))
    return json_data

data_counter =1
for scraped_data_obj in scraped_data:
    print(data_counter)
    data_counter+=1
    parsed_data = parse_data(scraped_data_obj)
    scraped_data_obj.parsed_data = parsed_data
    scraped_data_obj.save()

end = timer()

print(end - start)