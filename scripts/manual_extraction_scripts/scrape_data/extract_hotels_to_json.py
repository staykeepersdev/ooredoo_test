import json
import django
import os
import sys


# sys.path.append('/srv/www/ooredoo/')
sys.path.append('/Users/miro/Dev/staykeepersdev-ooredoo-ea9577a7e0dd/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.core.paginator import Paginator
from hotelbeds_data.models import APIHotel, APIFacility

api_hotels = APIHotel.objects.all().order_by("id")
all_facilities_list = list(
    APIFacility.objects.all().values_list(
        "code", "data__facilityGroupCode", "data__description__content"
    )
)

per_page = 1000
per_file = per_page * 10

paginator = Paginator(api_hotels, per_page)

from_hotel = 1
to_hotel = per_file
file = None

for page in range(1, paginator.num_pages + 1):
    print(page)

    if page == 1:
        hotels = []
        file_name = f"hotels_{from_hotel}_to_{to_hotel}.json"
        file = open(file_name, "w+", newline="", encoding="utf-8")

    for api_hotel in paginator.page(page).object_list:
        hotel_data = api_hotel.data
        rooms_data = hotel_data.get("rooms", "")
        for room in rooms_data:
            # Delete extra info for Room Facilities
            room_size_found = False
            room_size = ""

            facilities = room.get("roomFacilities", "")
            for facility in facilities:
                facility.pop("indFee", None)
                facility.pop("voucher", None)
                if not room_size_found:
                    facility_code = facility["facilityCode"]
                    facility_group_code = facility["facilityGroupCode"]
                    if facility_code == 295 and facility_group_code == 60:
                        room_size_found = True
                        room_size = facility.get("number", "")

            room["roomSize"] = room_size

            # Flat RoomStays
            room_stays = room.get("roomStays", "")
            room_stays_string = ""
            for room_stay in room_stays:
                room_name = room_stay.get("stayType", "")
                facilities_string_array = []
                facilities = room_stay.get("roomStayFacilities", "")
                for facility in facilities:
                    facility_number = facility.get("number", "")
                    facility_code = str(facility["facilityCode"])
                    facility_group_code = facility["facilityGroupCode"]
                    for api_facility in all_facilities_list:
                        if (
                            facility_code == api_facility[0]
                            and facility_group_code == api_facility[1]
                        ):
                            facilities_string_array.append(
                                f"{facility_number}|{api_facility[2]}"
                            )
                            break
                    facilities_string = ",".join(facilities_string_array)
                room_stays_string += f"{room_name} - {facilities_string};"
            if room_stays_string:
                room["roomStays"] = room_stays_string

        # Delete hotel phones
        if "phones" in hotel_data:
            del hotel_data["phones"]

        # Delete InterestPoints
        if "interestPoints" in hotel_data:
            del hotel_data["interestPoints"]

        # Delete extra info for hotel facilities
        facilities = hotel_data.get("facilities", "")
        for facility in facilities:
            facility.pop("order", None)
            facility.pop("voucher", None)

        # Connect images to room and flat them
        images = hotel_data.get("images", "")
        if images:
            images_copy = images.copy()
            for image in images:
                image.pop("order", None)
                image.pop("visualOrder", None)
                image.pop("imageTypeCode", None)

                room_code = image.get("roomCode", "")
                if room_code:
                    for room in rooms_data:
                        if room["roomCode"] == room_code:
                            image.pop("roomCode", None)
                            image.pop("roomType", None)
                            image.pop("characteristicCode", None)
                            room.setdefault("images", "")
                            room["images"] += f'{image["path"]};'
                            images_copy.remove(image)

                            break

            hotel_data["images"] = ";".join([x["path"] for x in images_copy])

        # Connect wildcards to rooms
        wildcards = hotel_data.get("wildcards", "")
        if wildcards:
            wildcards_copy = wildcards.copy()

            if wildcards:
                for wildcard in wildcards:
                    room_code = wildcard.get("roomType", "")
                    if room_code:
                        for room in rooms_data:
                            if room["roomCode"] == room_code:
                                room["hotelRoomDescription"] = wildcard[
                                    "hotelRoomDescription"
                                ]["content"]
                                wildcards_copy.remove(wildcard)
                                break

        hotel_data["wildcards"] = wildcards_copy

        hotels.append(hotel_data)

    if (not page == 1 and page % 10 == 0) or page == paginator.num_pages:
        from_hotel = 1 + page * per_page
        to_hotel = (page + 10) * per_page

        hotels_json = {"hotels": hotels}
        file.write(json.dumps(hotels_json))
        file.close()
        if not (page == paginator.num_pages):
            hotels = []
            file_name = f"hotels_{from_hotel}_to_{to_hotel}.json"
            file = open(file_name, "w+", newline="", encoding="utf-8")
