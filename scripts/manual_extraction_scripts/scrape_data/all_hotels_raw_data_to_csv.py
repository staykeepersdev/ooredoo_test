import time, hashlib
import csv
import json
import re
import requests

import os
from io import StringIO
from django.core.files.base import ContentFile

import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ooredoo.settings')
django.setup()

from django.core.paginator import Paginator
from hotelbeds_data.models import APIHotel

hotels = APIHotel.objects.all().order_by('id')

paginator = Paginator(hotels, 1000) #1000
counter = 1

from_hotel = 1
to_hotel = 10000
file = None
for page in range(1, paginator.num_pages + 1):
    print(page)
    if (page % 10 == 0):
        from_hotel = 1 + (page-10) * 1000
        to_hotel = page * 1000
    if (page % 10 == 0 or page==1):
        if file:
            file.close()
        file_name = f'hotels_{from_hotel}_to_{to_hotel}.csv'
        file = open(file_name, 'w+', newline='', encoding="utf-8")
        csv_writer = csv.writer(file, delimiter=';')
        fields_to_add = (  
            'name',
            'hotelBedsID',
            'countryCode',
            'stateCode',
            'destinationCode',
            'longitude',
            'latitude',
            'categoryCode',
            'categoryGroupCode',
            'chainCode',
            'accommodationTypeCode',

            'address',
            'postalCode',
            'city',
            'boardCodes',

            'roomCode',
            'roomType',	
            'characteristicCode',
            'stayType',	

            'web',
            'S2C',
            'ranking',

            'lastUpdate',
            'images'
        )
        csv_writer.writerow(fields_to_add)
    for hotel in paginator.page(page).object_list:
        hotel_data = hotel.data
        counter +=1

        image_content = hotel_data.get('images', '')
        images = ''
        for image in image_content[:3]:
            images += f'http://photos.hotelbeds.com/giata/original/{image["path"]},'
        try:
            longitude = hotel_data['coordinates']['longitude'],
            latitude = hotel_data['coordinates']['latitude']
        except:
            longitude = ''
            latitude = ''
        hotel_row = (
            hotel_data['name']['content'],
            hotel_data['code'],
            hotel_data.get('countryCode', ''),
            hotel_data.get('stateCode', ''),
            hotel_data.get('destinationCode', ''),
            longitude,
            latitude,
            hotel_data.get('categoryCode', ''),
            hotel_data.get('categoryGroupCode', ''),
            hotel_data.get('chainCode', ''),
            hotel_data.get('accommodationTypeCode', ''),

            hotel_data['address']['content'],
            hotel_data.get('postalCode',''),
            hotel_data['city']['content'],
            ','.join(hotel_data.get('boardCodes', '')),

            ## RoomSuff empty fields
            '', #roomCode
            '', #roomType
            '', #characteristicCode
            '', #roomStay


            hotel_data.get('web', ''),
            hotel_data.get('S2C', ''),
            hotel_data.get('ranking', ''),

            hotel_data.get('lastUpdate'),
            images
        )
        csv_writer.writerow(
            hotel_row
        )
        for room in hotel_data.get('rooms',''):
            room_row = list(hotel_row)

            room_stays_content = room.get('roomStays', '')
            room_stays = ''
            for room_stay in room_stays_content:
                description = room_stay.get("description", '')
                room_stays += f'{room_stay["stayType"]} ({description}) ,'

            room_row[15] = room['roomCode']
            room_row[16] = room['roomType']
            room_row[17] = room['characteristicCode']
            room_row[18] = room_stays

            csv_writer.writerow(room_row)

