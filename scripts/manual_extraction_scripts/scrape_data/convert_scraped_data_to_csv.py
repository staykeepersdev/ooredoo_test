import time, hashlib
import csv
import json
import re
import requests

import os
from io import StringIO
from django.core.files.base import ContentFile

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_scraper.models import ScrapedData

scraped_data = ScrapedData.objects.filter(csv='')

counter =1
for scraped_data_obj in scraped_data:
    print(counter)
    counter +=1
    fields_to_add = (  
        "rounded_hour",
        "scraped_time",
        "check_in",
        "check_out",
        "city",
        "country",
        "hotel_name",
        "hotel_code",
        "hotel_stars",        
        "room_type",
        "board_type",
        "original_price",
        "offers",
        "cancellation_policies",
    )
    csv_buffer = StringIO(newline='')
    csv_writer = csv.writer(csv_buffer, lineterminator='\n')
    # f = csv.writer(open("test.csv", "w+", newline=''))
    # Write CSV Header, If you dont need that, remove this line
    csv_writer.writerow(fields_to_add)

    for x in scraped_data_obj.parsed_data:
        csv_writer.writerow([x[field] for field in fields_to_add])
    
    csv_file = ContentFile(csv_buffer.getvalue())
    csv_file_name = f'{scraped_data_obj.country}_{scraped_data_obj.city}_{scraped_data_obj.check_in}_{scraped_data_obj.check_out}_{scraped_data_obj.created_at.strftime("%Y-%m-%d_%H-00")}.csv'
    scraped_data_obj.csv.save(csv_file_name, csv_file)

    

