import datetime
import json
import time, hashlib
import requests
import os

from datetime import date

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_api.services import get_hotels_availability, get_hotels_content
from hotelbeds_scraper.models import ScrapedData

country='Scotland'
city="Edinburgh"
city_shortcode = 'EDI'


#limited to 999 hotels
params = {
    "fields": "code",
    "language":"ENG",
    "from": 1,
    "to": 1000,
    'destinationCode': city_shortcode
}
hotels_data = get_hotels_content(params)['hotels']
hotel_ids = []
for hotel in hotels_data:
    hotel_ids.append(int(hotel['code']))

today = datetime.date.today()
for i in range(1,31):
    check_in = today + datetime.timedelta(days=i)
    check_out = check_in + datetime.timedelta(days=1)


    params = {
        "stay": {
            "checkIn": check_in.strftime("%Y-%m-%d"),
            "checkOut": check_out.strftime("%Y-%m-%d")
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": 2,
                "children": 0
            }
        ],
        "hotels": {
            "hotel": hotel_ids
        },
        "filter": {
            "paymentType": 'AT_WEB',
            "packaging": False
        }
    }
    try:
        hotels_raw_data = get_hotels_availability(params).get('hotels').get('hotels')

        ScrapedData.objects.create(
            check_in=check_in,
            check_out=check_out,
            country=country,
            city=city,
            raw_data = hotels_raw_data,
        )
    except:
        print(check_in)