import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.utils import timezone
from hotelbeds_data.models import APIRateComment
from hotelbeds_api.clients.ratecomments_api_client import RateCommentAPIClient

CLIENT = RateCommentAPIClient()
now = timezone.now()
for page in range(1,90):
    print(page)
    rate_comments = CLIENT.get_rate_comments_from_page(page).get('rateComments')
    rate_comments_objects = []
    for rate_comment in rate_comments:
        
        try:
            rate_comments_objects.append(
                APIRateComment(
                    data=rate_comment,
                    code=rate_comment['code'],
                    incoming=rate_comment['incoming'],
                    rate_codes=rate_comment['commentsByRates'][0]['rateCodes'],
                    date_start=rate_comment['commentsByRates'][0]['comments'][0]['dateStart'],
                    date_end=rate_comment['commentsByRates'][0]['comments'][0]['dateEnd'],
                    description=rate_comment['commentsByRates'][0]['comments'][0]['description'],
                    updated_at=now
                )
            )
        except:
            pass

    APIRateComment.objects.bulk_create(rate_comments_objects)
