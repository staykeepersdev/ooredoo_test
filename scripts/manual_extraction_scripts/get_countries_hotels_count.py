import csv
import os
import sys 
sys.path.append('..')

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_api.clients.content_api_client import CountriesAPIClient, HotelsAPIClient

def get_hotels_count_by_country(country_code, from_hotel=1, to_hotel=1):
    params = {
        'fields': 'id',
        'from': from_hotel,
        'to': to_hotel,
        'countryCode': country_code
    }
    hotels_response = HotelsAPIClient().get_hotels(params=params)
    total_hotels = hotels_response.get('total')
    return total_hotels

params = { 
    'from': '1',
    'to': '210',
    'fields': 'id,description'
}
countries = CountriesAPIClient().get_list(params=params).get('countries')

with open('country_hotels_count.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    for country in countries:
        country_code = country.get('code')
        country_name = country.get('description').get('content')
        total_hotels = get_hotels_count_by_country(country_code=country_code)
        writer.writerow([country_name, country_code, total_hotels])





