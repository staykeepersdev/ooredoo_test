import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.utils import timezone
from django.db.models import Q
from hotelbeds_data.models import APIHotelCategory
from hotels.models import  Hotel

from hotelbeds_api.clients.content_api_client import HotelsCategoryAPIClient

HOTELS_CATEGORY_API_CLIENT = HotelsCategoryAPIClient()

now = timezone.now()

hotel_categories  = HOTELS_CATEGORY_API_CLIENT.get_hotel_categories().get('categories')
hotel_categories_objects = []
for hotel_category in hotel_categories:
    
    try:
        hotel_categories_objects.append(
            APIHotelCategory(
                data=hotel_category,
                code=hotel_category['code'],
                simple_code=hotel_category['simpleCode'],
                updated_at=now
            )
        )
    except:
        pass

APIHotelCategory.objects.bulk_create(hotel_categories_objects)