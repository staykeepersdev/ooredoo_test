import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.utils import timezone
from django.db.models import Q
from hotelbeds_data.models import APIHotelCategory
from hotels.models import  Hotel

hotels = Hotel.objects.filter(api_hotel_category=None)
for hotel in hotels:
    api_hotel_category_code = hotel.category_code
    try:
        hotel.api_hotel_category = APIHotelCategory.objects.get(code=api_hotel_category_code)
        hotel.save()
    except:
        print(api_hotel_category_code)