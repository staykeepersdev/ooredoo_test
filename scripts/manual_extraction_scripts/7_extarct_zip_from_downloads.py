import time, hashlib
import json
import requests
import zipfile
import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()



with zipfile.ZipFile('downloads/id_7803-20191204173219.zip', 'r') as archive:

    for file in archive.namelist():
        if file.lower().startswith('destinations/d_doh/'):
            archive.extract(file, 'extracted/')