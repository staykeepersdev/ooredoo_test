import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel
from hotels.models import Hotel, HotelRoom, PhoneNumber, HotelImage

hotels = Hotel.objects.filter(images__isnull=True, phone_numbers__isnull=True)
api_hotels = APIHotel.objects.filter(hotel__in=hotels)

counter = 1
for api_hotel in api_hotels:

    print(counter)
    counter +=1

    hotel = api_hotel.hotel

    phones_data = api_hotel.data.get('phones')
    if phones_data:
        for phone_data in phones_data:
            PhoneNumber.objects.get_or_create(
                hotel=hotel,
                phone_number=phone_data.get('phoneNumber'),
                phone_type=phone_data.get('phoneType')
            )

    images_data = api_hotel.data.get('images')
    if images_data:
        for image_data in images_data:
            room_code = image_data.get('roomCode')
            if room_code:
                try:
                    hotel_room = HotelRoom.objects.get(
                        hotel=hotel,
                        room_code=room_code
                    )
                except:
                    hotel_room=None
            else:
                hotel_room = None

            HotelImage.objects.get_or_create(
                hotel=hotel,
                giata_path=image_data.get('path'),
                order=int(image_data.get('order')),
                hotel_room=hotel_room
            )


