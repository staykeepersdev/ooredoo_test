import time, hashlib
import json
import requests

import os

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.utils import timezone
from django.db.models import Q
from hotelbeds_data.models import APIFacility
from hotels.models import  Hotel, HotelRoom

hotels = Hotel.objects.filter(facilities=None)

for count, hotel in enumerate(hotels):
    print(count, hotel.pk)

    api_hotel = hotel.api_hotel
    
    hotel_facilities = api_hotel.data.get('facilities')
    hotel_facilities_codes = []

    # Generate query so we hit the database only once
    # If is needed so we dont filter with Q()
    if hotel_facilities:
        q_objects = Q() 
        for facility in hotel_facilities:
            indYesOrNo = facility.get('indYesOrNo', True)
            indLogic = facility.get('indLogic', True)
            if indYesOrNo and indLogic:
                q_objects |= Q(code=facility['facilityCode'], api_facility_group__code=facility['facilityGroupCode']) #
        
        facilities_obj_list = list(APIFacility.objects.filter(q_objects).distinct())
        hotel.facilities.add(*facilities_obj_list)

    ## get rooms facilities
    rooms_data = api_hotel.data.get('rooms', '')
    for room_data in rooms_data:
        
        hotel_room = HotelRoom.objects.get(
                hotel=hotel,
                room_code=room_data.get('roomCode'),
                room_type=room_data.get('roomType'),
                characteristic_code=room_data.get('characteristicCode')
        )

        # Generate query so we hit the database only once
        room_facilities = room_data.get('roomFacilities', '')
        
        if room_facilities: 
            room_q_objects = Q()
            for facility in room_facilities:
                indYesOrNo = facility.get('indYesOrNo', True)
                indLogic = facility.get('indLogic', True)
                if indYesOrNo and indLogic:
                    room_q_objects |= Q(code=facility['facilityCode'], api_facility_group__code=facility['facilityGroupCode']) 
            
            room_facilities_obj_list = list(APIFacility.objects.filter(room_q_objects).distinct())
            hotel_room.facilities.add(*room_facilities_obj_list)
            

