import django
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom

calendar_rooms = CalendarRoom.objects.filter(
    request_data__has_any_keys=['location_params']
).all()


empty_property_type = []
hotel_property_type = []
guest_suite_property_type = []
cabin_property_type = []
villa_property_type = []
bungalow_property_type = []
other_property_type = []

count_correct_property_types = 0
for calendar_room in calendar_rooms:
    property_type = calendar_room.request_data['location_params'].get('property_type', '')

    if property_type == '':
        empty_property_type.append(calendar_room.id)

    elif property_type.lower() == 'hotel':
        hotel_property_type.append(calendar_room.id)

    elif property_type.lower() == 'guest suite':
        guest_suite_property_type.append(calendar_room.id)

    elif property_type.lower() == 'cabin':
        cabin_property_type.append(calendar_room.id)

    elif property_type.lower() == 'villa':
        villa_property_type.append(calendar_room.id)

    elif property_type.lower() == 'bungalow':
        bungalow_property_type.append(calendar_room.id)

    elif property_type.lower() not in ['studio', 'apartment']:
        other_property_type.append(calendar_room.id)

    else:
        count_correct_property_types += 1


# print('Empty property type: ', empty_property_type, '\n\n')
# print('Hotel property type: ', hotel_property_type, '\n\n')
# print('Guest Suite property type: ', guest_suite_property_type, '\n\n')
# print('Cabin property type: ', cabin_property_type, '\n\n')
# print('Villa property type: ', villa_property_type, '\n\n')
# print('Bungalow property type: ', bungalow_property_type, '\n\n')
# print('Other property type: ', other_property_type, '\n\n')

# print('Count of correct property types: ', count_correct_property_types)
defect_rooms_list = empty_property_type + hotel_property_type + guest_suite_property_type + cabin_property_type + villa_property_type + bungalow_property_type + other_property_type

with open('defect_rooms_list.txt', 'w') as f:
    print(*defect_rooms_list, sep=", ", file=f)