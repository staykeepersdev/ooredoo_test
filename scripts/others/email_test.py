import django
import random
import os
import sys

sys.path.append('/srv/www/ooredoo/')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.core.mail import EmailMultiAlternatives

subject = 'TEST EMAIL SUBJECT'
email_plaintext_message = 'TEST EMAIL BODY'
to_emails = ['damyan@winwinnkeeper.net']

mail = EmailMultiAlternatives(
    subject=subject,
    body=email_plaintext_message,
    to=to_emails
)
mail.send()