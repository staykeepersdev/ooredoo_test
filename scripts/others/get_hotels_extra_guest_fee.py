import datetime
import django
import json
import os
import re
import sys
import math

sys.path.append(
    os.path.join(
        os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
        'ooredoo'
    )
)
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from django.core.paginator import Paginator

from hotelbeds_data.models import APIHotel
from hotelbeds_api.services import get_hotels_availability
from hotels.models import HotelRoom, Hotel

def get_params(check_in,check_out, hotel_ids, adults, kids=0):
    params = {
        "stay": {
            "checkIn": check_in,
            "checkOut": check_out
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": adults,
                "children": kids
            }
        ],
        "hotels": {
            "hotel": hotel_ids
        },
        "filter": {
            "paymentType": 'AT_WEB',
            "packaging": True,
            "maxRatesPerRoom" : 1
        }
    }

    # if kids:
    #     params['occupancies'][0]['paxes'] = [
    #         {
    #             "type": "CH",
    #             "age": kids
    #         },
    #     ]

    return params

hotels = {}

tomorrow = datetime.date.today() + datetime.timedelta(days=1)

day_to_check  = [5,30,90]
paxes_list = [(2,0),(3,0),(4,0)]

all_hotels = APIHotel.objects.all().order_by('id')
tomorrow = datetime.date.today() + datetime.timedelta(days=1)

paginator = Paginator(all_hotels, 1000)
for page in range(1, paginator.num_pages + 1):
    print(page)

    hotel_ids = list(paginator.page(page).object_list.values_list('code', flat=True))

    for day in range(len(day_to_check)):
        check_in = tomorrow + datetime.timedelta(days=(day_to_check[day]))
        check_out = check_in + datetime.timedelta(days=1)

        check_in_str = check_in.strftime("%Y-%m-%d")
        check_out_str = check_out.strftime("%Y-%m-%d")
        for paxes in paxes_list:
            adults = paxes[0]
            params = get_params(check_in_str, check_out_str, hotel_ids, adults,)

            try:
                hotels_raw_data = get_hotels_availability(params).get('hotels').get('hotels')
                for hotel_data in hotels_raw_data:
                    hotel_code = hotel_data['code']
                    hotels.setdefault( hotel_code, { 'name': hotel_data['name'], 'rooms': {} } )

                    rooms = hotels[hotel_code]['rooms']
                    for room_data in hotel_data['rooms']:
                        room_code = room_data['code']
                        
                        rooms.setdefault(room_code,{
                            'name': room_data['name'],
                            'prices': {},
                            'extra_guest_fee' : 0
                        })
                        
                        room_rate_net = float(room_data['rates'][0]['net'])
                        prices = rooms[room_code]['prices']
                        prices.setdefault(check_in_str,[])
                        rooms[room_code]['prices'][check_in_str].append(room_rate_net)
                        
            except Exception as e:
                print(e)

# Get the extra_guest_price
print('we are starting the extra guest assignment')
counter = 0
for hotel_code in hotels:
    counter+=1
    if counter % 1000 == 0:
        print(counter)
    hotel_object = hotels[hotel_code]
    hotel_rooms = hotel_object['rooms']
    for room_code in hotel_rooms:
        room_object = hotel_rooms[room_code]
        day_prices = room_object['prices']
        for price_list in day_prices.values():
            if len(price_list) > 1:
                adults2 = price_list[0]
                adults3 = price_list[1]

                price_diff = math.ceil(adults3 - adults2)
                if price_diff == 0:
                    try:
                        adults4 = price_list[2]
                        price_diff = math.ceil(adults4 - adults3)
                    except:
                        pass
                if room_object['extra_guest_fee'] < price_diff:
                    room_object['extra_guest_fee'] = price_diff

        # After all prices check update the model
        if room_object['extra_guest_fee'] > 0:

            # Minimum 4 extra guest at AirBnB
            if room_object['extra_guest_fee'] < 4:
                extra_guest_fee = 4
            elif room_object['extra_guest_fee'] > 237:
                extra_guest_fee = 237
            else:
                extra_guest_fee = room_object['extra_guest_fee']

            hotel_room = HotelRoom.objects.filter(room_code=room_code, hotel__api_hotel__code=hotel_code).first()
            if hotel_room:
                hotel_room.extra_guest_fee = extra_guest_fee
                hotel_room.save()
            else:
                try:
                    room_code_split = room_code.split('.', 1)
                    hotel = Hotel.objects.get(api_hotel__code=hotel_code)
                    HotelRoom.objects.create(
                        hotel=hotel,
                        room_code=room_code,
                        room_type=room_code_split[0],
                        characteristic_code=room_code_split[1],
                        extra_guest_fee=extra_guest_fee
                    )
                    print(f'CREATED room {hotel_code} - {room_code} - {room_object["extra_guest_fee"]}')
                except:
                    print(f'Missing HOTEL {hotel_code} - ({room_code})')