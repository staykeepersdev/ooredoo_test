import time
import datetime
import sys, os
import threading
import time
import math

current_path = os.path.join(
    os.path.dirname(os.path.realpath(__file__)).split('ooredoo', 1)[0],
    'ooredoo'
)
sys.path.append(current_path)

import django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from hotelbeds_data.models import APIHotel
from hotelbeds_api.clients.content_api_client import HotelsAPIClient

def get_total_hotels():
    params ={
        "fields" : "code",
        "from": 1,
        "to": 2,
        "lastUpdateTime": LAST_UPDATED_TIME
    }    
    
    # Exceeded limit fix
    while True:
        response_data = HotelsAPIClient().get_hotels(params)
        if 'error' in response_data:
            time.sleep(2)
        else:
            break 
    return response_data.get('total')

def get_hotels_from_page(from_hotel, to_hotel):
    global updated_hotels
    global created_hotels

    api_hotels_to_add = []
    api_hotels_to_update = []

    params ={
        "fields" : "all",
        "from": from_hotel,
        "to": to_hotel,
        "lastUpdateTime": LAST_UPDATED_TIME
    }
    
    # Exceeded limit fix
    while True:
        response_data = HotelsAPIClient().get_hotels(params)
        if 'error' in response_data:
            time.sleep(2)
        else:
            break 
    hotels_data = response_data.get('hotels')   

    for hotel in hotels_data:
        code = hotel.get('code')

        date_str = hotel.get('lastUpdate')
        updated_at = datetime.datetime.strptime(date_str, '%Y-%m-%d')
        if code in DB_API_HOTELS_CODES:
            hotel_obj = APIHotel.objects.get(code=code)
            hotel_obj.data = hotel
            hotel_obj.updated_at = updated_at

            api_hotels_to_update.append(hotel_obj)
            updated_hotels+=1
        
        else:
            api_hotels_to_add.append(
                APIHotel(
                    code=code,
                    data=hotel,
                    updated_at=updated_at,
                )
            )
            created_hotels +=1
    # Bulk Create
    if api_hotels_to_add:
        APIHotel.objects.bulk_create(api_hotels_to_add, ignore_conflicts=True)
        
    # Bulk Update
    if api_hotels_to_update:
        APIHotel.objects.bulk_update(api_hotels_to_update, ['data', 'updated_at'])

def run_threaded(job_func, parm):
    job_thread = threading.Thread(target=job_func, args=parm)
    job_thread.start()
    return job_thread

DB_API_HOTELS_CODES = list(APIHotel.objects.all().values_list('code', flat=True))
YESTERDAY = (datetime.date.today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')
LAST_UPDATED_TIME = YESTERDAY

HOTELS_PER_THREAD = 500
MAX_THREADS = 5 # (4)

TOTAL_HOTELS = get_total_hotels()
REQUESTS_AMOUNT = math.ceil(TOTAL_HOTELS / HOTELS_PER_THREAD)

updated_hotels = 0
created_hotels = 0

for page in range(REQUESTS_AMOUNT):
    # Check running threads before starting new one
    while threading.active_count() >= MAX_THREADS:
        time.sleep(2)

    from_hotel = page * HOTELS_PER_THREAD + 1
    to_hotel = (page+1) * HOTELS_PER_THREAD

    run_threaded(get_hotels_from_page, (from_hotel, to_hotel) )

logs_folder = os.path.join(current_path, 'logs')
with open(f'{logs_folder}/api-hotels-update.txt', 'a+') as file:
    file.write(f'Total hotels: {TOTAL_HOTELS} | Updated: {updated_hotels} | Created: {created_hotels} | Total requests: {REQUESTS_AMOUNT}| Last updated at: {LAST_UPDATED_TIME}\n')
