
import datetime

from django.contrib.auth.mixins import UserPassesTestMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views import View
from django.http import JsonResponse


from .services import (
    get_available_destinations_rooms,
    scrape_destination,
    extract_scraped_data,
    convert_scraped_data_to_csv
)

from calendar_app.services import prepare_auto_hostify_rooms
from calendar_app.models import CalendarRoom


# Destination Hotels Price Checker Front End View
class DestinationPricesViews(View):
    template_name = 'price-checker.html'
    results_template_name = 'price-checker-results.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        try:
            hotel_ids = country = city = city_shortcode = ''

            hotel_ids = request.POST.get('hotel_ids', '')
            if hotel_ids:
                hotel_ids = hotel_ids.split(',')
            else:
                country = request.POST.get('country')
                city = request.POST.get('city')
                city_shortcode = request.POST.get('city_shortcode')

            starting_date = datetime.datetime.strptime(request.POST.get('starting_date', ''), '%Y-%m-%d').date()           
            days_in_advance = int(request.POST.get('days_in_advance', 1))
            length_of_stay = int(request.POST.get('length_of_stay', 1))
            
            scraped_data_ids = scrape_destination(hotel_ids, country, city, city_shortcode, starting_date, days_in_advance, length_of_stay)
            extract_scraped_data(scraped_data_ids)
            scraped_data_objects = convert_scraped_data_to_csv(scraped_data_ids)

            return render(request, self.results_template_name, {'scraped_data': scraped_data_objects})

        except Exception as e:

            return render(request, self.template_name, {'error': e})

class DestinationListView(UserPassesTestMixin, View):
    template_name = 'destination-list.html'
    def get(self, request, *args, **kwargs):
        destination_code = self.kwargs['destination_code']
        hotels, hotels_total = get_available_destinations_rooms(destination_code)
        params = {
            'destination_code': destination_code,
            'hotels': hotels,
            'hotels_total': hotels_total,
            'airbnb_accounts': [key for key,value in CalendarRoom.AIRBNB_ACCOUNTS ]
        }
        return render(request, self.template_name, params)
    
    def test_func(self):
        return self.request.user.is_superuser
    

class AjaxPrepareAutoHostifyRooms(View):
        
    def post(self, request, *args, **kwargs):        
        import_status = prepare_auto_hostify_rooms(request)
        return JsonResponse(import_status)