import csv
import datetime
import statistics

from collections import defaultdict as ddict
from decimal import Decimal
from io import StringIO

from django.core.files.base import ContentFile
from django.db.models.functions import Length
from django.db.models import Count

from currencies.models import ExchangeRate

from hotels.models import Hotel, HotelRoom
from hotelbeds_api.services import get_hotels_availability, get_hotels_content
from hotelbeds_data.models import APIRateComment, APIHotel

from hotelbeds_scraper.models import ScrapedData

def get_destination_hotel_ids(city_shortcode):
    # Get From DB if exists
    api_hotels = APIHotel.objects.filter(data__destinationCode=city_shortcode)
    if api_hotels.exists():
        hotel_ids = list(api_hotels.values_list('code', flat=True))
   
    # Get from HotelBeds
    else:        
        params = {
            "fields": "code",
            "language":"ENG",
            "from": 1,
            "to": 1000,
            'destinationCode': city_shortcode.upper()
        }
        hotels_data = get_hotels_content(params)['hotels']
        hotel_ids = []
        for hotel in hotels_data:
            hotel_ids.append(int(hotel['code']))
    return hotel_ids

def get_params(check_in,check_out, hotel_ids):
    params = {
        "stay": {
            "checkIn": check_in.strftime("%Y-%m-%d"),
            "checkOut": check_out.strftime("%Y-%m-%d")
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": 2,
                "children": 0
            }
        ],
        "hotels": {
            "hotel": hotel_ids
        },
        "filter": {
            "paymentType": 'AT_WEB',
            "packaging": True
        }
    }
    return params
def add_availability_avg_price(hotels, total_days_check):
    for hotel_code in hotels:
        hotel_object_rooms = hotels[hotel_code]['rooms']
        for room_code in hotel_object_rooms:
            room_object = hotel_object_rooms[room_code]
            room_object['average_price'] = '%.2f' % ( float(statistics.mean(room_object['prices'])) )
            room_object['availability'] = int(len(room_object['prices']) / total_days_check * 100)

def check_matched_hotels_description(hotels_dictionary, hotels_queryset):
    small_descritpion_hotels = hotels_queryset.annotate(length=Length('description')).filter(length__lt=150)
    small_descritpion_hotel_codes = list(small_descritpion_hotels.values_list('api_hotel__code', flat=True))
    
    for code in small_descritpion_hotel_codes:
        hotels_dictionary[code]['errors'].append('small_description')

def check_room_stays(hotels_dictionary, hotels_queryset):
    rooms = HotelRoom.objects.filter(hotel__in=hotels_queryset).exclude(room_stays=None).exclude(room_stays='')
    hotel_rooms_set = set(rooms.values_list('hotel__api_hotel__code','room_code'))
    hotel_rooms_dict = ddict(list)

    for hotel_id, room_code in hotel_rooms_set:
        hotel_rooms_dict[hotel_id].append(room_code)
    
    for hotel_code, room_codes in hotel_rooms_dict.items():
        hotel_room_objects = hotels_dictionary[hotel_code]['rooms']

        for room_code in room_codes:
            if room_code in hotel_room_objects:
                hotel_room_objects[room_code]['room_stays'] = True

def check_hotel_photos(hotels_dictionary, hotels_queryset):
    no_photos_hotels = hotels_queryset.annotate(number_of_photos=Count('images')).filter(number_of_photos__lt=3)
    no_photos_hotels_codes = list(no_photos_hotels.values_list('api_hotel__code', flat=True))

    for code in no_photos_hotels_codes:
        hotels_dictionary[code]['errors'].append('no_photos')

def check_hotel_rooms_facilities(hotels_dictionary, hotels_queryset):
    ## Get NO FACILITIES HOTELS
    no_facilities_hotels = hotels_queryset.annotate(number_of_facilities=Count('facilities')).filter(number_of_facilities__lt=5)
    no_facilities_hotels_codes = list(no_facilities_hotels.values_list('api_hotel__code', flat=True))

    for code in no_facilities_hotels_codes:
        hotels_dictionary[code]['errors'].append('no_facilities')
        
    # ## GET NO FACILITIES ROOMS
    # rooms = HotelRoom.objects.filter(hotel__in=hotels_queryset).annotate(number_of_facilities=Count('facilities')).filter(number_of_facilities__lt=5)
    # hotel_rooms_set = set(rooms.values_list('hotel__api_hotel__code','room_code'))
    # hotel_rooms_dict = ddict(list)

    # for hotel_id, room_code in hotel_rooms_set:
    #     hotel_rooms_dict[hotel_id].append(room_code)
    
    # for hotel_code, room_codes in hotel_rooms_dict.items():
    #     hotel_room_objects = hotels_dictionary[hotel_code]['rooms']

    #     for room_code in room_codes:
    #         if room_code in hotel_room_objects:
    #             hotel_room_objects[room_code]['no_facilities'] = True

def get_available_destinations_rooms(city_shortcode):
    hotels = {}
    hotelbeds_hotel_ids = []
    hotel_ids = get_destination_hotel_ids(city_shortcode)
    hotels_total = len(hotel_ids)
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)

    days_to_check = 6 #10
    times_to_check = 4 # 4

    total_days_check = days_to_check*times_to_check

    for day in range(days_to_check):
        # print(f'Day #{day}')
        # availabilty now/90days/180days/270days
        for multiplier in range(times_to_check):
            check_in = tomorrow + datetime.timedelta(days=(multiplier*90 + day))
            check_out = check_in + datetime.timedelta(days=1)
            params = get_params(check_in, check_out, hotel_ids)
            # Max 1 rate per room 
            params['filter']['maxRatesPerRoom'] = 1
            
            # Hotels 3+ stars
            params["reviews"] = [{
                    "type": "TRIPADVISOR",
                    "minRate": 4
            }]

            try:
                hotels_raw_data = get_hotels_availability(params).get('hotels').get('hotels')
                for hotel_data in hotels_raw_data:
                    hotel_code = hotel_data['code']
                    hotelbeds_hotel_ids.append(hotel_code)
                    hotels.setdefault(hotel_code,{ 'name': hotel_data['name'], 'rooms': {}, 'errors': [] })

                    rooms = hotels[hotel_code]['rooms']
                    for room_data in hotel_data['rooms']:
                        room_code = room_data['code']
                        
                        rooms.setdefault(room_code,{
                            'name': room_data['name'],
                            'prices': [] ,
                            'room_stays': False,
                        })
                        
                        room_rate_net = float(room_data['rates'][0]['net'])
                        rooms[room_code]['prices'].append(room_rate_net)
                        
            except Exception as e:
                print(e)

    matched_api_hotels = Hotel.objects.filter(api_hotel__code__in=(set(hotelbeds_hotel_ids)))

    # Add Availability + Average_price
    add_availability_avg_price(hotels, total_days_check)

    # Check Description size
    check_matched_hotels_description(hotels, matched_api_hotels)

    # Check for missing room stay
    check_room_stays(hotels, matched_api_hotels)

    # Check Hotel
    check_hotel_photos(hotels, matched_api_hotels)

    # Check Hotel and Room Facilities
    check_hotel_rooms_facilities(hotels, matched_api_hotels)

    
    return hotels, hotels_total
            


def scrape_destination(hotel_ids, country, city, city_shortcode, starting_date, days_in_advance, length_of_stay):
    scraped_data_ids = []
    
    if not hotel_ids:        
        hotel_ids = get_destination_hotel_ids(city_shortcode)

    for i in range(1, days_in_advance + 1):
        check_in = starting_date + datetime.timedelta(days=i)
        check_out = check_in + datetime.timedelta(days=length_of_stay)


        params = get_params(check_in, check_out, hotel_ids)
        try:
            hotels_raw_data = get_hotels_availability(params).get('hotels').get('hotels')

            scraped_data_object = ScrapedData.objects.create(
                check_in=check_in,
                check_out=check_out,
                country=country,
                city_shortcode=city_shortcode,
                city=city,
                raw_data = hotels_raw_data,
            )
            scraped_data_ids.append(scraped_data_object.pk)
        except:
            print(check_in)
    
    return scraped_data_ids

def get_hotels_types_and_adresses(json_data, hotel_codes):
    hotel_objects = list(Hotel.objects.filter(
        api_hotel__code__in=hotel_codes
    ).values_list('api_hotel__code','address', 'accommodation_type_code', 'api_hotel_category__simple_code'))
    if hotel_objects:
        for rate in json_data:
            rate_hotel_code = rate.get('hotel_code','')            

            for hotel_object in hotel_objects:
                if int(hotel_object[0]) == int(rate_hotel_code):
                    rate['hotel_address'] = hotel_object[1]
                    rate['hotel_type'] = hotel_object[2]
                    rate['hotel_stars'] = hotel_object[3]

                    break    
    return json_data  



def get_rate_comments_from_db(json_data, rate_comments_id, check_in, check_out):
    rate_objects = list(APIRateComment.objects.filter(
        code__in=rate_comments_id,                   
        date_start__lte=check_in,
        date_end__gte=check_out,
    ).values_list('incoming','code','rate_codes', 'description'))

    # Add each comment to the final result
    if rate_objects:
        for rate in json_data:
            rate_comments_id = rate.get('rate_comment','')            
            if rate_comments_id:
                rate_description = ''
                rate_comment_split = rate_comments_id.split('|')

                for rate_object in rate_objects:
                    try:
                        if (int(rate_comment_split[0]) == rate_object[0] and
                                rate_comment_split[1] == rate_object[1] and
                                int(rate_comment_split[2]) in rate_object[2]):

                                rate_description += rate_object[3]
                    except:
                        pass

                rate['rate_comment'] = rate_description
    return json_data

def parse_data(scraped_data_obj , rates):
    rate_comments_id = hotel_codes = []
    
    json_data = []

    check_in = scraped_data_obj.check_in
    check_out = scraped_data_obj.check_out
    country = scraped_data_obj.country
    city = scraped_data_obj.city
    created_at = scraped_data_obj.created_at
    scraped_time = created_at.strftime("%Y-%m-%d %H:%M:%S")
    rounded_hour = created_at.strftime("%Y-%m-%d %H:00:00")
    hotels_data = scraped_data_obj.raw_data
    if hotels_data:
        for hotel in hotels_data:
            hotel_name = hotel['name']
            hotel_code = hotel['code']
            hotel_codes.append(hotel_code)
            hotel_type = ''
            hotel_stars = hotel['categoryName']
            hotel_adress = ''
            hotel_currency = hotel['currency']
            city_zone = hotel.get('zoneName', '')
            for room in hotel['rooms']:
                room_type = room['name']
                room_code = room['code']
                for rate in room['rates']:

                    # Get rates_ids
                    rate_comments_id_string = rate.get('rateCommentsId','')
                    if rate_comments_id_string:
                        rate_comment_id = rate_comments_id_string.split('|')[1]
                        rate_comments_id.append(rate_comment_id)

                    board_type = rate['boardName']

                    # Get 4 prices
                    price = rate['net'] + hotel_currency
                    if hotel_currency == 'EUR':
                        price_in_EUR = rate['net']
                        price_in_GBP = str(round(Decimal(rate['net']) * rates['EUR-GBP'],2))
                        price_in_USD = str(round(Decimal(rate['net']) * rates['EUR-USD'],2))
                    elif hotel_currency == 'GBP':
                        price_in_EUR = str(round(Decimal(rate['net']) * rates['GBP-EUR'],2))
                        price_in_GBP = rate['net']
                        price_in_USD = str(round(Decimal(rate['net']) * rates['GBP-USD'],2))
                    else:
                        price_in_EUR = ''
                        price_in_GBP = ''
                        price_in_USD = ''

                    # Get cancelation policies
                    cancelation_policies = ''
                    try:
                        cancellation_policies_list = rate['cancellationPolicies']
                        for policy in cancellation_policies_list:
                            cancelation_policies += f"{policy['amount']} {hotel_currency} from: {policy['from']} "
                    except:
                        pass
                    # Get offers
                    offers= ''
                    try:
                        offers_policies_list = rate['offers']
                        for offer in offers_policies_list:
                            offers += f"{offer['name']} {offer['amount']} {hotel_currency} "
                    except:
                        pass

                    rate_data = {
                        'rounded_hour': rounded_hour,
                        'scraped_time': scraped_time,
                        'hotel_code': hotel_code,
                        'check_in': check_in,
                        'check_out': check_out,
                        'country': country,
                        'city': city,
                        'city_zone': city_zone,
                        'hotel_name': hotel_name,
                        'hotel_type': hotel_type,
                        "hotel_stars": hotel_stars,
                        'hotel_address': hotel_adress,
                        'room_type': room_type,
                        'room_code': room_code,
                        'board_type': board_type,
                        'original_price': price,
                        'price_in_EUR': price_in_EUR,
                        'price_in_GBP': price_in_GBP,
                        'price_in_USD': price_in_USD,
                        'rate_comment': rate_comments_id_string,
                        'cancellation_policies': cancelation_policies,
                        'offers': offers
                    }
                    json_data += [rate_data]
                
    json_data = get_rate_comments_from_db(json_data, rate_comments_id, check_in, check_out)
    
    json_data = get_hotels_types_and_adresses(json_data, hotel_codes)
    return json_data

def get_curruncy_rates():
    rates = {
        'EUR-GBP' : ExchangeRate.objects.get(base__name='EUR', currency__name='GBP').rate ,
        'EUR-USD' : ExchangeRate.objects.get(base__name='EUR', currency__name='USD').rate,
        'GBP-EUR' :  ExchangeRate.objects.get(base__name='GBP', currency__name='EUR').rate,
        'GBP-USD' : ExchangeRate.objects.get(base__name='GBP', currency__name='USD').rate
    }
    return rates

def extract_scraped_data(scraped_data_ids):
    
    rates = get_curruncy_rates()

    scraped_data = ScrapedData.objects.filter(id__in=scraped_data_ids)

    for scraped_data_obj in scraped_data:

        parsed_data = parse_data(scraped_data_obj, rates)
        scraped_data_obj.parsed_data = parsed_data
        scraped_data_obj.save()


def convert_scraped_data_to_csv(scraped_data_ids):
    scraped_data = ScrapedData.objects.filter(id__in=scraped_data_ids).filter(csv='')
    
    fields_to_add = (  
        "rounded_hour",
        "scraped_time",
        "check_in",
        "check_out",
        "city",
        "city_zone",
        "country",
        "hotel_name",
        "hotel_code",
        "hotel_stars",        
        "room_type",
        "room_code",
        "board_type",
        "original_price",
        "offers",
        "cancellation_policies",
    )
    
    for scraped_data_obj in scraped_data:
        if scraped_data_obj.parsed_data:
        
            csv_buffer = StringIO(newline='')
            csv_writer = csv.writer(csv_buffer, lineterminator='\n')
            # f = csv.writer(open("test.csv", "w+", newline=''))
            # Write CSV Header, If you dont need that, remove this line
            csv_writer.writerow(fields_to_add)

            for x in scraped_data_obj.parsed_data:
                csv_writer.writerow([x[field] for field in fields_to_add])
            
            csv_file = ContentFile(csv_buffer.getvalue().encode('utf-8'))
            csv_file_name = f'{scraped_data_obj.country}_{scraped_data_obj.city}_{scraped_data_obj.check_in}_{scraped_data_obj.check_out}_{scraped_data_obj.created_at.strftime("%Y-%m-%d_%H-00")}.csv'
            scraped_data_obj.csv.save(csv_file_name, csv_file)
    
    return scraped_data