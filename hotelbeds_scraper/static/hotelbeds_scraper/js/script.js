$(function() {
        
    // On input change trigger select state function
    function trigger_room_type_state(input_element){
      var $room_type = input_element.closest('.room_type')
      if (input_element.val() > 0){
        $room_type.addClass('selected');
      }
      else{
        $room_type.removeClass('selected');
      }
    }
    // Recalculate total rooms
    function calculate_total_rooms(){
      var sum = 0;
      $('.room_type.selected .rooms_count').each(function() {
        sum += Number($(this).val());
      });
      $('#total').text(sum);
    };

    // Asign event On input change trigger select state
    $('.room_type .rooms_count').on('change', function(){
        trigger_room_type_state($(this));
        calculate_total_rooms();
    });

    // Auto room adding
    function auto_add_rooms(element){
        element.closest('.room_type').find('.rooms_count').val(2).trigger('change')
    }

    // Initial selected rooms
    $hotels_without_error = $('.hotel:not(.no_photos, .no_facilities, .small_description)');
    $('#hotels_without_errors').text($hotels_without_error.length)

    $hotels_without_error.each(function(){      
        var $room = $(this).find('.room_type:not(.not_available) .code:not(:contains("APT"))').first()
        var $apartment= $(this).find('.room_type:not(.not_available) .code:contains("APT")').first()
        if ($room){
            auto_add_rooms($room)
        }
        if ($apartment){
            auto_add_rooms($apartment)
        }
    });

    // Inital calculation
    calculate_total_rooms();

    // Send rooms for creation
    $('#create-listings').on('click', function(){
      
      $('body').css('cursor', 'progress');
      $('#create-listings').prop('disabled', true);

      var rooms = '';
      var url = $(this).data('url');
      
      $('.room_type.selected').each(function(){
        var hotel_code = $(this).data('hotel');
        var room_code = $(this).find('.code').text();
        var amount = $(this).find('.rooms_count').val();
        rooms+= hotel_code  +  ',' + room_code + ',' + amount + ';'
      });
      
      var params = {
        'rooms': rooms,
        'airbnb_account' : $('#airbnb_account').val(),
        'csrfmiddlewaretoken' : $('input[name="csrfmiddlewaretoken"]:first').val(),
      }
      
      
      
      $.post( url, params, function( data ) {
        $("body").css("cursor", "default");
        $('#create-listings').prop('disabled', false);

        if (data == '0'){
          alert('Something went wrong! Listings were not created.')
        }
        else{
          alert("Imported without errors rooms: " + data['ready'] + ". Rooms with_errors: " + data['error']+ ". Errors : " + data['errors_list'] );
        }
      });

    });
});