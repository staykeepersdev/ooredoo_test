from django.apps import AppConfig


class HotelbedsScraperConfig(AppConfig):
    name = 'hotelbeds_scraper'
