from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from django.utils import timezone


class ScrapedData(models.Model):

    check_in = models.CharField(max_length=20)
    check_out = models.CharField(max_length=20)
    country = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    ## Blank true for old searches
    city_shortcode = models.CharField(max_length=5, blank=True, null=True) 
    raw_data = JSONField(null=True, blank=True)
    parsed_data = JSONField(null=True, blank=True)
    csv = models.FileField(upload_to='scraped_data', blank=True, verbose_name="CSV file:")
    created_at = models.DateTimeField(auto_now_add=True)
