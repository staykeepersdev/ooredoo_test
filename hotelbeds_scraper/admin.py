from django.contrib import admin
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget

from .models import ScrapedData

@admin.register(ScrapedData)
class ScrapedDataAdmin(admin.ModelAdmin):
    list_display=('pk', 'country', 'city','check_in', 'city_shortcode', 'check_out', 'created_at')
    list_filter=('country',)
    exclude = ('raw_data',)
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget}
    }