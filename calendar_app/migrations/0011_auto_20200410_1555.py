# Generated by Django 2.2.7 on 2020-04-10 12:55

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion

def create_hotel_rooms(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    CalendarRoomType = apps.get_model('calendar_app', 'CalendarRoomType')
    CalendarRoom = apps.get_model('calendar_app', 'CalendarRoom')

    for room_type in CalendarRoomType.objects.all():
        if room_type.hostify_ids and room_type.title:

            hostify_ids = room_type.hostify_ids.split(',')        
            titles =  room_type.title.split(',')
            for i,hostify_id in enumerate(hostify_ids):
                try:
                    title = titles[i].strip()
                except:
                    title = 'missing_title'
                CalendarRoom.objects.create(
                    calendar_room_type=room_type,
                    hostify_id=hostify_id.strip(),
                    title=title,
                    hostify_status='manually',
                    auto_pricing=True,
                )

class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0010_auto_20200410_1348'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='hostify_internal_code',
        ),
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='request_data',
        ),
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='response_data',
        ),
        migrations.AddField(
            model_name='calendarroomtype',
            name='booking_com_id',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='calendarroomtype',
            name='pbi_days',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='calendarroomtype',
            name='pbi_price',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='calendarroomtype',
            name='pbi_price_percentage',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='calendarroomtype',
            name='calendar_hotel',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calendar_room_types', to='calendar_app.CalendarHotel'),
        ),
        migrations.CreateModel(
            name='CalendarRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=1024, null=True)),
                ('hostify_id', models.CharField(blank=True, max_length=255, null=True)),
                ('hostify_internal_code', models.PositiveIntegerField(blank=True, null=True)),
                ('markup', models.CharField(blank=True, max_length=255, null=True)),
                ('auto_pricing', models.BooleanField(default=False)),
                ('request_data', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True)),
                ('response_data', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True)),
                ('errors', models.TextField(blank=True, null=True)),
                ('hostify_status', models.CharField(choices=[('ready', 'Ready for listing'), ('listed', 'Listed'), ('not_listed', 'Not listed'), ('manually', 'Manually listed'), ('deactivated', 'Deactivated'), ('info_errors', 'Information eError')], default='not_listed', max_length=50)),
                ('calendar_room_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calendar_rooms', to='calendar_app.CalendarRoomType')),
            ],
        ),
        migrations.RunPython(create_hotel_rooms),
    ]
