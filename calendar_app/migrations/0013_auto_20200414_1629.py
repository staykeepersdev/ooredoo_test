# Generated by Django 2.2.7 on 2020-04-14 13:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0012_auto_20200410_1617'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='booking_com_id',
        ),
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='hostify_ids',
        ),
        migrations.RemoveField(
            model_name='calendarroomtype',
            name='title',
        ),
    ]
