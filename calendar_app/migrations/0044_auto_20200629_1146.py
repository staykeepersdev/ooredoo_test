# Generated by Django 2.2.7 on 2020-06-29 08:46

from django.db import migrations, models

def set_new_airbnb_statuses(apps, schema_editor):
    CalendarRoom = apps.get_model('calendar_app', 'CalendarRoom')   

    #Unlisted
    CalendarRoom.objects.filter(listed_at_airbnb=False).update(airbnb_status='unlisted')

    #Listed
    CalendarRoom.objects.filter(listed_at_airbnb=True).update(airbnb_status='listed')

class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0043_auto_20200626_1313'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendarroom',
            name='airbnb_status',
            field=models.CharField(choices=[('listed', 'Listed'), ('unlisted', 'Unlisted'), ('not_set', 'Not set')], default='not_set', max_length=50),
        ),
        migrations.AlterField(
            model_name='calendarroom',
            name='hostify_status',
            field=models.CharField(choices=[('ready', 'Ready for listing'), ('listed', 'Listed'), ('listed_with_errors', 'Listed with errors'), ('not_listed', 'Not listed'), ('manually', 'Manually listed'), ('deactivated', 'Deactivated'), ('info_errors', 'Information Error'), ('waiting', 'Waiting in Queue'), ('updating', 'Updating in Queue'), ('update_errors', 'Update Errors'), ('processed', 'Being processed'), ('failed', 'Failed')], default='not_listed', max_length=50),
        ),
        migrations.RunPython(set_new_airbnb_statuses)
    ]
