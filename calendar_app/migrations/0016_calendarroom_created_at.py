# Generated by Django 2.2.7 on 2020-04-16 08:59

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0015_auto_20200415_1551'),
    ]

    operations = [
        migrations.AddField(
            model_name='calendarroom',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
