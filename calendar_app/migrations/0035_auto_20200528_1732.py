# Generated by Django 2.2.7 on 2020-05-28 14:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0034_calendarroom_listed_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='calendarroom',
            name='markup',
            field=models.CharField(default='20', max_length=255),
        ),
    ]
