# Generated by Django 2.2.7 on 2020-05-22 14:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0031_auto_20200521_1646'),
    ]

    operations = [
        migrations.RenameField(
            model_name='calendarroom',
            old_name='airbnb_listing_id',
            new_name='hostify_child_id',
        ),
    ]
