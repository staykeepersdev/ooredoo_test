# Generated by Django 2.2.7 on 2020-04-10 10:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('calendar_app', '0009_calendarhotelroom_hostify_internal_code'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='CalendarHotelRoom',
            new_name='CalendarRoomType',
        ),
    ]
