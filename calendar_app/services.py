import csv
import datetime
import random
import re

from typing import Tuple

from django.utils import timezone
from django.http import HttpResponse
from django.urls import reverse

from booking_com.models import BookingComHotel, BookingComHotelRoom
from hotelbeds_data.models import APIHotel, APIFacility, APICountry
from hotels.models import HotelRoom
from hostify_api.listing_api_client import ListingAPIClient

from revenue.models import ExtraGuestPrice

from .models import CalendarHotel, CalendarRoomType, CalendarRoom, CalendarDay

from .static_data.all_room_types import ALL_HOTEL_ROOM_TYPES
from .static_data.nickname_adjectives import ADJECTIVES
from .static_data.zones import ZONES
from .static_data.hb_rooms_and_beds import ROOM_TYPES, ROOM_STAY_FACILITIES, ROOM_OCCUPANCY_CAP

from .static_data.hostify_custom_fields_ids import HOSTIFY_CUSTOM_FIELDS_IDS

# Import and get an instance of a logger
import logging
logger = logging.getLogger(__name__)

# Get Countries Description
from hotelbeds_data.services import get_countries
COUNTRIES = get_countries()

LISTING_API_CLIENT = ListingAPIClient()

# Original bed types from the list 
# BED_TYPES = ['single_bed', 'double_bed', 'king_bed', 'queen_bed', 'sofa_bed', 'couch', 'bunk_bed']
BED_TYPES = ['single_bed', 'double_bed', 'double_bed', 'double_bed', 'sofa_bed', 'couch', 'bunk_bed']

def get_or_create_c_hotel(api_hotel, b_com_hotel_id, b_com_hotel_matched):
    calendar_hotel, _ = CalendarHotel.objects.get_or_create(
        hotelbeds_api_hotel=api_hotel,
        defaults={
            'name': api_hotel.data['name']['content'],
            'booking_com_id': b_com_hotel_id,
            'booking_com_matched' : b_com_hotel_matched
        }
    )
    return calendar_hotel

# Get Booking.com hotel and room if we have them scraped
def get_b_com_hotel_and_room(hotel, b_com_hotel_id, b_com_room_type, b_com_room_id, room):    
    b_com_hotel = BookingComHotel.objects.filter(booking_com_id=b_com_hotel_id).first()
    
    if b_com_hotel:
        # Check if connection exists already
        if not b_com_hotel.hotel:
            b_com_hotel.hotel = hotel
            b_com_hotel.save()
        
        b_com_rooms = BookingComHotelRoom.objects.filter(booking_com_hotel=b_com_hotel)
        if b_com_room_type:
            b_com_room = b_com_rooms.filter(room_type=b_com_room_type).first()
        
        elif b_com_room_id:
            b_com_room = b_com_rooms.filter(booking_com_id=b_com_room_id).first()           
    
        if b_com_room:            
            b_com_room.hotel_room = room
            b_com_room.save()  

    else:        
        b_com_room = None
    
    return b_com_hotel, b_com_room

def get_or_create_c_room_type(
        calendar_hotel,
        room_code,
        b_com_room_type, b_com_room_id,
        b_com_room_matched,
        price, margin
    ):
    

    calendar_room_type, _ = CalendarRoomType.objects.get_or_create(
        calendar_hotel=calendar_hotel,
        code=room_code,
        defaults={
            'booking_com_id' : b_com_room_id,
            'booking_com_room_type' : b_com_room_type,
            'booking_com_matched': b_com_room_matched,
            'pbi_price': price,
            'pbi_price_percentage': margin
        }
    )    
    return calendar_room_type

# Generate new Hostify Internal code
def get_new_internal_code():
    try:
        hostify_last_code = CalendarRoom.objects.all().filter(
            hostify_internal_code__isnull=False
        ).order_by('hostify_internal_code').last().hostify_internal_code
        
        if hostify_last_code < 10000:
            hostify_last_code = 10000
        hostify_last_code = hostify_last_code or 10000 # if we don't have hotel rooms with hostify_internal_code
    except:
        # when there are no rooms created yet
        hostify_last_code = 10000
    
    return hostify_last_code + 1


# Get manually matched rooms and beds from static file
def get_manual_matched_rooms(csv_room_type):
    beds = []
    person_capacity = 0        

    for i, bed in enumerate(csv_room_type[5:]):
        if int(bed or 0):
            bed_count = int(bed)
            beds.append(
                {
                    "bed_type": BED_TYPES[i],
                    "bed_number": int(bed)
                }
            )
            # Single Beds
            if i == 0 or i == 6:
                person_capacity += bed_count
            # Sofas
            elif i == 4 or i == 5:
                pass
            # Double beds
            else:
                person_capacity += (bed_count*2)
                
    if not len(beds):
        print(f'# Missing beds for  {csv_room_type[0]}')
        person_capacity = 2
        beds.append(
            {
                "bed_type": BED_TYPES[1],
                "bed_number": 1
            }
        )
    
    rooms = [
        {
            "room_id": "new",
            "name": csv_room_type[3].title(),
            "room_type": csv_room_type[3].lower(),
            'bed': beds
        }
    ]

    return rooms, person_capacity

# Get HotelBeds matched rooms and beds from HB RoomStays field
def get_hb_matched_rooms(room_stays):
    
    rooms = []
    person_capacity = 0

    for stay in room_stays:
        beds = []
        room_type_name = stay['stayType']
        room_type = ROOM_TYPES.get(room_type_name)
        
        room = {
            "room_id": "new",
            "name":room_type.title(),
            "room_type": room_type,
        }

        facilities_data = stay.get('roomStayFacilities', '')
    
        for facility in facilities_data:
            facility_code = f"{facility['facilityCode']}-{facility['facilityGroupCode']}"

            bed_type = ROOM_STAY_FACILITIES.get(facility_code)
            if bed_type:
                number_beds = int(facility['number'])

                if facility_code == '1-61' or facility_code == '993-61':
                    person_capacity+= (1*number_beds)
                else:
                    person_capacity+= (2*number_beds)

                beds.append(
                {
                    "bed_type": bed_type,
                    "bed_number": number_beds
                }
            
        )
        room['bed'] = beds
        rooms.append(room)
    
    return rooms, person_capacity


# Get Booking Restrictions
def get_booking_restrictions_params(hostify_id, room, b_com_room, amenities):
    ## BOOKING.COM WRONG DATA IMPORT NOT FIXED
    # if b_com_room:
    #     checkin_start = b_com_room.checkin_start
    #     checkin_end = b_com_room.checkin_end
    #     checkout = b_com_room.checkout
    # else:
    #     checkin_start = "15:00:00"
    #     checkin_end = "22:00:00"
    #     checkout = "10:00:00"

    checkin_start = "15:00:00"
    checkin_end = "22:00:00"
    checkout = "10:00:00"

    # Depricated - > ExtraGuestPrice Model should be deleted

    # try:
    #     extra_guest_price = ExtraGuestPrice.objects.get(
    #         country__code=room.hotel.country_code
    #     ).price
    # except:
    #     extra_guest_price=0
    
    extra_guest_price = room.extra_guest_fee    

    booking_restrictions_params = {        
        "listing_id": hostify_id,
        "price": 1000,
        "occupancy": 2,
        "extra_guest_price": extra_guest_price,
        "currency": "GBP",
        "min_stay_default": 1,
        "checkin_start": checkin_start,
        "checkin_end": checkin_end,
        "checkout": checkout,
        "children_allowed": 1 if room.max_children > 0 else 0,
        "infants_allowed ": 1 if room.max_children > 0 else 0,
        "pets_allowed ": 1 if 'pets allowed' in ','.join(amenities) else 0,
        "min_notice_hours": 168
    }
    
    return booking_restrictions_params

# Get Photos Params (We don't check for downloaded photos anymore !)

def get_photos_params(request, hostify_id, room, hotel):
    room_photos_ids = list(room.images.all().order_by('order').values_list('id', flat=True))
    if len(room_photos_ids) < 10:
        hotel_photos_count =  10 - len(room_photos_ids)
        hotel_photos_ids = list(
            hotel.images.all().filter(
                hotel_room=None
            ).order_by('order')[:hotel_photos_count].values_list('id', flat=True)
        )
    else:
        hotel_photos_ids = []

    all_photos_ids = room_photos_ids + hotel_photos_ids
    if all_photos_ids:
        photos = [
            request.build_absolute_uri(reverse('get-hotel-image', args=[hotel.id, photo_id])) for photo_id in all_photos_ids
        ]

        photos_params = {
            'listing_id' : hostify_id,
            'photos' : photos
        }
    else:
        photos_params = []
    
    return photos_params

# Replace Quuen / King inside title

def replace_title_bed_types(name):
    bed_titles = (
        ['King Size','Double'],
        ['Queen Size', 'Double'],
        ['King Bed','Double Bed'],
        ['Queen Bed', 'Double Bed']
    )
    for bed_title in bed_titles:
        name = name.replace(bed_title[0], bed_title[1])
    
    name = name.replace('Double Double', 'Double')
    return name

# Updates all 'Actual Hotel name' and hotel labels from summary
def replace_all_hotel_related_occurences(hotel_summary, hotel_name):
    words_to_check = [(hotel_name, 'The building'), ('hotel', 'building'), ('Hotel', 'Building')] # Order is important

    for word_set in words_to_check:
        if word_set[0] in hotel_summary:
            hotel_summary = hotel_summary.replace(word_set[0], word_set[1])

    return hotel_summary


def add_hotel_summary_notes(hotel_summary, hotelbeds_api_hotel):
    SUMMARY_NOTE1 = 'As a result of local government measures and guidelines put in place by service providers – guests may find that some facilities or services are not available.'
    SUMMARY_NOTE2 = 'Please note that this room is with a minimum occupancy of two people.'
    SUMMARY_NOTE3 = 'Please note that all guests must present a valid form of ID upon arrival. Also, as is customary, the accommodation may place a temporary hold on your credit card toward incidentals that you may charge to your stay.'

    # CHECK IF: Hotel summary notes already updated so return as is ...
    # WARNING: There is a Python bug if the Check string is longer than ~100 characters !
    if 'Please note that all guests must present a valid form of ID upon arrival.' in hotel_summary:
        return hotel_summary

    # OTHERWISE: Update them
    hotel_summary += f'\n\n{SUMMARY_NOTE1}'

    if hotelbeds_api_hotel.min_2_adults:
        hotel_summary += f'\n\n{SUMMARY_NOTE2}'

    hotel_summary += f'\n\n{SUMMARY_NOTE3}'

    return hotel_summary


def is_guest_suite_or_apartment(room_type_code, rooms):
    is_apartment = False
    
    # Check if room_type_code includes 'APT' and we have at least 1 room of type 'bedroom'
    if 'APT' in room_type_code:        
        for room in rooms:
            if room['room_type'] == 'bedroom':
                is_apartment = True

    if is_apartment:
        property_type = 'apartment'
        property_type_group = 'apartments'
    else:
        property_type = 'guest suite'
        property_type_group = 'secondary units'

    return property_type, property_type_group


def update_initial_amenities(room_amenities):
    for idx, room_amenity in enumerate(room_amenities):
        # Remove all Breakfast
        if 'breakfast' in room_amenity.lower():
            del room_amenities[idx]

    # Add baseline amenities
    baseline_amenities = ['Towels', 'Bed Linens', 'Hot Water', 'Bathroom', 'Essentials']
    for baseline_amenity in baseline_amenities:
        if not baseline_amenity in room_amenities:
            room_amenities.append(baseline_amenity)

    return room_amenities


def prepare_auto_hostify_rooms(request):
    import_status ={
        'error': 0,
        'ready': 0,
        'errors_list' : '',
    }
    errors = {}
    hostify_id = ''

    hotel_rooms_list = request.POST.get('rooms')
    airbnb_account = request.POST.get('airbnb_account')
    hotel_rooms = hotel_rooms_list.split(';')

    # remove the last empty element if exists
    hotel_rooms = list(filter(None, hotel_rooms)) 
    for hotel_room in hotel_rooms:
        hotel_room_line = hotel_room.split(',')
        hotel_code = hotel_room_line[0]
        room_code = hotel_room_line[1]
        listings_count = int(hotel_room_line[2])

        # Get Database objects 
        api_hotel = APIHotel.objects.filter(code=int(hotel_code)).first()
        try:
            hotel = api_hotel.hotel
        except:
            import_status['errors_list'] += f'HB Hotel with code {hotel_code} doesn\'t exists.\n'
            continue

        room = HotelRoom.objects.filter(hotel=hotel,room_code=room_code).first()
        if not room:
            import_status['errors_list'] += f'{hotel_code} -> {room_code} : HB Hotel Code -> Room Code combo doesn\'t exists.\n'
            continue

        # Get or Create The CalendarHotel obj        
        calendar_hotel = get_or_create_c_hotel(api_hotel, None, False)

        # Get or Create The CalendarRoomType obj
        calendar_room_type = get_or_create_c_room_type(
            calendar_hotel,
            room_code,
            None,
            None,
            False,
            None,
            None,
        )
        existing_rooms_counter = calendar_room_type.calendar_rooms.count()
        
        listings_count = listings_count - existing_rooms_counter
        # Create multiple similar rooms 
        for i in range(listings_count):

            # We dont have the mutliplied once SUI.1B-1 is convert to SUI.1B
            static_room_code = re.sub(r'\-\d+$', '', room_code)
            csv_room_type = ALL_HOTEL_ROOM_TYPES.get(static_room_code)

            # Get hostify internal code
            room_internal_code = get_new_internal_code()
            # Location
            country = COUNTRIES[hotel.country_code]
            city = hotel.city.title()

            tags = list(set([hotel_code,room_code, country, hotel.destination_code, city, 'LEAP','AUTOCREATED']))

            listing_number = existing_rooms_counter + i + 1

            listing_nickname = f"LP_{hotel.destination_code} {hotel.name} {room.room_code} 0{listing_number} - {room_internal_code}"
            
            # Rooms Layout
            if room.room_stays:
                rooms, person_capacity = get_hb_matched_rooms(room.room_stays)
                if not person_capacity:
                    rooms, person_capacity = get_manual_matched_rooms(csv_room_type)
            else:
                rooms, person_capacity = get_manual_matched_rooms(csv_room_type)

            property_type, property_type_group = is_guest_suite_or_apartment(calendar_room_type.code, rooms)

            location_params = {
                "name": listing_nickname,
                "property_type": property_type,
                "property_type_group": property_type_group,
                "listing_type": "entire home",
                "pms_services" : 1,
                "lat": float(hotel.latitude),
                "lng": float(hotel.longitude),
                "address": f'{hotel.address.strip()}, {hotel.city.title()}',
                "city": city,
                "zipcode": hotel.postal_code,
                "country": country,
                "street": hotel.address.strip(),
                "tags": tags,
            }
        
            # Add hostify id / title /code to the room
            calendar_room = CalendarRoom.objects.create(
                title = listing_nickname,
                hostify_internal_code = room_internal_code,
                calendar_room_type = calendar_room_type,
                airbnb_account = airbnb_account
            )
        

            # Check max occupancy CAP
            room_type_code = static_room_code.split('.')[0]
            max_occupancy_cap = ROOM_OCCUPANCY_CAP[room_type_code]
            if person_capacity > max_occupancy_cap:
                person_capacity = max_occupancy_cap

            # Check for room specified capacity
            if 'C3' in room_code:
                person_capacity = 3
            elif 'C4' in room_code:
                person_capacity = 4
            
            # Check marked listings for 3 adults price
            if api_hotel.max_3_adults and person_capacity > 3:
                person_capacity=3
            
            

            # Bathrooms matched from static list
            bathrooms = csv_room_type[2] or 0       
            
            layout_params = {
                "listing_id": hostify_id,
                "person_capacity" : person_capacity,
                "bathrooms": int(bathrooms),
                "rooms": rooms
            }
            
            # AMENITIES
            hotel_amenities_values = list(hotel.facilities.all().values_list('hostify_description','description'))
            hotel_amenities = [x[0] or x[1] for x in hotel_amenities_values]
            
            room_amenities_values= list(room.facilities.all().values_list('hostify_description','description'))
            room_amenities = [x[0] or x[1] for x in room_amenities_values]

            amenities = list(set(hotel_amenities + room_amenities))
            amenities = update_initial_amenities(amenities)

            amenities_params = {
                "listing_id": hostify_id,
                'amenities': amenities
            }

            # TRANSLATIONS
            zone_number = str(api_hotel.data.get('zoneCode'))
            zone_name = ZONES.get(hotel.destination_code).get(zone_number).strip()
            if zone_name.lower() == 'downtown':
                zone_name = "Good Location"

            summary = hotel.description

            # Find and replace all mentions of the actual Hotel name within the hotel summary
            summary = replace_all_hotel_related_occurences(summary, hotel.name)

            if len(summary) < 80:
                errors['summary'] = 'missing'

            # Additional summary notes
            summary = add_hotel_summary_notes(summary, api_hotel)
            name = f'{random.choice(ADJECTIVES)} {room.description} at {zone_name}'.title()

            name = replace_title_bed_types(name)

            translations_params = {
                'listing_id': hostify_id,
                'name': name,
                'summary': summary
            }

            # Booking Restrictions
            booking_restrictions_params = get_booking_restrictions_params(hostify_id, room, None, amenities)

            # Photos Params        
            photos_params = get_photos_params(request, hostify_id, room, hotel)
            if not photos_params:
                errors['photos'] = 'missing'
            
            request_data = {
                'location_params' : location_params,
                'layout_params' : layout_params,
                'amenities_params' : amenities_params,
                'translations_params' : translations_params,
                'booking_restrictions_params' : booking_restrictions_params,
                'photos_params' : photos_params
            }

            # Store the request
            calendar_room.request_data = request_data
            # calendar_room.response_data = response_data
            if not errors:
                calendar_room.hostify_status = 'ready'
                import_status['ready'] +=1
            else:
                calendar_room.hostify_status = 'info_errors'
                calendar_room.errors = errors
                import_status['error'] +=1
            calendar_room.save()
    
    return import_status


def assign_listing_to_hostify_users(hostify_parent_id):
    USER1_ID = 1918 # jovie@winwinnkeeper.net
    USER2_ID = 1922 # jowie@winwinneeper.net
    USER3_ID = 1919 # margarette@winwinnkeeper.net
    USER4_ID = 1792 # hassan@staykeepers.com

    USER5_ID = 1925 # anna@winwinnkeeper.net
    USER6_ID = 1935 # pavlinageorgieva@winwinnkeeper.net

    try:
        # Make sure the Hostify Parents are correctly assigned
        response1 = LISTING_API_CLIENT.user_add_listing({'user_id': USER1_ID, 'listing_id': hostify_parent_id})
        response2 = LISTING_API_CLIENT.user_add_listing({'user_id': USER2_ID, 'listing_id': hostify_parent_id})
        response3 = LISTING_API_CLIENT.user_add_listing({'user_id': USER3_ID, 'listing_id': hostify_parent_id})
        response4 = LISTING_API_CLIENT.user_add_listing({'user_id': USER4_ID, 'listing_id': hostify_parent_id})

        response5 = LISTING_API_CLIENT.user_add_listing({'user_id': USER5_ID, 'listing_id': hostify_parent_id})
        response6 = LISTING_API_CLIENT.user_add_listing({'user_id': USER6_ID, 'listing_id': hostify_parent_id})

        success = response1['success'] and response2['success'] and response3['success'] and response4['success'] and response5['success'] and response6['success']
        return success

    except Exception as e:
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        logger.error(f'{now} EXCEPTION Assign listing to Hostify Users: {str(e)}')


def create_hostify_listing(calendar_room_id):
    calendar_room = CalendarRoom.objects.get(id=calendar_room_id)
    calendar_room.hostify_status = 'processed'
    calendar_room.save()

    request_data = calendar_room.request_data
    response_data = {}
    errors = ''

    # LOCATION (getting Hostify Listing ID)
    location_params = request_data['location_params']
    location_params['name'] += ''

    # Property types & Listing types are listed here:
    # @link https://app.hostify.com/docs#listing-location
    
    # --> Only Used for listings created before 25.06.2020 
    if 'APT' in calendar_room.calendar_room_type.code:
        location_params['property_type'] = 'apartment'
    else:
        location_params['property_type'] = 'studio'

    location_params['listing_type'] = 'entire home' # or maybe private room ?
    # <--
    try:
        location_response = LISTING_API_CLIENT.process_location(location_params)

        hostify_id = int(location_response.get('listingId'))
    except Exception as e:
        calendar_room.hostify_status = 'failed'
        calendar_room.errors = e
        calendar_room.save()
        return False

    # Layout
    layout_response, layout_errors = process_part(hostify_id, 'layout', request_data['layout_params'])

    # Amenities
    amenities_response, amenities_errors = process_part(hostify_id, 'amenities', request_data['amenities_params'])

    # Translations
    translations_response, translations_errors = process_part(hostify_id, 'translations', request_data['translations_params'])

    # Booking restrictions
    booking_restrictions_response, booking_restrictions_errors = process_part(hostify_id, 'booking_restrictions', request_data['booking_restrictions_params'])

    # Photos
    photos_response, photos_errors = process_part(hostify_id, 'photos', request_data['photos_params'])

    # Set additional meta data for each Hostify listing
    update_hostify_custom_fields(calendar_room)

    response_data = {
        'location_response': location_response,
        'layout_response': layout_response,
        'amenities_response': amenities_response,
        'translations_response': translations_response,
        'booking_restrictions_response': booking_restrictions_response,
        'photos_response': photos_response
    }

    # Store the request
    calendar_room.response_data = response_data
    calendar_room.hostify_id = hostify_id
    errors = layout_errors + amenities_errors + translations_errors + booking_restrictions_errors + photos_errors
    if errors:
        calendar_room.hostify_status = 'listed_with_errors'
    else:
        calendar_room.hostify_status = 'listed'

    calendar_room.errors = errors
    calendar_room.save()

    # SLOW OPERATION - worse case, up to 8 seconds
    assign_listing_to_hostify_users(calendar_room.hostify_id)


def update_hostify_listing(calendar_room_id, part):
    calendar_room = CalendarRoom.objects.get(id=calendar_room_id)
    calendar_room.hostify_status = 'updating'
    calendar_room.save()

    hostify_id = calendar_room.hostify_id
    request_data = calendar_room.request_data
    response_data = calendar_room.response_data
    errors = ''

    response, errors = process_part(hostify_id, part, request_data[f'{part}_params'])

    # Overwrite the updated part only
    response_data[f'{part}_response'] = response

    # Store the updated results
    calendar_room.response_data = response_data
    calendar_room.errors = errors
    if errors:
        calendar_room.hostify_status = 'update_errors'
    else:
        calendar_room.hostify_status = 'listed'
    calendar_room.save()

# Dynamically call each process function
def process_part(hostify_id, part, params):
    response, errors = '', ''

    try:
        params['listing_id'] = hostify_id  
        response = getattr(LISTING_API_CLIENT, f'process_{part}')(params)

        if not response['success']:
            errors += f'{part} not submitted'
    except:
        response = 'Error'
        errors += f'{part} not submitted'

    return response, errors


def update_hostify_custom_fields(calendar_room: object) -> Tuple[bool, str]: 
    if not calendar_room.hostify_id:
        return False, 'No Hostify ID'

    # Mark all LEAP listings with 800 - for financial purposes
    UNIQUE_NUMBER = 800
    
    calendar_room_type = calendar_room.calendar_room_type
    calendar_hotel = calendar_room_type.calendar_hotel
    hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
    django_hotel_url = f'https://www.api.hotels.staylivegrow.com/admin/calendar_app/calendarroom/{calendar_room.id}/change/'

    # Custom fields data
    HOSTIFY_CUSTOM_FIELDS_DATA = {
        'SOURCE_HOTEL_ID': hotelbeds_api_hotel.code,
        'BCOM_HOTEL_ID': calendar_hotel.booking_com_id,
        'DJANGO_URL': django_hotel_url,
        'UNIQUE_NUMBER': UNIQUE_NUMBER
    }

    # Run update requests
    for key, value in HOSTIFY_CUSTOM_FIELDS_DATA.items():
        field_value = {
            "custom_field_id": HOSTIFY_CUSTOM_FIELDS_IDS[key],
            "value": value,
            "listing_ids": [calendar_room.hostify_id]
        }
        try:
            response = LISTING_API_CLIENT.set_custom_fields_values(field_value)
            if not response['success']:
                return False, response['error']
        except Exception as e:
            return False, str(e)
    
    calendar_room.updated_custom_fields = True
    calendar_room.save()
    return True, ''



# Docs:
# https://app.hostify.com/docs#listing-clone
def push_listing_to_airbnb_queue(calendar_room_id):
    response, error = '', ''

    calendar_room = CalendarRoom.objects.get(id=calendar_room_id)
    # Exception when someone tries to clone listing that is been in the queue(doesn't have child id YET)
    if not calendar_room.hostify_child_id:
        hostify_id = calendar_room.hostify_id
        listing_nickname = calendar_room.title

        try:
            # 1 extra query to remove the hotel name from the listing nickname
            calendar_hotel = calendar_room.calendar_room_type.calendar_hotel
            hotel_name = calendar_hotel.name
            
            # ugly but it should work
            listing_nickname = listing_nickname.replace(f'{hotel_name}', '')
        except:
            pass

        push_data = {
            'listing_id': hostify_id,
            'username': 'rm@staykeepers.com',
            'integration': calendar_room.airbnb_account,
            'nickname': listing_nickname,
            'sync_pricing': 1,
            'channel_currency': 'GBP',
            'price_markup': int(calendar_room.markup),
            'security_deposit': 0,
            'cleaning_fee': 0,
            'instant_booking': 'everyone', # everyone | experienced | government_id | experienced_guest_with_government_id
            'cancellation_policy': 'strict' # strict | moderate | flexible
        }

        try:
            response = LISTING_API_CLIENT.clone_listing(push_data)
            if not response['success']:
                error += 'Cloning error\n'
        except:
            response = 'Error'
            error += 'Cloning error\n'

        # Set calendar room status
        if error: calendar_room.cloning_status == 'cloning_with_errors'
        else: calendar_room.cloning_status = 'cloning'

        # Store the cloning results
        calendar_room.clone_response_data = response
        calendar_room.errors = calendar_room.errors + error
        calendar_room.save()

def get_csv_response(queryset):   
    
    field_names = [
        'calendar_room_type',
        'title',
        'hostify_id',
        'hostify_internal_code',
        'airbnb_account',
        'markup',
        'auto_pricing',
        'hostify_status',
        'created_at',
    ]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=rooms_at_hostify.csv'
    writer = csv.writer(response, delimiter=';')

    writer.writerow(field_names)
    for obj in queryset:
        writer.writerow([str(getattr(obj, field)).replace('\n', ' ').replace('\r', '') for field in field_names])

    return response

# Docs:
# https://app.hostify.com/docs#listing-channel-list
def toggle_listing_on_airbnb(calendar_room_id, state):
    calendar_room = CalendarRoom.objects.get(id=calendar_room_id)
    hostify_child_id = calendar_room.hostify_child_id

    push_data = {
        'listing_id': hostify_child_id,
        'channel_listed': int(state),
    }

    errors = ''
    try:
        response = LISTING_API_CLIENT.toggle_listing_on_airbnb(push_data)

        # Store the active state
        if response['success']:

            # If activated ... also update the listed_at in Django & the AIRBNB_DATE_LISTED custom field in Hostify
            try:
                if state:
                    calendar_room.listed_at = timezone.now()

                    field_value = {
                        "custom_field_id": HOSTIFY_CUSTOM_FIELDS_IDS['AIRBNB_DATE_LISTED'],
                        "value": calendar_room.listed_at.strftime("%Y-%m-%d"),
                        "listing_ids": [calendar_room.hostify_id]
                    }
                    LISTING_API_CLIENT.set_custom_fields_values(field_value)
            except Exception as e:
                errors += f'# EXCEPTION: Toggle room on AirBnb: {str(e)}\n'
                errors += f'Room Title: {calendar_room.title} | Hostify ID: {calendar_room.hostify_id}'
            if state:
                calendar_room.airbnb_status = 'listed'
                calendar_room.rejected_at_airbnb = False
                calendar_room.auto_pricing=True

                ## Mark All days for updating from the same room_type
                CalendarDay.objects.filter(
                    calendar_hotel_room=calendar_room.calendar_room_type
                ).filter(
                    date__gte=datetime.date.today()
                ).update(updated_at_hostify=False)
            else:
                calendar_room.airbnb_status = 'unlisted'
                calendar_room.auto_pricing=False
                
            calendar_room.save()
        else:
            # Marked as not listable due to some restrictions from AirBnB
            calendar_room.rejected_at_airbnb = True
            if calendar_room.errors:
                calendar_room.errors = f'{calendar_room.errors}\n{str(response)}'
            else:
                calendar_room.errors = str(response)
            calendar_room.save()
            
    except Exception as e:
        errors += f'# EXCEPTION: Toggle room on AirBnb: {str(e)}\n'
        errors += f'Room Title: {calendar_room.title} | Hostify ID: {calendar_room.hostify_id}'

    return errors