ROOM_TYPES= {
    'BED': 'bedroom',
    'LIV': 'living room',
} 

# "62 are alternatives"
ROOM_STAY_FACILITIES = {
    '155-61' : 'double_bed', #'king_bed'
    '284-61' : 'sofa_bed',
    '294-61' : 'double_bed', #'queen_bed'
    '150-61' : 'double_bed',
    # '1-62' : 'single_bed',
    '1-61' : 'single_bed',
    # '155-62' : 'king_bed',
    # '992-61' : '', #Rollaways on demand
    # '150-62' : 'double_bed',
    '993-61' : 'bunk_bed',
    # '992-62' : '', #Rollaways on demand
    # '294-62' : 'queen_bed',
    # '284-62' : 'sofa_bed',
    # '993-62' : 'bunk_bed',
}

ROOM_OCCUPANCY_CAP = {
    "APT" :	4,
    "BED" :	1,
    "BOA" :	1,
    "BUN" :	2,
    "CAB" :	2,
    "CHA" :	2,
    "CTG" :	2,
    "CUE" :	2,
    "DBL" :	2,
    "DBT" :	2,
    "DUS" :	1,
    "FAM" :	4,
    "HOM" :	4,
    "JSU" :	2,
    "LOD" :	4,
    "MH" : 2,
    "QUA" :	4,
    "ROO" :	2,
    "SGL" :	1,
    "STU" :	2,
    "SUI" :	2,
    "TLN" :	2,
    "TPL" :	3,
    "TWH" :	4,
    "TWN" :	2,
    "VIL" :	6,
}