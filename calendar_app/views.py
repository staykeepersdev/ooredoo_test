import operator

from django.db.models import Q
from django.http import JsonResponse
from django.views import View
from functools import reduce

from tags.models import Tag

from .admin import CalendarRoomAdmin
from .tasks import (
    update_title_prefix_task,
    update_title_suffix_task,
    update_price_markup_task
)
from .models import CalendarRoom


class DefaultUpdateView(View):

    def get_queryset(self, request):
        all_selected = int(request.POST.get('all_selected', 0))
        room_ids = request.POST.get('room_ids')
        query_params = request.POST.get('query_params')
        
        rooms_queryset = CalendarRoom.objects.all()

        if all_selected:
            if query_params:                
                query_params_dict = {}
                query_params = query_params.split('&')
                for query_param in query_params:
                    query_param = query_param.split('=')
                    
                    # Search query -> filter by ModelAdmin search_fields
                    if query_param[0] == 'q':
                        search_term = query_param[1]
                        orm_lookups = ["%s__icontains" % search_field 
                                        for search_field in CalendarRoomAdmin.search_fields]
                        for bit in search_term.split():
                            or_queries = [Q(**{orm_lookup: bit})
                                        for orm_lookup in orm_lookups]
                            rooms_queryset = rooms_queryset.filter(reduce(operator.or_, or_queries))
                            
                    # Normal Filter params
                    else:
                        query_params_dict[query_param[0]] = query_param[1]

                rooms_queryset = rooms_queryset.filter(**query_params_dict)

        else:
            room_ids = filter(None, room_ids.split(';'))
            rooms_queryset = rooms_queryset.filter(id__in=room_ids)
        
        return rooms_queryset.distinct()

class AjaxUpdateAirBnBAccounts(DefaultUpdateView):
        
    def post(self, request, *args, **kwargs):
        rooms_queryset = self.get_queryset(request)

        airbnb_account = request.POST.get('airbnb_account')        
        updated = rooms_queryset.update(airbnb_account=airbnb_account)
        return JsonResponse({'updated': updated })

class AjaxUpdateRoomsTags(DefaultUpdateView):
    def post(self, request, *args, **kwargs):
        rooms_queryset = self.get_queryset(request)
        selected_tag = request.POST.get('selected_tag')
        if selected_tag:
            tag = Tag.objects.get(id=selected_tag)
        else:
            new_tag_name = request.POST.get('new_tag_name')
            new_tag_description = request.POST.get('new_tag_description')
            new_tag_value = request.POST.get('new_tag_value')
            new_tag_type = request.POST.get('new_tag_type')
            tag = Tag.objects.create(
                name = new_tag_name,
                tag_type = new_tag_type,
                value = new_tag_value,
                description =new_tag_description
            )
        if tag.tag_type == 'markup':
            rooms_queryset = rooms_queryset.filter(airbnb_status='listed', hostify_child_id__isnull=False)
            self.update_rooms_markup(rooms_queryset, tag)
        elif tag.tag_type == 'title_prefix':
            self.add_rooms_title_prefix(rooms_queryset, tag)
        elif tag.tag_type == 'title_suffix':
            self.add_rooms_title_suffix(rooms_queryset, tag)
        

        # Adding the tag to the selected queryset
        for room in rooms_queryset:
            room.tags.add(tag)

        return JsonResponse({'updated': rooms_queryset.count() })
    
    def add_rooms_title_prefix(self, rooms_queryset, tag):
        prefix = tag.value
        for room in rooms_queryset:
            update_title_prefix_task(room.pk, prefix, priority=10)

    def add_rooms_title_suffix(self, rooms_queryset, tag):
        suffix = tag.value
        for room in rooms_queryset:
            update_title_suffix_task(room.pk, suffix, priority=10)

    def update_rooms_markup(self, rooms_queryset, tag):
        markup = int(tag.value)
        for room in rooms_queryset:
            update_price_markup_task(room.pk, markup, priority=10)