from calendar_app.models import CalendarRoom
from calendar_app.services import process_part
from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()

def add_title_affix(room_id, prefix=None, suffix=None):
    room = CalendarRoom.objects.get(pk=room_id)
    # Update request_params
    try:
        new_title = room.request_data['translations_params']['name']
        if prefix:
            new_title = f'{prefix} {new_title}'
        if suffix:
            new_title = f'{new_title} {suffix}'
        room.request_data['translations_params']['name'] = new_title

        room.save()

        if room.hostify_id:
            if room.hostify_child_id and room.hostify_child_id != '0':
                update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                    'listing_id': room.hostify_child_id,
                    'description': {
                        "name": new_title
                    }
                })
            
            # If we update the parent before the AirBnB we don't get the AirBnB listing updated
            response, errors = process_part(
                room.hostify_id,
                'translations',
                room.request_data['translations_params']
            )            

    except:
        pass

def update_price_markup(room_id, markup):
    room = CalendarRoom.objects.get(pk=room_id)
    # Update request_params
    try:

        if room.hostify_id:
            # We only need to update the Child listing. Parent doesn't have markup.
            if room.hostify_child_id and room.hostify_child_id != '0':
                update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                    'listing_id': room.hostify_child_id,
                    'price_markup': int(markup)
                })

    except:
        pass


def update_auto_amenities(room_id, auto_amenities_list):
    auto_amenities_list = auto_amenities_list.split(',')
    room = CalendarRoom.objects.get(pk=room_id)
    # Update request_params
    try:
        new_amenities= room.request_data['amenities_params']['amenities']
        for amenity in auto_amenities_list:
            if amenity in new_amenities:
                new_amenities.remove(amenity)
            else:
                new_amenities.append(amenity)

        room.request_data['amenities_params']['amenities'] = new_amenities

        room.save()

        if room.hostify_id:

            response, errors = process_part(
                room.hostify_id,
                'amenities',
                room.request_data['amenities_params']
            )

            if room.hostify_child_id and room.hostify_child_id != '0':
                update_response = LISTING_API_CLIENT.update_listing_on_airbnb({
                    'listing_id': room.hostify_child_id,
                    'amenities': new_amenities
                })
    except:
        pass
