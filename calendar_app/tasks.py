from background_task import background

from .bulk_services import (
    add_title_affix,
    update_price_markup,
    update_auto_amenities
)
from .services import (
    create_hostify_listing,
    update_hostify_listing,
    push_listing_to_airbnb_queue,
    toggle_listing_on_airbnb
)

@background(schedule=0)
def create_hostify_listing_task(calendar_room_id):
    create_hostify_listing(calendar_room_id)

@background(schedule=0)
def update_hostify_listing_task(calendar_room_id, part):
    update_hostify_listing(calendar_room_id, part)


# AirBnb - Push, List, Unlist
@background(schedule=0)
def push_listing_to_airbnb_queue_task(calendar_room_id):
    push_listing_to_airbnb_queue(calendar_room_id)

@background(schedule=0)
def list_listing_on_airbnb_task(calendar_room_id):
    toggle_listing_on_airbnb(calendar_room_id, True)

@background(schedule=0)
def unlist_listing_on_airbnb_task(calendar_room_id):
    toggle_listing_on_airbnb(calendar_room_id, False)

# Bulk updates
@background(schedule=0)
def update_title_prefix_task(calendar_room_id, prefix):
    add_title_affix(calendar_room_id, prefix=prefix)

@background(schedule=0)
def update_title_suffix_task(calendar_room_id, suffix):
    add_title_affix(calendar_room_id, suffix=suffix)

@background(schedule=0)
def update_title_prefix_and_suffix_task(calendar_room_id, prefix, suffix):
    add_title_affix(calendar_room_id, prefix=prefix, suffix=suffix)

@background(schedule=0)
def update_price_markup_task(calendar_room_id, markup):
    update_price_markup(calendar_room_id, markup)

@background(schedule=0)
def update_auto_amenities_task(calendar_room_id, auto_amenities_list):
    update_auto_amenities(calendar_room_id, auto_amenities_list)