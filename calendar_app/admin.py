import sys

from django import forms
from django_admin_listfilter_dropdown.filters import (
    DropdownFilter,
    RelatedDropdownFilter,
    ChoiceDropdownFilter
)
from django.contrib import admin, messages
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget
from django.utils.html import format_html, mark_safe
from django.urls import path
from django.shortcuts import redirect, render

from .models import (
    Batch, CalendarHotel, CalendarRoomType, CalendarRoom, CalendarDay
)
from .services import (
    get_csv_response,
)
from .admin_views import (
    calendar_room_change_view,
    calendar_room_changelist_view
)
from .tasks import (
    create_hostify_listing_task,
    update_hostify_listing_task,
    push_listing_to_airbnb_queue_task,
    list_listing_on_airbnb_task,
    unlist_listing_on_airbnb_task
)

from .admin_custom_filters import HostifyChildIdFilter, NestedAutoCompleteFilter

@admin.register(Batch)
class BatchAdmin(admin.ModelAdmin):
    search_fields = ('name',)

class CalendarRoomInline(admin.TabularInline):
    model = CalendarRoom
    extra = 0
    exclude = ('calendar_room_type_id', 'request_data', 'response_data')
    show_change_link = True

class CalendarRoomTypeInline(admin.TabularInline):
    model = CalendarRoomType
    extra = 0
    exclude = ('hotelbeds_hotel_id', 'request_data', 'response_data', 'errors')
    show_change_link = True


@admin.register(CalendarHotel)
class CalendarHotelAdmin(admin.ModelAdmin):
    inlines = (CalendarRoomTypeInline,)
    list_display=('name', 'hotelbeds_api_hotel', 'get_hotel_room_types_count', )
    raw_id_fields = ('hotelbeds_api_hotel',)
    search_fields = ('name', 'hotelbeds_api_hotel__code')

    def get_hotel_room_types_count(self,obj):
        return obj.calendar_room_types.count()
    get_hotel_room_types_count.short_description = 'Room Types Count'


@admin.register(CalendarRoomType)
class CalendarRoomType(admin.ModelAdmin):
    inlines = (CalendarRoomInline,)
    list_display = ('calendar_hotel', 'code', 'hotelbeds_hotel_id', 'get_rooms_count')
    search_fields = ('code', 'calendar_hotel__name')
    raw_id_fields = ('calendar_hotel',)

    def get_rooms_count(self,obj):
        return obj.calendar_rooms.count()
    get_rooms_count.short_description = 'Room Count'
    

class RoomCountryFilter(NestedAutoCompleteFilter):
    title = 'Country' # display title
    field_name = 'calendar_room_type__calendar_hotel__hotelbeds_api_hotel__hotel__country'

class RoomDestinationNameFilter(NestedAutoCompleteFilter):
    title = 'Destination Name' # display title
    field_name = 'calendar_room_type__calendar_hotel__hotelbeds_api_hotel__hotel__destination'

class CsvImportForm(forms.Form):
    csv_file = forms.FileField()

@admin.register(CalendarRoom)
class CalendarRoomAdmin(admin.ModelAdmin):
    change_list_template = "admin/custom_calendar_rooms_list.html"

    # Custom views
    render_change_form = calendar_room_change_view
    changelist_view = calendar_room_changelist_view

    list_display = (
        'id',
        'title',
        'airbnb_account',
        'hb_hotel_info',
        # 'bcom_info',

        # Hostify & AirBnb Ids
        'hostify_id', 'hostify_internal_code',
        'hostify_child_id',
        'airbnb_status',
        'rejected_at_airbnb',

        'auto_pricing',
        'markup',
        'hostify_status',
        'cloning_status',
        'hostify_link',

        'batch',
        'get_tags',
        'get_manual_updates',
        
        'status_updated_at',
        'cloning_updated_at',
        'created_at'
    )
    list_filter = (
        RoomCountryFilter,
        RoomDestinationNameFilter,
        ('airbnb_account', ChoiceDropdownFilter),
        ('batch', RelatedDropdownFilter),
        ('hostify_status', ChoiceDropdownFilter),
        ('cloning_status', ChoiceDropdownFilter),
        ('tags', RelatedDropdownFilter),
        ('manual_updates', RelatedDropdownFilter),
        ('airbnb_status', ChoiceDropdownFilter),
        HostifyChildIdFilter,
        'auto_pricing',
        'rejected_at_airbnb'

    )
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget}
    }
    raw_id_fields = ('calendar_room_type',)
    search_fields = (
        'title',
        'hostify_id',
        'hostify_internal_code',
        'hostify_child_id',
        'request_data__translations_params__name'
    )
    readonly_fields=('markup','weekly_discount','monthly_discount')
    list_editable = ('batch',)
    autocomplete_fields =('tags','manual_updates')
    date_hierarchy = 'created_at'
    actions = [
        'push_at_hostify',
        'export_as_csv',
        'blank_action1',

        # Update actions
        'update_layout_at_hostify',
        'update_amenities_at_hostify',
        'update_translations_at_hostify',
        'update_booking_restrictions_at_hostify',
        'update_photos_at_hostify',
        'blank_action2',

        # AirBnb actions
        'clone_from_hostify_to_airbnb',
        'blank_action3',

        'list_on_airbnb',
        'unlist_on_airbnb'
    ]

    # Hacky way to bypass security for nested autocomplete
    def lookup_allowed(self, lookup, value):
        return True
    
    # fix for autcomplete filters
    class Media:
        css = {'all': ('/static/admin/css/admin-extra.css',)}

    # Manage all actions ... disable bulk delete
    def get_actions(self, request):
        actions = super().get_actions(request)
        if not request.user.is_superuser:
            return []
        if 'delete_selected' in actions:
            del actions['delete_selected']
        return actions

    def hb_hotel_info(self, obj):
        # Add Hotel ID & HB link
        try:
            calendar_hotel = obj.calendar_room_type.calendar_hotel
            hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel

            hb_hotel_link = f'https://www.hotelbeds.com/accommodation/factsheet?id={hotelbeds_api_hotel.code}'
            hb_hotel_info = f'<b><a href="{hb_hotel_link}" target="_blank">HB Hotel ID: {hotelbeds_api_hotel.code}</a></b><br>'
        except:
            hb_hotel_info = ''

        # Add Hotel Country & City
        try:
            location = obj.request_data['location_params']
            country, city = location['country'], location['city']
            hb_hotel_info += f'<br>{country}, {city}'
        except:
            hb_hotel_info += ''

        return format_html(hb_hotel_info)


    def get_tags(self, obj):
        return ", ".join([str(tag) for tag in obj.tags.all()])
    get_tags.short_description = 'Tags'
    
    def get_manual_updates(self, obj):
        return ", ".join([str(manual_update) for manual_update in obj.manual_updates.all()])
    get_manual_updates.short_description = 'Manual Updates'

    # def bcom_info(self,obj):
    #     bcom_info = ''

    #     try:
    #         calendar_room_type = obj.calendar_room_type
    #         calendar_hotel = obj.calendar_room_type.calendar_hotel

    #         bcom_room_type_id = calendar_room_type.booking_com_id
    #         bcom_room_type = calendar_room_type.booking_com_room_type
            
    #         bcom_hotel_id = calendar_hotel.booking_com_id
    #         try:
    #             b_com_hotel_id = f'<b>BHotel ID: <a href="{calendar_hotel.hotelbeds_api_hotel.hotel.b_com_hotel.page_url}" target="_blank">{bcom_hotel_id}</a></b><br><br>'
    #         except:
    #             b_com_hotel_id =  f'<b>BHotel ID: {bcom_hotel_id}</b><br><br>'

    #         bcom_info = (
    #             f'{b_com_hotel_id}'
    #             f'<small class="nowrap" title="B.com Room Type">{bcom_room_type}</small><br>'
    #             f'<small class="nowrap" title="B.com Room ID">{bcom_room_type_id}</small>'
    #         )
    #     except:
    #         bcom_info = ''

    #     return format_html(bcom_info)
    
    def hostify_link(self,obj):
        if obj.hostify_id:
            return mark_safe(f'<a href="https://staykeepers.co/listings/view/{obj.hostify_id}" target="_blank">Room @ Hostify</a>')
        else:
            return '-'
    hostify_link.short_description = 'Hostify Link'

    # Custom action functions
    def export_as_csv(self, request, queryset):
        return get_csv_response(queryset)

    def push_at_hostify(self, request, queryset):
        results = {}

        for calendar_room in queryset:
            if calendar_room.hostify_status in ['ready','info_errors']:
                create_hostify_listing_task(calendar_room.id)
                calendar_room.hostify_status = 'waiting'
                calendar_room.save()
                room_status = 'Send in queue for creation'
            else:
                room_status = f'NOT LISTED WITH STATUS - {calendar_room.hostify_status}'

            results.setdefault(room_status, 0)
            results[room_status] +=1

        message = ""
        for result in results:
            message += f'{result} : {results[result]}. '

        self.message_user(request, message)

    def blank_action1(self, request, queryset): pass
    def blank_action2(self, request, queryset): pass
    def blank_action3(self, request, queryset): pass

    # Custom update functions
    def update_layout_at_hostify(self, request, queryset):
        self.update_at_hostify(request, queryset, 'layout')

    def update_amenities_at_hostify(self, request, queryset):
        self.update_at_hostify(request, queryset, 'amenities')

    def update_translations_at_hostify(self, request, queryset):
        self.update_at_hostify(request, queryset, 'translations')

    def update_booking_restrictions_at_hostify(self, request, queryset):
        self.update_at_hostify(request, queryset, 'booking_restrictions')

    def update_photos_at_hostify(self, request, queryset):
        self.update_at_hostify(request, queryset, 'photos')


    def update_at_hostify(self, request, queryset, part):
        for calendar_room in queryset:
            if self.allow_hostify_update(calendar_room.hostify_status):
                update_hostify_listing_task(calendar_room.id, part)


    def clone_from_hostify_to_airbnb(self, request, queryset):
        msgs = []

        for calendar_room in queryset:
            # Only listed rooms without child id can be updated
            if self.allow_hostify_to_airbnb_push(calendar_room.hostify_status) and not calendar_room.hostify_child_id:
                msgs.append(str(calendar_room))
                push_listing_to_airbnb_queue_task(calendar_room.id)

        if(msgs):
            self.show_user_messages('AirBnb Push Queue', msgs, request)

    def list_on_airbnb(self, request, queryset):
        msgs, errs = [], []

        for calendar_room in queryset:
            if self.allow_toggle_on_airbnb(calendar_room):
                list_listing_on_airbnb_task(calendar_room.id, priority=10)
                msgs.append(str(calendar_room))

        #         room_toggle_errors = toggle_listing_on_airbnb(calendar_room.id, True)

        #         if not room_toggle_errors:
        #             msgs.append(str(calendar_room))
        #         else:
        #             errs.append(room_toggle_errors)
        
        if(msgs): self.show_user_messages('AirBnb -> listed', msgs, request, messages.SUCCESS)
        # if(errs): self.show_user_messages('AirBnb -> listed', errs, request, messages.ERROR)

    def unlist_on_airbnb(self, request, queryset):
        msgs, errs = [], []

        for calendar_room in queryset:
            if self.allow_toggle_on_airbnb(calendar_room):
                unlist_listing_on_airbnb_task(calendar_room.id, priority=10)
                msgs.append(str(calendar_room))
        #         room_toggle_errors = toggle_listing_on_airbnb(calendar_room.id, False)

        #         if not room_toggle_errors:
        #             msgs.append(str(calendar_room))
        #         else:
        #             errs.append(room_toggle_errors)
        
        if(msgs): self.show_user_messages('AirBnb -> unlisted', msgs, request, messages.SUCCESS)
        # if(errs): self.show_user_messages('AirBnb -> unlisted', errs, request, messages.ERROR)

    
    def show_user_messages(self, staging, msgs, request, level=messages.SUCCESS):
        self.message_user(request, f'The following listings are in {staging}:', level=level)
        
        for msg in msgs:
            self.message_user(request, msg, level=messages.INFO)

    
    # Only listed rooms can be updated
    def allow_hostify_to_airbnb_push(self, room_status):
        return room_status in ['listed', 'manually']

    # Only listed rooms can be updated
    def allow_hostify_update(self, room_status):
        return room_status in [
            'listed',
            'listed_with_errors',
        ]
    
    # Only sucessfully cloned & unlisted rooms ... can be listed (activated)
    def allow_toggle_on_airbnb(self, calendar_room):
        return calendar_room.hostify_child_id



    # Custom actions
    push_at_hostify.short_description = "Push selected rooms @ Hostify"
    export_as_csv.short_description = "Export selected rooms as CSV"
    blank_action1.short_description = "----------"

    # Custom updates
    update_layout_at_hostify.short_description = "Update Layout for selected rooms @ Hostify"
    update_amenities_at_hostify.short_description = "Update Amenities for selected rooms @ Hostify"
    update_translations_at_hostify.short_description = "Update Translations for selected rooms @ Hostify"
    update_booking_restrictions_at_hostify.short_description = "Update Booking Restrictions for selected rooms @ Hostify"
    update_photos_at_hostify.short_description = "Update Photos for selected rooms @ Hostify"
    blank_action2.short_description = "----------"

    # AirBnb updates - Push, List, Unlist
    clone_from_hostify_to_airbnb.short_description = "Clone selected rooms from Hostify to AirBnb"
    blank_action3.short_description = "----------"

    list_on_airbnb.short_description = "List (activate) selected rooms on AirBnb"
    unlist_on_airbnb.short_description = "Unlist (de-activate) selected rooms on AirBnb"


@admin.register(CalendarDay)
class CalendarDayAdmin(admin.ModelAdmin):
    list_display = (
        'calendar_hotel_room',
        'date',
        'price',
        'currency',        
        'is_available',
        'updated_at',
        'updated_at_hostify'
    )
    date_hierarchy = 'date'
    list_filter = ('is_available',)
    search_fields = ('calendar_hotel_room__calendar_hotel__name','calendar_hotel_room__code',)
    raw_id_fields = ('calendar_hotel_room',)
    # list_editable = ('updated_at_hostify',)

