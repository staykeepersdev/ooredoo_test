from admin_auto_filters.filters import AutocompleteFilter, AutocompleteSelect
from django.contrib import admin
from django.db.models.fields.related import ForeignKey, OneToOneField, OneToOneRel


# Based on:
# @link https://stackoverflow.com/a/43200689/11095878
class HostifyChildIdFilter(admin.SimpleListFilter):
    title = 'has hostify child id'
    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'hostify_child_id__isnull'

    def lookups(self, request, model_admin):
        return (
            ('False', 'With child id'),
            ('True', 'Without child id'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'False':
            return queryset.filter(hostify_child_id__isnull=False)
        if self.value() == 'True':
            return queryset.filter(hostify_child_id__isnull=True)

## The search field (last one) should be ForeignKey
class NestedAutoCompleteFilter(AutocompleteFilter):
    ALLOWED_NESTED_CLASSES = (ForeignKey, OneToOneField, OneToOneRel)
    def __init__(self, request, params, model, model_admin):
        if self.parameter_name is None:
            self.parameter_name = '{}__{}__exact'.format(self.field_name,
                                                         self.field_pk)
        admin.SimpleListFilter.__init__(self, request, params, model, model_admin)

        if self.rel_model:
            model = self.rel_model

        look_up_field = self.field_name
        
        if not self.field_name.startswith('__') and '__' in self.field_name:
            path = self.field_name.split('__') 
            for i in range(len(path)):
                name = path[i]
                
                f = model._meta.get_field(name)
                if i  < (len(path) - 1) and isinstance(f, self.ALLOWED_NESTED_CLASSES):
                    model = f.remote_field.model
                look_up_field = name
            remote_field = f.remote_field
        else:
            remote_field = model._meta.get_field(self.field_name).remote_field

        widget = AutocompleteSelect(remote_field,
                                    model_admin.admin_site,
                                    custom_url=self.get_autocomplete_url(request, model_admin),)
        form_field = self.get_form_field()
        field = form_field(
            queryset=self.get_queryset_for_field(model, look_up_field),
            widget=widget,
            required=False,
        )

        self._add_media(model_admin, widget)

        attrs = self.widget_attrs.copy()
        attrs['id'] = 'id-%s-dal-filter' % self.field_name
        if self.is_placeholder_title:
            # Upper case letter P as dirty hack for bypass django2 widget force placeholder value as empty string ("")
            attrs['data-Placeholder'] = self.title
        self.rendered_widget = field.widget.render(
            name=self.parameter_name,
            value=self.used_parameters.get(self.parameter_name, ''),
            attrs=attrs
        )