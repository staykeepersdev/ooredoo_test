from datetime import datetime, timedelta

from django.db.models import Q

from tags.models import Tag
from .models import CalendarRoom

strftime = datetime.strftime

# Change List View for CelendarRoom
def calendar_room_changelist_view(self, request, extra_context=None):
    extra_context = extra_context or {}

    ## Added tags as extra context for bulk updates
    extra_context['markup_tags'] = Tag.objects.filter(tag_type='markup')
    extra_context['title_affix_tags'] = Tag.objects.filter(
         Q(tag_type='title_prefix') | Q(tag_type='title_suffix') 
    ).distinct()

    extra_context['airbnb_accounts'] = [k for k,v in CalendarRoom.AIRBNB_ACCOUNTS]

    return super(self.__class__, self).changelist_view(request, extra_context)

# Change View for CalendarRoom
def calendar_room_change_view(self, request, context, *args, **kwargs):
    self.change_form_template = 'admin/custom_calendar_room_change_form.html'

    # Check In is 30 days from today, Check Out is 35 days from today

    TODAY = datetime.today()
    OFFSET_CHECK_IN = 30
    OFFSET_CHECK_OUT = 35
    
    check_in = TODAY + timedelta(days=OFFSET_CHECK_IN)
    check_out = TODAY + timedelta(days=OFFSET_CHECK_OUT)
    check_in_str, check_out_str = strftime(check_in, '%Y-%m-%d'), strftime(check_out, '%Y-%m-%d')
    
    # Get current object
    calendar_room = context['original']

    try:
        # Hotel Beds: Hotel code & Url
        calendar_hotel = calendar_room.calendar_room_type.calendar_hotel
        hotelbeds_api_hotel = calendar_hotel.hotelbeds_api_hotel
        hb_hotel_code = hotelbeds_api_hotel.code
        hb_hotel_url = f'https://www.hotelbeds.com/accommodation/factsheet?id={hb_hotel_code}'

        # Stay.Live.Grow: Hotel Id & Url
        slg_hotel_id = hotelbeds_api_hotel.hotel.id
        slg_hotel_url = f'https://www.staylivegrow.com/hotels/{slg_hotel_id}?check_in={check_in_str}&check_out={check_out_str}&adults=1'
    except:
        hb_hotel_code, hb_hotel_url = None, None
        slg_hotel_id, slg_hotel_url = None, None

    # Hostify Parent & Child: Ids & Urls
    hostify_id, hostify_child_id = None, None
    hostify_parent_url, hostify_child_url = None, None
    if calendar_room:
        hostify_id = calendar_room.hostify_id
        hostify_child_id = calendar_room.hostify_child_id
        
        if hostify_id: hostify_parent_url = f'https://staykeepers.co/listings/view/{calendar_room.hostify_id}'
        if hostify_child_id: hostify_child_url = f'https://staykeepers.co/listings/view/{calendar_room.hostify_child_id}'

    # Extend the context
    context.update({
        'hb_hotel_code': hb_hotel_code,
        'hb_hotel_url': hb_hotel_url,
        
        'slg_hotel_id': slg_hotel_id,
        'slg_hotel_url': slg_hotel_url,
        
        'hostify_parent_id': hostify_id,
        'hostify_parent_url': hostify_parent_url,
        
        'hostify_child_id': hostify_child_id,
        'hostify_child_url': hostify_child_url
    })

    return super(self.__class__, self).render_change_form(request, context, *args, **kwargs)
