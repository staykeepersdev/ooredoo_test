from django.contrib.postgres.fields import JSONField

from django.db import models
from django.db.models.functions import Now

from hotels.models import HotelRoom
from hotelbeds_data.models import APIHotel
from tags.models import Tag, ManualUpdateTag

class Batch(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class CalendarHotel(models.Model):
    name = models.CharField(max_length=255)
    hotelbeds_api_hotel = models.OneToOneField(
        APIHotel,
        on_delete=models.CASCADE,
        related_name='calendar_hotel'
    )
    
    # @deprecated Front Contacts related
    front_contact_id = models.CharField(max_length=40, blank=True, null=True)
    front_contact_email = models.CharField(max_length=100, blank=True, null=True)

    front_contact_duplicated_with_id = models.CharField(max_length=40, blank=True, null=True)
    front_contact_zero_handles = models.BooleanField(blank=True, null=True)
    
    def __str__(self):
        return self.name



class CalendarRoomType(models.Model):
    calendar_hotel = models.ForeignKey(
        CalendarHotel,
        on_delete=models.CASCADE,
        related_name='calendar_room_types'
    )
    code = models.CharField(max_length=255)
    hotel_room = models.OneToOneField(
        HotelRoom,
        on_delete=models.CASCADE,
        related_name='calendar_room_type',
        blank=True,
        null=True
    )
       
    hotelbeds_hotel_id = models.CharField(max_length=255, blank=True, null=True)

    hotelbeds_contract = models.CharField(max_length=255, blank=True, null=True)
    hotelbeds_contract_is_moderate = models.BooleanField(default=False)

    # Alternative are always moderate AirbnB Policy
    hotelbeds_alternative_contract = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f'{self.calendar_hotel.name} - {self.code}'

    def save(self, *args, **kwargs):        
        if not self.hotelbeds_hotel_id:
            self.hotelbeds_hotel_id = self.calendar_hotel.hotelbeds_api_hotel.code

        super().save(*args, **kwargs) 


class CalendarRoom(models.Model):
    HOSTIFY_STATUSES = (
        ('ready', 'Ready for listing'),
        ('listed', 'Listed'),
        ('listed_with_errors', 'Listed with errors'),
        ('not_listed', 'Not listed'),
        ('manually', 'Manually listed'),
        ('deactivated', 'Deactivated'),
        ('info_errors', 'Information Error'),
        ('waiting', 'Waiting in Queue'),
        
        ('updating', 'Updating in Queue'),
        ('update_errors', 'Update Errors'),
        
        ('processed', 'Being processed'),
        ('failed', 'Failed')
    )
    CLONING_STATUSES = (
        ('not_cloned', 'Not cloned'),
        
        # Cloning is the push to the Hostify's Cloning queue
        ('cloning', 'Cloning'),
        ('cloning_with_errors', 'Cloning with errors'),

        # Cloned is the actual push to AirBnb done by Hostify
        ('cloned', 'Cloned'),
        ('cloned_with_errors', 'Cloned with errors'),
    )
    AIRBNB_STATUSES = (
        ('listed', 'Listed'),
        ('unlisted', 'Unlisted'),
        ('not_set', 'Not set')
    )
    AIRBNB_ACCOUNTS = (
        ('Stay.Live.Grow.', 'Stay.Live.Grow.'),
        ('Urai', 'Urai'),
        ('Stoyan Kyosev', 'Stoyan Kyosev'),
        ('Konnie', 'Konnie'),
        ('Gospodin', 'Gospodin'),
        ('Stay Live Grow (Ivo)', 'Stay Live Grow (Ivo)'),
        ('Hristina - New', 'Hristina - New'),
        ('Shelly Yordanova', 'Shelly Yordanova'),
        ('Svetlana Velkova', 'Svetlana Velkova')
    )

    CANCELLATION_POLICIES = (
        ('strict', 'strict'),
        ('moderate', 'moderate'),
        ('flexible', 'flexible')
    )

    calendar_room_type = models.ForeignKey(
        CalendarRoomType,
        on_delete=models.CASCADE,
        related_name='calendar_rooms'
    )

    title = models.CharField(max_length=1024, blank=True, null=True)
    airbnb_account = models.CharField(
        max_length=50,
        choices=AIRBNB_ACCOUNTS,
        default='Stay.Live.Grow.',
        help_text="Also called Hostify integration account"
    )

    # Hostify related
    hostify_id = models.CharField(max_length=255, blank=True, null=True)
    hostify_internal_code = models.PositiveIntegerField(blank=True, null=True)
    hostify_child_id = models.CharField(
        max_length=40,
        blank=True,
        null=True,
        help_text="AirBnb child at Hostify"
    )
    hostify_vrbo_child_id = models.CharField(
        max_length=40,
        blank=True,
        null=True,
    )
    channel_listing_id = models.CharField(
        max_length=40,
        blank=True,
        null=True,
        help_text="AirBnb ID"
    )
    updated_custom_fields = models.BooleanField(default=False)

    airbnb_status = models.CharField(
        max_length=50,
        choices=AIRBNB_STATUSES,
        default='not_set'
    )

    rejected_at_airbnb = models.BooleanField(default=False)

    markup_at_hostify = models.PositiveSmallIntegerField(
        blank=True,
        null=True,
        help_text=(
            '<b>Active Markup</b> extracted from <b>Hostify</b> via <b>Cron Job</b>.<br>'
            'If working correctly it should be updated <b>4 times per day</b>.'
        )
    )
    markup_temporary = models.PositiveSmallIntegerField(
        blank=True,
        null=True,
        help_text='Temporary overrides the default markup'
    )
    markup = models.PositiveSmallIntegerField(
        default=28,
        blank=False,
        null=False,
        help_text='Initial <b>default</b> markup'
    )
    
    weekly_discount = models.PositiveSmallIntegerField(default=0, blank=False, null=False)
    monthly_discount = models.PositiveSmallIntegerField(default=0, blank=False, null=False)


    alternative_contract = models.BooleanField(default=False)
    cancellation_policy = models.CharField(
        max_length=50,
        choices=CANCELLATION_POLICIES,
        default='strict'
    )

    auto_pricing = models.BooleanField(default=False)

    batch = models.ForeignKey(Batch, blank=True, null=True, on_delete=models.SET_NULL)
    tags = models.ManyToManyField(Tag, blank=True)
    
    request_data = JSONField(null=True, blank=True)
    response_data = JSONField(null=True, blank=True)
    clone_response_data = JSONField(null=True, blank=True)

    notes = models.TextField(blank=True, null=True)
    
    manual_updates = models.ManyToManyField(ManualUpdateTag, blank=True)
    errors = models.TextField(blank=True, null=True)

    hostify_status = models.CharField(
        max_length=50,
        choices=HOSTIFY_STATUSES,
        default='not_listed'
    )
    cloning_status = models.CharField(
        max_length=50,
        choices=CLONING_STATUSES,
        default='not_cloned'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    status_updated_at = models.DateTimeField(blank=True, null=True)
    cloning_updated_at = models.DateTimeField(blank=True, null=True)
    listed_at = models.DateTimeField(blank=True, null=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initial_hostify_status = self.hostify_status
        self._initial_cloning_status = self.cloning_status

    def save(self, *args, **kwargs):
        if self.hostify_status != self._initial_hostify_status:
            self.status_updated_at = Now()

        if self.cloning_status != self._initial_cloning_status:
            self.cloning_updated_at = Now()
        
        return super().save(*args, **kwargs)

    class Meta():
        verbose_name_plural = 'Rooms at Hostify'
        verbose_name = 'Room at Hostify'

    def __str__(self):
        return f'{self.title} - {str(self.hostify_id)}'
    
    @property
    def has_active_initial_promotion(self):
        if hasattr(self, 'initial_airbnb_promotion'):
            return self.initial_airbnb_promotion.is_active
        else:
            return False
    

class ListingGroup(models.Model):
    EXPEDIA=1906
    CHANNELS = (
        (EXPEDIA, 'Expedia'),
    )
    hostify_id = models.CharField(max_length=255, blank=True, null=True)
    calendar_room_type = models.ForeignKey(
        CalendarRoomType,
        on_delete=models.CASCADE,
        related_name='listing_groups'
    )
    channel = models.PositiveIntegerField(
        choices=CHANNELS,
        default=EXPEDIA,
    )

    def __str__(self):
        return f'{self.calendar_room_type}'
    
    def get_auto_pricing_listing(self):
        return self.listings.filter(auto_pricing=True).first()
    
    def get_active_listing(self):
        return self.listings.filter(is_active=True).first()
    
    def get_auto_pricing_hostify_id(self):
        listing = self.get_active_listing()
        if listing:
            return listing.hostify_id
        else:
            return None
    

class Listing(models.Model):
    listing_group = models.ForeignKey(
        ListingGroup,
        on_delete=models.CASCADE,
        related_name='listings',
    )
    title = models.CharField(max_length=1024, blank=True, null=True)
    hostify_internal_code = models.PositiveIntegerField(blank=True, null=True)
    hostify_id = models.CharField(max_length=255, blank=True, null=True)

    auto_pricing = models.BooleanField(default=False)
    batch = models.ForeignKey(Batch, blank=True, null=True, on_delete=models.SET_NULL)
    
    request_data = JSONField(null=True, blank=True)
    response_data = JSONField(null=True, blank=True)

    reserved_to = models.DateField(null=True, blank=True)

    is_active = models.BooleanField(blank=True, default=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.listing_group} - {self.title}'
    


class CalendarDay(models.Model):

    calendar_hotel_room = models.ForeignKey(
        CalendarRoomType,
        on_delete=models.CASCADE,
        related_name='days'
    )

    date = models.DateField()
    price = models.FloatField(blank=True, null=True)
    currency = models.CharField(max_length=3, default='GBP')
    is_available = models.BooleanField(blank=True, default=False)
    is_alternative = models.BooleanField(blank=True, default=False)
    updated_at = models.DateTimeField(blank=True, null=True)
    updated_at_hostify = models.BooleanField(blank=True, default=False)

    class Meta:
        unique_together = ['calendar_hotel_room', 'date', 'is_alternative']


class AirBnbAccount(models.Model):

    name = models.CharField(unique=True, max_length=64)
    allow_auto_pricing = models.BooleanField(
        blank=False,
        null=False,
        default=False,  
        help_text='Allow sending of Auto-Pricing data to Hostify.<br><b>WARNING:</b> DO NOT re-activate more than 1 account per day to avoid Hostify flooding !'
    )

    class Meta:
        verbose_name = 'AirBnb Account'
        verbose_name_plural = 'AirBnb Accounts'


class HotelFrontContact(models.Model):
    
    calendar_hotel = models.OneToOneField(
        CalendarHotel,
        on_delete=models.CASCADE,
        related_name='hotel_front_contact'
    )

    # Front Contacts related
    contact_name = models.CharField(max_length=255)
    contact_id = models.CharField(max_length=40, blank=True, null=True)
    contact_email = models.CharField(max_length=100, blank=True, null=True)
    contact_description = models.TextField(blank=True, null=True)

    contact_master = models.ForeignKey(
        'HotelFrontContact',
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='front_contacts',
    )

    is_notified_initially = models.BooleanField(default=False)
    is_notified_internally_no_email = models.BooleanField(
        default=False,
        help_text='No email exists for this hotel. Object automatically stamped after internal email sent.'
    )
    
    notified_initially_at = models.DateTimeField(blank=True, null=True)
    notified_internally_at = models.DateTimeField(blank=True, null=True)
    
    is_notified_after_first_check_in = models.BooleanField(default=False)
    is_notified_after_first_check_out = models.BooleanField(default=False)

    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, blank=True, null=True)

    @property
    def is_master(self):
        return not self.contact_master

    def __str__(self):
        return str(self.contact_id)
