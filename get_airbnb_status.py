import django
import os

import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from background_task.models import Task

from calendar_app.models import CalendarRoom
from calendar_app.tasks import list_listing_on_airbnb_task

from hostify_api.listing_api_client import ListingAPIClient
LISTING_API_CLIENT = ListingAPIClient()
CLONE_STATUSES = {
    'SUCCESS': 'cloned',
    'ERROR' : 'cloned_with_errors',
}
# First we will LIST the already cloned listings from the previous RUN
# Adding 3mins delay between hostify success cloning and actually listing that listing
# Added multiple tries for CLONED LISTINGS.

# Get Already Cloned listings
cloned_rooms = CalendarRoom.objects.filter(
    hostify_status='listed',
    cloning_status='cloned',
    airbnb_status='not_set',
    rejected_at_airbnb=False,
)
for room in cloned_rooms:

    # Checking for existing task that haven't been runned yet
    existing_task = Task.objects.filter(task_name='calendar_app.tasks.list_listing_on_airbnb_task').filter(task_params=f'[[{room.id}], {{}}]')
    if not existing_task.exists():
        list_listing_on_airbnb_task(room.id, priority=10)

# Get initial list of rooms
cloning_rooms = CalendarRoom.objects.filter(
    cloning_status='cloning',
    clone_response_data__has_any_keys=['job_id']
).all()

for room in cloning_rooms:
    job_id = room.clone_response_data['job_id']
    clone_listing_state = LISTING_API_CLIENT.get_clone_listing_state(job_id)
    
    try:
        clone_listing_data = clone_listing_state['data']
        clone_listing_status = clone_listing_data['status']
        
        # Possible status values: PENDING, PROGRESS, SUCCESS, ERROR
        if clone_listing_status in CLONE_STATUSES:

            room.hostify_child_id = clone_listing_data.get('new_listing_id', '') # The Hostify Child Id
            room.cloning_status = CLONE_STATUSES[clone_listing_status]
            room.clone_response_data = clone_listing_data
            room.save()
                
    except:
        pass

# Get AirBnB ID for  all listings that are listed
airbnb_rooms = CalendarRoom.objects.filter(
    airbnb_status='listed', 
    channel_listing_id__isnull=True,
    hostify_id__isnull=False
)

for room in airbnb_rooms:
    hostify_response = LISTING_API_CLIENT.get_listing(room.hostify_id)
    try:
        channel_id = hostify_response['listing']['channel_listing_id']
        room.channel_listing_id = channel_id
        room.save()
    except:
        pass
