import django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from datetime import datetime
from pytz import timezone

from calendar_app.models import (
    CalendarHotel,
    CalendarRoom,
    CalendarRoomType,
    CalendarDay
)

from hostify_api.calendar_api_client import CalendarAPIClient


now = datetime.now(timezone('UTC'))
london_time =  now.astimezone(timezone('Europe/London'))
price=int(london_time.strftime('%H%M'))
if price == 0:
    price = 2400

CALENDAR_API_CLIENT = CalendarAPIClient()
hostify_ids = [38692,50404,32616,69929,37157,38637]
params = {
    "calendar": [{
        "start_date": "2020-12-01",
        "end_date": "2020-12-02",
        "is_available": True,
        "price": price
    }]
}
for hostify_id in hostify_ids:
    response = CALENDAR_API_CLIENT.put_calendar_days(hostify_id, params)