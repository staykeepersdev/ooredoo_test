from django.contrib import admin

from .models import Tag, ManualUpdateTag

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'tag_type', 'value')
    search_fields = ('name', 'description')
    list_filter = ('tag_type',)

@admin.register(ManualUpdateTag)
class ManualUpdateTag(admin.ModelAdmin):
    search_fields = ('name', 'description')