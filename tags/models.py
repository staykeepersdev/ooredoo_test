from django.db import models

class Tag(models.Model):
    TAG_TYPES = (
        ('markup', 'Mark Up'),
        ('title_prefix', 'Title Prefix'),
        ('title_suffix', 'Title Suffix'),
        ('amenities_switch', 'Amenities Switch'),
    )

    name = models.CharField(max_length=255, unique=True)
    tag_type = models.CharField(max_length=255, choices=TAG_TYPES, default='markup')
    value = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class ManualUpdateTag(models.Model):
    name = models.CharField(max_length=255, unique=True)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name