import django
import os

import sys
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from calendar_app.models import CalendarRoom

from calendar_app.tasks import (
    update_auto_amenities_task
)


united_states_listings = CalendarRoom.objects.filter(
    airbnb_status='listed'
).filter(
    calendar_room_type__calendar_hotel__hotelbeds_api_hotel__hotel__country__pk__exact=197
).filter(tags__tag_type='amenities_switch').order_by('pk')


for room in united_states_listings:
    tags = room.tags.filter(tag_type='amenities_switch')
    for tag in tags:
        update_auto_amenities_task(
            room.pk,
            tag.value,
            priority=9
        )