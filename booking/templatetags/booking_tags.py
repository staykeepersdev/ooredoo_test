from django import template

register = template.Library()

@register.filter(name='booking_status')
def booking_status(value):
    return value.replace('HB_', '').replace('_', ' ').capitalize()

@register.simple_tag()
def var_dump(var):
    print(var)