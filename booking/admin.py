import json
from django.conf import settings
from django.contrib import admin
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget
from django.utils.html import format_html

from .models import Reservation

@admin.register(Reservation)
class ReservationAdmin(admin.ModelAdmin):
    model = Reservation
    class Media:
        css = {'all': ('/static/admin/css/admin-extra.css',)}

    list_display = (
        'is_staff',
        'booking_reference',
        'referral_channel',
        'hotel_name_location',        
        'room_name_board_name',
        'full_name',
        'created_at',
        'check_in',
        'check_out',
        'nights',
        'adults',
        'children_ages',
        'res_status',
        'total_amount',
        'amounts_breakdown',
        'updated_at',
        'uuid'
    )
    search_fields = (
        'referral_channel__code', 'hotel__name',
        'holder_first_name', 'holder_last_name', 'holder_email',
        'hotelbeds_hotel_id', 'hotelbeds_booking_reference',
        'uuid'
    )
    raw_id_fields = ('hotel',)

    list_filter = ('is_staff', 'status', 'referral_channel')
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget}
    }

    # fields
    def hotel_name_location(self, obj):
        country_code = destination_code = ''
        if obj.hotel:
            country_code, destination_code = obj.hotel.country_code, obj.hotel.destination_code

        return format_html(f"<b>{obj.hotel}</b> {country_code} - {destination_code}")

    def booking_reference(self, obj):
        booking_reference = (
            f'<a href="/admin/booking/reservation/{obj.id}/change/" class="booking_reference">'
            f'{obj.hotelbeds_booking_reference or "- - - - - - - - - - -"}'
            f'</a><br>'
        )

        # If reservation is Source-Confirmed
        links = ''
        if obj.status == 'HB_confirmed':
            booking_voucher_url = settings.BOOKING_VOUCHER_URL.replace('UUID', str(obj.uuid))
            booking_cancel_url = settings.BOOKING_CANCEL_URL.replace('UUID', str(obj.uuid))
            links = (
                f'<br><a href="{booking_voucher_url}">Voucher</a><br>'
                f'<a href="{booking_cancel_url}">Cancel Booking</a>'
            )

        return format_html(booking_reference + links)

    def full_name(self, obj):
      return f"{obj.holder_first_name} {obj.holder_last_name}"

    def room_name_board_name(self, obj):
        room_name = obj.room_name or ''
        board_name = obj.board_name or ''
        
        return format_html(f"<b>{room_name}</b> {board_name}")

    def res_status(self, obj):
        return format_html(f'<span class="{obj.status}">{obj.get_status_display()}</span>')

    def amounts_breakdown(self, obj):
        if not obj.total_amount: return '-'

        amounts = ''
        for prop, amount in obj.total_breakdown().items():
            name = self.line_item(prop)
            amounts += f'<b>{name}:</b> {amount}<br>'

        return format_html(amounts)

    # utils
    def has_add_permission(self, request, obj=None):
        return False

    def line_item(self, raw_name):
        return raw_name.capitalize().replace('_', ' ')