from django.shortcuts import get_object_or_404
from rest_framework import status

from common.services import send_email_with_template, assemble_room_paxes
from hotels.models import Hotel
from referrals.models import Channel
from .models import Reservation
from .serializers import ReservationSerializer

from .payments import create_square_payment
from hotelbeds_api.clients.booking_api_client import BookingAPIClient

from .services_emails import (
    send_internal_reservation_mail,
    send_customer_reservation_mail,
    send_error_reservation_email
)

BOOKING_API_CLIENT = BookingAPIClient()

def valid_book_params(data):
    fields = ['name', 'surname', 'email', 'phone', 'hotel_id', 'ratekey', 'nonce', 'amount']
    for f in fields:
        if data.get(f) == '' or data.get(f) == None:
            return False

    # @todo add additional validation for:
    # ratekey, nonce, amount ?

    return True    

def book_room(request, data):
    # reservation holder
    first_name = data.get('first_name')
    last_name = data.get('last_name')
    email = data.get('email')
    phone = data.get('phone')

    # get hotel object and api code
    hotel_id = data.get('hotel_id')
    hotel = Hotel.objects.get(pk=hotel_id)
    hotelbeds_hotel_id = hotel.api_hotel.code

    # get voucher data
    room_name = data.get("room_name")

    check_in = data.get("check_in")
    check_out = data.get("check_out")
    adults = data.get("adults")
    children = data.get("children", None)

    ratekey = data.get('ratekey')
    amount = float(data.get('amount'))

    # @todo card holder - square should take care of these ...
    # card_holder = data.get('card_holder')
    # card_number = data.get('card_number')
    # expiration_date = data.get('expiration_date')
    # cvc = data.get('cvc')

    # Get Referral Channel data ... if present
    refcode = data.get('refcode', None)
    referral_channel = Channel.objects.filter(code=refcode).first()

    # Process ...

    room_paxes = assemble_room_paxes(1, first_name, last_name, children)

    # 1. Create new reservation
    reservation = Reservation(
        referral_channel=referral_channel,
        holder_first_name=first_name,
        holder_last_name=last_name,
        holder_email=email,
        holder_phone=phone,
        request_data=data,
        hotel=hotel,
        room_name=room_name,
        check_in=check_in,
        check_out=check_out,
        adults=adults,
        paxes=room_paxes,
        hotelbeds_hotel_id=hotelbeds_hotel_id,
        hotelbeds_ratekey=ratekey,
        total_amount=amount
    )
    reservation.save()

    # 2. ONLY STAFF USERS (admins) can book for free
    reservation.user = request.user
    reservation.is_staff = request.user.is_staff
    payment_required = True if reservation.is_customer else False

    # 3. Payment via square, requires reservation data
    if payment_required:
        nonce = data.get('nonce')
        payment_success, payment_response = create_square_payment(nonce, amount, reservation)
        reservation.square_transaction_response = payment_response # Audit log

        # 3. Update reservation status
        if(payment_success):
            reservation.square_transaction_id = payment_response.get('payment').get('id')
            reservation.status = 'payment_successful'
            reservation.save()
        else:
            reservation.status = 'payment_unsuccessful'
            reservation.save()

            send_error_reservation_email(reservation)
            return payment_response, status.HTTP_400_BAD_REQUEST

    else:
        reservation.status = 'payment_skipped'
        reservation.save()

    # 4. Reservation - hotel beds
    # 4.1. Hotel beds - reservation remark
    remark = '''Name: {0}
                Surname: {1}
                Email: {2}
                Phone: {3}
                Reservation ID: {4}'''.format(first_name, last_name, email, phone, reservation.id)

    # 4.2. Hotel beds - reservation request object
    reservation_request = {
    	"holder": {
	    	"name": first_name,
		    "surname": last_name
	    },
	    "rooms": [{
		    "rateKey": ratekey,
		    "paxes": room_paxes
        }],
        "clientReference": "STAY LIVE GROW",
        "remark": remark,
        "tolerance" : 5.00
    }
    booking_response = BOOKING_API_CLIENT.booking(reservation_request)

    # Audit log
    reservation.hotelbeds_booking_response = booking_response

    if 'booking' in booking_response:
        reservation.status = 'HB_confirmed'
        reservation.hotelbeds_booking_reference = booking_response['booking']['reference']
        reservation.save() # Fail-safety - initially save only the response

        # All booked rooms response
        rooms = booking_response['booking']['hotel']['rooms']
        if len(rooms): # Needed for single room type properties, for ex: HOSTEL
            first_room = rooms[0]
            
            # 1st room - rates response
            first_room_rates = first_room['rates']
            first_room_first_rate = first_room['rates'][0]

            # Board name & rate comments
            reservation.board_name = first_room_first_rate['boardName']
            reservation.rate_comments = first_room_first_rate['rateComments']

            # Cancellation policies
            room_cancellation_policies = first_room_first_rate['cancellationPolicies']
            if len(room_cancellation_policies):
                # 1st cancellation policy: From date ... amount owed
                reservation.cancellation_policies_from = room_cancellation_policies[0]['from']
                reservation.cancellation_policies_amount = room_cancellation_policies[0]['amount']

        reservation.supplier_name = booking_response['booking']['hotel']['supplier']['name']
        reservation.supplier_vat = booking_response['booking']['hotel']['supplier']['vatNumber']

        reservation.save()

        # Send mails
        if reservation.is_customer:
            send_customer_reservation_mail(reservation, hotel)
        
        send_internal_reservation_mail(reservation, hotel)

    elif 'error' in booking_response:
        reservation.status = 'HB_error'
        reservation.save()
        
        send_error_reservation_email(reservation)
        return booking_response, status.HTTP_400_BAD_REQUEST
     
    # print('booking_response: ', booking_response)

    return ReservationSerializer(reservation, context={"request": request}).data, status.HTTP_200_OK


def cancel_booking(request, uuid):
    reservation = get_object_or_404(Reservation, uuid=uuid)

    cancellation_response = BOOKING_API_CLIENT.cancel_booking(
        booking_reference=reservation.hotelbeds_booking_reference
    )
    if 'booking' in cancellation_response:
        if cancellation_response['booking']['status'] == "CANCELLED":
            reservation.hotelbeds_booking_response = cancellation_response
            reservation.status = "cancelled"
            reservation.save()
            return ReservationSerializer(reservation, context={"request": request}).data, status.HTTP_200_OK
    
    return cancellation_response, status.HTTP_400_BAD_REQUEST