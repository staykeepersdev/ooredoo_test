import pdfkit
import platform

from django.db.models import F
from django.template import loader, Context, Template

from hotels.models import Hotel, PhoneNumber, HotelImage


# Phase 1: Template rendering
def render_template(file, context):
    template = loader.get_template(file)
    return template.render(context)


# Phase 2: PDFKit rendering
def render_pdf(data, template_name='vouchers/voucher.html'):
 
    options = {
        'page-size': 'Letter',
        'encoding': "UTF-8",
        'quiet': ''
    }
    string_in = render_template(template_name, data)
    pdf_content = pdfkit.from_string(string_in, False, options=options)
    return pdf_content


def create_voucher(reservation, request):
    media_url = request.build_absolute_uri('/media/')
    static_url = request.build_absolute_uri('/static/')

    # Get hotel booking phone
    hotel_booking_phone = PhoneNumber.objects.filter(
        hotel_id=reservation.hotel_id,
        phone_type='PHONEBOOKING'
    ).first()
    if hotel_booking_phone: hotel_booking_phone = hotel_booking_phone.phone_number

    # Get hotel featured image
    hotel_featured_image = HotelImage.objects.filter(
        hotel_id=reservation.hotel_id,
    ).exclude(image='').first()
    
    if hotel_featured_image: 
        hotel_featured_image = hotel_featured_image.image.name

    # Convert children str to list
    children = reservation.request_data['children'].split(',')

    # Render Voucher PDF based on Reservation data
    context = {
        'reservation': reservation,
        'hotel_featured_image': hotel_featured_image,
        'hotel_booking_phone': hotel_booking_phone,
        'children': children,

        # metadata
        'meta': {
            'static_url': static_url,
            'media_url': media_url,
            'categories_1to5': list(range(1, 6)) # Inclusive 1..5
        }
    }
    
    voucher_content = render_pdf(context)
    voucher_name = f'Booking  {reservation.hotelbeds_booking_reference}.pdf'
    return voucher_content, voucher_name