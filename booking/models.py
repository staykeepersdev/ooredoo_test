from datetime import datetime
import uuid

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from django.db import models

from hotels.models import Hotel
from referrals.models import Referral, Channel

# Shortcuts
TOTAL_COMMISSION_PERCENTAGE = settings.TOTAL_COMMISSION_PERCENTAGE
PAYMENT_FEE_PERCENTAGE = settings.PAYMENT_FEE_PERCENTAGE


class Reservation(models.Model):
    RESERVATION_STATUSES = (
        ('new', 'New'),
        ('payment_successful', 'Payment successful'),
        ('payment_unsuccessful', 'Payment unsuccessful'),
        ('payment_skipped', 'Payment skipped'),
        ('HB_confirmed', 'Hotel Beds Confirmed'),
        ('HB_error', 'Hotel Beds Error'),
        ('cancelled', 'Cancelled'),
    )
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)

    total_amount = models.FloatField(default=0)
    status = models.CharField(
        choices=RESERVATION_STATUSES,
        max_length=32,
        default='new'
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # personal
    holder_first_name = models.CharField(max_length=100)
    holder_last_name = models.CharField(max_length=30)
    holder_email = models.CharField(max_length=100)
    holder_phone = models.CharField(max_length=100)

    # internal
    reservation_notes = models.TextField(blank=True)
    is_staff = models.BooleanField(default=False)
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.SET_NULL,
        related_name='reservations',
        null=True,
        blank=True
    )

    # request data
    request_data = JSONField(blank=True, null=True)
    response_data = JSONField(blank=True, null=True)

    # square related
    square_transaction_id = models.CharField(max_length=256, blank=True, null=True, unique=True)
    square_transaction_response = models.TextField(blank=True)
    
    # Hotel info
    hotel = models.ForeignKey(Hotel, blank=True, null=True, on_delete=models.SET_NULL, related_name='reservations')
    room_name = models.CharField(max_length=100, blank=True, null=True)
    board_name = models.CharField(max_length=100, blank=True, null=True)
    check_in = models.DateField()
    check_out = models.DateField()
    adults = models.PositiveIntegerField(blank=True, null=True)
    paxes = JSONField(blank=True, null=True)
    rate_comments = models.TextField(blank=True)

    # Cancellation policies ... from date ... amount surcharge
    cancellation_policies_from = models.CharField(max_length=50, blank=True, null=True)
    cancellation_policies_amount = models.FloatField(default=0, blank=True, null=True)

    # Referral related
    referral_channel = models.ForeignKey(Channel, blank=True, null=True, on_delete=models.PROTECT, related_name='referral_channels')
    
    # hotelbeds related
    hotelbeds_hotel_id = models.BigIntegerField(blank=True, null=True)
    hotelbeds_ratekey = models.CharField(max_length=1000)
    hotelbeds_booking_reference = models.CharField(max_length=100, blank=True, null=True, unique=True)
    hotelbeds_booking_response = JSONField(blank=True, null=True)

    # Voucher related

    supplier_name = models.CharField(max_length=1000, blank=True, null=True)
    supplier_vat = models.CharField(max_length=1000, blank=True, null=True)

    @property
    def nights(self):
        if not (self.check_in and self.check_out):
            return 0

        check_in, check_out = self.check_in, self.check_out

        # Typecast to date objects
        if isinstance(check_in, str): check_in = datetime.strptime(check_in, "%Y-%m-%d").date()
        if isinstance(check_out, str): check_out = datetime.strptime(check_out, "%Y-%m-%d").date()

        return (check_out - check_in).days

    @property
    def children_ages(self):
      if(self.paxes == None): return []

      paxes_ages = [pax['age'] for pax in self.paxes if pax['type'] == 'CH']
      return paxes_ages

    @property
    def is_customer(self):
        return not self.is_staff

    def total_breakdown(self):
        # Validation
        total_amount = self.total_amount
        if not total_amount: return {}
        
        # Is Referral Reservation:
        if self.referral_channel:
            return self.referral_reservation_breakdown(total_amount)

        # Is Direct Reservation:
        if not self.referral_channel:

            if self.is_customer:
                return self.direct_reservation_breakdown(total_amount)

            if self.is_staff:
                return self.staff_reservation_breakdown(total_amount)

    def referral_reservation_breakdown(self, total_amount):
        partner_commission_percentage = self.referral_channel.referral.commission_percentage
        win_win_commission_percentage = TOTAL_COMMISSION_PERCENTAGE - PAYMENT_FEE_PERCENTAGE - partner_commission_percentage

        # amounts
        partner_amount = (total_amount * partner_commission_percentage / 100)
        square_amount = (total_amount * PAYMENT_FEE_PERCENTAGE / 100)
        win_win_amount = (total_amount * win_win_commission_percentage / 100)
        hotel_beds_amount = total_amount - square_amount - win_win_amount - partner_amount

        return {
            "partner_amount": round(partner_amount, 2),
            "square_amount": round(square_amount, 2),
            "win_win_amount": round(win_win_amount, 2),
            "hotel_beds_amount": round(hotel_beds_amount, 2)
        }
    
    def direct_reservation_breakdown(self, total_amount):
        win_win_commission_percentage = TOTAL_COMMISSION_PERCENTAGE - PAYMENT_FEE_PERCENTAGE

        # amounts
        square_amount = (total_amount * PAYMENT_FEE_PERCENTAGE / 100)
        win_win_amount = (total_amount * win_win_commission_percentage / 100)
        hotel_beds_amount = total_amount - square_amount - win_win_amount

        return {
            "square_amount": round(square_amount, 2),
            "win_win_amount": round(win_win_amount, 2),
            "hotel_beds_amount": round(hotel_beds_amount, 2)
        }

    def staff_reservation_breakdown(self, total_amount):
	    return {"total_hotel_beds_amount": round(total_amount, 2)}
