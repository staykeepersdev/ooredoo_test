from django.conf import settings
from django.forms.models import model_to_dict

from common.services import send_email_with_template



def send_customer_reservation_mail(reservation, hotel):
    subject = f'SLG-HB Reservation - {reservation.hotelbeds_booking_reference} - Confirmed'
    context = {
        "reservation": model_to_dict(reservation),
        "reservation_length": reservation.nights,
        "reservation_children_ages": reservation.children_ages,
        "hotel": hotel,
        "booking_voucher_url": settings.BOOKING_VOUCHER_URL.replace('UUID', str(reservation.uuid)),
        "booking_cancel_url": settings.BOOKING_CANCEL_URL.replace('UUID', str(reservation.uuid))
    }

    template_name = 'emails/customer_reservation_email'
    to_emails = [reservation.holder_email]

    send_email_with_template(subject, context, template_name, to_emails)


def send_internal_reservation_mail(reservation, hotel):
    booking_type = 'STAFF BOOKING' if reservation.is_staff else 'Reservation'
    
    subject = f'SLG-HB {booking_type} - {reservation.hotelbeds_booking_reference} - Confirmed'
    context = {
        "reservation": model_to_dict(reservation),
        "reservation_length": reservation.nights,
        "reservation_children_ages": reservation.children_ages,
        "hotel": hotel,
        "booking_voucher_url": settings.BOOKING_VOUCHER_URL.replace('UUID', str(reservation.uuid)),
        "booking_admin_url": settings.BOOKING_ADMIN_URL.replace('RESERVATION_ID', str(reservation.id))
    }

    template_name = 'emails/internal_reservation_email'
    to_emails = [settings.DEFAULT_RECEIVER_EMAIL, settings.SECONDARY_RECEIVER_EMAIL, settings.DEFAULT_TECHNOLOGY_EMAIL]

    send_email_with_template(subject, context, template_name, to_emails)


def send_error_reservation_email(reservation):
    subject = f'!!! SLG-HB - RESERVATION ERROR !!!'
    context = {"reservation": model_to_dict(reservation)}

    template_name = 'emails/error_reservation_email'
    to_emails = [settings.DEFAULT_TECHNOLOGY_EMAIL]

    send_email_with_template(subject, context, template_name, to_emails)