{% load booking_tags %}

New reservation for: {{ hotel.name }}

Hotel address:
{{ hotel.address }},
{{ hotel.city }} {{ hotel.get_country }}, {{ hotel.postal_code }}

Hotel phone: {{ hotel.get_phone_number }}

Room type and board: {{ reservation.room_name }} - {{ reservation.board_name }}
Reservation status: {{ reservation.status|booking_status }}
Booking reference: {{ reservation.hotelbeds_booking_reference }}

Check In: {{ reservation.check_in }}
Check Out: {{ reservation.check_out }}

Nights: {{ reservation_length }}
Guests:
    {{ reservation.adults }} adult{{ reservation.adults|pluralize }}
    
    {% if reservation_children_ages %},
        {{ reservation_children_ages|length }} child{{ reservation_children_ages|length|pluralize:"en" }}
        (Ages: {{ reservation_children_ages|join:", " }})
    {% endif %}

Your name: {{ reservation.holder_first_name }} {{ reservation.holder_last_name }}
Phone: {{ reservation.holder_phone }}
Email: {{ reservation.holder_email }}

Total: £{{ reservation.total_amount|floatformat:2 }}

Review Booking: {{ booking_voucher_url }}

Cancel Booking: {{ booking_cancel_url }}