from django.urls import path
from .views import (
    Booking,
    BookingRetrieve,
    BookingCancelation,
    DownloadVoucherPDF
)

urlpatterns = [
    # Boooking related
    path('', Booking.as_view(), name='book'),
    path('<uuid:uuid>/', BookingRetrieve.as_view(), name='booking-retreive'),
    path('<uuid:uuid>/cancel', BookingCancelation.as_view(), name='booking-cancelation'),
    path('vouchers/<uuid:uuid>', DownloadVoucherPDF.as_view(), name='booking-cancelation'),
]