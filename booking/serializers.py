from rest_framework import serializers

from .models import Reservation
from hotels.serializers import HotelsSerializer

class ReservationSerializer(serializers.ModelSerializer):
    hotel = HotelsSerializer(read_only=True)
    children = serializers.SerializerMethodField()
    
    class Meta:
        model = Reservation
        fields = (
            'uuid',
            'hotelbeds_booking_reference',
            'status',
            'created_at',
            'holder_first_name',
            'holder_last_name',
            'holder_email',
            'holder_phone',
            'hotel',
            'room_name',
            'board_name',
            'rate_comments',
            'check_in',
            'check_out',
            'adults',
            'children',
            'supplier_name',
            'supplier_vat',
            'cancellation_policies_from',
            'cancellation_policies_amount'
        )
    
    def get_hotel(self, instance):
        hotel = instance.hotel
        return HotelsSerializer(hotel, read_only=True, context={"request": self.context.get('request')}).data
    
    def get_children(self, instance):
        children = [pax['age'] for pax in instance.paxes if pax['type'] == 'CH']
        return children
