from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render

from rest_framework import generics, status, views
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

# from .models import Reservation

from .models import Reservation
from .serializers import ReservationSerializer
from .services import (
    valid_book_params,
    book_room,
    cancel_booking
)
from .services_vouchers import create_voucher
from .swagger_docs import (
    post_booking_schema,
    get_booking_schema,
    del_booking_schema
)



class Booking(views.APIView):
    permission_classes = (AllowAny,) 
    # serializer_class = HotelsSerializer
   
    @post_booking_schema
    def post(self, request, *args, **kwargs):
        data = self.request.data
        # if not valid_book_params(data): return Response(status=status.HTTP_400_BAD_REQUEST)

        book_response, book_status = book_room(request, data)
        return Response(book_response, book_status)

class BookingRetrieve(generics.RetrieveAPIView):
    queryset = Reservation.objects.all()
    permission_classes = (AllowAny,) 
    serializer_class = ReservationSerializer 
    lookup_field  = 'uuid'

    @get_booking_schema
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class BookingCancelation(views.APIView):
    permission_classes = (AllowAny,)
    
    @del_booking_schema
    def delete(self, request, *args, **kwargs):
        reservation_uuid = self.kwargs['uuid']

        reservation_response, reservation_status = cancel_booking(request, reservation_uuid)

        return Response(reservation_response, reservation_status)


class DownloadVoucherPDF(APIView):

    def get(self, request, *args, **kwargs):
        reservation_uuid = self.kwargs['uuid']

        # Find Reservation data based on uuid -> From Booking->Reservation model
        try:
            reservation = Reservation.objects.select_related('hotel').get(uuid=reservation_uuid)
        except ObjectDoesNotExist:
            return HttpResponse("Error: No valid reservation with provided uuid.", status.HTTP_404_NOT_FOUND)

        voucher_content,voucher_name = create_voucher(reservation, request)
        
        response = HttpResponse(voucher_content)
        response['Content-Type'] = 'application/pdf'
        response['Content-disposition'] = 'attachment;filename='
        response['Content-disposition'] += voucher_name
        
        return response
