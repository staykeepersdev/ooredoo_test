import uuid
import datetime
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)

from square.client import Client

from django.conf import settings

# Square data
SQUARE_ACCESS_TOKEN = settings.SQUARE_ACCESS_TOKEN
SQUARE_ENVIRONMENT = settings.SQUARE_ENVIRONMENT

# Booking urls
BOOKING_ADMIN_URL = settings.BOOKING_ADMIN_URL
BOOKING_VOUCHER_URL = settings.BOOKING_VOUCHER_URL

def create_square_payment(nonce, amount, reservation):

    idempotency_key = str(uuid.uuid1())
    client = Client(
        access_token=SQUARE_ACCESS_TOKEN,
        environment=SQUARE_ENVIRONMENT,
    )
    payments_api = client.payments

    amount = int(amount)

    payment_notes =  'Booking admin url: ' + BOOKING_ADMIN_URL.replace('RESERVATION_ID', str(reservation.id)) + '\n'
    payment_notes += 'Booking voucher url: ' + BOOKING_VOUCHER_URL.replace('UUID', str(reservation.uuid)) + '\n'
    if reservation.referral_channel:
        payment_notes += 'Referral: ' + reservation.referral_channel.referral.company_name + '\n'

    body = {}
    body['source_id'] = f'{nonce}' #nonce
    body['idempotency_key'] = idempotency_key
    body['amount_money'] = {}
    body['amount_money']['amount'] = int(amount * 100)  # We are sending 10$ as cents -> 1000 centes
    body['amount_money']['currency'] = 'GBP'
    body['note'] = f'Payment for reservation #{reservation.id}\n\n{payment_notes}'

    result = payments_api.create_payment(body)

    if result.is_success():
        return True, result.body
    elif result.is_error():
        now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        logger.error(f'{now} Error payment with nonce {nonce} , amount {amount}, ENV: {SQUARE_ENVIRONMENT}. SquareError: {str(result.errors)}')
        return False, result.errors # return result.errors