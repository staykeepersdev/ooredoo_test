from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from common.services import (open_api_param, open_api_schema)

post_booking_schema = swagger_auto_schema(
    operation_summary='POST /booking/',
    operation_description="<b>Creates a new Booking</b>.",
    request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        # Personal info
        'first_name': open_api_schema('str', '<b>The first name</b> of the Booking Holder.'),
        'last_name': open_api_schema('str', '<b>The last name</b> of the Booking Holder.'),
        'email': open_api_schema('str', '<b>Valid email address</b> of the Booking Holder.'),
        'phone': open_api_schema('str', '<b>The phone number</b> of the Booking Holder.'),

        # Hotel, room & board info
        'hotel_id': open_api_schema('int', '<b>Unique id</b> of the hotel.'),
        'room_type': open_api_schema('str', 'Room type.<br>Example: <b>DBL.ST</b>'),
        'room_name': open_api_schema('str', 'Room name.<br>Example: <b>DOUBLE STANDARD</b>'),
        'board_name': open_api_schema('str', 'Board name.<br>Examples: <b>ROOM ONLY, BED AND BREAKFAST</b>'),

        # Reservation info
        'check_in': open_api_schema('str', '<b>Check In</b> date.<br>Format: <b>YYYY-MM-DD</b>'),
        'check_out': open_api_schema('str', '<b>Check Out</b> date.<br>Format: <b>YYYY-MM-DD</b>'),
        'adults': open_api_schema('int', '<b>Number of adults</b>.<br>Example: <b>4</b>'),

        # Reservation rates
        'ratekey': open_api_schema(
        'str',
        """\x00
            A specially encoded string that holds the following information:<br><br>
            &bull; Check In & Check Out dates, Hotel Id and Room Id<br>
            &bull; The specific nightly rate for the given period<br>
            &bull; Other internal-only information
            <br><br>
            <b>Rate key example</b>:<br>
            <code>20200320|20200323|W|272|114599|SGL.ST|FIT-RO-ALL|RO||1&tilde;1&tilde;0||N@04&tilde;&tilde;20d4d&tilde;-1042722160&tilde;N&tilde;&tilde;257DCD4019BB439158210166633100AAUK0000001000100010520d4d</code>
        """
        ),
        'amount': open_api_schema('int', 'The total amount for the whole stay.<br>Example: <b>450</b>'),
        'nonce': open_api_schema('str', 'Square API nonce'),
    },
    required=['first_name', 'last_name', 'email', 'phone', 'hotel_id', 'room_type', 'room_name', 'board_name', 'check_in', 'check_out', 'adults', 'ratekey', 'amount', 'nonce'],
    )
#   responses={status.HTTP_200_OK: HotelsSerializer(many=True)}
)

booking_uuid_param = open_api_param('str', 'uuid', '<b>Unique uuid value</b><br>Example: <b>26f2b45f-5bbb-488e-af6a-b12979a1290a</b>', in_=openapi.IN_PATH, required=True)

get_booking_schema = swagger_auto_schema(
    operation_summary='GET /booking/{uuid}',
    operation_description="<b>Retrieves detailed booking information via uuid.</b>",
    manual_parameters=[booking_uuid_param],
#   responses={status.HTTP_200_OK: HotelsSerializer(many=True)}
)


del_booking_schema = swagger_auto_schema(
    operation_summary='DEL /booking/{uuid}',
    operation_description="<b>Deletes a booking based on unique uuid.</b>",
    manual_parameters=[booking_uuid_param],
#   responses={status.HTTP_200_OK: HotelsSerializer(many=True)}
)