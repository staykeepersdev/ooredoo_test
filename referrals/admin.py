from django.contrib import admin
from django.utils.html import format_html
from django.db.models import Count

from .models import (Application, Referral, Channel)
from booking.models import Reservation


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = (
        'data',
        'created',
        'is_checked'
    )


@admin.register(Referral)
class ReferralAdmin(admin.ModelAdmin):
    list_display = (
        'company_logo',
        'company_name',
        'user',
        'commission',
        'city',
        'phone',
        'email'
    )
    list_display_links = ('company_logo', 'company_name',)

    class Media:
        css = {'all': ('/static/admin/css/admin-extra.css',)}

    def company_logo(self, obj):
        if obj.logo:
            return format_html('<img src="{}" width=50" />'.format(obj.logo.url))

    def commission(self, obj):
        return f'{obj.commission_percentage} %'

    # def bookings_overview(self, obj):
    #     status_options = {k: v for (k, v) in Reservation.RESERVATION_STATUSES}
    #     status_counts = Reservation.objects.filter(referral=obj).values('status').annotate(count=Count('id')).order_by('-status')

    #     booking_stats = ''
    #     for status_count in list(status_counts):
    #         status = status_count['status']
    #         count = status_count['count']
            
    #         try: status_name = status_options[status]
    #         except: status_name = ''
            
    #         booking_stats += f'<b class="{status}">{status_name}</b>: {count}<br>'

    #     return format_html(f'<div class="booking-stats">{booking_stats}</div>')


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = (
        'referral',
        'name',
        'code',
        'page_hits',
        'unique_sessions',
        'reservations_count'
    )