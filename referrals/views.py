from django.conf import settings
from django.shortcuts import render

from rest_framework import status, generics
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from common.services import send_simple_mail

from .models import Channel, Application
from .serializers import ChannelSerializer, DummySerializer
from .services import (
    track_page_hits_sessions,
    get_filtered_reservations_queryset,
    get_referral_reservations,
    get_referral_overview,
    get_csv_response
)
from .permissions import IsUserReferral


class ApplicationsFormDataView(APIView):
    def post(self, request, *args, **kwargs):
        application = Application.objects.create(data=request.data)

        # Internal notification
        send_to = [settings.DEFAULT_AFFILIATES_EMAIL]
        send_simple_mail(send_to, application, 'Affiliate application')

        return Response(
            {'message': f'{application} created'},
            status=status.HTTP_201_CREATED
        )


class SessionsTracker(APIView):
    def post(self, request, *args, **kwargs):
        response = track_page_hits_sessions(request.data)
        return Response(data=response, status=status.HTTP_200_OK)


class ReportChannels(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated, IsUserReferral)

    serializer_class = ChannelSerializer

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return Channel.objects.filter(referral__user=self.request.user)


class ReportReservations(generics.ListAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated, IsUserReferral)

    serializer_class = DummySerializer # Required by Swagger :(
    
    def get(self, request, *args, **kwargs):
        reservations = get_referral_reservations(self.get_queryset(), request).data
        overview = get_referral_overview(request.user, self.get_queryset())

        return Response({
            'reservations': reservations,
            'overview': overview
        })

    def get_queryset(self):
        return get_filtered_reservations_queryset(self.kwargs['start_date'], self.request.user)


class ReportReservationsCSV(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated, IsUserReferral)

    def get(self, request, *args, **kwargs):
        reservations = get_referral_reservations(self.get_queryset(), request).data
        return get_csv_response(reservations)

    def get_queryset(self):
        return get_filtered_reservations_queryset(self.kwargs['start_date'], self.request.user)