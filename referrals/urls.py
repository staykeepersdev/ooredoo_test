from django.urls import path
from .views import (
    ApplicationsFormDataView,
    SessionsTracker,
    ReportChannels,
    ReportReservations,
    ReportReservationsCSV
)

urlpatterns = [
    path('applications/', ApplicationsFormDataView.as_view(), name='apply'),
    path('sessions-tracker/', SessionsTracker.as_view(), name='sessions-tracker'),
    path('reports/channels', ReportChannels.as_view(), name='report-channels'),
    path('reports/reservations/<start_date>', ReportReservations.as_view(), name='report-reservations'),
    path('reports/reservations/<start_date>/csv', ReportReservationsCSV.as_view(), name='report-reservations-csv'),
]