import re
from django.apps import apps
from django.db import models

from hotels.models import Hotel
from locations.models import City
from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator

from django.db import models
from django.contrib.postgres.fields import JSONField


class Application(models.Model):
    data = JSONField()
    created = models.DateTimeField(auto_now_add=True)
    is_checked = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'applications'

    def __str__(self):
        return f'Application #{self.id}'


User = get_user_model()

class Referral(models.Model):
    user = models.OneToOneField(
        User, 
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name="referral"
    )
    company_name = models.CharField(max_length=2048)
    commission_percentage = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(20)],
        default=5, 
    )

    logo = models.ImageField(upload_to="referrals/logos", blank=True, null=True)

    city = models.ForeignKey(
        City,
        on_delete=models.SET_NULL,
        related_name='cities',
        blank=True,
        null=True
    )
    address = models.CharField(max_length=1024, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    
    phone = models.CharField(max_length=40, blank=True, null=True)
    email = models.EmailField(max_length=100, blank=True, null=True)
    website = models.URLField(blank=True, null=True)

    class Meta:
        ordering = ['company_name']
        verbose_name_plural = 'referrals'

    def __str__(self):
        return self.company_name

class Channel(models.Model):
    referral = models.ForeignKey(
        Referral,
        related_name='channels',
        on_delete=models.CASCADE
    )

    name = models.CharField(max_length=255,)
    code = models.CharField(
        max_length=50,
        unique=True,
        help_text="Enter referral-channel shortcode to be used for User tracking/statistics in the Frontend. The code should be lowercase and alpha-numeric."
    )
    unique_sessions = models.PositiveIntegerField(default=0)
    page_hits = models.PositiveIntegerField(default=0)

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'channels'

    def __str__(self):
        return f'{self.referral} - {self.name}'

    def save(self, *args, **kwargs):
        # Enforce alpha-numeric lowercase & dash channel code
        self.code = re.sub(r'[^a-zA-Z0-9\-]+', '', self.code).lower()
        super().save(*args, **kwargs)

    def reservations_count(self):
        Reservation = apps.get_model('booking', 'Reservation')
        return Reservation.objects.filter(referral_channel=self.id).count()