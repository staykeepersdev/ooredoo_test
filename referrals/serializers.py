from rest_framework import serializers

from .models import Channel
from booking.models import Reservation

class ChannelSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Channel
        fields = (
            'name',
            'code',
            'unique_sessions',
            'page_hits',
            'reservations_count'
        )


class ReferralReservationsSerializer(serializers.ModelSerializer):
    guest_name = serializers.SerializerMethodField()
    hotel_name = serializers.SerializerMethodField()
    hotel_location = serializers.SerializerMethodField()
    revenue = serializers.SerializerMethodField()
    
    class Meta:
        model = Reservation
        fields = (
            'hotelbeds_booking_reference',
            'guest_name',
            'hotel_name',
            'hotel_location',
            'room_name',
            'board_name',
            'check_in',
            'check_out',
            'total_amount',
            'revenue'
        )

    def get_guest_name(self, instance):
        return f'{instance.holder_first_name} {instance.holder_last_name}'

    def get_hotel_name(self, instance):
        return instance.hotel.name

    def get_hotel_location(self, instance):
        return f'{instance.hotel.country_code} {instance.hotel.destination_code}'

    def get_revenue(self, instance):
        return instance.total_breakdown().get('partner_amount', 0)


class DummySerializer(serializers.ModelSerializer):
    class Meta:
        model = Reservation
        fields =('hotelbeds_booking_reference',)