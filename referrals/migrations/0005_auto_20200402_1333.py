# Generated by Django 2.2.7 on 2020-04-02 10:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('referrals', '0004_channel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='channel',
            name='code',
            field=models.CharField(help_text='Enter referral-channel shortcode to be used for User tracking/statistics in the Frontend. The code should be lowercase and alpha-numeric.', max_length=50, unique=True),
        ),
    ]
