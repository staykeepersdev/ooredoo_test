import csv

from datetime import datetime
from dateutil.relativedelta import relativedelta

from django.conf import settings
from django.db.models import F, Sum

from booking.models import Reservation
from .models import Channel
from .serializers import ChannelSerializer, ReferralReservationsSerializer

from django.http import HttpResponse


# Track referrals page hits & sessions ...
def track_page_hits_sessions(request_data):
    refcode = request_data.get('refcode', None)
    new_session = request_data.get('new_session', False)

    channel = Channel.objects.filter(code=refcode).first()
    if not channel: return ''

    # Update Hits & Sessions
    channel.page_hits = channel.page_hits + 1
    response = 'page_hit'
    
    if new_session:
        channel.unique_sessions = channel.unique_sessions + 1
        response = 'page_hit + session'

    channel.save()
    return response


# Referral reservations & overview ...
def get_filtered_reservations_queryset(start_date, user):
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    plus_one_month = start_date + relativedelta(months=1, day=1)

    return Reservation.objects.filter(
        referral_channel__referral__user=user,
        created_at__range=[start_date, plus_one_month]
    ).exclude(hotelbeds_booking_reference__isnull=True)

def get_referral_reservations(reservations_queryset, request):
    return ReferralReservationsSerializer(reservations_queryset, many=True, context={"request": request})

def get_referral_overview(user, reservations_queryset):
    referral = user.referral
    if referral:
        aggregates = reservations_queryset.aggregate(
            total_reservations_amount=Sum('total_amount'),
            total_revenue=(Sum('total_amount') * referral.commission_percentage / 100)
        )
        return {
            'total_reservations_amount': aggregates.get('total_reservations_amount', 0),
            'client_commission_percent': referral.commission_percentage,
            'total_revenue': aggregates.get('total_revenue', 0)
        }

    return None


# CSV related ...
def get_csv_response(queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment;filename=export.csv'

    # Init csv writer
    writer = csv.writer(response)

    # CSV Header
    if len(queryset):
        writer.writerow(get_csv_titles(queryset[0]))

    # CSV Body
    for row in queryset:
        writer.writerow(get_csv_values(row))

    return response

def get_csv_titles(header):
    raw_titles = dict(header).keys()
    return [raw_title.replace('_', ' ').capitalize() for raw_title in raw_titles]

def get_csv_values(row):
    return dict(row).values()