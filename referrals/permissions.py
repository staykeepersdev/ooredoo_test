from rest_framework.permissions import BasePermission

class IsUserReferral(BasePermission):
    message = 'User IS NOT Referral'

    def has_permission(self, request, view):
        return request.user.is_referral