from django.contrib import admin
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget

from .models import (
    APIHotel,
    APIHotelCategory,
    APIRoom,
    APIRateComment,
    APIFacility,
    APIFacilityGroup,
    APICountry,
    APIDestination
)

class DefaultAPIAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'code', 'updated_at')
    search_fields = ('code',)
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget}
    }

@admin.register(APIHotel)
class APIHotelAdmin(DefaultAPIAdmin):
    pass

@admin.register(APIHotelCategory)
class APIHotelCategoryAdmin(DefaultAPIAdmin):
    pass

@admin.register(APIRoom)
class APIRoomAdmin(DefaultAPIAdmin):
    pass

@admin.register(APIRateComment)
class APIRateCommentAdmin(DefaultAPIAdmin):
    list_display = ('__str__', 'incoming', 'code', 'updated_at')

@admin.register(APIFacility)
class APIFacilityAdmin(DefaultAPIAdmin):
    list_display = DefaultAPIAdmin.list_display + ('description', 'hostify_description','api_facility_group')
    search_fields = DefaultAPIAdmin.search_fields + ('description',)
    list_editable = ('hostify_description',)
    list_filter = ('api_facility_group',)


@admin.register(APIFacilityGroup)
class APIFacilityGroupAdmin(DefaultAPIAdmin):
    list_display = DefaultAPIAdmin.list_display + ('description',)

@admin.register(APICountry)
class APICountryAdmin(DefaultAPIAdmin):
    list_display = DefaultAPIAdmin.list_display + ('description',)
    search_fields = DefaultAPIAdmin.search_fields + ('description',)

@admin.register(APIDestination)
class APIDestinationAdmin(DefaultAPIAdmin):
    list_display = DefaultAPIAdmin.list_display + ('description',)
    search_fields = DefaultAPIAdmin.search_fields + ('description',)
    