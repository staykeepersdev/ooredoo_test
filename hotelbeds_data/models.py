import datetime

from django.db import models
from django.contrib.postgres.fields import ArrayField, JSONField
from django.utils import timezone


class APIDataModel(models.Model):
    code = models.CharField(max_length=18, null=True, blank=True)
    data = JSONField(null=True, blank=True)
    updated_at = models.DateField(blank=True, null=True)

    class Meta:
        abstract = True
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__data = self.data
    
    def save(self, *args, **kwargs):        
        if not self.id or self.data != self.__data:
            self.code = self.data.get('code') 

        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.__class__.__name__} #{self.code}'

class APIHotelCategory(APIDataModel):
    simple_code = models.PositiveIntegerField(blank=True, null=True)

class APIHotel(APIDataModel):
    code = models.BigIntegerField(null=True, blank=True)
    
    # Marked hotels have higher price for one than two adults
    price_for_one = models.BooleanField(default=False)

    # Marked hotels have higher difference betwee 4 and 3 adults than 3 to 2 adults
    max_3_adults = models.BooleanField(default=False)

    # Marked hotels have minimum of 2 guests per stay
    min_2_adults = models.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__data = self.data

    def save(self, *args, **kwargs):        
        if not self.id or self.data != self.__data:
            self.code = self.data.get('code')
            
            date_str = self.data.get('lastUpdate')
            self.updated_at = datetime.datetime.strptime(date_str, '%Y-%m-%d')        

        super().save(*args, **kwargs)

class APIRoom(APIDataModel):
    pass


class APIRateComment(APIDataModel):    
    incoming = models.PositiveIntegerField(blank=True, null=True)
    rate_codes = ArrayField(models.PositiveIntegerField(), blank=True, null=True)
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)


class APIFacilityGroup(APIDataModel):
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.description

class APIFacility(APIDataModel):
    api_facility_group = models.ForeignKey(APIFacilityGroup, on_delete=models.CASCADE)
    description = models.TextField(blank=True, null=True)
    hostify_description = models.CharField(max_length=255, blank=True, null=True)
    
    def __str__(self):
        return self.description

class APICountry(APIDataModel):
    description = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return self.description
    

class APIDestination(APIDataModel):
    description = models.TextField(blank=True, null=True)
    
    def __str__(self):
        return self.description