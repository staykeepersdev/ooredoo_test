from django.apps import AppConfig


class HotelbedsDataConfig(AppConfig):
    name = 'hotelbeds_data'
