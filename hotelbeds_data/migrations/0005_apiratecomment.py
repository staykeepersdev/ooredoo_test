# Generated by Django 2.2.7 on 2020-01-14 14:14

import django.contrib.postgres.fields
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hotelbeds_data', '0004_auto_20191202_1609'),
    ]

    operations = [
        migrations.CreateModel(
            name='APIRateComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True)),
                ('updated_at', models.DateField(blank=True, null=True)),
                ('code', models.CharField(blank=True, max_length=18, null=True)),
                ('incoming', models.PositiveIntegerField(blank=True)),
                ('rate_codes', django.contrib.postgres.fields.ArrayField(base_field=models.PositiveIntegerField(), blank=True, size=None)),
                ('date_start', models.DateField(blank=True, null=True)),
                ('date_end', models.DateField(blank=True, null=True)),
                ('description', models.TextField(blank=True, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
