from hotelbeds_data.models import APICountry

def get_countries(values=['code', 'description']):
    countries_values = APICountry.objects.all().values_list(*values)

    COUNTRIES = {}
    for key, value in countries_values:
        COUNTRIES[key] = value

    return COUNTRIES
