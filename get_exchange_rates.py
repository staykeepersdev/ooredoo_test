import time, hashlib
import json
import requests

import os

import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ooredoo.settings")
django.setup()

from currencies.client import ExchangeAPIClient
from currencies.models import ExchangeRate, Currency

EXCHANGE_API_CLIENT = ExchangeAPIClient()

euro_gbp_rates = EXCHANGE_API_CLIENT.get_exchange_rate(
    base_currency="EUR", to_currency=""
)
base = euro_gbp_rates.get("base")

base_currency, _ = Currency.objects.get_or_create(code=base, defaults={"name": base})
rates = euro_gbp_rates.get("rates")

for key, value in rates.items():
    to_currency, _ = Currency.objects.get_or_create(code=key, defaults={"name": key})
    rate = ExchangeRate.objects.update_or_create(
        base=base_currency, currency=to_currency, defaults={"rate": value}
    )
