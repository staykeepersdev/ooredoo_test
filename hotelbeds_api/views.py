from rest_framework.views import APIView
from rest_framework.response import Response

from .services import (
    get_hotels_content,
    get_cache_file
)

# DRF Yet Another Swagger Generator 
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


class GetHotelsContent(APIView):
    # authentication_classes = [authentication.TokenAuthentication]
    # permission_classes = [permissions.IsAdminUser]

    countryCode = openapi.Parameter(
        'countryCode', openapi.IN_QUERY,
        description='Filter to limit the results for an specific country by country code.',
        type=openapi.TYPE_STRING
    )
    from_hotel = openapi.Parameter(
        'from', openapi.IN_QUERY,
        description='Number of the initial record to receive, if nothing is indicated 1 is the default value.',
        type=openapi.TYPE_INTEGER
    )
    to_hotel = openapi.Parameter(
        'to', openapi.IN_QUERY,
        description='Number of the final record to receive, if nothing is indicated 100 is the default value.',
        type=openapi.TYPE_INTEGER
    )
    language = openapi.Parameter(
        'language', openapi.IN_QUERY,
        description='Language Code in which you want the descriptions to be returned. If not specifically indicated, English is the default language.',
        type=openapi.TYPE_STRING
    )


    @swagger_auto_schema(
        operation_summary='get',
        operation_description='Get Hotels by countryCode.',
        manual_parameters=[countryCode,from_hotel, to_hotel, language]
    )
    def get(self, request, format=None):
        params = {
            'countryCode': 'QA',
            'fields': 'all',
            'from': 1,
            'to': 5,
            'language': "ENG"
        }
        hotels_data = get_hotels_content(params)
        return Response(hotels_data)
        
class GetCacheFile(APIView):

    def get(self, request, format=None):
        request_response = get_cache_file()
        return Response(request_response)
