from .base import BaseAPIClient

class RateCommentAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint='hotel-content-api/1.0/types/ratecomments')
    
    def get_params(self, page):
        from_rate = (page-1) * 1000 + 1
        to_rate = page * 1000

        params = {
            "fields": "all",
            "language":"ENG",
            "from": from_rate,
            "to": to_rate,
        }
        return params

    def get_rate_comments_from_page(self, page):
        params = self.get_params(page)        
        return self.get_list(params=params)