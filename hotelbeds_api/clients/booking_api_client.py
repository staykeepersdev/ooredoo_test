from .base import BaseAPIClient

from django.conf import settings

HOTELEBDS_BOOKING_URL = settings.HOTELEBDS_BOOKING_URL

class BookingAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint=HOTELEBDS_BOOKING_URL)

    def booking(self, data):
        self.endpoint = HOTELEBDS_BOOKING_URL
        return self.post_item(data=data)
    
    def cancel_booking(self, booking_reference):
        self.endpoint = '/hotel-api/1.0/bookings/'
        return self.delete_item(identifier=booking_reference)