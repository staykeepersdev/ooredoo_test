from .base import BaseAPIClient


class HotelsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='/hotel-content-api/1.0/hotels')

    def get_hotels(self, params=None):
        return self.get_list(params=params)
    

class HotelsCategoryAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='/hotel-content-api/1.0/types/categories')

    def get_hotel_categories(self, params=None):
        if not params:
            params = self.get_params()
        return self.get_list(params=params)



class CountriesAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='/hotel-content-api/1.0/locations/countries')

class DestinationsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='/hotel-content-api/1.0/locations/destinations')
    