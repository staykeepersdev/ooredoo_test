from .base import BaseAPIClient

class FacilityGroupsAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint='hotel-content-api/1.0/types/facilitygroups')
    
    def get_all_facilitiy_groups(self): 
        params = self.get_params()     
        return self.get_list(params=params)
        