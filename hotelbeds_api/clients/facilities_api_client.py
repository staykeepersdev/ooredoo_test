from .base import BaseAPIClient

class FacilitiesAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint='hotel-content-api/1.0/types/facilities')
    
    def get_all_facilities(self): 
        params = self.get_params()     
        return self.get_list(params=params)
        