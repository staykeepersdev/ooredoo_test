import hashlib
import json
import requests
import time

from urllib.parse import urljoin

from django.conf import settings

API_KEY = settings.HOTELBEDS_API_KEY
SECRET = settings.HOTELBEDS_SECRET
BASE_URL = settings.HOTELBEDS_BASE_URL


class BaseAPIClient:
    def __init__(
                self,
                endpoint=None,
                url=BASE_URL,
                api_key=API_KEY,
                secret=SECRET,
    ):
        self.endpoint = endpoint
        self.url = url
        self.api_key = api_key
        self.secret = secret

    def headers(self):
        headers = {            
            'Api-key': self.api_key,
            'X-Signature': self.get_time_signautre(),
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip',
            'Accept': 'application/json'
        }
        return headers
        
    def get_params(self, _from=0, _to=1000, language='ENG'):
        params = {
            "fields": "all",
            "language": language,
            "from": _from,
            "to": _to,
        }
        return params
    
    def get_page_params(self, page, language='ENG'):
        _from = (page-1) * 1000 + 1
        _to = page * 1000

        return self.get_params(_from, _to, language)
    
    
    def get_time_signautre(self):
        sigStr = "%s%s%d" % (self.api_key, self.secret,int(time.time()))
        signature = hashlib.sha256(sigStr.encode('utf-8')).hexdigest()
        return signature

    def cleaned_data(self, response):
        data = json.loads(response.content)
        if 'auditData' in data:
            del data['auditData']
        return data

    def _post_json_response(self, url: str, data=None):
        response = requests.post(url=url, headers=self.headers(), json=data)
        return self.cleaned_data(response)
    
    def _delete_json_response(self, url: str):
        response = requests.delete(url=url, headers=self.headers())
        return self.cleaned_data(response)

    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers(), params=params)
        return self.cleaned_data(response)

    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def detail_url(self, identifier: int):
        return urljoin(self.list_url(), str(identifier))

    def get_list(self, params=None):
        if not params:
            params = self.get_params()
            
        url = self.list_url()
        return self._get_json_response(url=url, params=params)
            
    def get_item(self, identifier: int, params=None):
        url = self.detail_url(identifier=identifier)
        return self._get_json_response(url=url, params=params)    
    
    def post_item(self, data=None):       
        url = self.list_url()
        return self._post_json_response(url=url, data=data)
    
    def delete_item(self, identifier: int):
        url = self.detail_url(identifier=identifier)
        return self._delete_json_response(url=url)