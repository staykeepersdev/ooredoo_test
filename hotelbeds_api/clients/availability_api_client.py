from .base import BaseAPIClient


class HotelsAvailbilityAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint='/hotel-api/1.0/hotels')

    def get_hotels_availability(self, data):
        return self.post_item(data=data)
