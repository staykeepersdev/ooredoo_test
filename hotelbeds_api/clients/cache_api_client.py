import requests
from .base import BaseAPIClient

from ooredoo.settings import HOTELBEDS_CACHE_API_URL

class CacheFileAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(url=HOTELBEDS_CACHE_API_URL)
        self.endpoint_prefix = 'aif2-pub-ws/files/'
        
        # Set default endpoint to be Full Rates
        self.endpoint = self.endpoint_prefix + 'full'
    
    def get_file(self, params=None):
        return requests.get(url=self.list_url(), headers=self.headers())

    # Hotel Beds - Cache API operations ...
    # @link https://developer.hotelbeds.com/documentation/hotels/cache-api/operations/
    
    def get_full_rates(self):
        self.endpoint = self.endpoint_prefix + 'full'
        return self.get_file()

    def confirm_version(self, version):
        self.endpoint = self.endpoint_prefix + 'confirm'
        
        headers = self.headers()
        headers['X-Version'] = str(version)
        
        return requests.get(url=self.list_url(), headers=headers)

    def get_update_rates(self):
        self.endpoint = self.endpoint_prefix + 'update'
        return self.get_file()
