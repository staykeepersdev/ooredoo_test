from .base import BaseAPIClient

class CheckRateAPIClient(BaseAPIClient):

    def __init__(self):
        super().__init__(endpoint='hotel-api/1.0/checkrates')
    
    def get_params(self, rate_key):
        params = {
            "upselling": "False",
            "rooms": [
                {
                    "rateKey": rate_key
                }
            ]
        }
        return params

    def check_rate(self, rate_key):
        data = self.get_params(rate_key)        
        return self.post_item(data=data)