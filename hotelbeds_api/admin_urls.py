from django.urls import path
from .views import (
    GetHotelsContent,
    GetHotelsAvailability,
    GetCacheFile,
)


urlpatterns = [
    path('get-hotels-content', GetHotelsContent.as_view(), name='get-hotels-content'),
    path('get-hotels-availability/', GetHotelsAvailability.as_view(), name='get-hotels-availability'),
    path('get-cache-file/', GetCacheFile.as_view(), name='get-cache-file'),
]
