import re
from .clients.content_api_client import HotelsAPIClient
from .clients.availability_api_client import  HotelsAvailbilityAPIClient
from .clients.cache_api_client import CacheFileAPIClient
from .clients.checkrate_api_client import CheckRateAPIClient

CACHE_FILE_API_CLIENT = CacheFileAPIClient()
CHECK_RATE_API_CLIENT = CheckRateAPIClient()
HOTELS_API_CLIENT = HotelsAPIClient()
HOTELS_AVAILABILITY_API_CLIENT = HotelsAvailbilityAPIClient()

def get_hotels_content(params):

    hotels_data = HOTELS_API_CLIENT.get_hotels(params=params)
    return hotels_data


def get_hotels_availability(data):

    hotels_data = HOTELS_AVAILABILITY_API_CLIENT.get_hotels_availability(data=data)
    return hotels_data

def get_check_rate(rate_key):

    hotels_data = CHECK_RATE_API_CLIENT.check_rate(rate_key=rate_key)
    return hotels_data

def get_filename_from_content_disposition(cd):    
    if not cd:
        return None
    fname = re.findall('filename=(.+)', cd)
    if len(fname) == 0:
        return None
    return fname[0].strip('"')

def get_cache_file():
    try:
        file_response = CACHE_FILE_API_CLIENT.get_file()
        filename = get_filename_from_content_disposition(file_response.headers.get('content-disposition'))
        with open(f'downloads/{filename}', 'wb') as file:
            file.write(file_response.content)
        return 'The Cache Api File have been downloaded inside downloads folder.'
    except Exception as e:
        return f'{e}'