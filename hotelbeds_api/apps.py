from django.apps import AppConfig


class HotelbedsApiConfig(AppConfig):
    name = 'hotelbeds_api'
