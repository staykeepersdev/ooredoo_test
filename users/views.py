from django.contrib.auth import authenticate, login

from rest_framework import status, generics
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import UserSerializer


class LoginUser(APIView):
    authentication_classes = ()

    def post(self, request):
        data = request.data
        email = data.get('email')
        password = data.get('password')

        if email is None or password is None:
            return Response(
                data={'error': 'Please provide both email & password.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        user = authenticate(email=email, password=password)

        if user:
            login(request, user)
            token, _ = Token.objects.get_or_create(user=user)

            return Response(
                data={'token': token.key},
                status=status.HTTP_200_OK
            )

        return Response(
            data={'error': 'Invalid email/password provided.'},
            status=status.HTTP_401_UNAUTHORIZED
        )


class CurrentUserInfo(generics.RetrieveAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user