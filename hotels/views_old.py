# Hotels_OLD
class Hotels(generics.ListAPIView):
    permission_classes = (IsAuthenticated,) 
    serializer_class = HotelsSerializer
    orderby_fields = ('ranking', '-ranking', 'category_code', '-category_code')
    pagination_class = PaginationSerializer

    # open api params
    country_code = open_api_param('str', 'country_code', 'Filter by: 2-letter country code.')
    city = open_api_param('str', 'city', 'Filter by: case-insensitive city name.')
    guests = open_api_param('int', 'guests', 'Filter by: the number of potential guests.')
    # min_price = open_api_param('int', 'min_price', 'Filter by: the bottom bracket for room price - per night.')
    # max_price = open_api_param('int', 'max_price', 'Filter by: the top bracket for room price - per night.')
    # check_in = open_api_param('str', 'check_in', 'Filter by: the day the guests are planning to arrive and register at a hotel.<br>Date format: <b>YYYY-MM-DD</b>')
    # check_out = open_api_param('str', 'check_out', 'Filter by: the day the guests are planning to leave a hotel at the end of their stay.<br>Date format: <b>YYYY-MM-DD</b>')

    orderby = open_api_param('str', 'orderby', 'Order by asc/desc:<br><b>ranking</b> or <b>-ranking</b><br><b>category_code</b> or <b>-category_code</b>')
    
    @swagger_auto_schema(
      operation_summary='GET /hotels/',
      operation_description="<b>Returns a list of all hotels.</b> Supports optional params for <b>filtering & ordering</b>.",
      manual_parameters=[country_code, city, guests, orderby],
      responses={status.HTTP_200_OK: HotelsSerializer(many=True)}
    )
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def filter_queryset(self, queryset):
      filtered_qs = self.filterby(queryset)
      filtered_ordered_qs = self.orderby(filtered_qs)
      return filtered_ordered_qs
    
    def get_queryset(self):
      return Hotel.objects.all()

    # filter
    def filterby(self, queryset):
      query = self.request.query_params
      p = { # params
        'country_code': None, 'city': None,
        'guests': 1,
        'min_price': 0, 'max_price': 99999,
        'check_in': '2000-01-01',
        'check_out': '2100-01-01'
      }
      for param, default in p.items():
        p[param] = query.get(param, default)

      # by country_code, city
      if(p['country_code'] and p['city']):
        queryset = queryset.filter(
          country_code__iexact=p['country_code'],
          city__iexact=p['city']
        )

      '''
      # by guests ... room max_pax based ... tested: correctly reduces the resultset
      queryset = queryset.filter( Q(rooms__min_pax__lte=p['guests']) & Q(rooms__max_pax__gte=p['guests']) ).distinct()

      # by date range ... tested: correctly reduces the resultset by half ... but can be removed if unnecessary in Live data mode
      queryset = queryset.filter(rooms__inventory__date__range=[p['check_in'], p['check_out']]).distinct()
      # filtered_hotel_ids = list(queryset.values_list('id', flat=True).distinct())
      '''
      filtered_hotel_ids = [87, 71, 51, 80, 132, 92, 115, 114, 60, 112, 108, 135, 11, 42, 121, 117, 88, 82, 113, 43, 120, 79, 26, 72, 57, 81, 61, 77, 30, 21, 131, 17, 104, 5, 91, 74, 54, 138, 63, 35, 105, 107, 39, 93, 89, 31, 14, 2, 62, 75, 126, 46, 32, 38, 140, 139, 137, 25, 141, 94, 122, 49, 20, 76, 106, 143, 58]

      # get hotel rooms
      rooms = HotelRoom.objects.filter(
        hotel_id__in=filtered_hotel_ids
      ).distinct()
      
      # filter by guests ... inventory allotment based
      rooms = rooms.annotate(
        count_days=Count('inventory'),
        min_allotment=Min('inventory__allotment')
      ).filter(min_allotment__gte=p['guests'])
      # @todo THIS SHOULD BE IMPROVED: allotment * room max pax = capacity # X(by room type)
      # possible room types are: BED / DBL / DBT / DUS / JSU / QUA / ROO / SGL / SUI / TPL / TWN

      # test: filter by guests ... tested: works for hotel id 2
      print("Hotel room - days/min_allotment")
      for room in list(rooms.all()):
        if(room.hotel_id == 2):
          print("{0} {1} - {2}/{3}".format(room.hotel_id, room.room_code, room.count_days, room.min_allotment))

      # by price range ... based on maximum daily amount in day range
      # rooms = rooms.annotate(
      #   max_amount=Max(F('inventory__prices__amount'))
      # ).filter(max_amount__range=(p['min_price'], p['max_price']))
      # @todo this should be pomymorphing based on price_per_pax boolean field
      
      # @todo annotate average amount
      # queryset = queryset.annotate(average_amount=Avg(F('rooms__inventory__prices__amount')))
      # print(queryset.all()[0].average_amount)

      return queryset.distinct()

    # # order
    def orderby(self, queryset):
        orderby = self.request.query_params.get('orderby')
        if orderby in self.orderby_fields: queryset = queryset.order_by(orderby)
        return queryset

class HotelImages(generics.ListAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = HotelImagesSerializer

    @swagger_auto_schema(
      operation_summary='GET /hotels/{$id}/images/',
      operation_description="<b>Returns a list of all images</b> related to this hotel.",
      responses={status.HTTP_200_OK: HotelImagesSerializer(many=True)}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        hotel_id = self.kwargs['pk']
        return HotelImage.objects.filter(hotel_id=hotel_id, hotel_room_id__isnull=True)

# Hotel rooms related
class HotelRooms(generics.ListAPIView):
    permission_classes = (IsAuthenticated,) 
    serializer_class = HotelRoomsSerializer
    # ordering_fields = ('default_price', 'rating')
    pagination_class = PaginationSerializer

    @swagger_auto_schema(
      operation_summary='GET /hotels/{id}/rooms/',
      operation_description="<b>Returns a list of all rooms</b> related to this hotel.",
      responses={status.HTTP_200_OK: HotelRoomsSerializer(many=True)}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        hotel_id = self.kwargs['hotel_id']
        return HotelRoom.objects.filter(hotel_id=hotel_id)


class HotelDetails(generics.RetrieveAPIView):
    permission_classes = (AllowAny,) 
    serializer_class = HotelSerializer
    queryset = Hotel.objects.all()

    @swagger_auto_schema(
      operation_summary='GET /hotels/{id}/',
      operation_description="<b>Returns a single hotel</b> with detailed information - based on <b>unique id</b>.",
      responses={status.HTTP_200_OK: HotelSerializer}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

class HotelRoomDetails(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,) 
    serializer_class = HotelRoomSerializer
    lookup_fields = ['pk']

    @swagger_auto_schema(
      operation_summary='GET /hotels/{hotel_id}/rooms/{id}/',
      operation_description="<b>Returns a single hotel room</b> with detailed information - based on unique <b>hotel_id</b> and <b>room id</b>.",
      responses={status.HTTP_200_OK: HotelRoomsSerializer}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        hotel_id = self.kwargs['hotel_id']
        return HotelRoom.objects.filter(hotel_id=hotel_id)

class HotelRoomImages(generics.ListAPIView):
    permission_classes = (IsAuthenticated,) 
    serializer_class = HotelRoomImagesSerializer
    
    @swagger_auto_schema(
      operation_summary='GET /hotels/{hotel_id}/rooms/{id}/images/',
      operation_description="<b>Returns a list of all images</b> related to this <b>hotel room</b>.",
      responses={status.HTTP_200_OK: HotelRoomImagesSerializer(many=True)}
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        hotel_id = self.kwargs['hotel_id']
        hotel_room_id = self.kwargs['pk']
        return HotelImage.objects.filter(hotel_id=hotel_id).filter(hotel_room_id=hotel_room_id).order_by('order')