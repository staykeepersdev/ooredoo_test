from rest_framework import serializers
from rest_framework import pagination
from rest_framework.response import Response

from .models import (
    Hotel,
    HotelImage,
    PhoneNumber,
    HotelRoom
)
from rooms_inventory.serializers import (
    InventoryDaySerializer,
    PriceDaySerializer
)

from .services import get_obj_facilities

class PhoneNumbersSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneNumber
        fields = '__all__'

class HotelRoomsSerializer(serializers.ModelSerializer):
    images = serializers.SerializerMethodField()
    class Meta:
        model = HotelRoom
        exclude = (
            'api_room',
        )
    
    def get_images(self, instance):
        images = instance.images.all().order_by('order')
        return HotelImagesSerializer(images, many=True, context={"request": self.context.get('request')}).data

class HotelRoomSerializer(HotelRoomsSerializer):
    inventory = InventoryDaySerializer(read_only=True, many=True)


class HotelImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = HotelImage
        exclude = ('giata_path',)

class HotelRoomImagesSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    class Meta:
        model = HotelImage
        exclude = ('giata_path',)
    
    def get_image(self, image):
        request = self.context.get('request')
        image_url = image.image.url
        return request.build_absolute_uri(image_url)

class BaseHotelSerializer(serializers.ModelSerializer):
    phone_number = serializers.SerializerMethodField()
    stars = serializers.SerializerMethodField()
    class Meta:
        model = Hotel
        fields = (
            'id',
            'name',
            'description',
            'phone_number',

            'country_code',
            'destination_code',
            'latitude',
            'longitude',

            'accommodation_type_code',
            'stars',

            'city',
            'address',
            'postal_code'
        )

    def get_phone_number(self, obj):
        return obj.get_phone_number()
    
    def get_stars(self,obj):
        if obj.api_hotel_category:
            return obj.api_hotel_category.simple_code
        else:
            return ''


class HotelsSerializer(BaseHotelSerializer):
    image_url = serializers.SerializerMethodField()
    class Meta(BaseHotelSerializer.Meta):
        fields = BaseHotelSerializer.Meta.fields + ('image_url',)
    
    def get_image_url(self, hotel):
        try:
            request = self.context.get('request')
            image_url = hotel.images.order_by('order')[0].image.url
            return request.build_absolute_uri(image_url)
        except:
            return ''

class HotelSerializer(BaseHotelSerializer):
    images = HotelImagesSerializer(many=True, read_only=True)
    facilities = serializers.SerializerMethodField()
    class Meta(BaseHotelSerializer.Meta):
        fields = BaseHotelSerializer.Meta.fields + ('images', 'facilities')

    def get_facilities(self, obj):
        return get_obj_facilities(obj)
        

# utils
class PaginationSerializer(pagination.PageNumberPagination):
    page_size = 15

    def get_paginated_response(self, data):
        to = self.page.number * self.page_size
        total = self.page.paginator.count
        if to > total:
            to = total
        return Response({
            'pagination':{
                'page': self.page.number,
                'from': (self.page.number - 1) * self.page_size + 1,
                'to': to,
                'per_page': self.page_size,
                'total': total,
            },
            'items': data
        })