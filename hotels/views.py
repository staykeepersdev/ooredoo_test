import json
import datetime

from django.db.models import Count, Avg, Min, Max, Q, F
from django.http import HttpResponse

from rest_framework import generics, status, views
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .models import Hotel, HotelRoom, HotelImage
from .serializers import (
    HotelSerializer, HotelsSerializer, HotelImagesSerializer,
    HotelRoomsSerializer, HotelRoomSerializer, HotelRoomImagesSerializer,
    PaginationSerializer
)
from .services import (
    get_available_hotels,
    get_available_hotel_rooms,
    get_hotel_room_check_rate,
    get_image_response
)

from .swagger_docs import (
    post_hotels_availability_schema,
    get_hotels_rooms_availability_schema,
    post_room_check_rate_schema
)


class HotelsAvailability(views.APIView):
    permission_classes = (AllowAny,) 
    serializer_class = HotelsSerializer

    @post_hotels_availability_schema
    def post(self, request, *args, **kwargs):
        data_params = self.request.data
        data = get_available_hotels(request, data_params)       

        return Response(data)

class HotelRoomsAvailability(views.APIView):
    permission_classes = (AllowAny,) 
    serializer_class = HotelsSerializer

    @get_hotels_rooms_availability_schema
    def get(self, request, *args, **kwargs):
        query_params = self.request.query_params
        hotel_id = self.kwargs['hotel_id']
        hotel = Hotel.objects.get(pk=hotel_id)
        rooms_data, currency = get_available_hotel_rooms(request, query_params, hotel)

        hotel_data = HotelSerializer(hotel, context={"request": request}).data

        hotel_data['rooms'] = rooms_data
        hotel_data['currency'] = currency

        return Response(hotel_data)


class HotelRoomsCheckRate(views.APIView):
    permission_classes = (AllowAny,)

    @post_room_check_rate_schema
    def post(self, request, *args, **kwargs):
        hotel_id = self.kwargs['hotel_id']
        get_object_or_404(Hotel, pk=hotel_id)

        rate_key = self.request.data.get('rate_key')
        rate_data = get_hotel_room_check_rate(rate_key, request)

        return Response(rate_data)


class GetHotelImage(views.APIView):
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        image_id = self.kwargs['image_id']
        image = get_object_or_404(HotelImage, pk=image_id)

        image_data = get_image_response(image)
        return HttpResponse(image_data, content_type="image/png")