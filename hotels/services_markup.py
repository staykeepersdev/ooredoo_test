from django.conf import settings
from decimal import Decimal

# Percentage in numbers (ex. 20%)
TOTAL_COMMISSION_PERCENTAGE = settings.TOTAL_COMMISSION_PERCENTAGE

def is_markup_required(user):
    # Staff users (admins) should see raw amounts
    if user.is_staff: return False

    # Guests and regular users should see markup amounts
    return True


def apply_markup(amount):
    # Sanitize
    amount = Decimal(amount)
    
    markup = (amount * TOTAL_COMMISSION_PERCENTAGE / 100)
    return round(amount + markup, 2)

def apply_rate_markup(rate):
    rate['net'] = apply_markup(rate['net'])
    
    # Total Net is needed for checkrate only
    try:
        rate['totalNet'] =  apply_markup(rate['totalNet'])
    except: pass

    try:
        cancellation_policies = rate['cancellationPolicies']
        for policy in cancellation_policies:
            policy['amount'] = apply_markup(policy['amount'])
    except: pass

    try:
        offers = rate['offers']
        for offer in offers:
            offer['amount'] = apply_markup(offer['amount'])
    except: pass

    try:
        breakdowns = rate['rateBreakDown']
        for breakdown in breakdowns.values():
            for sub_breakdown in breakdown:
                sub_breakdown['amount'] = apply_markup(sub_breakdown['amount'])
    except: pass

    return rate

def apply_rooms_markup(rooms_data):
    for room_data in rooms_data:
        for rate in room_data['rates']:
            rate = apply_rate_markup(rate)

    return rooms_data