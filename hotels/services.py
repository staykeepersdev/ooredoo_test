import sys
import traceback
import re
import requests

from collections import defaultdict as ddict
from decimal import Decimal

from common.services import assemble_room_paxes
from .services_markup import (
    is_markup_required,
    apply_markup,
    apply_rate_markup,
    apply_rooms_markup
)
from .models import Hotel, HotelRoom

from currencies.models import ExchangeRate
from hotelbeds_data.models import APIRateComment

from hotelbeds_api.services import get_hotels_availability, get_check_rate


def get_obj_facilities(obj):
    facilities_array = obj.facilities.all().order_by(        
        'api_facility_group__description'
    ).values_list('api_facility_group__description', 'description').distinct()

    facilities = ddict(list)
    for group, facility in facilities_array:
        facilities[group].append(facility)
    return facilities

def get_hotels_availability_params(data_params, hotel_ids):
    adults = int(data_params.get('adults') or 0)
    children = str(data_params.get('children', ''))
    check_in = data_params.get('check_in')
    check_out = data_params.get('check_out')

    # category ~= HotelBeds categoryCode ... possible values: 1..5EST / 4..5LUX / BOU / SPC / SUP
    min_category = int(data_params.get('min_category') or 1)
    max_category = int(data_params.get('max_category') or 5)
    min_rate = int(data_params.get('min_rate') or 1)
    max_rate = int(data_params.get('max_rate') or 2000)

    if children:
        clidren_count = len(children.split(','))
    else:
        clidren_count = 0
    # payment_type = data_params.get('payment_type', 'BOTH') # possible values: AT_WEB, AT_HOTEL, BOTH

    # Trip Advisor based reviews filtering
    MIN_REVIEWS_COUNT = 3
    min_review_rate = data_params.get('min_review_rate') or False
    max_review_rate = data_params.get('max_review_rate') or False
    
    reviews = None
    if min_review_rate and max_review_rate:
        reviews = [{
            "type": "TRIPADVISOR",
            "minRate": int(min_review_rate),
            "maxRate": int(max_review_rate),
            "minReviewCount": MIN_REVIEWS_COUNT
	    }]

    params = {
        "stay": {
            "checkIn": check_in,
            "checkOut": check_out
        },
        "occupancies": [
            {
                "rooms": 1,
                "adults": adults,
                "children": clidren_count,
                "paxes": assemble_room_paxes(1, children=children)
            }
        ],
        "hotels": {"hotel": hotel_ids},
        "filter": {
            "minRate": min_rate,
            "maxRate": max_rate,
            "minCategory": min_category,
            "maxCategory": max_category,
            "paymentType": 'AT_WEB',
            "packaging": True
        }
    }

    # Extended search params by: TA reviews, geo, or hotel ids
    if reviews: params['reviews'] = reviews
    
    return params

def filter_by_map_location(data_params, queryset):
    lat_min = data_params.get('sw_lat') or False
    lat_max = data_params.get('sw_lng') or False
    lng_min = data_params.get('ne_lat') or False
    lng_max = data_params.get('ne_lng') or False
    search_by_map = int(data_params.get('search_by_map', 0))

    if search_by_map and lat_min and lng_min and lat_max and lng_max:
        queryset = queryset.filter(
            latitude__gte=lat_min,
            latitude__lte=lat_max,
            longitude__gte=lng_min,
            longitude__lte=lng_max
        )
    return queryset

def get_available_hotels(request, data_params):
    # Init
    page = int(data_params.get('page', 1))
    pagination = dict()
    from_hotel = to_hotel = 0
    hotels = Hotel.objects.exclude(images__isnull=True).all()

    # Filter by hotel name
    hotel_name = data_params.get('hotel_name')
    if hotel_name:
        hotels = hotels.filter(name__icontains=hotel_name)

    # Filter by destination
    destination_code = data_params.get('destination_code')
    if destination_code:
        hotels = hotels.filter(destination_code=destination_code)

    # Filter by map location
    hotels = filter_by_map_location(data_params, hotels)

    # Filter by Hotel Type 
    accommodation_types = data_params.get('accommodation_types')
    if accommodation_types:
        hotels = hotels.filter(accommodation_type_code__in=accommodation_types.split(','))
    
    hotel_ids = list(hotels[:2000].values_list('api_hotel__code', flat=True))
    params = get_hotels_availability_params(data_params, hotel_ids)

    # Ordering
    order_by = data_params.get('order_by', '-minRate')
    reverse = False
    
    if order_by[0] == '-':
        order_by = order_by[1:]
        reverse = True
    
    try:
        hotels_data = get_hotels_availability(params).get('hotels').get('hotels')
        hotels_data.sort(key=lambda hotel: float(hotel[order_by]), reverse=reverse)
        
        # Hotels Pagination
        hotels_count = len(hotels_data)
        hotels_per_page = 30
        
        if hotels_count > hotels_per_page:
            page = int(data_params.get('page', 1))
            
            if page == 1:
                from_hotel = 0
            else:
                from_hotel = (page - 1) * hotels_per_page
            
            to_hotel = page * hotels_per_page
            if to_hotel > hotels_count:
                to_hotel = hotels_count

            hotels_data = hotels_data[from_hotel:to_hotel]
            
        else:
            to_hotel = hotels_count

        for hotel in hotels_data:
            try:
                hotel_obj = Hotel.objects.get(api_hotel__code=hotel.get('code'))
                hotel['id'] = hotel_obj.id

                # Adding the main image
                image = hotel_obj.images.all().exclude(image='').order_by('order')
                if image.exists():
                    image_path = request.build_absolute_uri(image[0].image.url)
                    hotel['image'] = image_path

                # Adding address
                hotel['address'] = hotel_obj.address.title().strip(',') + ',' + hotel_obj.city

                # Adding stars digit
                hotel['stars'] =  hotel_obj.api_hotel_category.simple_code if hotel_obj.api_hotel_category else ''

                # Adding accommodation type
                hotel['accommodation_type'] = hotel_obj.accommodation_type_code.capitalize()

                # Adding facilities
                facilities = hotel_obj.facilities.all().values_list('description', flat=True).distinct()
                hotel['facilities'] = list(facilities)

            except:
                hotel['image'] = ''

            del hotel['rooms'] 

            # If required: apply markup for each listing
            if is_markup_required(request.user):
                hotel['minRate'] = apply_markup(hotel['minRate'])
                hotel['maxRate'] = apply_markup(hotel['maxRate'])

        pagination['from_hotel'] = from_hotel + 1
        pagination['to_hotel'] = to_hotel
        pagination['hotels_count'] = hotels_count
        pagination['page'] = page
        pagination['hotels_per_page'] = hotels_per_page
        
    except:
        hotels_data = []  
    
    data = {
        'hotels': hotels_data,
        'pagination': pagination
    }

    return data

def get_rate_comments_from_db(rooms_data, query_params, hotel):
    rate_comments_id = []
    for room_data in rooms_data:
        ## append room facilities

        hotel_room_object = HotelRoom.objects.filter(hotel=hotel).filter(room_code=room_data['code']).first()
        if hotel_room_object:
            room_data['facilities'] = get_obj_facilities(hotel_room_object)

        for rate in room_data['rates']:
            rate_comments_id_string = rate.get('rateCommentsId','')

            if rate_comments_id_string:
                rate_comment_id = rate_comments_id_string.split('|')[1]
                rate_comments_id.append(rate_comment_id)

    # Get all objects from the database as a array                    
    rate_objects = list(APIRateComment.objects.filter(
        code__in=rate_comments_id,                   
        date_start__lte=query_params.get('check_in'),
        date_end__gte=query_params.get('check_out'),
    ).values_list('incoming','code','rate_codes', 'description'))

    # Add each comment to the final result
    if rate_objects:
        for room_data in rooms_data:
            for rate in room_data['rates']:
                rate_comments_id = rate.get('rateCommentsId','')
                rate_description = ''
                if rate_comments_id:
                    rate_comment_split = rate_comments_id.split('|')

                    for rate_object in rate_objects:
                        if (int(rate_comment_split[0]) == rate_object[0] and
                                rate_comment_split[1] == rate_object[1] and
                                int(rate_comment_split[2]) in rate_object[2]):

                                rate_description += rate_object[3]

                rate['rateComments'] = rate_description
    return rooms_data

def get_available_hotel_rooms(request, query_params, hotel):
    hotelbeds_hotel_id = hotel.api_hotel.code
    hotel_ids = [hotelbeds_hotel_id]
    params = get_hotels_availability_params(query_params, hotel_ids)
    
    try:
        rooms_data_json = get_hotels_availability(params).get('hotels').get('hotels')[0].get('rooms')
        currency = get_hotels_availability(params).get('hotels').get('hotels')[0].get('currency')

        # Get all rateCommentsId so we could make only 1 query to the database
        rooms_data = get_rate_comments_from_db(rooms_data_json, query_params, hotel)
        
        # If required: apply markup for each room based on user type/status
        if is_markup_required(request.user):
            rooms_data = apply_rooms_markup(rooms_data)
    
    except Exception as e:
        rooms_data = currency = ''
    return rooms_data, currency

def get_hotel_room_check_rate(rate_key, request):
   
    try:
        raw_data = get_check_rate(rate_key)

        room_data = raw_data.get('hotel').get('rooms')[0]
        rate_data = room_data.get('rates')[0]

        rate_data['roomCode'] = room_data.get('code')
        rate_data['roomName'] = room_data.get('name')

        rate_data['currency'] = raw_data.get('hotel').get('currency')
        rate_data['totalNet'] = raw_data.get('hotel').get('totalNet')

        # If required: apply markup to Net Amount during check-rate process
        if is_markup_required(request.user):
            rate_data = apply_rate_markup(rate_data)
    
    except:
        rate_data = ''
    
    return rate_data


def get_image_response(image):
    xxl_url = 'http://photos.hotelbeds.com/giata/xxl/'
    xl_url = 'http://photos.hotelbeds.com/giata/xl/ '
    bigger_url = 'http://photos.hotelbeds.com/giata/bigger/'

    giata_urls = [xxl_url,xl_url, bigger_url]

    for giata_url in giata_urls:
        image_url = giata_url + image.giata_path
        # Steam the image from the url
        response = requests.get(image_url, stream=True)

        # Was the request OK?
        if response.status_code != requests.codes.ok:
            # Nope, error handling, skip file etc etc etc
            continue
        else:

            return response.content
