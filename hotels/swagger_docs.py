from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import generics, status


from .serializers import (
    HotelsSerializer
)
from common.services import (open_api_param, open_api_schema)

# Occupancies data definition & open_api_schema setup
occupancies_data_definition = {
  'type': 'object',
  'properties': {
    'rooms': {
      'type': 'number',
      'description': 'Number of rooms.'
    },
    'adults': {
      'type': 'number',
      'description': 'Number of adults in each room.'
    },
    'children': {
      'type': 'number',
      'description': 'Number of children in each room.<br><br><b>Note:</b> If 1 or more children are part of the availability request, you will have to list them individually in the <b>Paxes array</b> with <b>type=CH</b>. You will have to provide the age for each child.'
    },
    'paxes': {
      'items': {
        'type': 'object',
        'properties': {
          'type': {
            'type': 'string',
            'description': 'The pax type. You will have to pass the string <b>CH</b> for each child.'
          },
          'age': {
            'type': 'number',
            'description': 'The age of each child as integer.'
          }
        }
      }
    }
  }
}
operation_desc = '''\x00
  <b>Returns a list of available hotels.</b><br><br>
  You can filter by:<br>
  <b>
    &bull; check_in
    &bull; check_out
    &bull; guests<br>
    &bull; min_rate
    &bull; max_rate<br>
    &bull; min_category
    &bull; max_category<br>
    &bull; accommodation_types &bull; payment_types<br>
    &bull; occupancies array
  </b>
'''

# Hotel related
post_hotels_availability_schema = swagger_auto_schema(
    operation_summary='POST to /hotels/availability',
    operation_description=operation_desc,
    request_body=openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        'check_in': open_api_schema('str', 'Filter by <b>Check In</b> date.<br>Format: <b>YYYY-MM-DD</b>'),
        'check_out': open_api_schema('str', 'Filter by <b>Check Out</b> date.<br>Format: <b>YYYY-MM-DD</b>'),
        'guests': open_api_schema('int', 'Filter by <b>number of guests</b>.<br>Example: <b>4</b>'),
        'min_rate': open_api_schema('int', 'Filter by <b>minimum nightly rate/amount</b>.<br>Example: <b>100</b>'),
        'max_rate': open_api_schema('int', 'Filter by <b>maximum nightly rate/amount</b>.<br>Example: <b>800</b>'),
        'min_category': open_api_schema('int', 'Filter by <b>minimum hotel category (hotel stars)</b>.<br>Example: <b>3</b>'),
        'max_category': open_api_schema('int', 'Filter by <b>maximum hotel category (hotel stars)</b>.<br>Example: <b>5</b>'),
        'accommodation_types': open_api_schema('str', 'Filter by one or more <b>accommodation types</b> separated by comma.<br>Example: <b>Hotel,ApartHotel,Apartment</b>'),
        'payment_types': open_api_schema('str', 'Filter by one or both <b>payment types</b> separated by comma.<br><b>AT_WEB</b> means the payment for the booking can be done online.<br><b>AT_HOTEL</b> means the payment for the booking should be done at the reception desk.<br>Example: <b>AT_WEB,AT_HOTEL</b>'),
        'occupancies': open_api_schema(
          'arr',
          '''\x00
          The <b>guests grouping <i>(arrangement)</i></b> into individual rooms.<br>
          Represented by an <b>array of objects</b>, with <b>each object defining 1 room</b>.<br><br>
          <b>Note:</b> Click on the <b>occupancies &gt;</b> on the left to learn more.<br>
          Example of <b>occupancies POST payload</b>:<br>

              "occupancies": [{
                "rooms": 1,
                "adults": 1,
                "children": 1,
                "paxes": [{
                  "type": "CH",
                  "age": 2
                }]
              }]

          ''',
          items=occupancies_data_definition
        )
      },
      required=['check_in', 'check_out', 'guests']
    ),
    responses={status.HTTP_200_OK: HotelsSerializer(many=True)}
)

#Get hotel rooms availability
get_hotels_rooms_availability_schema = swagger_auto_schema(
    operation_summary="GET\n/hotels/{id}/rooms/availability",
    operation_description="""\x00
    Returns a <b>list of rooms availability <i>(with inlined rates array for each room)</i></b> based on the following path & query params:<br>
    <b>Hotel Id, Check In & Check Out dates, Guests count</b>.<br><br><b>Note about rates:</b><br>
    The response includes a <b>rates array</b> with accurate pricing information <b>for each available room</b> for the selected combination of <b>Hotel, Check In & Check Out dates, Guests count</b>.
    Within each rate object a specially encoded string property called <b>rate_key</b> will be <b>needed for the /check-rate/ endpoint</b> to make a <b>required final rate calculation with guaranteed high-precision</b>.
    """,
    manual_parameters=[
      open_api_param('str', 'check_in', 'Filter by <b>Check In</b> date.<br>Format: <b>YYYY-MM-DD</b>', required=True),
      open_api_param('str', 'check_out', 'Filter by <b>Check Out</b> date.<br>Format: <b>YYYY-MM-DD</b>', required=True),
      open_api_param('int', 'guests', 'Filter by <b>Guests count</b>.<br>Example: <b>4</b>', required=True)
    ],
    responses={status.HTTP_200_OK: HotelsSerializer}
)

# Post room check rate
post_room_check_rate_schema = swagger_auto_schema(
    operation_summary='POST /hotels/{id}/rooms/check-rate/',
    operation_description="""\x00
        <b>A strict requirement dictates</b> that you will have to make an additional request to the <b>/hotels/{id}/rooms/check-rate/</b> endpoint.<br><br>This endpoint serves as a <b><u>strongly required</u> final rate verification step</b>,
        before you can create a booking via the <b>/booking/ endpoint request</b>.
    """,
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        properties={
            'rate_key': open_api_schema(
                'str',
                """\x00
                A specially encoded string that holds the following information:<br><br>
                &bull; Check In & Check Out dates, Hotel Id and Room Id<br>
                &bull; The specific nightly rate for the given period<br>
                &bull; Other internal-only information
                <br><br>
                <b>Rate key example</b>:<br>
                <code>20200320|20200323|W|272|114599|SGL.ST|FIT-RO-ALL|RO||1&tilde;1&tilde;0||N@04&tilde;&tilde;20d4d&tilde;-1042722160&tilde;N&tilde;&tilde;257DCD4019BB439158210166633100AAUK0000001000100010520d4d</code>
                """
            )
        }
    )
)