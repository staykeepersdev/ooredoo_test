from admin_auto_filters.filters import AutocompleteFilter
from django.contrib import admin
from django.contrib.postgres import fields
from django_json_widget.widgets import JSONEditorWidget
from rooms_inventory.models import InventoryDay

from .models import Hotel, HotelRoom, HotelImage, PhoneNumber
from .admin_filters import HotelImagesFilter




class HotelImagesInline(admin.TabularInline):
    model = HotelImage
    extra = 0
    raw_id_fields = ('hotel_room',)
    fields = ('image_original','hotel_room', 'order',)
    readonly_fields = ('image_original',)
    ordering = ('order',)
    show_change_link = True


class HotelRoomsInline(admin.TabularInline):
    model = HotelRoom
    extra = 0
    exclude = ('api_room',)
    show_change_link = True
    raw_id_fields = ('facilities',)

class PhoneNumberInline(admin.TabularInline):
    model = PhoneNumber
    extra = 0

class InventoryDaysInline(admin.TabularInline):
    model = InventoryDay
    extra = 0
    show_change_link = True

class CountryFilter(AutocompleteFilter):
    title = 'Country'
    field_name = 'country'

class DestionationFilter(AutocompleteFilter):
    title = 'Destination Name'
    field_name = 'destination'

@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    inlines = [HotelRoomsInline,PhoneNumberInline, HotelImagesInline]
    list_display = (
        'name',
        'get_hotel_api_code',
        'country_code',
        'destination_code',
        'category_code',
        'accommodation_type_code',
        'city',
        'ranking',
        'address'
    )
    search_fields = ('name','api_hotel__code')
    list_filter = (
        CountryFilter,
        DestionationFilter,
        HotelImagesFilter,
        'accommodation_type_code',
    )
    # autocomplete_fields = ('facilities',)
    raw_id_fields = ('api_hotel', 'api_hotel_category', 'facilities', 'destination', 'country')

    def get_hotel_api_code(self, obj):
        return obj.api_hotel.code
    get_hotel_api_code.short_description = 'Api code'

    class Media():
        pass


@admin.register(HotelRoom)
class HotelRoomAdmin(admin.ModelAdmin):
    inlines = [InventoryDaysInline]
    list_display = ('hotel', 'room_code', 'room_type', 'characteristic_code', 'description')
    
    search_fields = ('hotel__name',)
    list_filter = ('room_type',)
    
    raw_id_fields = ('api_room', 'hotel')
    autocomplete_fields = ('facilities',)
    formfield_overrides = {
        fields.JSONField: {'widget': JSONEditorWidget}
    }
@admin.register(HotelImage)
class HotelImageAdmin(admin.ModelAdmin):
    list_display = ('hotel','order','hotel_room')
    raw_id_fields = ('hotel', 'hotel_room')