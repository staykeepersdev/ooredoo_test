from django.db import models
from django.contrib.postgres.fields import JSONField
from django.utils.html import format_html
from hotelbeds_data.models import APIHotel, APIRoom, APIFacility, APIHotelCategory

from .utils import hotel_directory_path

from hotelbeds_data.models import APICountry, APIDestination

class Hotel(models.Model):
    api_hotel = models.OneToOneField(
        APIHotel,
        on_delete=models.CASCADE,
        related_name='hotel',
        blank=True,
        null=True
    )
    api_hotel_category = models.ForeignKey(
        APIHotelCategory,
        on_delete=models.CASCADE,
        related_name='hotels',
        blank=True,
        null=True
    )

    name = models.CharField(max_length=2048)
    description = models.TextField()
    
    country = models.ForeignKey(
        APICountry,
        on_delete=models.SET_NULL,
        related_name='hotels',
        blank=True,
        null=True
    )
    
    country_code = models.CharField(max_length=3)
    destination = models.ForeignKey(
        APIDestination,
        on_delete=models.SET_NULL,
        related_name='hotels',
        blank=True,
        null=True
    )
    destination_code = models.CharField(max_length=3)    
    latitude = models.DecimalField(max_digits=25, decimal_places=20, null=True)
    longitude = models.DecimalField(max_digits=25, decimal_places=20, null=True)
    
    accommodation_type_code = models.CharField(max_length=64, blank=True, null=True)
    category_code = models.CharField(max_length=5, blank=True, null=True)
    
    address = models.CharField(max_length=1024, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    city = models.CharField(max_length=64)
    
    email = models.EmailField(max_length=100, blank=True, null=True)
    web = models.URLField(blank=True, null=True)    
    ranking = models.PositiveIntegerField(blank=True, null=True)

    facilities = models.ManyToManyField(APIFacility, blank=True)

    def get_phone_number(self, phone_type='PHONEHOTEL'):
        phone_numbers = self.phone_numbers.all()
        hotel_phone_number = phone_numbers.filter(phone_type=phone_type)
        
        phone_number = ''
        
        if hotel_phone_number.exists():
            phone_number = hotel_phone_number[0].phone_number
        elif phone_numbers.count():
            phone_number = phone_numbers[0].phone_number

        return phone_number

    def get_country(self):
        country = APICountry.objects.filter(code=self.country_code).values_list('description', flat=True).first()
        return country or ''

    def __str__(self):
        return self.name

class HotelRoom(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='rooms')
    api_room = models.ForeignKey(APIRoom, on_delete=models.CASCADE, blank=True, null=True)

    room_code = models.CharField(max_length=18)
    room_type = models.CharField(max_length=3)
    characteristic_code = models.CharField(max_length=15)

    min_pax = models.PositiveIntegerField(blank=True, null=True)
    max_pax = models.PositiveIntegerField(blank=True, null=True)
    max_adults = models.PositiveIntegerField(blank=True, null=True)
    max_children = models.PositiveIntegerField(blank=True, null=True)
    min_adults = models.PositiveIntegerField(blank=True, null=True)

    description = models.CharField(max_length=100, blank=True, null=True)
    type_description = models.CharField(max_length=100, blank=True, null=True)
    characteristic_description = models.CharField(max_length=100, blank=True, null=True)

    facilities = models.ManyToManyField(APIFacility, blank=True)
    room_stays = JSONField(null=True, blank=True)

    # Fee added above 2 adults
    extra_guest_fee = models.PositiveIntegerField(default=0)
    

    def __str__(self):
        return self.room_code
    


class PhoneNumber(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='phone_numbers')
    phone_number = models.CharField(max_length=50)
    phone_type = models.CharField(max_length=50)

class HotelImage(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE, related_name='images')
    hotel_room = models.ForeignKey(
        HotelRoom,
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        related_name='images'
    )
    giata_path = models.CharField(max_length=1024)
    image = models.ImageField(upload_to=hotel_directory_path, blank=True, null=True)
    order = models.PositiveIntegerField(blank=True, null=True)

    @property    
    def image_preview(self):
        return format_html(f"<img src='http://photos.hotelbeds.com/giata/small/{self.giata_path}'/>")
    
    def image_original(self):
        return format_html(
            f"<img src='http://photos.hotelbeds.com/giata/original/{self.giata_path}' style='width:75px;'/>"
        )
