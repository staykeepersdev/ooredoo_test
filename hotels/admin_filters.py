from django.contrib.admin import SimpleListFilter

class HotelImagesFilter(SimpleListFilter):
    title = 'Hotel images'
    parameter_name = 'hotel_images'

    def lookups(self, request, model_admin):
        return (
            ('with_images', 'With images'),
            ('without_images', 'Without images'),
        )

    def queryset(self, request, queryset):
        if self.value() == 'with_images':
            return queryset.exclude(images__isnull=True).distinct()

        if self.value() == 'without_images':
            return queryset.filter(images__isnull=True).distinct()

        return queryset
