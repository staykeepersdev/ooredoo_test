from django.urls import path
from .views import (
    HotelsAvailability,
    HotelRoomsAvailability,
    HotelRoomsCheckRate,
    GetHotelImage
)

urlpatterns = [
    # Essential endpoints
    path('availability/', HotelsAvailability.as_view(), name='hotels-availability'),
    path('<int:hotel_id>/rooms/availability/', HotelRoomsAvailability.as_view(), name='hotel-rooms-availability'),
    path('<int:hotel_id>/rooms/check-rate/', HotelRoomsCheckRate.as_view(), name='hotel-rooms-check-rate'),
    path('<int:hotel_id>/images/<int:image_id>.jpg', GetHotelImage.as_view(), name='get-hotel-image'),

    # Future endpoints - Hotel related
    # path('', Hotels.as_view(), name='hotels-list'),
    # path('availability/', HotelsAvailability.as_view(), name='hotels-availability'),
    # path('<int:pk>/', HotelDetails.as_view(), name='hotel'),
    # path('<int:pk>/images/', HotelImages.as_view(), name='hotel-image'),

    # Future endpoints - Hotel room related
    # path('<int:hotel_id>/rooms/', HotelRooms.as_view(), name='hotel-rooms'),
    # path('<int:hotel_id>/rooms/availability/', HotelRoomsAvailability.as_view(), name='hotel-rooms-availability'),
    # path('<int:hotel_id>/rooms/check-rate/', HotelRoomsCheckRate.as_view(), name='hotel-rooms-check-rate'),
    # path('<int:hotel_id>/rooms/<int:pk>/', HotelRoomDetails.as_view(), name='hotel-room'),
    # path('<int:hotel_id>/rooms/<int:pk>/images/', HotelRoomImages.as_view(), name='hotel-room-images'),
]