def hotel_directory_path(instance, filename):

    return f'hotels/{instance.hotel.id}/{filename}'
