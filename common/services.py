from django.conf import settings
from django.core.mail import send_mail
from django.forms.models import model_to_dict

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from drf_yasg import openapi

def send_simple_mail(send_to, instance, email_type):
    model_dict = model_to_dict(instance)
    object_id = model_dict['id']
    if 'data' in model_dict:
        model_dict['data'] = '\n'.join(['%s: %s' % (key, value) for (key, value) in model_dict['data'].items()])
    content = '\n'.join(['%s: %s' % (key, value) for (key, value) in model_dict.items()])

    send_mail(f'SLG Hotels - New {email_type} #{object_id}',
                content,
                settings.DEFAULT_FROM_EMAIL,
                send_to)

def send_email_with_template(subject, context, template_name, to_emails):
    email_html_message = render_to_string(f'{template_name}.html', context)
    email_plaintext_message = render_to_string(f'{template_name}.txt', context)

    mail = EmailMultiAlternatives(
        subject=subject,
        body=email_plaintext_message,
        to=to_emails
    )
    mail.attach_alternative(email_html_message, 'text/html')
    mail.send()


# Swagger / OpenAPI related
def open_api_param(type_is, name, desc, required=False, items=None, in_=openapi.IN_QUERY):
    if(type_is == 'str'): type = openapi.TYPE_STRING
    if(type_is == 'int'): type = openapi.TYPE_INTEGER
    if(type_is == 'arr'): type = openapi.TYPE_ARRAY
    if(type_is == 'bool'): type = openapi.TYPE_BOOLEAN
    if(type_is != 'arr'): items = None

    return openapi.Parameter(name, in_=in_, description=desc, type=type, required=required, items=items)

def open_api_schema(type_is, desc, items=None):
    if(type_is == 'str'): type = openapi.TYPE_STRING
    if(type_is == 'int'): type = openapi.TYPE_INTEGER
    if(type_is == 'arr'): type = openapi.TYPE_ARRAY
    if(type_is == 'bool'): type = openapi.TYPE_BOOLEAN
    if(type_is != 'arr'): items = None

    return openapi.Schema(type=type, description=desc, items=items)



# According to HB docs:

## Availability Request: The adult name is not required
## @link https://developer.hotelbeds.com/docs/read/apitude_booking/Availability

## Booking: At least one adult name per room is mandatory
## @link https://developer.hotelbeds.com/docs/read/apitude_booking/Booking -> Holder and pax names

def assemble_room_paxes(room_id, holder_first_name=None, holder_last_name=None, children=None):
    paxes = []

    if holder_first_name and holder_last_name:
        paxes.append({
            "roomId": room_id,
            "type": "AD",
            "name": holder_first_name,
            "surname": holder_last_name
        })

    if children:
        children = str(children).split(',')

        for child in children:
            paxes.append({
                "roomId": room_id,
                "type": "CH",
                "age": int(child)
            })
    return paxes