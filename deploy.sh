#!/bin/bash

cd /srv/www/ooredoo/
source venv/bin/activate

git checkout master
git pull

find . -name "*.pyc" -exec rm -rf {} \;

pip install -r requirements.txt

python manage.py migrate
python manage.py collectstatic --noinput

service gunicorn restart
supervisorctl restart django-background-tasks
