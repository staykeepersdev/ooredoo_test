from django.core.validators import MinLengthValidator
from django.db import models

# Create your models here.

class Currency(models.Model):
    code = models.CharField(max_length=3, validators=[MinLengthValidator(3)])
    name = models.CharField(max_length=25)

    def Meta(self):
        verbose_name_plural='currencies'

    def __str__(self):
        return self.name

class ExchangeRate(models.Model):
    base = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name="base_rates")
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name="currency_rates")
    rate = models.DecimalField(max_digits=20, decimal_places=10)

    def __str__(self):
        return self.base.name + '-' + self.currency.name