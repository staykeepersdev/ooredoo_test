import hashlib
import json
import requests
import time

from urllib.parse import urljoin

from django.conf import settings


class ExchangeAPIClient:
    def __init__(self):
        self.url = 'https://api.exchangeratesapi.io/latest'

    def headers(self):
        headers = {            
            'Content-Type': 'application/json',
            'Accept-Encoding': 'gzip',
            'Accept': 'application/json'
        }
        return headers

    def create_get_params(self, base_currency, to_currency):
        params = {
            'base': base_currency,
            'symbols': to_currency,
        }
        return params

    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers(), params=params)
        return response

    def get_exchange_rate(self, base_currency, to_currency):
        params = self.create_get_params( base_currency, to_currency )
        response = self._get_json_response(url=self.url, params=params)
        return json.loads(response.content)

