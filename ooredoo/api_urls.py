from django.urls import path, include

urlpatterns = [
     # path('', include('hotelbeds_api.admin_urls')),
     path('users/', include('users.urls')),
     path('hotels/', include('hotels.urls')),
     path('inventory/', include('rooms_inventory.urls')),
     path('booking/', include('booking.urls')),
     path('locations/', include('locations.urls')),
     path('referrals/', include('referrals.urls')),
]
