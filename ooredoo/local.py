# Local Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'ooredoo',
        'USER': 'ooredoo_admin',
        'PASSWORD': 'ooredoo123',
        'HOST': '167.71.42.97',
        'PORT': '',
    }
}
