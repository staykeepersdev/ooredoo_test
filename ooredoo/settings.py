"""
Django settings for ooredoo project.

Generated by 'django-admin startproject' using Django 2.2.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import environ

PROJECT_NAME = 'Hotels API'
PROJECT_TERMS_OF_SERVICE = "https://www.google.com/policies/terms/"

env = environ.Env(
    DEBUG=(bool, False)
)
# reading .env file
environ.Env.read_env()

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#h520pssd1h)mnax_duf#rmyqtqw#u=x*pg(=v=o-b7-b_=*(z'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['167.71.42.97']

DATA_UPLOAD_MAX_MEMORY_SIZE = 25242880 

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third-party apps
    'admin_auto_filters',
    'background_task',
    'corsheaders',
    'django_admin_listfilter_dropdown',
    'django_extensions',
    'django_json_widget',
    'drf_yasg',
    'rest_framework',
    'rest_framework.authtoken',    

    # Local apps
    'booking',
    'booking_com',
    'calendar_app',
    'common',
    'currencies',
    'hotels',
    'hotelbeds_api',
    'hotelbeds_data',
    'hotelbeds_scraper',
    'locations',
    'ooredoo',
    'referrals',
    'revenue',
    'rooms_inventory',
    'tags',
    'users',
    'octoparse',
    'hostify',
    
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ooredoo.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'ooredoo.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

DATA_UPLOAD_MAX_NUMBER_FIELDS = 500000


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    # {
    #     'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    # },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    # },
    # {
    #     'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    # },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')


# Media files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')


GEOIP_PATH = os.path.join(BASE_DIR, 'geoip/')

# Mail SFTP settings
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = 'no-reply@staylivegrow.com'
EMAIL_HOST_PASSWORD = 'RESET76@'
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
DEFAULT_RECEIVER_EMAIL = 'sisi@winwinnkeeper.net' # contact@staykeepers.com
SECONDARY_RECEIVER_EMAIL = 'contact@staylivegrow.com'
DEFAULT_TECHNOLOGY_EMAIL = 'technology@winwinnkeeper.net'
DEFAULT_AFFILIATES_EMAIL = 'affiliate@staylivegrow.com'

# Booking urls - FE & BE
BOOKING_URL = 'https://staylivegrow.com/booking/UUID/'
BOOKING_VOUCHER_URL = f'{BOOKING_URL}voucher'
BOOKING_CANCEL_URL = f'{BOOKING_URL}cancel'

BOOKING_ADMIN_URL = 'https://www.api.hotels.staylivegrow.com/admin/booking/reservation/?id__exact=RESERVATION_ID'


# Commissions & fees percentages
TOTAL_COMMISSION_PERCENTAGE = 20
PAYMENT_FEE_PERCENTAGE = 3


# CORS configuration

CORS_ORIGIN_ALLOW_ALL = True

# Rest framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication'
    ],
}

# Custom user model
AUTH_USER_MODEL = 'users.User'

# Background tasks

BACKGROUND_TASK_RUN_ASYNC = True
BACKGROUND_TASK_ASYNC_THREADS  = 12
MAX_ATTEMPTS  = 5
MAX_RUN_TIME = 600 # 10mins

# Swagger documentation

SWAGGER_SETTINGS = {
   'SECURITY_DEFINITIONS': {
      'Basic': {
            'type': 'basic'
      },
      'Bearer': {
            'type': 'Token',
            'name': 'Authorization',
            'in': 'header',
            'description': '''This authentication scheme uses a simple token-based HTTP Authentication scheme.<br>
            For clients to authenticate, the token key should be included in the Authorization HTTP header.<br>
            The key should be prefixed by the `stri`ng literal "Token", with whitespace separating the two strings.<br>
            For example:<br><br>
            ```
            Authorization: Token 9944b09199c62bcf9418ad846**********
            ```
            ''',
      }
   }
}
SWAGGER_HIDE_ENDPOINTS_FROM_DOCS = [
    # Users related
    '/users/login/',
    '/users/user/',

    # Hotels related
    '/hotels/{hotel_id}/images/{image_id}.jpg',

    # Referrals related
    '/referrals/applications/',
    '/referrals/reports/channels',
    '/referrals/reports/reservations/{start_date}',
    '/referrals/reports/reservations/{start_date}/csv',
    '/referrals/sessions-tracker/'
]


# HotelBeds app token
try:
    HOTELBEDS_API_KEY = env('HOTELBEDS_API_KEY')
    HOTELBEDS_SECRET = env('HOTELBEDS_SECRET')
    HOTELBEDS_BASE_URL = 'https://api.hotelbeds.com'
    HOTELEBDS_BOOKING_URL = '/hotel-api/1.2/bookings'
    HOTELBEDS_ENVIRONMENT = 'production'
    HOTELBEDS_CACHE_API_URL = 'http://aif2.hotelbeds.com/'
except:
    HOTELBEDS_API_KEY = 'p7dyvyhxdqdznrpe23pgjqhk' # 6zuee8yw6ff55qmp96k9egma # p7dyvyhxdqdznrpe23pgjqhk
    HOTELBEDS_SECRET = "QgbhyvVYnA" # f6PqqhUtG5 # QgbhyvVYnA
    HOTELBEDS_BASE_URL = 'https://api.test.hotelbeds.com'
    HOTELEBDS_BOOKING_URL = '/hotel-api/1.0/bookings'
    HOTELBEDS_ENVIRONMENT = 'sandbox'
    HOTELBEDS_CACHE_API_URL = 'http://aif2.int.hotelbeds.com/'



# SquareUp app token
try:
    SQUARE_ACCESS_TOKEN = env('SQUARE_PRODUCTION_TOKEN')
    SQUARE_ENVIRONMENT = 'production'
except:
    SQUARE_ACCESS_TOKEN = 'EAAAEHmBEMYb6TKUy6BUZ3dayAmIPYBDVnQ2h_TtnZu0YYbXXY8e1UKxyddyZ3I0'
    SQUARE_ENVIRONMENT = 'sandbox'

print(f'HotelBeds ENV {HOTELBEDS_ENVIRONMENT} | Square ENV {SQUARE_ENVIRONMENT}')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '{levelname} {asctime} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'file': {
            'formatter': 'simple',
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': 'logs/error.log',
        },
    },
    'loggers': {
        'global': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}

# Local settings

try:
    from .local import *
except ImportError:
    print('Unable to load local.py:')