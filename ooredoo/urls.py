from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView
from django.urls import path, include

from .swagger_docs import schema_view


from hotelbeds_scraper.views import (
    DestinationPricesViews,
    DestinationListView,
    AjaxPrepareAutoHostifyRooms
)
from calendar_app.views import (
    AjaxUpdateAirBnBAccounts,
    AjaxUpdateRoomsTags
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('webhooks/', include('webhooks.urls')),
    path('api/v1/', include('ooredoo.api_urls')),
    path('api/v1/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # Admin Custom Functins
    path(
        'admin/ajax-update-airbnb-accounts/',
        AjaxUpdateAirBnBAccounts.as_view(),
        name='ajax-update-airbnb-accounts'
    ),
    path(
        'admin/ajax-update-rooms-tags/', 
        AjaxUpdateRoomsTags.as_view(),
        name='ajax-update-rooms-tags'
    ),

    path('accounts/login/', auth_views.LoginView.as_view()),

    path('price-checker/', DestinationPricesViews.as_view(), name='price-checker'),

    # Auto Listing ( order of urls is important )
    path(
        'destination-list/ajax-prepare-auto-hostify-rooms/',
        AjaxPrepareAutoHostifyRooms.as_view(),
        name='ajax-prepare-auto-hostify-rooms'
    ),

    path(
        'destination-list/<destination_code>/', 
        DestinationListView.as_view(),
        name='auto-city-dashboard'
    ),
    
    # octoparse test for url link
    # path('octoparse/', include('octoparse.urls'))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Hotels SLG Admin"
admin.site.site_title = "Hotels SLG Admin Portal"
admin.site.index_title = "Welcome to Hotels SLG Portal"
admin.site.index_template="admin/custom_index.html"
