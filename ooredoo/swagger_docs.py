from collections import OrderedDict
from django.conf import settings

from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from drf_yasg.generators import OpenAPISchemaGenerator

HIDE_ENDPOINTS_FROM_DOCS = settings.SWAGGER_HIDE_ENDPOINTS_FROM_DOCS

# Quick solution for ordering of endpoints with hotels first ... booking last
class CustomSchemaGenerator(OpenAPISchemaGenerator):
   def get_paths_object(self, paths):
      
      paths = OrderedDict(sorted(paths.items(), key=lambda p: p[0].startswith('/booking/')))
      
      for path in list(paths):
         if path in HIDE_ENDPOINTS_FROM_DOCS:
            del paths[path]

      return paths

API_INTRO_DESCRIPTION = f'''
<br><br>
<h3>This is the official API documentation for the <b>{settings.PROJECT_NAME}</b> platform.</h3>
It has a detailed information on how you can request:
<br><br>

1. A <b>list of available hotels</b> for specific <b>Check In & Check Out dates</b> and for specific <b>number of Guests</b>
2. Get <b>detailed rooms availability information</b> for specific hotel along with <b>room rates for the given dates</b>
3. <b>Final rate verification</b> via the <b>/check-rate/ endpoint</b>
<br><br>

Note that the list above has to be fulfilled in the <b><u>exact given order</u></b>, before proceeding with the Booking endpoints:

1. <b>The /booking/ endpoint</b> allows you to create a new booking
2. <b>The /booking/{{uuid}} endpoint</b> allows you to retrieve detailed booking information via uuid.
3. <b>The /booking/{{uuid}}/cancel/</b> endpoint allows you to delete a booking based on unique uuid.

'''

schema_view = get_schema_view(
   openapi.Info(
      title=settings.PROJECT_NAME,
      default_version='v1',
      description=API_INTRO_DESCRIPTION,
      terms_of_service=settings.PROJECT_TERMS_OF_SERVICE,
      contact=openapi.Contact(email=settings.DEFAULT_TECHNOLOGY_EMAIL),
      license=openapi.License(name="Commercial License"),
   ),
   public=True,
   generator_class = CustomSchemaGenerator
)
