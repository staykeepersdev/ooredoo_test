import random
import time

from datetime import timedelta

from django.db.models import Q
from django.utils import autoreload, timezone
from compat import close_connection

from background_task.management.commands.process_tasks import _configure_log_std,  logger
from background_task.management.commands.process_tasks import Command as OriginalCommand
from background_task.models import Task
from background_task.settings import app_settings
from background_task.tasks import tasks, autodiscover

class Command(OriginalCommand):

    def run(self, *args, **options):
        duration = options.get('duration', 0)
        sleep = options.get('sleep', 5.0)
        queue = options.get('queue', None)
        log_std = options.get('log_std', False)
        is_dev = options.get('dev', False)
        sig_manager = self.sig_manager

        if is_dev:
            # raise last Exception is exist
            autoreload.raise_last_exception()

        if log_std:
            _configure_log_std()

        autodiscover()

        start_time = time.time()

        while (duration <= 0) or (time.time() - start_time) <= duration:
            if sig_manager.kill_now:
                # shutting down gracefully
                break

            self.clean_stuck_tasks()

            if not self._tasks.run_next_task(queue):
                # there were no tasks in the queue, let's recover.
                close_connection()
                logger.debug('waiting for tasks')
                time.sleep(sleep)
            else:
                # there were some tasks to process, let's check if there is more work to do after a little break.
                time.sleep(random.uniform(sig_manager.time_to_wait[0], sig_manager.time_to_wait[1]))

    def clean_stuck_tasks(self):
        now = timezone.now()
        max_run_time = app_settings.BACKGROUND_TASK_MAX_RUN_TIME
        expires_at = now - timedelta(seconds=max_run_time)
        locked_and_expired = Task.objects.filter( Q(locked_by__isnull=False) & Q(locked_at__lt=expires_at) )
        for task in locked_and_expired:
            if not task.locked_by_pid_running():
                task.locked_by = None
                task.locked_at = None
                task.save()