# -*- coding: utf-8 -*-
from django.contrib import admin
from background_task.models import Task
from background_task.admin import TaskAdmin

class CustomTaskAdmin(TaskAdmin):
    list_filter = ['task_name']

admin.site.unregister(Task)
admin.site.register(Task, CustomTaskAdmin)
