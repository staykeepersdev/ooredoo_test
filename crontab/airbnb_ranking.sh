#!/bin/bash

cd /home/miroslav/Dev/wwpt_projects/ooredoo
. venv/bin/activate

cd /home/miroslav/Dev/wwpt_projects/ooredoo/_scripts/scrapers/airbnb
PATH=$PATH:/usr/local/bin
export PATH
scrapy crawl airbnb_ranking