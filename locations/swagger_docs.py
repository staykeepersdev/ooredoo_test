from common.services import (open_api_param, open_api_schema)

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import status

get_cities_schema = swagger_auto_schema(
    operation_summary='GET /locations/cities/',
    operation_description='<b>Returns a list of supported cities.</b><br><br>You can also filter the cities by <b>is_popular</b> status.',
    manual_parameters=[
      open_api_param('bool', 'is_popular', 'Defines the return of all cities or only the popular ones.', required=False)
    ]
)
get_find_cities_schema = swagger_auto_schema(
    operation_summary='GET /locations/find-cities/{city}',
    operation_description='<b>Returns a list of cities found by partial match of city names <i>(start with)</i>.</b>',
    manual_parameters=[
        open_api_param('str', 'city', 'The first characters of possible city names.', in_=openapi.IN_PATH, required=True),
    ]
)