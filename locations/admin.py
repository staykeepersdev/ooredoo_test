from django.contrib import admin
from django.utils.html import escape, format_html

from .models import Country, City, Search

@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    search_fields = ('name',)

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ('name', 'code')
    list_filter = ('country', 'is_active', 'is_popular')

    list_display = ('is_active', 'is_popular', 'image_tag', 'name', 'code', 'country')
    list_editable = ('is_active','is_popular')
    list_display_links = ('name',)

    def image_tag(self, obj):
        if obj.image:
            return format_html('<img src="{}" width="100" />'.format(obj.image.url))

@admin.register(Search)
class CityAdmin(admin.ModelAdmin):
    list_display = ('ip_address', 'created', 'query')