# Generated by Django 2.2.7 on 2020-03-06 07:21

from django.db import migrations

def populate_countries_cities(apps, schema_editor):
    destinations = [
      {"code": "LON", "country_code": "UK", "country": "United Kingdom", "city": "London"},
      {"code": "BKK", "country_code": "TH", "country": "Thailand", "city": "Bangkok"},
      {"code": "PAR", "country_code": "FR", "country": "France", "city": "Paris"},

      {"code": "DXB", "country_code": "AE", "country": "United Arab Emirates", "city": "Dubai"},
      {"code": "DOH", "country_code": "QA", "country": "Qatar", "city": "Doha"},
      {"code": "KUL", "country_code": "MY", "country": "Malaysia", "city": "Kuala Lumpur"},

      {"code": "KTM", "country_code": "NP", "country": "Nepal", "city": "Katmandu"},
      {"code": "JAV", "country_code": "ID", "country": "Indonesia", "city": "Jakarta"},
      {"code": "DYU", "country_code": "TJ", "country": "Tajikistan", "city": "Dushanbe"},
    ]

    Country = apps.get_model('locations', 'Country')
    City = apps.get_model('locations', 'City')

    for destination in destinations:
        country = Country.objects.create(
            code=destination['country_code'],
            name=destination['country']
        )

        City.objects.create(
            country_id=country.id,
            code=destination['code'],
            name=destination['city'],
            is_active=True,
            is_popular=True
        )

class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(populate_countries_cities),
    ]
