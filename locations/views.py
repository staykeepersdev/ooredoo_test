from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.views import APIView

from .models import City
from .services import (
    get_cities_queryset,
    get_popular_cities,
    get_countries_cities,
    get_request_ip,
    track_search
)

from .swagger_docs import (
    get_cities_schema,
    get_find_cities_schema
)

CACHE_DURATION = 60 * 60 # 1 hour

class Cities(APIView):
    
    @get_cities_schema
    def get(self, request):
        is_popular = request.query_params.get('is_popular', False)

        # Init queryset
        cities_queryset = get_cities_queryset()

        if is_popular:
            return get_popular_cities(request, cities_queryset)
        else:
            return get_countries_cities(request, cities_queryset)


class FindCities(APIView):
    
    # Cache requested url
    @get_find_cities_schema
    @method_decorator(cache_page(CACHE_DURATION))
    def get(self, request, *args, **kwargs):
        city = kwargs.get('city', '')

        # Init queryset
        cities_queryset = get_cities_queryset(city)

        # Keep track of interesting searches
        ip = get_request_ip(request)
        track_search(ip, city, cities_queryset)

        return get_countries_cities(request, cities_queryset)