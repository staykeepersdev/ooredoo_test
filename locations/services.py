from collections import defaultdict as ddict
from django.db.models import F, Value
from django.db.models.functions import Concat
from rest_framework.response import Response

from .models import City, Search


def get_cities_queryset(city_starts_with=''):
    cities_queryset = City.objects.filter(is_active=True).order_by('country')

    if city_starts_with != '':
        cities_queryset = cities_queryset.filter(name__istartswith=city_starts_with)
    
    return cities_queryset


def get_popular_cities(request, destinations_queryset):
    MAX_POPULAR_CITIES = 9
    media_url = request.build_absolute_uri('/media/')

    destinations_list = destinations_queryset.filter(
        is_popular=True
    ).values(
        'code',
        country_name=F('country__name')
    ).annotate(
        city=F('name'),
        image=Concat(Value(media_url), F('image'))
    ).distinct()[:MAX_POPULAR_CITIES]

    return Response(destinations_list)


def get_countries_cities(request, destinations_queryset):
    MAX_AUTO_COMPLETE_CITIES = 10
    
    destinations_list = destinations_queryset.values_list(
        'name',
        'code',
        'country__name'
    ).distinct()[:MAX_AUTO_COMPLETE_CITIES]
    
    destinations = ddict(list)
    for city, code, country in destinations_list:
        destinations[country].append({
            'city': city,
            'destination_code': code
        })

    return Response(destinations)


def track_search(ip, query, queryset):
    SAVE_SEARCH_WITH_MIN_LENGTH = 3

    # Track searches with minimum length and zero results
    if len(query) >= SAVE_SEARCH_WITH_MIN_LENGTH:
        count = queryset.distinct().count()

        if(count == 0):
            Search.objects.create(ip_address=ip, query=query)


def get_request_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')

    return ip