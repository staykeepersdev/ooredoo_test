from django.db import models

class Country(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=2, help_text="ISO Country Code. Examples: BG, UK, US")

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'countries'

    def __str__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=3, help_text="HB Destination Code. Examples: LON, DOH")

    country = models.ForeignKey(
        Country,
        on_delete=models.CASCADE,
        related_name="cities"
    )
    image = models.ImageField(upload_to="cities", blank=True, null=True)

    is_active = models.BooleanField(default=False)
    is_popular = models.BooleanField(default=False)

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'cities'

    def __str__(self):
        return f'{self.name}, {self.country.name}'


class Search(models.Model):
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    
    query = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'searches'
    
    def __str__(self):
        return self.query