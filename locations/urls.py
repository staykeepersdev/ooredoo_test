from django.urls import path, include

from .views import Cities, FindCities

urlpatterns = [
    path('cities/', Cities.as_view(), name='cities'),
    path('find-cities/<city>', FindCities.as_view(), name='find-cities'),
]