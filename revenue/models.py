from django.contrib.postgres.fields import JSONField
from django.db import models
from hotelbeds_data.models import APICountry

class ExtraGuestPrice(models.Model):

    country = models.OneToOneField(
        APICountry,
        related_name='extra_guest_price',
        on_delete=models.CASCADE
    )

    price = models.PositiveSmallIntegerField(default=0)


class NonLeapListingInfo(models.Model):

    hostify_parent_id = models.PositiveIntegerField()
    listing_nickname = models.CharField(max_length=255)
    
    # Direct AirBnb Connection - Listing is both Parent-&-Child
    id_airbnb_direct_connection = models.BigIntegerField(null=True)

    # Stores AirBnb children count & first child
    count_airbnb_children = models.PositiveSmallIntegerField(default=0)
    hostify_child_id_airbnb = models.PositiveIntegerField(null=True)
    id_airbnb = models.BigIntegerField(null=True)

    # Stores Booking.com children count & first child
    count_booking_com_children = models.PositiveSmallIntegerField(default=0)
    hostify_child_id_booking_com = models.PositiveIntegerField(null=True)
    id_booking_com = models.BigIntegerField(null=True)

    count_other_children = models.PositiveSmallIntegerField(default=0)
    count_total_children = models.PositiveSmallIntegerField(default=0)

    # Store all children as json
    children = JSONField(null=True, blank=True)

# from django.db import models
# from hotelbeds_data.models import APICountry

# class ExtraGuestPrice(models.Model):

#     country = models.OneToOneField(
#         APICountry,
#         related_name='extra_guest_price',
#         on_delete=models.CASCADE
#     )

#     price = models.PositiveSmallIntegerField(default=0)