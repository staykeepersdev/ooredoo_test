from django.contrib import admin

from .models import ExtraGuestPrice

@admin.register(ExtraGuestPrice)
class ExtraGuestPriceAdmin(admin.ModelAdmin):
    list_display = ('country', 'price')
    search_fields = ('country__code', 'country__description',)
